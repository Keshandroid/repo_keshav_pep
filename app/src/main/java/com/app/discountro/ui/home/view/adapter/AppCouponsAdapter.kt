package com.app.discountro.ui.home.view.adapter

import android.content.Context
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.ItemRowCouponsBinding
import com.app.discountro.fireStore.models.RoModel
import com.bumptech.glide.Glide

class AppCouponsAdapter(
    private val context: Context,
    private var listCoupons: ArrayList<RoModel.CouponModel>,
    private val clickListener: AppCouponsClickListener,
    private var logoUrl: String?
) : RecyclerView.Adapter<AppCouponsAdapter.MyViewHolder>() {

    class MyViewHolder(val binding:ItemRowCouponsBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        return MyViewHolder(ItemRowCouponsBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (logoUrl != null && logoUrl?.isNotEmpty() == true) {
            holder.binding.ivProfile.visibility = View.VISIBLE

            if(logoUrl.equals("ALL_COUPONS")){
                Glide.with(context).load(listCoupons[position].logo_url).into(holder.binding.ivProfile)
            }else{
                Glide.with(context).load(logoUrl).into(holder.binding.ivProfile)
            }


        }else{
            holder.binding.ivProfile.visibility = View.INVISIBLE
        }

        //holder.tvCouponCopied.visibility = View.GONE
        if (listCoupons[position].isCopied!!) {
            holder.binding.rlCouponCode.visibility = View.VISIBLE
            holder.binding.rlCopyCoupon.visibility = View.GONE
        } else {
            holder.binding.rlCopyCoupon.visibility = View.VISIBLE
            holder.binding.rlCouponCode.visibility = View.GONE
        }

        holder.binding.tvTitle.text = listCoupons[position].offerUrl
        holder.binding.tvCouponDesc.text = listCoupons[position].offerName
        holder.binding.tvCouponCode.text = listCoupons[position].code

        holder.binding.rlCopyCoupon.setOnClickListener {
            clickListener.onCopyCouponClick(it, position)
            hideCoverTimer(60000 * 30, position)
            listCoupons[position].apply { isCopied = true }
            notifyDataSetChanged()
        }

        holder.binding.root.setOnClickListener {
            clickListener.couponRootClick(listCoupons[position], position)
        }
    }

    fun setNewListAndRefresh(listCoupons: ArrayList<RoModel.CouponModel>, logoUrl: String?) {
        this.listCoupons = listCoupons
        this.logoUrl = logoUrl
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return listCoupons.size
    }

    private fun hideCoverTimer(time: Long, position: Int) {
        val timer = object : CountDownTimer(time, 100) {
            override fun onTick(p0: Long) {
            }

            override fun onFinish() {
                listCoupons[position].isCopied = false
                notifyItemChanged(position)
            }
        }.start()
    }

    interface AppCouponsClickListener{
        fun couponRootClick(data: RoModel.CouponModel, position: Int)
        fun onCopyCouponClick(view: View, position: Int)
    }
}
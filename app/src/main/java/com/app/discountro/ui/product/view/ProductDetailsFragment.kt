package com.app.discountro.ui.product.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.Interface.*
import com.app.discountro.R
import com.app.discountro.databinding.ProductItemDetailsBinding
import com.app.discountro.fireStore.helper.FireStoreHelper
import com.app.discountro.ui.home.view.BrowserActivity
import com.app.discountro.ui.home.view.interfaces.CompareProductInterface
import com.app.discountro.ui.product.adapter.CompareProductsAdapter
import com.app.discountro.ui.product.adapter.CreateListAdapter
import com.app.discountro.ui.product.adapter.ProductsImagesViewPagerAdapter
import com.app.discountro.ui.product.model.FavoritePurchasedCustomListModel
import com.app.discountro.ui.product.model.ListCollectionModel
import com.app.discountro.ui.product.model.ProductModel
import com.app.discountro.utils.FireStorageKeys.Companion.KEYUT_PRODUCT_LIST
import com.app.discountro.utils.KeysUtils.Companion.KEY_PRODUCT_MODEL
import com.app.discountro.utils.Utils
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson

class ProductDetailsFragment : Fragment(), CreateListAdapter.CreateListListener{
    val TAG = ProductDetailsFragment::class.java.simpleName
    private lateinit var binding: ProductItemDetailsBinding
    private var browserActivity: BrowserActivity? = null
    private val auth: FirebaseAuth = Firebase.auth
    lateinit var productsImagesViewPagerAdapter: ProductsImagesViewPagerAdapter
    private lateinit var compareProductsAdapter: CompareProductsAdapter
    lateinit var createListAdapter: CreateListAdapter
    lateinit var editItemSaveBottomSheet: BottomSheetDialog
    lateinit var createNewProductListBottomSheet: BottomSheetDialog
    private var productData: ProductModel? = null
    lateinit var ivFavoriteEditBottomSheet : ImageView
    private var filterList: ArrayList<ListCollectionModel> = arrayListOf()
    var isListItemSelected = false
    var selectedListId: String? = null
    var selectedListProducts: ArrayList<FavoritePurchasedCustomListModel> = arrayListOf()

    //NEW AD
    val imgList: ArrayList<String> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        val bundle = this.arguments
        if (bundle != null) {
            productData = arguments!!.getSerializable(KEY_PRODUCT_MODEL) as ProductModel
//            productData = getSerializable(requireActivity(), KEY_PRODUCT_MODEL, ProductModel::class.java)
        }
        Log.e(TAG, "onCreate: product Model :- $productData ")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = ProductItemDetailsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        browserActivity = activity as BrowserActivity?


        if(!productData?.product_name.isNullOrBlank()){
            browserActivity?.appbarTitle?.text = productData?.product_name
            binding.tvProductPrice.text = "${productData?.currency} ${productData?.max_price}"
        }else{
            if(!productData?.name.isNullOrBlank()){
                browserActivity?.appbarTitle?.text = productData?.name
                binding.tvProductPrice.text = " ${productData?.price}"
            }else{
                browserActivity?.appbarTitle?.text = "My Product"
                binding.tvProductPrice.text = "0"
            }
        }

        if(productData?.isFavorite!!){
            binding.ivFavorite.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_favorite_fill))
        }else{
            binding.ivFavorite.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_not_favorite))
        }

        binding.tvProductDesc.text = productData?.description ?: ""

        productData?.date_add?.toDate()?.let {
            binding.tvProductTime.text = "Updated ${Utils.convertUtcToLocalTime(productData?.date_add?.toDate()!!)}"
        }

        initClick()
        setAdapter() // compare products recyclerview

    }


    private fun initClick() {
        binding.cvFavorite.setOnClickListener {

            // TODO :- This model is used for if using the allproduct collection then the uncomment code and comment product collection
            /*val requireModel = FavoritePurchasedCustomListModel(
                id_product = productData?.id_product,
                url = productData?.url,
                url_hash = productData?.hash_url
            )*/

            // TODO :- This model is used for if using the old product collection then the uncomment code and comment all product collection
            val requireModel = FavoritePurchasedCustomListModel(
                id_product = productData?.id_product,
                url_hash = productData?.product_urls?.get(0)
            )

            if(!productData?.isFavorite!!){
                addFavoriteProduct(requireModel)
            }else {
                removeFavoriteProduct(requireModel)
            }
        }

        binding.ivNotification.setOnClickListener {
            editSaveItemBottomSheet()
        }
    }

    fun setAdapter(){

        //NEW
        val adapter =
            CompareProductsAdapter(requireContext(), object : CompareProductInterface {
                override fun onItemClicked(view: View?, directLink: String) {
                    // do something here

                    //Log.d("WEB_URL",""+directLink)

                    //browserActivity!!.openDirectLinkFromAppMode(directLink)
                }
            }, productData!!.seller_list)
        binding.rvCompareProducts!!.adapter = adapter
        binding.rvCompareProducts?.layoutManager = LinearLayoutManager(requireContext())




        createListAdapter = CreateListAdapter(filterList,this)

        //OLD
        //productsImagesViewPagerAdapter = ProductsImagesViewPagerAdapter(requireContext(), productData?.image ?: arrayListOf())

        //NEW AD
        if(!productData!!.product_image.isNullOrEmpty()){
            imgList.add(productData!!.product_image.toString())
        }

        productsImagesViewPagerAdapter = ProductsImagesViewPagerAdapter(requireContext(), imgList ?: arrayListOf())

        binding.productImagesViewPager.adapter = productsImagesViewPagerAdapter

        binding.tabIndicator.setupWithViewPager(binding.productImagesViewPager)

        binding.tabIndicator.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {}

            override fun onTabUnselected(p0: TabLayout.Tab?) {}

            override fun onTabSelected(p0: TabLayout.Tab?) {
                /*if (p0?.position == splashIntroItemModel.size - 1) {
                    loadLastScreen()
                }*/
            }
        })
    }

    private fun editSaveItemBottomSheet() {

        editItemSaveBottomSheet = BottomSheetDialog(requireContext(), R.style.AppBottomSheetDialogTheme)
        editItemSaveBottomSheet.setContentView(R.layout.edit_product_item)
        var isPurchased = false
        val ivClose = editItemSaveBottomSheet.findViewById<ImageView>(R.id.ivClose)
        val cvCreateList = editItemSaveBottomSheet.findViewById<CardView>(R.id.cvCreateList)
        val tvNoListAvailable = editItemSaveBottomSheet.findViewById<TextView>(R.id.tvNoListAvailable)
        val switchNotification = editItemSaveBottomSheet.findViewById<Switch>(R.id.switchNotification)

        var ivProduct = editItemSaveBottomSheet.findViewById<ImageView>(R.id.ivProduct)
        var tvTitle = editItemSaveBottomSheet.findViewById<TextView>(R.id.tvTitle)
        var tvDetails = editItemSaveBottomSheet.findViewById<TextView>(R.id.tvDetails)
        var tvPrice = editItemSaveBottomSheet.findViewById<TextView>(R.id.tvPrice)

        val iv25Per = editItemSaveBottomSheet.findViewById<ImageView>(R.id.iv25Per)
        val iv50Per = editItemSaveBottomSheet.findViewById<ImageView>(R.id.iv50Per)
        val rvList = editItemSaveBottomSheet.findViewById<RecyclerView>(R.id.rvList)
        val ll25Per = editItemSaveBottomSheet.findViewById<LinearLayout>(R.id.ll25Per)
        val ll50Per = editItemSaveBottomSheet.findViewById<LinearLayout>(R.id.ll50Per)
        ivFavoriteEditBottomSheet = editItemSaveBottomSheet.findViewById(R.id.ivFavorite)!!
        val ivMarkFullFilled = editItemSaveBottomSheet.findViewById<ImageView>(R.id.ivMarkFullFilled)
        val llUpdate = editItemSaveBottomSheet.findViewById<LinearLayout>(R.id.llUpdate)
        editItemSaveBottomSheet.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        isListItemSelected = false
        var notificationDiscount = 25

        getList(rvList!!, tvNoListAvailable!!)

        rvList?.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        rvList?.adapter = createListAdapter

        if(!productData!!.product_image.isNullOrEmpty()){
            Glide.with(requireContext()).load(productData!!.product_image).placeholder(ContextCompat.getDrawable(requireContext(), R.drawable.ic_no_product_image)).into(
                ivProduct!!
            )
        }else{
            ivProduct!!.setImageDrawable(context!!.getDrawable(R.drawable.ic_no_product_image))
        }

        if(!productData!!.product_brand.isNullOrEmpty()){
            tvTitle!!.setText(productData!!.product_brand)
        }

        if(!productData!!.product_name.isNullOrEmpty()){
            tvDetails!!.setText(productData!!.product_name)
        }

        if(!productData!!.max_price.isNullOrEmpty()){
            tvPrice!!.setText(productData!!.max_price)
        }



        if(productData?.isFavorite!!){
            ivFavoriteEditBottomSheet?.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_favorite_fill))
        }else{
            ivFavoriteEditBottomSheet?.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_not_favorite))
        }



        ivFavoriteEditBottomSheet.setOnClickListener {

            // TODO :- This model is used for if using the allproduct collection then the uncomment code and comment product collection
            /*val requireModel = FavoritePurchasedCustomListModel(
                id_product = productData?.id_product,
                url = productData?.url,
                url_hash = productData?.hash_url
            )*/

            // TODO :- This model is used for if using the old product collection then the uncomment code and comment all product collection
            val requireModel = FavoritePurchasedCustomListModel(
                id_product = productData?.id_product,
                url_hash = productData?.product_urls?.get(0)
            )


            if(productData?.isFavorite!!){
                removeFavoriteProduct(requireModel, ivFavoriteEditBottomSheet)
            }else{
                addFavoriteProduct(requireModel)
            }
        }

        ll25Per?.setOnClickListener {
            iv25Per?.visibility = VISIBLE
            iv50Per?.visibility = GONE
            notificationDiscount = 25
        }

        ll50Per?.setOnClickListener {
            iv25Per?.visibility = GONE
            iv50Per?.visibility = VISIBLE
            notificationDiscount = 50
        }

        cvCreateList?.setOnClickListener {
            createNewProductListBottomSheet(rvList!!, tvNoListAvailable!!)
        }


        ivMarkFullFilled?.setOnClickListener{
            if (!isPurchased){
                ivMarkFullFilled.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_success_feedback))
                isPurchased = true
            }else{
                ivMarkFullFilled.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_right_rounded_cut))
                isPurchased = false
            }
        }

        llUpdate?.setOnClickListener {
            Log.e(TAG, "editSaveItemBottomSheet: purchased $isPurchased and isListItemSelected $isListItemSelected" )
            if(isListItemSelected){

                // TODO :- If using product collection then uncomment and allproduct collection comment code
                addPurchasedProduct(
                    isPurchased,
                    editItemSaveBottomSheet,
                    FavoritePurchasedCustomListModel(
                        id_product = productData?.id_product,
                        url_hash = productData?.product_urls?.get(0),
                        mute_notification = switchNotification?.isChecked,
                        notification_min_discount = notificationDiscount
                    )
                )


                // TODO :-  If using allproduct collection then used it and product collection uncomment code
                /*addPurchasedProduct(
                    isPurchased,
                    editItemSaveBottomSheet,
                    FavoritePurchasedCustomListModel(
                        id_product = productData?.id_product,
                        url = productData?.url,
                        url_hash = productData?.hash_url,
                        mute_notification = switchNotification?.isChecked,
                        notification_min_discount = notificationDiscount
                    )
                )*/
            }else{
                Toast.makeText(editItemSaveBottomSheet.context, "Please select anyone list..", Toast.LENGTH_SHORT).show()
            }
        }

        ivClose?.setOnClickListener {
            editItemSaveBottomSheet.dismiss()
        }

        editItemSaveBottomSheet.show()
    }

    private fun createNewProductListBottomSheet(rvList: RecyclerView, tvNoListAvailable: TextView) {
        createNewProductListBottomSheet = BottomSheetDialog(requireContext(), R.style.AppBottomSheetDialogTheme)
        createNewProductListBottomSheet.setContentView(R.layout.create_product_list_bottomsheet_layout)
        val etListName = createNewProductListBottomSheet.findViewById<EditText>(R.id.etListName)
        val etListDis = createNewProductListBottomSheet.findViewById<EditText>(R.id.etListDis)
        val llUpdate = createNewProductListBottomSheet.findViewById<LinearLayout>(R.id.llUpdate)
        val ivClose = createNewProductListBottomSheet.findViewById<ImageView>(R.id.ivClose)
        createNewProductListBottomSheet.behavior.state = BottomSheetBehavior.STATE_EXPANDED

        llUpdate?.setOnClickListener {
            if(etListName!!.text.isNotBlank()){
                val requireModel = ListCollectionModel(
                    listName = etListName.text.toString(),
                )
                addListProduct(rvList, tvNoListAvailable, requireModel)
            }else{
                Toast.makeText(createNewProductListBottomSheet.context, "Please Enter Create List Name..", Toast.LENGTH_SHORT).show()
            }
        }

        ivClose?.setOnClickListener {
            createNewProductListBottomSheet.dismiss()
        }

        createNewProductListBottomSheet.show()
    }

    private fun addListProduct(rvList: RecyclerView, tvNoListAvailable: TextView, requireModel: ListCollectionModel){
        FireStoreHelper().addListCollection(
            auth.currentUser!!.uid,
            requireModel,
            object : FireStoreAddListInterface {
                override fun onSuccess(msg: String) {
                    Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
                    createNewProductListBottomSheet.dismiss()
                    filterList.add(requireModel)
                    if(filterList.isNotEmpty()){
                        rvList.visibility = VISIBLE
                        tvNoListAvailable.visibility = GONE
                    }else{
                        rvList.visibility = GONE
                        tvNoListAvailable.visibility = VISIBLE
                    }
                    createListAdapter.updateList(filterList)
                }
                override fun onError(error: String) {
                    Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    private fun addPurchasedProduct(isPurchased: Boolean, bottomSheet: BottomSheetDialog, productModel: FavoritePurchasedCustomListModel) {

        if(isPurchased) {
            FireStoreHelper().addPurchasedProduct(
                auth.currentUser!!.uid,
                productModel,
                object : FireStoreAddPurchasedProductInterface {
                    override fun onSuccess(msg: String) {
                        bottomSheet.dismiss()
                        Toast.makeText(requireContext(), "Added successfully..", Toast.LENGTH_SHORT).show()
                    }

                    override fun onError(error: String) {
                        Log.e(TAG, "Error adding document $error")
                        Toast.makeText(
                            requireContext(),
                            "Something went wrong to added product",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            )
        }

        var isProductAdding = true
        selectedListProducts.forEachIndexed{ index, model ->
            if(model.id_product == productModel.id_product){
                if(model != productModel){
                    selectedListProducts[index] = productModel
                }
                isProductAdding = false
                return@forEachIndexed
            }
        }

        if(isProductAdding){
            selectedListProducts.add(productModel)
        }

//        selectedListProducts.add(productModel)


        if(!selectedListId.isNullOrBlank()) {
            val requireModel = ListCollectionModel(
                productsList = selectedListProducts,
                listId = selectedListId
            )

            val addListInProduct: MutableMap<String, Any> = mutableMapOf()

            addListInProduct[KEYUT_PRODUCT_LIST] = selectedListProducts


            FireStoreHelper().addListProduct(
                auth.currentUser!!.uid,
                requireModel,
                addListInProduct,
                object : FireStoreAddListProductInterface {
                    override fun onSuccess(msg: String) {
                        if(bottomSheet.isShowing){
                            bottomSheet.dismiss()
                            Toast.makeText(
                                requireContext(),
                                "Product added in list successfully..",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                    override fun onError(error: String) {
                        Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            )
        }
    }

    private fun addFavoriteProduct(productModel: FavoritePurchasedCustomListModel, favoriteImageView: ImageView? = null) {

        FireStoreHelper().addFavoriteProduct(
            auth.currentUser!!.uid,
            productModel,
            object : FireStoreAddFavoriteProductInterface {
                override fun onSuccess(msg: String) {
                    productData?.isFavorite = true
                    binding.ivFavorite.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_favorite_fill))
                    if(::ivFavoriteEditBottomSheet.isInitialized){
                        ivFavoriteEditBottomSheet.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_favorite_fill))
                    }
                    favoriteImageView?.let {
                        favoriteImageView.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_not_favorite))
                    }
                }
                override fun onError(error: String) {
                    Log.e(TAG, "Error adding document $error")
                    productData?.isFavorite = false
                    binding.ivFavorite.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_not_favorite))
                    if(::ivFavoriteEditBottomSheet.isInitialized){
                        ivFavoriteEditBottomSheet.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_not_favorite))
                    }
                }
            }
        )
    }

    private fun removeFavoriteProduct(productModel: FavoritePurchasedCustomListModel, favoriteImageView: ImageView? = null) {
        FireStoreHelper().removeFavoriteProduct(
            auth.currentUser!!.uid,
            productModel,
            object : FireStoreRemoveFavoriteProductInterface {
                override fun onSuccess(msg: String) {
                    productData?.isFavorite = false
                    binding.ivFavorite.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_not_favorite))
                    favoriteImageView?.let {
                        favoriteImageView.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_not_favorite))
                    }
                }
                override fun onError(error: String) {
                    Log.e(TAG, "Error unFavorite Product $error")
                }
            }
        )
    }


    private fun getList(recyclerView: RecyclerView, noDataFound: TextView) {

        FireStoreHelper().getListProductFilter(
            auth.currentUser!!.uid,
            object: FireStoreGetFilterListListener {
                override fun onSuccess(productFilterList: QuerySnapshot) {
                    filterList.clear()
                    for (i in productFilterList) {
                        val jsonData = Utils.convertToJson(i.data)
                        val gsn = Gson()
                        val requireModel: ListCollectionModel = gsn.fromJson(
                            jsonData.toString(),
                            ListCollectionModel::class.java
                        )
                        filterList.add(requireModel)
                    }
                    if(filterList.isNotEmpty()){
                        recyclerView.visibility = VISIBLE
                        noDataFound.visibility = GONE
                    }else{
                        recyclerView.visibility = GONE
                        noDataFound.visibility = VISIBLE
                    }
                    createListAdapter.updateList(filterList)
                }

                override fun onError(error: String) {
                    Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    override fun onItemClick(data: ListCollectionModel) {
        isListItemSelected = true
        selectedListId = data.listId
        selectedListProducts.clear()
        selectedListProducts.addAll(filterList[filterList.indexOf(data)].productsList)
        Log.e(TAG, "onItemClick: list name :- $selectedListId and selectedListProduct $selectedListProducts")
    }

}
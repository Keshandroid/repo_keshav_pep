package com.app.discountro.ui.home.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.ItemRowStoreBinding
import com.app.discountro.ui.home.view.models.FavoriteStore
import com.bumptech.glide.Glide

class AppFavoriteStoresAdapter(val context : Context, val favoritesStoreList : ArrayList<FavoriteStore>, val listener : StoresCouponsListener) : RecyclerView.Adapter<AppFavoriteStoresAdapter.ViewHolder>() {

    class ViewHolder(val binding : ItemRowStoreBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemRowStoreBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context).load(favoritesStoreList[position].logoUrl).into(holder.binding.ivStoreImg)
        holder.binding.root.setOnClickListener {
            listener.favoriteStoreClick(favoritesStoreList[position])
        }
    }

    override fun getItemCount(): Int {
        return favoritesStoreList.size
    }

    interface StoresCouponsListener{
        fun favoriteStoreClick(data : FavoriteStore)
    }
}
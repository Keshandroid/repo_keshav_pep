package com.app.discountro.ui.account.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.app.discountro.R
import com.app.discountro.databinding.TransactionDetailsFragmentBinding
import com.app.discountro.ui.home.view.BrowserActivity

class TransactionDetailsFragment() : Fragment() {
    private lateinit var binding: TransactionDetailsFragmentBinding
    private var browserActivity: BrowserActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = TransactionDetailsFragmentBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        browserActivity = activity as BrowserActivity?
        browserActivity?.appbarTitle?.text = "Order details"
    }
}
package com.app.discountro.ui.sign_up.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.app.discountro.R
import com.app.discountro.common.constant.AppConstant
import com.app.discountro.databinding.ActivitySignUpBinding
import com.app.discountro.ui.home.view.BrowserActivity
import com.app.discountro.ui.login.view.LoginActivity
import com.app.discountro.ui.splash.model.MyTranslationVarModel
import com.app.discountro.utils.FireStorageKeys
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.SharedPrefsUtils
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class SignUpActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySignUpBinding
    private val auth: FirebaseAuth = Firebase.auth
    private lateinit var googleSignInClient: GoogleSignInClient
    private val appConstant: AppConstant = AppConstant()
    val db = Firebase.firestore
    private var gLoginUserName: String = ""
    var bPassword = false
    private var socialType: String = ""
    private lateinit var callbackManager: CallbackManager

    private lateinit var myTranslationVarModel: MyTranslationVarModel
    private lateinit var myTranslationSignUp: MyTranslationVarModel
    private val myTranslationArrayData: ArrayList<MyTranslationVarModel> = arrayListOf()


    private val resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            Log.e("Sunny", "ResultCode: ${result.resultCode}")
            Log.e("Sunny", "ActivityResult: ${Activity.RESULT_OK} ")
            if (result.resultCode == Activity.RESULT_OK) {
                val data = result.data
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    val account = task.getResult(ApiException::class.java)!!
                    Toast.makeText(
                        this,
                        myTranslationSignUp.success_login.ifEmpty {
                            resources.getString(R.string.success_login)
                        },
                        Toast.LENGTH_SHORT
                    ).show()
                    // Google Sign In was successful, authenticate with Firebase
                    Log.e("AccountDetails:-", "Google ID ${account.id}")
                    Log.e("AccountDetails:-", "Google familyName ${account.familyName}")
                    Log.e("AccountDetails:-", "Google LastName ${account.givenName}")
                    gLoginUserName =
                        account.givenName.toString() + " " + account.familyName.toString()
                    firebaseAuthWithGoogle(account.idToken!!)
                } catch (e: ApiException) {
                    appConstant.dismissDiscRoProgress()
                    Log.w("Exception", "Google sign in failed", e)
                }
            } else {
                appConstant.dismissDiscRoProgress()
            }

        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getTranslationData()
        initView()
    }

    private fun initView() {
        setTranslationDataFromPref()
        onPwdEyeClickListener()
        onBackClickListner()
        onSignUpClickListener()
        onAlreadyHaveClickListener()
        firebaseGoogleAuth()
        onGButtonPressListener()
        facebookFireLogin()
    }


    private fun setTranslationDataFromPref() {
        myTranslationSignUp = myTranslationArrayData[0]
        Log.e(
            "MyDataValue_SunnyPatel",
            "setTranslationDataFromPref: ${myTranslationSignUp.welcome_to}"
        )
        with(binding){
            tvSignUpTxt.text = myTranslationSignUp.sign_up
            tvFullNameTxt.text = myTranslationSignUp.full_name
            etFullName.hint = myTranslationSignUp.enter_your_full_name
            tvEmailTxt.text = myTranslationSignUp.e_mail
            etEmail.hint = myTranslationSignUp.enter_your_e_mail
            tvPwdTxt.text = myTranslationSignUp.password
            etPassword.hint = myTranslationSignUp.enter_your_password
            cbSignUp.text = myTranslationSignUp.sign_up
            tvAlreadyAcTxt.text = myTranslationSignUp.already_have_an_account
            tvLogin.text = myTranslationSignUp.login
            tvSignUpWithTxt.text = myTranslationSignUp.sign_up_with
            clFacebookLogin.text = myTranslationSignUp.facebook
            clGoogleLogin.text = myTranslationSignUp.google
        }

    }

    private fun getTranslationData() {
        Log.e(
            "MySharePrefDataValue", "getTranslationData: ${
                SharedPrefsUtils.getModelPreferences(
                    KeysUtils.KeyModelTranslation,
                    MyTranslationVarModel::class.java
                )
            }"
        )
        myTranslationVarModel = SharedPrefsUtils.getModelPreferences(
            KeysUtils.KeyModelTranslation,
            MyTranslationVarModel::class.java
        ) as MyTranslationVarModel
        myTranslationArrayData.add(myTranslationVarModel)
        myTranslationArrayData[0]
    }

    private fun facebookFireLogin() {
        FacebookSdk.fullyInitialize()
        AppEventsLogger.activateApp(application)
        fireFacebookLogin()
        onFbButtonClickListener()
    }

    private fun onFbButtonClickListener() {
        binding.clFacebookLogin.setOnClickListener {
            appConstant.showDiscoRoProgress(this@SignUpActivity)
            binding.lbFacebookLoginBtn.performClick()
        }
    }

    private fun fireFacebookLogin() {
        callbackManager = CallbackManager.Factory.create()
        binding.lbFacebookLoginBtn.setPermissions("email", "public_profile")
        binding.lbFacebookLoginBtn.registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult) {
                    Log.d("FacebookSunny", "facebook:onSuccess:$result")
                    handleFacebookAccessToken(result.accessToken)
                }

                override fun onCancel() {
                    appConstant.dismissDiscRoProgress()
                    Log.d("FacebookSunny", "facebook:onCancel")
                }

                override fun onError(error: FacebookException) {
                    appConstant.dismissDiscRoProgress()
                    Log.d("FacebookSunny", "facebook:onError", error)
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Pass the activity result back to the Facebook SDK
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d("FacebookAccessToken", "handleFacebookAccessToken:$token")

        val credential = FacebookAuthProvider.getCredential(token.token)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Toast.makeText(
                        this,
                        myTranslationSignUp.success_login.ifEmpty {
                            resources.getString(R.string.success_login)
                        },
                        Toast.LENGTH_SHORT
                    ).show()
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("facebookAccessToken", "signInWithCredential:success")
                    val user = auth.currentUser
                    socialType = KeysUtils.KeyFirefacebook
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("facebookAccessToken", "signInWithCredential:failure", task.exception)
                    Toast.makeText(
                        baseContext,
                        myTranslationSignUp.authantication_fail.ifEmpty {
                            resources.getString(R.string.authantication_fail)
                        },
                        Toast.LENGTH_SHORT
                    ).show()
                    updateUI(null)
                }
            }.addOnFailureListener {
                LoginManager.getInstance().logOut()
                Toast.makeText(this@SignUpActivity, it.message, Toast.LENGTH_LONG).show()
                Log.e("MyFbErrorsss", "handleFacebookAccessToken: ${it.message}")
                appConstant.dismissDiscRoProgress()
            }
    }


    private fun onPwdEyeClickListener() {
        binding.clHideUnHide.setOnClickListener {
            pwdHideUnHideFunction()
        }
    }

    private fun pwdHideUnHideFunction() {

        if (bPassword) {
            binding.etPassword.transformationMethod =
                PasswordTransformationMethod.getInstance()
            bPassword = false
            binding.ivHideUnHide.background = resources.getDrawable(R.drawable.ic_eye)
        } else {
            binding.etPassword.transformationMethod =
                HideReturnsTransformationMethod.getInstance()
            bPassword = true
            binding.ivHideUnHide.background = resources.getDrawable(R.drawable.ic_eye_close)
        }

    }

    private fun onGButtonPressListener() {
        binding.clGoogleLogin.setOnClickListener {
            appConstant.showDiscoRoProgress(this@SignUpActivity)
            val signInIntent = googleSignInClient.signInIntent
            resultLauncher.launch(signInIntent)
            revokeAccess()
        }
    }

    private fun revokeAccess() {
        googleSignInClient.revokeAccess().addOnCompleteListener(this) {}
    }

    private fun firebaseGoogleAuth() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_ID))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)

    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    socialType = KeysUtils.KeyFireGoogle
                    // Sign in success, update UI with the signed-in user's information
                    Log.e("SunnyPatel", "signInWithCredential:success")
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.e("SunnyPatel", "signInWithCredential:failure", task.exception)
                    updateUI(null)
                }
            }.addOnFailureListener {
                appConstant.dismissDiscRoProgress()
            }
    }

    private fun onAlreadyHaveClickListener() {

        binding.tvLogin.setOnClickListener {
            val intent = Intent(this@SignUpActivity, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        if (currentUser != null) {
            updateUI(currentUser)
        }
    }

    private fun onSignUpClickListener() {
        binding.cbSignUp.setOnClickListener {
            if (binding.etFullName.text.isNotEmpty() &&
                binding.etEmail.text.isNotEmpty() &&
                binding.etPassword.text.isNotEmpty()
            ) {
                firebaseLoginWithEmailAuth()
            } else {
                Toast.makeText(
                    this,
                    myTranslationSignUp.fill_creadential.ifEmpty {
                        resources.getString(
                            R.string.fill_creadential
                        )
                    },
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    private fun firebaseLoginWithEmailAuth() {
//        auth = Firebase.auth
        appConstant.showDiscoRoProgress(this@SignUpActivity)
        gLoginUserName = binding.etFullName.text.toString()
        auth.createUserWithEmailAndPassword(
            binding.etEmail.text.toString(),
            binding.etPassword.text.toString()
        )
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Log.e("SunnyAuthLogin", "LoginSuccess")
                    val user = auth.currentUser
                    socialType = KeysUtils.KeyFireEmail
                    updateUI(user)
                }
                appConstant.dismissDiscRoProgress()
            }.addOnFailureListener {
                Toast.makeText(this@SignUpActivity, it.message, Toast.LENGTH_SHORT).show()
                appConstant.dismissDiscRoProgress()
            }
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            SharedPrefsUtils.setStringPreference(KeysUtils.KeyUUID, user.uid)
            /*   Toast.makeText(
                   this@SignUpActivity,
                   resources.getString(R.string.success_reg_with) + user.email,
                   Toast.LENGTH_SHORT
               ).show()*/

            Log.e("FireAuAccountDetails:-", "Google ID ${user.uid}")
            Log.e("FireAuAccountDetails:-", "Google LastName ${user.email}")
            Log.e("FireAuAccountDetails:-", "Google ProfilePicURL ${user.photoUrl}")
            Log.e("FireAuAccountDetails:-", "Google IDToken ${user.providerId}")
            fireStoreCreateUsers(user)
        }
        appConstant.dismissDiscRoProgress()
    }

    private fun fireStoreCreateUsers(user: FirebaseUser?) {

        gLoginUserName.ifEmpty {
            gLoginUserName = ""
        }
        gLoginUserName.ifBlank {
            gLoginUserName = ""
        }
        if (user != null) {

            val userTable = mutableMapOf(
                FireStorageKeys.KEYUT_ID_USER to user.uid,
                FireStorageKeys.KEYUT_ID_NAME to gLoginUserName,
                FireStorageKeys.KEYUT_P_TOKEN to "",
                FireStorageKeys.KEYUT_EMAIL to user.email,
                FireStorageKeys.KEYUT_PWD to "",
                FireStorageKeys.KEYUT_DB_REG to "",
                FireStorageKeys.KEYUT_ACTIVE to "",
                FireStorageKeys.KEYUT_SOCIAL_TYPE to socialType,
                FireStorageKeys.KEYUT_USER_PUBLIC_TOKEN to generateRandomNumber().toString()
            )
            Log.e("FireAuAccountDetails", "user all data: $userTable")
            db.collection(FireStorageKeys.KEY_USER_TABLE).document(user.uid).set(userTable)
                .addOnSuccessListener {
                    Log.e("userAuthFireStorage", "Success: $it")
                }.addOnFailureListener {
                    LoginManager.getInstance().logOut()


                    Log.e("userAuthFireStorage", "Fail: ${it.message}")
                }
        }
        intentToMainScreen(user)
    }

    private fun generateRandomNumber(): Long {
        return (1000000000L..9999999999L).random()
    }

    private fun intentToMainScreen(user: FirebaseUser?) {
        if (user != null) {
            SharedPrefsUtils.setStringPreference(KeysUtils.KeyUUID, user.uid)
            Log.e("SignupActivity", "BrowserActivity::class.java 3")

            val intent = Intent(this@SignUpActivity, BrowserActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun onBackClickListner() {
        binding.cvBack.setOnClickListener {
            onBackPressed()
            finish()
        }
    }
}
package com.app.discountro.ui.notification.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.discountro.R
import com.app.discountro.databinding.FragmentAppModeNotificationBinding
import com.app.discountro.fireStore.helper.FireStoreHelper
import com.app.discountro.ui.home.view.BrowserActivity
import com.app.discountro.ui.notification.adapter.AppNotificationAdapter
import com.app.discountro.ui.notification.model.NotificationModel
import com.app.discountro.utils.FireStorageKeys
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.google.gson.GsonBuilder

class AppModeNotificationFragment : Fragment() {
    val TAG = AppModeNotificationFragment::class.java.simpleName
    lateinit var binding : FragmentAppModeNotificationBinding
    private var browserActivity: BrowserActivity? = null
    private lateinit var notificationAdapter : AppNotificationAdapter
    private val auth: FirebaseAuth = Firebase.auth
    private var notificationList: ArrayList<NotificationModel> = ArrayList()
    private var filterSelectedName = "Latest"


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentAppModeNotificationBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        browserActivity = activity as BrowserActivity?
        browserActivity?.appbarTitle?.text = "Notifications"

        initClick()
        setAdapter()
        getNotification()
    }

    private fun initClick() {
        binding.cvLatest.setOnClickListener {
            filterSelectedName = "Latest"
            filterChangeBackgroundColor(
                cvLatestColor = R.color.selected_red,
                tvLatest = R.color.white,
                cvThisWeek = R.color.white,
                tvThisWeek = R.color.black,
                cvThisMonth = R.color.white,
                tvThisMonth = R.color.black,
                cvThisYear = R.color.white,
                tvThisYear = R.color.black
            )

            Toast.makeText(requireContext(), filterSelectedName, Toast.LENGTH_SHORT).show()
        }

        binding.cvThisWeek.setOnClickListener {
            filterSelectedName = "ThisWeek"
            filterChangeBackgroundColor(
                cvLatestColor = R.color.white,
                tvLatest = R.color.black,
                cvThisWeek = R.color.selected_red,
                tvThisWeek = R.color.white,
                cvThisMonth = R.color.white,
                tvThisMonth = R.color.black,
                cvThisYear = R.color.white,
                tvThisYear = R.color.black
            )
            Toast.makeText(requireContext(), filterSelectedName, Toast.LENGTH_SHORT).show()
        }

        binding.cvThisMonth.setOnClickListener {
            filterSelectedName = "ThisMonth"
            filterChangeBackgroundColor(
                cvLatestColor = R.color.white,
                tvLatest = R.color.black,
                cvThisWeek = R.color.white,
                tvThisWeek = R.color.black,
                cvThisMonth = R.color.selected_red,
                tvThisMonth = R.color.white,
                cvThisYear = R.color.white,
                tvThisYear = R.color.black
            )
            Toast.makeText(requireContext(), filterSelectedName, Toast.LENGTH_SHORT).show()
        }

        binding.cvThisYear.setOnClickListener {
            filterSelectedName = "ThisYear"
            filterChangeBackgroundColor(
                cvLatestColor = R.color.white,
                tvLatest = R.color.black,
                cvThisWeek = R.color.white,
                tvThisWeek = R.color.black,
                cvThisMonth = R.color.white,
                tvThisMonth = R.color.black,
                cvThisYear = R.color.selected_red,
                tvThisYear = R.color.white
            )
            Toast.makeText(requireContext(), filterSelectedName, Toast.LENGTH_SHORT).show()
        }
    }

    private fun setAdapter() {
        binding.rvNotification.layoutManager = LinearLayoutManager(requireContext())
        notificationAdapter = AppNotificationAdapter(
            requireContext(),
            notificationList,
            object : AppNotificationAdapter.AppNotificationCallback {
                override fun itemNotificationClick(redirectLink: String) {
                    browserActivity?.openNewTabForNotification(redirectLink)
                    browserActivity?.clSwitchApp?.performClick()

                }

            })
        binding.rvNotification.adapter = notificationAdapter

    }

    private fun getNotification(){
        //GET DATA FROM FIREBASE
        FireStoreHelper.db?.collection(FireStorageKeys.KEY_USER_TABLE)!!.document(auth.currentUser!!.uid)
            ?.collection(FireStorageKeys.KEYUT_NOTIFICATION)
            ?.get()
            ?.addOnSuccessListener { result ->
                try {

                    if(!notificationList.isNullOrEmpty()){
                        notificationList.clear()
                        notificationAdapter.notifyDataSetChanged()
                    }

                    for (document in result) {
                        val jsonData = Gson().toJson(document.data)
                        val notificationModel = Gson().fromJson(jsonData, NotificationModel::class.java)
                        notificationList.add(notificationModel)
                    }

                    notificationAdapter.notifyDataSetChanged()
                    if(notificationList.isEmpty()){
                        binding.rvNotification.visibility = GONE
                        binding.llNoNotification.visibility = VISIBLE
                    }else{
                        binding.rvNotification.visibility = VISIBLE
                        binding.llNoNotification.visibility = GONE
                    }

                    Log.d(TAG,""+ GsonBuilder().setPrettyPrinting().create().toJson(notificationList))


                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }?.addOnFailureListener {
                Log.d("onFailed",""+it.message.toString())
            }
    }

    private fun convertToJson(mapData: Map<String, Any>?): String? {
        val gsn = Gson()
        return gsn.toJson(mapData)
    }

    fun filterChangeBackgroundColor(
        cvLatestColor: Int,
        tvLatest: Int,
        cvThisWeek: Int,
        tvThisWeek: Int,
        cvThisMonth: Int,
        tvThisMonth: Int,
        cvThisYear: Int,
        tvThisYear: Int
    ){
        binding.cvLatest.background.setTint(ContextCompat.getColor(requireContext(), cvLatestColor))
        binding.tvLatest.setTextColor(ContextCompat.getColor(requireContext(), tvLatest))

        binding.cvThisWeek.background.setTint(ContextCompat.getColor(requireContext(), cvThisWeek))
        binding.tvThisWeek.setTextColor(ContextCompat.getColor(requireContext(), tvThisWeek))

        binding.cvThisMonth.background.setTint(ContextCompat.getColor(requireContext(), cvThisMonth))
        binding.tvThisMonth.setTextColor(ContextCompat.getColor(requireContext(), tvThisMonth))

        binding.cvThisYear.background.setTint(ContextCompat.getColor(requireContext(), cvThisYear))
        binding.tvThisYear.setTextColor(ContextCompat.getColor(requireContext(), tvThisYear))
    }

}
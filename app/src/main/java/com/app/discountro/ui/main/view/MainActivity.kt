package com.app.discountro.ui.main.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import com.app.discountro.common.constant.AppConstant
import com.app.discountro.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    var findNavController: NavController? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
          //  initView()
    }

    private fun initView() {

       // findNavController = Navigation.findNavController(this@MainActivity, R.id.navHostMain)
        if (intent.hasExtra(AppConstant.DefaultValues.remoteMessage)) {
           //Condition For the Normal Navigation
        } else {
         //   findNavController!!.navigate(R.id.homeFragment)
        }
    }

    /*  override fun onBackPressed() {
          super.onBackPressed()
          finish()
      }

      private fun initView() {
          onSignOutClickListener()
          facebookFireLogin()
      }

      private fun onSignOutClickListener() {
          binding.acLogoutBtn.setOnClickListener {
              onFireSignOut()
              onSessionRemove()
              onSignOutActionIntent()
          }
      }

      private fun onSessionRemove() {
          SharedPrefsUtils.remove(KeysUtils.KeyUUID)
  //        SharedPrefsUtils.remove(KeysUtils.keyLangISO)
      }

      private fun onSignOutActionIntent() {
          val intent = Intent(this@MainActivity, WelcomeActivity::class.java)
          startActivity(intent)
          finish()
      }

      private fun facebookFireLogin() {
          FacebookSdk.fullyInitialize()
          AppEventsLogger.activateApp(application)
      }

      private fun onFireSignOut() {

          Firebase.auth.signOut()
          LoginManager.getInstance().logOut()
      }

      override fun onStart() {
          super.onStart()

          val currentUser = auth.currentUser
          if (currentUser != null) {
              updateUI(currentUser)
          }
      }

      private fun updateUI(currentUser: FirebaseUser) {
          binding.tvUserEmail.text =
              resources.getString(R.string.e_mail) + " : " + currentUser.email.toString()
          binding.tvLangISO.text =
              resources.getString(R.string.your_lang_iso) + " : " + SharedPrefsUtils.getStringPreference(
                  KeysUtils.keyLangISO
              )
      }*/
}
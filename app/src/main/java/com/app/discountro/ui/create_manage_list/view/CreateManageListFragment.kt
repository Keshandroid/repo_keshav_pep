package com.app.discountro.ui.create_manage_list.view

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.Interface.FireStoreAddListInterface
import com.app.discountro.Interface.FireStoreEditUpdateListInterface
import com.app.discountro.Interface.FireStoreGetFilterListListener
import com.app.discountro.Interface.FireStoreRemoveListInterface
import com.app.discountro.R
import com.app.discountro.databinding.FragmentCreateManageListBinding
import com.app.discountro.fireStore.helper.FireStoreHelper
import com.app.discountro.ui.create_manage_list.adapter.ListFilterAdapter
import com.app.discountro.ui.product.model.ListCollectionModel
import com.app.discountro.utils.Utils
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson

class CreateManageListFragment : Fragment(), ListFilterAdapter.ManageListListener {
    val TAG = CreateManageListFragment::class.java.simpleName
    lateinit var binding: FragmentCreateManageListBinding
    lateinit var listFilterAdapter: ListFilterAdapter
    private val auth: FirebaseAuth = Firebase.auth
    lateinit var createNewProductListBottomSheet: BottomSheetDialog
    var filterList: ArrayList<ListCollectionModel> = arrayListOf()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentCreateManageListBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initAdapter()
        initClick()
        getList(binding.rvList, binding.llNoList)
    }

    private fun initClick() {
        binding.cvCreateList.setOnClickListener {
            createEditListBottomSheet(binding.rvList, binding.llNoList)
        }
    }

    private fun initAdapter() {
        binding.rvList.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        listFilterAdapter = ListFilterAdapter(requireContext(), filterList, this)
        binding.rvList.adapter = listFilterAdapter
    }

    // Create and edit(update) list data bottomSheet
    private fun createEditListBottomSheet(rvList: RecyclerView, llNoListAvailable: LinearLayout, listCollectionModel: ListCollectionModel? = null, position: Int? = null) {
        createNewProductListBottomSheet = BottomSheetDialog(requireContext(), R.style.AppBottomSheetDialogTheme)
        createNewProductListBottomSheet.setContentView(R.layout.create_product_list_bottomsheet_layout)

        val etListName = createNewProductListBottomSheet.findViewById<EditText>(R.id.etListName)
        val etListDis = createNewProductListBottomSheet.findViewById<EditText>(R.id.etListDis)
        val llUpdate = createNewProductListBottomSheet.findViewById<LinearLayout>(R.id.llUpdate)
        val ivClose = createNewProductListBottomSheet.findViewById<ImageView>(R.id.ivClose)
        createNewProductListBottomSheet.behavior.state = BottomSheetBehavior.STATE_EXPANDED

        if(listCollectionModel != null){
            etListName?.setText(listCollectionModel.listName!!)
            listCollectionModel.listId?.let {
                etListDis?.setText(listCollectionModel.description)
            }
        }

        llUpdate?.setOnClickListener {
            if (listCollectionModel == null) {
                if (etListName!!.text.isNotBlank()) {
                    val requireModel = ListCollectionModel(
                        listName = etListName.text.toString(),
                        description = etListDis?.text.toString()
                    )
                    addListData(rvList, llNoListAvailable, requireModel)
                } else {
                    Toast.makeText(
                        createNewProductListBottomSheet.context,
                        "Please Enter Create List Name..",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                listCollectionModel.listId?.let {
                    val requireModel = ListCollectionModel(
                        listName = etListName?.text.toString(),
                        description = etListDis?.text.toString(),
                        listId = listCollectionModel.listId
                    )
                    editListData(requireModel, position!!)
                }
            }
        }

        ivClose?.setOnClickListener {
            createNewProductListBottomSheet.dismiss()
        }

        createNewProductListBottomSheet.show()
    }

    private fun addListData(rvList: RecyclerView, noListAvailable: LinearLayout, requireModel: ListCollectionModel){
        FireStoreHelper().addListCollection(
            auth.currentUser!!.uid,
            requireModel,
            object : FireStoreAddListInterface {
                override fun onSuccess(msg: String) {
                    Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
                    createNewProductListBottomSheet.dismiss()
                    filterList.add(requireModel)
                    if(filterList.isNotEmpty()){
                        rvList.visibility = View.VISIBLE
                        noListAvailable.visibility = View.GONE
                    }else{
                        rvList.visibility = View.GONE
                        noListAvailable.visibility = View.VISIBLE
                    }
                    listFilterAdapter.updateList(filterList)
                }
                override fun onError(error: String) {
                    Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    private fun getList(recyclerView: RecyclerView, noDataFound: LinearLayout) {
        binding.llLoading.visibility = VISIBLE

        FireStoreHelper().getListProductFilter(
            auth.currentUser!!.uid,
            object: FireStoreGetFilterListListener {
                override fun onSuccess(productFilterList: QuerySnapshot) {
                    binding.llLoading.visibility = GONE
                    filterList.clear()
                    for (i in productFilterList) {
                        val jsonData = Utils.convertToJson(i.data)
                        val gsn = Gson()
                        val requireModel: ListCollectionModel = gsn.fromJson(
                            jsonData.toString(),
                            ListCollectionModel::class.java
                        )
                        filterList.add(requireModel)
                    }
                    if(filterList.isNotEmpty()){
                        recyclerView.visibility = View.VISIBLE
                        noDataFound.visibility = View.GONE
                    }else{
                        recyclerView.visibility = View.GONE
                        noDataFound.visibility = View.VISIBLE
                    }
                    listFilterAdapter.updateList(filterList)
                }

                override fun onError(error: String) {
                    binding.llLoading.visibility = GONE
                    Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    private fun editListData(listCollectionModel: ListCollectionModel, position: Int){
        FireStoreHelper().editListData(
            auth.currentUser!!.uid,
            listCollectionModel,
            object : FireStoreEditUpdateListInterface {
                override fun onSuccess(msg: String) {
//                    filterList.remove(listCollectionModel)
//                    listProductFilterAdapter.notifyItemRemoved(position)
                    Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
                    createNewProductListBottomSheet.dismiss()
                    filterList[position] = listCollectionModel
                    listFilterAdapter.notifyItemChanged(position)
                }
                override fun onError(error: String) {
                    Log.e(TAG, "Error edit list :- $error")
                }
            }
        )
    }

    override fun removeList(position: Int, listCollectionModel: ListCollectionModel) {

        val builder = AlertDialog.Builder(context)

        // Set the dialog title and message
        builder.setTitle("Alert")
        builder.setMessage("Do you really want to delete?")

        // Set a positive button and its click listener
        builder.setPositiveButton("OK") { dialog, _ ->
            dialog.dismiss() // Close the dialog when the button is clicked
            binding.llLoading.visibility = VISIBLE

            FireStoreHelper().removeList(
                auth.currentUser!!.uid,
                listCollectionModel,
                object : FireStoreRemoveListInterface {
                    override fun onSuccess(msg: String) {
                        filterList.remove(listCollectionModel)
//                        listFilterAdapter.notifyItemRemoved(position)
                        listFilterAdapter.notifyDataSetChanged()

                        binding.llLoading.visibility = GONE

                        if(filterList.isNullOrEmpty()){
                            binding.llNoList.visibility = VISIBLE
                        }else{
                            binding.llNoList.visibility = GONE
                        }
                    }
                    override fun onError(error: String) {
                        binding.llLoading.visibility = GONE
                        Log.e(TAG, "Error remove list :-  $error")
                    }
                }
            )
        }

        builder.setNegativeButton("Cancel") { dialog, _ ->
            dialog.dismiss() // Close the dialog when the button is clicked
        }

        // Create and show the dialog
        val dialog = builder.create()
        dialog.show()
    }

    override fun editList(position: Int, listCollectionModel: ListCollectionModel) {
        createEditListBottomSheet(binding.rvList, binding.llNoList, listCollectionModel, position)
    }


}
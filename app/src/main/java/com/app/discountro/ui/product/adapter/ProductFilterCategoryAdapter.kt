package com.app.discountro.ui.product.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.R
import com.app.discountro.databinding.ItemProductFiltersBinding
import com.app.discountro.ui.product.model.FilterProductModel

class ProductFilterCategoryAdapter(val context: Context, private val filtersList: ArrayList<FilterProductModel>, private val filterListener: ProductFilterListener) :
    RecyclerView.Adapter<ProductFilterCategoryAdapter.ViewHolder>() {

    var itemSelectedPosition:Int? = null

    class ViewHolder(val binding: ItemProductFiltersBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemProductFiltersBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.tvCategoryName.text = filtersList[position].title

        if(filtersList[position].icon != null){
            holder.binding.ivCategoryImg.visibility = VISIBLE
            holder.binding.ivCategoryImg.setImageDrawable(ContextCompat.getDrawable(context, filtersList[position].icon!!))
        }else{
            holder.binding.ivCategoryImg.visibility = GONE
        }

        /*if(filtersList[position].backgroundColor != null){
            holder.binding.cvMain.background.setTint(ContextCompat.getColor(context, filtersList[position].backgroundColor!!))
        }else{
            holder.binding.cvMain.background.setTint(ContextCompat.getColor(context,  R.color.white))
        }

        if(filtersList[position].textColor != null){
            holder.binding.tvCategoryName.setTextColor(ContextCompat.getColor(context, filtersList[position].textColor!!))
            holder.binding.ivCategoryImg.setColorFilter(ContextCompat.getColor(context, filtersList[position].textColor!!))
        }else{
            holder.binding.tvCategoryName.setTextColor(ContextCompat.getColor(context, R.color.black))
            holder.binding.ivCategoryImg.setColorFilter(ContextCompat.getColor(context, R.color.black))
        }*/

        if(itemSelectedPosition == position){
            filtersList[position].isSelectedList = true
            holder.binding.cvMain.background.setTint(ContextCompat.getColor(context, R.color.selected_red))
            holder.binding.tvCategoryName.setTextColor(ContextCompat.getColor(context, R.color.white))
            holder.binding.ivCategoryImg.setColorFilter(ContextCompat.getColor(context, R.color.white))
        }else{
            filtersList[position].isSelectedList = false
            holder.binding.cvMain.background.setTint(ContextCompat.getColor(context, R.color.white))
            holder.binding.tvCategoryName.setTextColor(ContextCompat.getColor(context, R.color.black))
            holder.binding.ivCategoryImg.setColorFilter(ContextCompat.getColor(context, R.color.black))
        }        

        holder.binding.root.setOnClickListener {
            itemSelectedPosition = position
            filterListener.filterItemClick(filtersList[position])
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return filtersList.size
    }

    interface ProductFilterListener{
        fun filterItemClick(productData: FilterProductModel)
    }
}
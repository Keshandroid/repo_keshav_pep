package com.app.discountro.ui.home.model

import com.google.firebase.Timestamp


data class FeaturedModel(
    var country_code: String?,
    var date_add: Timestamp?,
    var id_featured_image: Int?,
    var image_url: String?,
    var redirect_url: String?,
    var subtitle: String?,
    var title: String?,
)

package com.app.discountro.ui.product.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.R
import com.app.discountro.databinding.ItemProductsBinding
import com.app.discountro.ui.product.model.ProductModel
import com.bumptech.glide.Glide


class PurchasedProductAdapter(val context: Context, private val purchasedProductList: ArrayList<ProductModel>, val listener: PurchasedProductListener) : RecyclerView.Adapter<PurchasedProductAdapter.ViewHolder>() {

    class ViewHolder(val binding: ItemProductsBinding):RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemProductsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        /**
         * TODO :- When uncomment from the using the product collection data and comment the allProduct collection data.
         * */
        holder.binding.tvProductName.text = purchasedProductList[position].product_name
        holder.binding.tvProductPrice.text = "${purchasedProductList[position].currency} ${purchasedProductList[position].max_price}"

//        holder.binding.ivProductImg.setImageDrawable(context.getDrawable(R.drawable.ic_no_product_image))
        if(!purchasedProductList.get(position).product_image.isNullOrEmpty()){
            Glide.with(context).load(purchasedProductList[position].product_image).into(holder.binding.ivProductImg)
        }else{
            holder.binding.ivProductImg.setImageDrawable(context.getDrawable(R.drawable.ic_no_product_image))
        }

        if(purchasedProductList[position].isFavorite!!){
            holder.binding.ivFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_favorite_fill))
        }else{
            holder.binding.ivFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_not_favorite))
        }
//        holder.binding.ivFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_favorite_fill))

        if(!purchasedProductList[position].seller_list.isNullOrEmpty()){
            Glide.with(context).load(purchasedProductList[position].seller_list!![0]!!.store_logo).into(holder.binding.ivStoreLogo)
        }


        /**
         * TODO :- When using the get from allProduct collection then used it and using Product collection then comment this and unComment the above code.
         * */
        /*holder.binding.tvProductName.text = purchasedProductList[position].name
        holder.binding.tvProductDesc.text = purchasedProductList[position].description
        holder.binding.tvProductPrice.text = "${purchasedProductList[position].price}"

        Glide.with(context).load(purchasedProductList[position].image!![0]).placeholder(ContextCompat.getDrawable(context, R.drawable.ic_no_product_image)).into(holder.binding.ivProductImg)

//        holder.binding.ivProductImg.setImageDrawable(context.getDrawable(R.drawable.ic_no_product_image))

        if(purchasedProductList[position].isFavorite!!){
            holder.binding.ivFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_favorite_fill))
        }else{
            holder.binding.ivFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_not_favorite))
        }

//        if(!purchasedProductList[position].seller_list.isNullOrEmpty()) {
//            Glide.with(context).load(purchasedProductList[position].seller_list!![0]!!.store_logo).into(holder.binding.ivStoreLogo)
//        }

        holder.binding.ivStoreLogo.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_no_product_image))*/
        
        holder.binding.root.setOnClickListener {
            listener.purchasedProductOnClick(purchasedProductList[position])
        }

        holder.binding.llDetailsFavorite.setOnClickListener {
            purchasedProductList[position].isFavorite = !purchasedProductList[position].isFavorite!!
            listener.purchasedFavoriteIconClick(position, purchasedProductList[position])
        }
    }

    override fun getItemCount(): Int {
        return  purchasedProductList.size
    }

    interface PurchasedProductListener{
        fun purchasedProductOnClick(productData: ProductModel)
        fun purchasedFavoriteIconClick(position: Int, productData: ProductModel)
    }
}
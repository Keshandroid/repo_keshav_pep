package com.app.discountro.ui.home.view.models

import com.app.discountro.ui.notification.model.NotificationModel
import com.google.gson.annotations.SerializedName

data class UserModel (
    @SerializedName("email")
    val email: String = "",

    @SerializedName("favorite_stores")
    val favorite_stores: ArrayList<FavoriteStore>? = null,

    @SerializedName("notification")
    val notification: ArrayList<NotificationModel>? = null,

)
package com.app.discountro.ui.product.adapter

import android.content.Context
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.view.marginTop
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.R
import com.app.discountro.databinding.ItemProductsBinding
import com.app.discountro.ui.product.model.ProductModel
import com.bumptech.glide.Glide
import java.util.*

class ProductsAdapter(val context: Context, private val productsList: ArrayList<ProductModel>, var listener : ProductClick) :
    RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {

    class ViewHolder(val binding: ItemProductsBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemProductsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        /**
         * TODO :- When uncomment from the using the product collection data and comment the allProduct collection data.
        * */
        holder.binding.tvProductName.text = productsList[position].product_name
        holder.binding.tvProductPrice.text = "${productsList[position].currency} ${productsList[position].max_price}"


        if(!productsList.get(position).product_image.isNullOrEmpty()){
            Glide.with(context).load(productsList[position].product_image).into(holder.binding.ivProductImg)
        }else{
            holder.binding.ivProductImg.setImageDrawable(context.getDrawable(R.drawable.ic_no_product_image))
        }



        if(productsList[position].isFavorite!!){
            holder.binding.ivFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_favorite_fill))
        }else{
            holder.binding.ivFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_not_favorite))
        }

        if(!productsList[position].seller_list.isNullOrEmpty()){
            Glide.with(context).load(productsList[position].seller_list!![0]!!.store_logo).into(holder.binding.ivStoreLogo)
        }



        /**
         * TODO :- When using the get from allProduct collection then used it and using Product collection then comment this and unComment the above code.
        * */
        /*holder.binding.tvProductName.text = productsList[position].name
        holder.binding.tvProductDesc.text = productsList[position].description
        holder.binding.tvProductPrice.text = "${productsList[position].price}"

        Glide.with(context).load(productsList[position].image!![0]).placeholder(ContextCompat.getDrawable(context, R.drawable.ic_no_product_image)).into(holder.binding.ivProductImg)

//        holder.binding.ivProductImg.setImageDrawable(context.getDrawable(R.drawable.ic_no_product_image))

        if(productsList[position].isFavorite!!){
            holder.binding.ivFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_favorite_fill))
        }else{
            holder.binding.ivFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_not_favorite))
        }



//        if(!productsList[position].seller_list.isNullOrEmpty()) {
//            Glide.with(context).load(productsList[position].seller_list!![0]!!.store_logo).into(holder.binding.ivStoreLogo)
//        }

        holder.binding.ivStoreLogo.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_no_product_image))*/


        holder.binding.root.setOnClickListener {
            listener.productOnClick(productsList[position])
        }

        holder.binding.llDetailsFavorite.setOnClickListener {
            productsList[position].isFavorite = !productsList[position].isFavorite!!
            listener.productFavoriteOnClick(position, productsList[position])
        }
    }

    override fun getItemCount(): Int {
        return productsList.size
    }

    interface ProductClick{
        fun productOnClick(productDetails : ProductModel)
        fun productFavoriteOnClick(position: Int, productModel : ProductModel)
    }
}
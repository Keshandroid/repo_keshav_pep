package com.app.discountro.ui.home.view.models

import java.io.Serializable


data class FavoriteStore(
    var checked: Boolean?,
    var storeName: String?,
    var logoUrl: String?
    ): Serializable
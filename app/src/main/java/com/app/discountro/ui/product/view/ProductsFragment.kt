package com.app.discountro.ui.product.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.discountro.Interface.*
import com.app.discountro.R
import com.app.discountro.databinding.ProductFragmentBinding
import com.app.discountro.fireStore.helper.FireStoreHelper
import com.app.discountro.fireStore.helper.FireStoreHelper.Companion.PARENT_ALL_PRODUCT_STORE_KEY
import com.app.discountro.fireStore.helper.FireStoreHelper.Companion.PARENT_PRODUCT_STORE_KEY
import com.app.discountro.ui.home.view.BrowserActivity
import com.app.discountro.ui.product.adapter.ProductFavoriteAdapter
import com.app.discountro.ui.product.adapter.ProductFilterCategoryAdapter
import com.app.discountro.ui.product.adapter.ProductListNameAdapter
import com.app.discountro.ui.product.adapter.ProductsAdapter
import com.app.discountro.ui.product.adapter.PurchasedProductAdapter
import com.app.discountro.ui.product.model.FavoritePurchasedCustomListModel
import com.app.discountro.ui.product.model.FilterProductModel
import com.app.discountro.ui.product.model.ListCollectionModel
import com.app.discountro.ui.product.model.ProductModel
import com.app.discountro.utils.FireStorageKeys.Companion.KEYUT_FAVORITE_PRODUCT
import com.app.discountro.utils.FireStorageKeys.Companion.KEYUT_PRODUCTS
import com.app.discountro.utils.FireStorageKeys.Companion.KEY_USER_TABLE
import com.app.discountro.utils.KeysUtils.Companion.KEY_PRODUCT_MODEL
import com.app.discountro.utils.Utils.Companion.convertToJson
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.google.gson.GsonBuilder

class ProductsFragment : Fragment(), ProductsAdapter.ProductClick, PurchasedProductAdapter.PurchasedProductListener,
    ProductFilterCategoryAdapter.ProductFilterListener, ProductFavoriteAdapter.FavoriteProductListener, ProductListNameAdapter.ListNameProductClick{
    val TAG = ProductsFragment::class.java.simpleName
    private val auth: FirebaseAuth = Firebase.auth
    private lateinit var binding: ProductFragmentBinding
    private lateinit var productFilterAdapter: ProductFilterCategoryAdapter
    private lateinit var productFavoriteAdapter: ProductFavoriteAdapter
    private lateinit var purchasedProductAdapter: PurchasedProductAdapter
    private lateinit var productAdapter: ProductsAdapter
    private lateinit var productListNameAdapter: ProductListNameAdapter
    private var browserActivity: BrowserActivity? = null
    private var productList: ArrayList<ProductModel> = arrayListOf()
    private var filterTitleList: ArrayList<FilterProductModel> = arrayListOf()
    private var favoriteProductIdList: ArrayList<String> = arrayListOf()
    private var favoriteProductList: ArrayList<ProductModel> = arrayListOf()
    private var purchasedProductList: ArrayList<ProductModel> = arrayListOf()
    private var filterProductsList: ArrayList<ProductModel> = arrayListOf()
    lateinit var createNewProductListBottomSheet: BottomSheetDialog
    private val limit = 10
    private var lastVisible: DocumentSnapshot? = null
    private var isScrolling = false
    private var isLastItemReached = false



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = ProductFragmentBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        browserActivity = activity as BrowserActivity?
        browserActivity?.appbarTitle?.text = "All Products"

        setAdapter()
        getAllProduct()
        getList()
    }

    private fun setAdapter() {
        setStaticFilterData()

        binding.rvProductFilters.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        productFilterAdapter = ProductFilterCategoryAdapter(requireContext(), filterTitleList, this)
        binding.rvProductFilters.adapter = productFilterAdapter


        binding.rvProducts.layoutManager = GridLayoutManager(requireContext(), 2)
        productAdapter = ProductsAdapter(requireContext(), productList, this)
        binding.rvProducts.adapter = productAdapter


        binding.rvFavoriteProducts.layoutManager = GridLayoutManager(requireContext(), 2)
        productFavoriteAdapter = ProductFavoriteAdapter(requireContext(), favoriteProductList, this)
        binding.rvFavoriteProducts.adapter = productFavoriteAdapter

        binding.rvPurchasedProducts.layoutManager = GridLayoutManager(requireContext(), 2)
        purchasedProductAdapter = PurchasedProductAdapter(requireContext(), purchasedProductList, this)
        binding.rvPurchasedProducts.adapter = purchasedProductAdapter

        binding.rvListProducts.layoutManager = GridLayoutManager(requireContext(), 2)
        productListNameAdapter = ProductListNameAdapter(requireContext(), filterProductsList, this)
        binding.rvListProducts.adapter = productListNameAdapter
    }

    private fun setStaticFilterData() {
        filterTitleList.add(
            FilterProductModel(
                "All",
                R.drawable.ic_all_grid,
                R.color.white,
                R.color.black
            )
        )
        filterTitleList.add(
            FilterProductModel(
                "New List",
                R.drawable.icon_plus,
                R.color.white,
                R.color.black
            )
        )
        filterTitleList.add(
            FilterProductModel(
                "Favorite",
                R.drawable.ic_not_favorite,
                R.color.white,
                R.color.black
            )
        )
        filterTitleList.add(
            FilterProductModel(
                "Purchased",
                R.drawable.ic_right_rounded_cut,
                R.color.white,
                R.color.black
            )
        )
    }

    override fun productOnClick(productDetails: ProductModel) {
        var bundle = Bundle()
        bundle.putSerializable(KEY_PRODUCT_MODEL, productDetails)
        browserActivity?.setFragment(ProductDetailsFragment(), "ProductDetailsFragment", bundle)
    }

    override fun productFavoriteOnClick(position: Int, productModel: ProductModel) {
        if(productModel.isFavorite!!){

            // TODO :- This model is used for if using the allproduct collection then the uncomment code and comment product collection
            /*val requireModel = FavoritePurchasedCustomListModel(
                id_product = productModel.id_product,
                url = productModel.url,
                url_hash = productModel.hash_url
            )*/

            // TODO :- This model is used for if using the old product collection then the uncomment code and comment all product collection
            val requireModel = FavoritePurchasedCustomListModel(
                id_product = productModel.id_product,
                url_hash = productModel.product_urls?.get(0)
            )
            addFavoriteProduct(position, requireModel, productModel)
        }else {
            removeFavoriteProduct(position, productModel, false)
        }
    }

    private fun getAllProduct() {
        //GET DATA FROM FIREBASE

        /**
         *   TODO :- If using allProduct collection data add in product list then uncomment this code
         * */
        /*binding.llProgressbar.visibility = VISIBLE
        FireStoreHelper.db?.collection(PARENT_ALL_PRODUCT_STORE_KEY)!!
            ?.get()
            ?.addOnSuccessListener { Result ->
                try {
                    productList.clear()

                    // Save the last visible document for pagination
                    lastVisible = Result.documents[Result.size() - 1]

                    for (document in Result) {
                        val jsonData = convertToJson(document.data)
                        val gsn = Gson()
                        val productModel: ProductModel = gsn.fromJson<ProductModel>(
                            jsonData.toString(),
                            ProductModel::class.java
                        )
                        // added the hashUrl in pojo
                        productModel.hash_url = document.id

                        if (productModel != null) {
                            if (productModel.id_product in favoriteProductIdList) {
                                productModel.isFavorite = true
                            }
                            productList.add(productModel)
                        }
                    }

                    Log.e(TAG, "getAllProduct: size ${productList.size}")
                    productAdapter.notifyDataSetChanged()
                    binding.llProgressbar.visibility = GONE

                    getFavoriteProduct()
//                        getPurchasedProducts()


                    if (!productList.isNullOrEmpty()) {
                        binding.rvProducts.visibility = VISIBLE
                        binding.llNoProducts.visibility = GONE
                    } else {
                        binding.llNoProducts.visibility = VISIBLE
                        binding.rvProducts.visibility = GONE
                    }
*/



                    //TODO That is scroll to load next item but currently data not get from the firestore
//                    val onScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
//                            override fun onScrollStateChanged(
//                                recyclerView: RecyclerView,
//                                newState: Int
//                            ) {
//                                super.onScrollStateChanged(recyclerView, newState)
//                                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
//                                    isScrolling = true
//                                }
//                            }
//
//                            override fun onScrolled(
//                                recyclerView: RecyclerView,
//                                dx: Int,
//                                dy: Int
//                            ) {
//                                super.onScrolled(recyclerView, dx, dy)
//
//                                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
//                                val firstVisibleItemPosition = linearLayoutManager!!.findFirstVisibleItemPosition()
//                                val visibleItemCount = linearLayoutManager.childCount
//                                val totalItemCount = linearLayoutManager.itemCount
//
//                                if (isScrolling && firstVisibleItemPosition + visibleItemCount == totalItemCount && !isLastItemReached) {
//                                    isScrolling = false
//                                    binding.llProgressbar.visibility = VISIBLE
//
//                                    FireStoreHelper.db?.collection(PARENT_PRODUCT_STORE_KEY)
//                                        ?.orderBy("product_name", Query.Direction.ASCENDING)
//                                        ?.startAfter(lastVisible)
//                                        ?.limit(limit.toLong())
//                                        ?.get()
//                                        ?.addOnCompleteListener { task ->
//                                            try {
//                                                if (task.isSuccessful) {
//                                                    for (d in task.result) {
//                                                        val jsonData = convertToJson(d.data)
//                                                        val gsn = Gson()
//                                                        val productModel: ProductModel = gsn.fromJson<ProductModel>(
//                                                            jsonData.toString(),
//                                                            ProductModel::class.java
//                                                        )
//                                                        Log.e(TAG, "getNewProduct List : $productModel")
//                                                        productList.add(productModel)
//                                                    }
//
//                                                    productAdapter.notifyDataSetChanged()
//                                                    lastVisible = task.result.documents[task.result.size() - 1]
//
//                                                    if (task.result.size() < limit) {
//                                                        isLastItemReached = true
//                                                    }
//
//                                                    binding.llProgressbar.visibility = GONE
//                                                }
//                                            }catch (ex:Exception){
//                                                binding.llProgressbar.visibility = GONE
//                                                Log.e(TAG, "getNewProduct catch : ${ex.message}")
//                                                ex.printStackTrace()
//                                            }
//                                        }
//                                        ?.addOnFailureListener {
//                                            binding.llProgressbar.visibility = GONE
//                                            Log.e(TAG, "getNewProduct fail : ${it.message}")
//                                        }
//                                    }
//                                }
//                        }
//                    binding.rvProducts.addOnScrollListener(onScrollListener)



             /*   } catch (ex: Exception) {
                    binding.llProgressbar.visibility = GONE
                    Log.e(TAG, "getAllProduct catch : ${ex.message}")
                    ex.printStackTrace()
                }
            }?.addOnFailureListener {
                binding.llProgressbar.visibility = GONE
                Log.e(TAG, "getAllProduct fail : ${it.message}")
            }*/





        /**
         *  TODO :- If using product collection data add in product list then uncomment this code
         */
        if(productList.size < 10) {
            binding.llProgressbar.visibility = VISIBLE
            FireStoreHelper.db?.collection(PARENT_PRODUCT_STORE_KEY)!!
                ?.orderBy("product_name", Query.Direction.ASCENDING)
                ?.limit(limit.toLong())
                ?.get()
                ?.addOnSuccessListener { Result ->
                    try {
                        productList.clear()

                        // Save the last visible document for pagination
                        lastVisible = Result.documents[Result.size() - 1]

                        for (document in Result) {
                            val jsonData = convertToJson(document.data)
                            val gsn = Gson()
                            val productModel: ProductModel = gsn.fromJson<ProductModel>(
                                jsonData.toString(),
                                ProductModel::class.java
                            )

                            if (productModel != null) {
                                if (productModel.id_product in favoriteProductIdList) {
                                    productModel.isFavorite = true
                                }
                                productList.add(productModel)
                            }
                        }

                        Log.e(TAG, "getAllProduct: size ${productList.size}")
                        productAdapter.notifyDataSetChanged()
                        binding.llProgressbar.visibility = GONE

                        getFavoriteProduct()
//                        getPurchasedProducts()


                        if (!productList.isNullOrEmpty()) {
                            binding.rvProducts.visibility = VISIBLE
                            binding.llNoProducts.visibility = GONE
                        } else {
                            binding.llNoProducts.visibility = VISIBLE
                            binding.rvProducts.visibility = GONE
                        }


                        //TODO That is scroll to load next item but currently data not get from the firestore
//                        val onScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
//                                override fun onScrollStateChanged(
//                                    recyclerView: RecyclerView,
//                                    newState: Int
//                                ) {
//                                    super.onScrollStateChanged(recyclerView, newState)
//                                    if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
//                                        isScrolling = true
//                                    }
//                                }
//
//                                override fun onScrolled(
//                                    recyclerView: RecyclerView,
//                                    dx: Int,
//                                    dy: Int
//                                ) {
//                                    super.onScrolled(recyclerView, dx, dy)
//
//                                    val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
//                                    val firstVisibleItemPosition = linearLayoutManager!!.findFirstVisibleItemPosition()
//                                    val visibleItemCount = linearLayoutManager.childCount
//                                    val totalItemCount = linearLayoutManager.itemCount
//
//                                    if (isScrolling && firstVisibleItemPosition + visibleItemCount == totalItemCount && !isLastItemReached) {
//                                        isScrolling = false
//                                        binding.llProgressbar.visibility = VISIBLE
//
//                                        FireStoreHelper.db?.collection(PARENT_PRODUCT_STORE_KEY)
//                                            ?.orderBy("product_name", Query.Direction.ASCENDING)
//                                            ?.startAfter(lastVisible)
//                                            ?.limit(limit.toLong())
//                                            ?.get()
//                                            ?.addOnCompleteListener { task ->
//                                                try {
//                                                    if (task.isSuccessful) {
//                                                        for (d in task.result) {
//                                                            val jsonData = convertToJson(d.data)
//                                                            val gsn = Gson()
//                                                            val productModel: ProductModel = gsn.fromJson<ProductModel>(
//                                                                jsonData.toString(),
//                                                                ProductModel::class.java
//                                                            )
//                                                            Log.e(TAG, "getNewProduct List : $productModel")
//                                                            productList.add(productModel)
//                                                        }
//
//                                                        productAdapter.notifyDataSetChanged()
//                                                        lastVisible = task.result.documents[task.result.size() - 1]
//
//                                                        if (task.result.size() < limit) {
//                                                            isLastItemReached = true
//                                                        }
//
//                                                        binding.llProgressbar.visibility = GONE
//                                                    }
//                                                }catch (ex:Exception){
//                                                    binding.llProgressbar.visibility = GONE
//                                                    Log.e(TAG, "getNewProduct catch : ${ex.message}")
//                                                    ex.printStackTrace()
//                                                }
//                                            }
//                                            ?.addOnFailureListener {
//                                                binding.llProgressbar.visibility = GONE
//                                                Log.e(TAG, "getNewProduct fail : ${it.message}")
//                                            }
//                                        }
//                                    }
//                            }
//                        binding.rvProducts.addOnScrollListener(onScrollListener)

                    } catch (ex: Exception) {
                        binding.llProgressbar.visibility = GONE
                        Log.e(TAG, "getAllProduct catch : ${ex.message}")
                        ex.printStackTrace()
                    }
                }?.addOnFailureListener {
                    binding.llProgressbar.visibility = GONE
                    Log.e(TAG, "getAllProduct fail : ${it.message}")
                }
        }
    }

    fun fetchNextProducts(){

    }

    // This is the user collection in product collection in favoriteCollection
    private fun addFavoriteProduct(
        position: Int,
        favoritePurchasedCustomListModel: FavoritePurchasedCustomListModel,
        productModel: ProductModel
    ) {

        FireStoreHelper().addFavoriteProduct(
            auth.currentUser!!.uid,
            favoritePurchasedCustomListModel,
            object : FireStoreAddFavoriteProductInterface {
                override fun onSuccess(msg: String) {
                    productAdapter.notifyItemChanged(position)
                    favoriteProductList.add(productModel)
                    productFavoriteAdapter.notifyDataSetChanged()
//                    productAdapter.notifyItemChanged(position)
                    productAdapter.notifyDataSetChanged()
                    purchasedProductAdapter.notifyDataSetChanged()
                    productListNameAdapter.notifyDataSetChanged()

                }
                override fun onError(error: String) {
                    Log.e(TAG, "Error adding document $error")
                }
            }
        )



      /*  val favoriteCollectionRef = FireStoreHelper.db?.collection(FireStorageKeys.KEY_USER_TABLE)
            ?.document(auth.currentUser!!.uid)?.collection(KEYUT_PRODUCTS)?.document(auth.currentUser!!.uid)?.collection(KEYUT_FAVORITE_PRODUCT)

        FireStoreHelper.db?.collection(FireStorageKeys.KEY_USER_TABLE)
            ?.document(auth.currentUser!!.uid)
            ?.collection(KEYUT_PRODUCTS)
            ?.document(auth.currentUser!!.uid)
            ?.collection(KEYUT_FAVORITE_PRODUCT)
            ?.get()
            ?.addOnSuccessListener { result ->
                try {

                    favoriteCollectionRef!!.document(productModel.id_product!!).set(productModel)
                        ?.addOnSuccessListener { documentReference ->

                            Log.e(TAG, "Favorite Product insert or update")
                        }
                        ?.addOnFailureListener { e ->
                            Log.e(TAG, "Error adding document", e)
                        }

                } catch (e: Exception) {
                    Log.e("FireStore", "Error getting documents ${e.message}")
                }
            }
            ?.addOnFailureListener {
                Log.e("FireStore", "Error getting documents", it)
            }*/
    }

    // This is the old direct AddFavorite collection
    /*private fun addFavoriteProduct(position: Int, productModel: ProductModel) {
        val collectionRef = FireStoreHelper.db?.collection(FireStorageKeys.KEY_USER_TABLE)
            ?.document(auth.currentUser!!.uid)?.collection(KEYUT_FAVORITE_PRODUCT)

        FireStoreHelper.db?.collection(FireStorageKeys.KEY_USER_TABLE)
            ?.document(auth.currentUser!!.uid)
            ?.collection(KEYUT_FAVORITE_PRODUCT)
            ?.get()
            ?.addOnSuccessListener { result ->
                try {

                    collectionRef!!.document(productModel.id_product!!).set(productModel)
                        ?.addOnSuccessListener { documentReference ->
                            favoriteProductList.add(productModel)
                            productAdapter.notifyItemChanged(position)
                            productFavoriteAdapter.notifyDataSetChanged()
                            Log.e(TAG, "Favorite Product insert or update")
                        }
                        ?.addOnFailureListener { e ->
                            Log.e(TAG, "Error adding document", e)
                        }

                } catch (e: Exception) {
                    Log.e("FireStore", "Error getting documents ${e.message}")
                }
            }
            ?.addOnFailureListener {
                Log.e("FireStore", "Error getting documents", it)
            }
    }*/

    private fun removeFavoriteProduct(position: Int, productModel: ProductModel, isFavoriteAdapter: Boolean){

        FireStoreHelper.db?.collection(KEY_USER_TABLE)!!.document(auth.currentUser!!.uid)
            .collection(KEYUT_PRODUCTS)
            ?.document(auth.currentUser!!.uid)
            ?.collection(KEYUT_FAVORITE_PRODUCT)
            ?.document(productModel.id_product!!)
            ?.delete()
            ?.addOnSuccessListener {
                Log.e(TAG, "favorite Product removed successfully")

                if(isFavoriteAdapter){
                    var filter = productList.filter {
                        productModel.id_product == it.id_product
                    }
                    productList[productList.indexOf(filter[0])].isFavorite = false
                    productFavoriteAdapter.notifyDataSetChanged()
                    productAdapter.notifyItemChanged(productList.indexOf(filter[0]))
                }else {
                    favoriteProductList.remove(productModel)
//                    productAdapter.notifyItemChanged(position)
                    productAdapter.notifyDataSetChanged()
                    purchasedProductAdapter.notifyDataSetChanged()
                    productFavoriteAdapter.notifyDataSetChanged()
                    productListNameAdapter.notifyDataSetChanged()
                }
            }
            ?.addOnFailureListener {
                Log.e("FireStore", "Error getting documents", it)
            }
    }

    private fun getFavoriteProduct(){
        //GET DATA FROM FIREBASE
        binding.llProgressbar.visibility = VISIBLE

        FireStoreHelper.db?.collection(KEY_USER_TABLE)!!.document(auth.currentUser!!.uid)
            .collection(KEYUT_PRODUCTS)
            ?.document(auth.currentUser!!.uid)
            ?.collection(KEYUT_FAVORITE_PRODUCT)
            ?.get()
            ?.addOnSuccessListener { result ->
                try {

                    if(!favoriteProductList.isNullOrEmpty()){
                        favoriteProductList.clear()
                    }

                    for (document in result) {
                        favoriteProductIdList.add(document.id)
                        val jsonData = Gson().toJson(document.data)
                        val favoriteProductModel = Gson().fromJson(jsonData, FavoritePurchasedCustomListModel::class.java)
                        Log.d(TAG, "all product list 1 :- $productList")
                        Log.d(TAG, "all favoriteProductModel list 1 :- $favoriteProductModel")


                        val getFavoriteModel = productList.filter {
                            it.id_product == favoriteProductModel!!.id_product!!
                        }
                        Log.d(TAG, "favorite product list 1 :- $favoriteProductList")
                        Log.d(TAG, "favorite current product list 1 :- $getFavoriteModel")

                        if(!getFavoriteModel.isNullOrEmpty()){
                            favoriteProductList.add(getFavoriteModel[0])
                        }
                    }

                    productFavoriteAdapter.notifyDataSetChanged()

                    for (model in favoriteProductList){
                        if(model in productList ){
                            productList[productList.indexOf(model)].isFavorite = true
                            productAdapter.notifyItemChanged(productList.indexOf(model))
                        }
                    }

                    binding.llProgressbar.visibility = GONE
                    Log.d(TAG,"favorite product id's :- "+ GsonBuilder().setPrettyPrinting().create().toJson(favoriteProductIdList))
                    Log.d(TAG,"favorite product list 2 :- "+ GsonBuilder().setPrettyPrinting().create().toJson(favoriteProductList))

                }catch (ex:Exception){
                    binding.llProgressbar.visibility = GONE
                    ex.printStackTrace()
                }
            }?.addOnFailureListener {
                binding.llProgressbar.visibility = GONE
                Log.d("onFailed",""+it.message.toString())
            }
    }

    override fun filterItemClick(productData: FilterProductModel) {
        if(productData.title == "All"){
            binding.rvProducts.visibility = VISIBLE
            binding.rvFavoriteProducts.visibility = GONE
            binding.rvPurchasedProducts.visibility = GONE
            binding.rvListProducts.visibility = GONE
        }else if(productData.title == "Favorite"){
            binding.rvFavoriteProducts.visibility = VISIBLE
            binding.rvProducts.visibility = GONE
            binding.rvPurchasedProducts.visibility = GONE
            binding.rvListProducts.visibility = GONE

            if(!favoriteProductList.isNullOrEmpty()){
                binding.rvFavoriteProducts.visibility = VISIBLE
                binding.llNoProducts.visibility = GONE
            }else{
                binding.rvFavoriteProducts.visibility = GONE
                binding.llNoProducts.visibility = VISIBLE
            }

        }else if(productData.title == "New List"){
            createNewProductListBottomSheet()
            /*binding.rvProducts.visibility = VISIBLE
            binding.rvFavoriteProducts.visibility = GONE
            binding.rvPurchasedProducts.visibility = GONE
            binding.rvListProducts.visibility = GONE*/
        }else if(productData.title == "Purchased"){
            getPurchasedProducts()
            binding.rvPurchasedProducts.visibility = VISIBLE
            binding.rvFavoriteProducts.visibility = GONE
            binding.rvProducts.visibility = GONE
            binding.rvListProducts.visibility = GONE
        } else {
            getListByName(productData.listId!!)
            binding.rvListProducts.visibility = VISIBLE
            binding.rvPurchasedProducts.visibility = GONE
            binding.rvFavoriteProducts.visibility = GONE
            binding.rvProducts.visibility = GONE
        }
    }

    override fun favoriteProductOnClick(productData: ProductModel) {
        val bundle = Bundle()
        bundle.putSerializable(KEY_PRODUCT_MODEL, productData)
        browserActivity?.setFragment(ProductDetailsFragment(), "ProductDetailsFragment", bundle)
    }

    override fun favoriteProductIconClick(position: Int, productData: ProductModel) {
        favoriteProductList.remove(productData)
        removeFavoriteProduct(position, productData, true)
    }

    private fun createNewProductListBottomSheet() {
        createNewProductListBottomSheet = BottomSheetDialog(requireContext(), R.style.AppBottomSheetDialogTheme)
        createNewProductListBottomSheet.setContentView(R.layout.create_product_list_bottomsheet_layout)
        val etListName = createNewProductListBottomSheet.findViewById<EditText>(R.id.etListName)
        val etListDis = createNewProductListBottomSheet.findViewById<EditText>(R.id.etListDis)
        val llUpdate = createNewProductListBottomSheet.findViewById<LinearLayout>(R.id.llUpdate)
        val ivClose = createNewProductListBottomSheet.findViewById<ImageView>(R.id.ivClose)
        createNewProductListBottomSheet.behavior.state = BottomSheetBehavior.STATE_EXPANDED

        llUpdate?.setOnClickListener {
            if(etListName!!.text.isNotBlank()){
                var requireModel = ListCollectionModel(
                    listName = etListName.text.toString(),
                    description = etListDis?.text.toString()
                )
                FireStoreHelper().addListCollection(
                    auth.currentUser!!.uid,
                    requireModel,
                    object : FireStoreAddListInterface {
                        override fun onSuccess(msg: String) {

                            Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
                            filterTitleList.add(
                                FilterProductModel(
                                    title = requireModel.listName,
                                    listId = requireModel.listId
                                )
                            )

                            productFilterAdapter.notifyItemChanged(
                                filterTitleList.indexOf(
                                    FilterProductModel(title = requireModel.listName, listId = requireModel.listId)
                                )
                            )

                            createNewProductListBottomSheet.dismiss()
                        }
                        override fun onError(error: String) {
                            Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
                        }
                    }
                )
            }else{
                Toast.makeText(createNewProductListBottomSheet.context, "Please Enter Create List Name..", Toast.LENGTH_SHORT).show()
            }
        }

        ivClose?.setOnClickListener {
            createNewProductListBottomSheet.dismiss()
        }

        createNewProductListBottomSheet.show()
    }

    private fun getList(){
        FireStoreHelper().getListProductFilter(
            auth.currentUser!!.uid,
            object: FireStoreGetFilterListListener {
                override fun onSuccess(productFilterList: QuerySnapshot) {

                    filterTitleList.clear()
                    setStaticFilterData()

                    for (i in productFilterList) {
                        val jsonData = convertToJson(i.data)
                        val gsn = Gson()
                        val requireModel: ListCollectionModel = gsn.fromJson(
                            jsonData.toString(),
                            ListCollectionModel::class.java
                        )

                        filterTitleList.add(
                            FilterProductModel(
                                title = requireModel.listName,
                                listId = requireModel.listId
                            )
                        )
                    }
                    if(!filterTitleList.isNullOrEmpty()){
                        productFilterAdapter.itemSelectedPosition = 0
                    }

                    productFilterAdapter.notifyDataSetChanged()
                }

                override fun onError(error: String) {
                    binding.llProgressbar.visibility = GONE
                    Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    private fun getPurchasedProducts(){
        binding.llProgressbar.visibility = VISIBLE
        purchasedProductList.clear()

        FireStoreHelper().getPurchasedProduct(
            auth.currentUser!!.uid,
            object: FireStoreGetPurchasedProductInterface {
                override fun onSuccess(purchasedProductListData: QuerySnapshot) {

                    for (i in purchasedProductListData) {
                        val jsonData = convertToJson(i.data)
                        val gsn = Gson()
                        val requireModel: FavoritePurchasedCustomListModel = gsn.fromJson(
                            jsonData.toString(),
                            FavoritePurchasedCustomListModel::class.java
                        )

                        val getPurchasedModel = productList.filter {
                            it.id_product == requireModel.id_product!!
                        }

                        Log.d(TAG, "purchased current product list 1 :- $getPurchasedModel")

                        if(!getPurchasedModel.isNullOrEmpty()) {
                            this@ProductsFragment.purchasedProductList.add(getPurchasedModel[0])
                        }
                    }

                    for (model in favoriteProductList){
                        if(model in purchasedProductList){
                            purchasedProductList[purchasedProductList.indexOf(model)].isFavorite = true
                        }
                    }


                    purchasedProductAdapter.notifyDataSetChanged()
                    binding.llProgressbar.visibility = GONE

                    if(!this@ProductsFragment.purchasedProductList.isNullOrEmpty()){
                        binding.llNoProducts.visibility = GONE
                        binding.rvPurchasedProducts.visibility = VISIBLE
                    }else{
                        binding.llNoProducts.visibility = VISIBLE
                        binding.rvPurchasedProducts.visibility = GONE
                    }

                }

                override fun onError(error: String) {
                    binding.llProgressbar.visibility = GONE
                    Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()

                    if(purchasedProductList.isEmpty()){
                        binding.llNoProducts.visibility = VISIBLE
                        binding.rvPurchasedProducts.visibility = GONE
                    }else{
                        binding.llNoProducts.visibility = GONE
                        binding.rvPurchasedProducts.visibility = VISIBLE
                    }
                }
            }
        )
    }

    private fun getListByName(listID: String) {
        binding.llProgressbar.visibility = VISIBLE
        filterProductsList.clear()

        FireStoreHelper().getListByName(
            auth.currentUser!!.uid,
            listID,
            object: FireStoreGetFilterProductsListListener {
                override fun onSuccess(productFilterList: DocumentSnapshot) {

                        val jsonData = convertToJson(productFilterList.data)
                        val gsn = Gson()
                        val requireModel: ListCollectionModel = gsn.fromJson(
                            jsonData.toString(),
                            ListCollectionModel::class.java
                        )

                    for(index in requireModel.productsList.indices){
                        val getProductModel = productList.filter {
                            it.id_product == requireModel.productsList[index].id_product
                        }
                        filterProductsList.addAll(getProductModel)
                    }

                    productListNameAdapter.notifyDataSetChanged()
                    binding.llProgressbar.visibility = GONE

                    if(!this@ProductsFragment.filterProductsList.isNullOrEmpty()){
                        binding.llNoProducts.visibility = GONE
                        binding.rvListProducts.visibility = VISIBLE
                    }else{
                        binding.llNoProducts.visibility = VISIBLE
                        binding.rvListProducts.visibility = GONE
                    }
                }

                override fun onError(error: String) {
                    binding.llProgressbar.visibility = GONE
                    Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    override fun purchasedProductOnClick(productData: ProductModel) {
        val bundle = Bundle()
        bundle.putSerializable(KEY_PRODUCT_MODEL, productData)
        browserActivity?.setFragment(ProductDetailsFragment(), "ProductDetailsFragment", bundle)
    }

    override fun purchasedFavoriteIconClick(position: Int, productData: ProductModel) {
        if(productData.isFavorite!!){
            val requireModel = FavoritePurchasedCustomListModel(
                id_product = productData.id_product,
                url = productData.url,
                url_hash = productData.hash_url
            )
            addFavoriteProduct(position, requireModel, productData)
        }else {
            removeFavoriteProduct(position, productData, false)
        }
    }

    override fun listNameProductOnClick(productDetails: ProductModel) {
        val bundle = Bundle()
        bundle.putSerializable(KEY_PRODUCT_MODEL, productDetails)
        browserActivity?.setFragment(ProductDetailsFragment(), "ProductDetailsFragment", bundle)
    }

    override fun listNameProductFavoriteOnClick(position: Int, productModel: ProductModel) {
        if(productModel.isFavorite!!){
            val requireModel = FavoritePurchasedCustomListModel(
                id_product = productModel.id_product,
                url = productModel.url,
                url_hash = productModel.hash_url
            )
            addFavoriteProduct(position, requireModel, productModel)
        }else {
            removeFavoriteProduct(position, productModel, false)
        }
    }
}
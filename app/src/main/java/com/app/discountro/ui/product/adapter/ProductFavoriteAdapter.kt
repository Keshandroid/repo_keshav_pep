package com.app.discountro.ui.product.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.R
import com.app.discountro.databinding.ItemProductsBinding
import com.app.discountro.ui.product.model.ProductModel
import com.bumptech.glide.Glide

class ProductFavoriteAdapter(val context: Context, private val favoriteProductList: ArrayList<ProductModel>, val listener: FavoriteProductListener) : RecyclerView.Adapter<ProductFavoriteAdapter.ViewHolder>() {

    class ViewHolder(val binding: ItemProductsBinding):RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemProductsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        /**
         * TODO :- When uncomment from the using the product collection data and comment the allProduct collection data.
         * */
        holder.binding.tvProductName.text = favoriteProductList[position].product_name
        holder.binding.tvProductPrice.text = "${favoriteProductList[position].currency} ${favoriteProductList[position].max_price}"

        //holder.binding.ivProductImg.setImageDrawable(context.getDrawable(R.drawable.ic_no_product_image))
        if(!favoriteProductList.get(position).product_image.isNullOrEmpty()){
            Glide.with(context).load(favoriteProductList[position].product_image).into(holder.binding.ivProductImg)
        }else{
            holder.binding.ivProductImg.setImageDrawable(context.getDrawable(R.drawable.ic_no_product_image))
        }

        holder.binding.ivFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_favorite_fill))

        if(!favoriteProductList[position].seller_list.isNullOrEmpty()){
            Glide.with(context).load(favoriteProductList[position].seller_list!![0]!!.store_logo).into(holder.binding.ivStoreLogo)
        }

        
        /**
         * TODO :- When using the get from allProduct collection then used it and using Product collection then comment this and unComment the above code.
         * */
        /*holder.binding.tvProductName.text = favoriteProductList[position].name
        holder.binding.tvProductDesc.text = favoriteProductList[position].description
        holder.binding.tvProductPrice.text = "${favoriteProductList[position].price}"

        Glide.with(context).load(favoriteProductList[position].image!![0]).placeholder(ContextCompat.getDrawable(context, R.drawable.ic_no_product_image)).into(holder.binding.ivProductImg)

//        holder.binding.ivProductImg.setImageDrawable(context.getDrawable(R.drawable.ic_no_product_image))

        holder.binding.ivFavorite.setImageDrawable(context.getDrawable(R.drawable.ic_favorite_fill))
        holder.binding.ivStoreLogo.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_no_product_image))*/


        holder.binding.root.setOnClickListener {
            listener.favoriteProductOnClick(favoriteProductList[position])
        }

        holder.binding.ivFavorite.setOnClickListener {
            listener.favoriteProductIconClick(position, favoriteProductList[position])
        }
    }

    override fun getItemCount(): Int {
        return  favoriteProductList.size
    }

    interface FavoriteProductListener{
        fun favoriteProductOnClick(productData: ProductModel)
        fun favoriteProductIconClick(position: Int, productData: ProductModel)
    }
}
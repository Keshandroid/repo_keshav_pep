package com.app.discountro.ui.splash.model

class SplashModel(
    var id_splash: Any? = "",
    var title: String = "",
    var image_url: String = "",
    var video_url: String = "",
    var description: String = "",
    var lang_iso: String = "",
    var order: Int = 0,
    var splash_type: Int = 0
) {
    fun SplashModel() {
    }
}

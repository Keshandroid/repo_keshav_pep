package com.app.discountro.ui.home.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.R
import com.app.discountro.ui.home.view.models.CountryCouponsModel

class CountriesCouponAdapter(
    private val activity: Context,
    private val listCountry: ArrayList<CountryCouponsModel>,
    private val callBackRecInt: CallBackRecInt
) : RecyclerView.Adapter<CountriesCouponAdapter.MyViewHolder>() {

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvCountry: TextView = view.findViewById(R.id.tvCountry)
        val cvParentCountry: CardView = view.findViewById(R.id.cvParentCountry)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(activity)
                .inflate(R.layout.item_coupons_country_column, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tvCountry.text = listCountry[position].countryName
        if (listCountry[position].isSelectedCountry) {
            holder.cvParentCountry.setCardBackgroundColor(
                ContextCompat.getColor(
                    activity,
                    R.color.selected_red
                )
            )
            holder.tvCountry.setTextColor(ContextCompat.getColor(activity, R.color.white))
        } else {
            holder.cvParentCountry.setCardBackgroundColor(
                ContextCompat.getColor(
                    activity,
                    R.color.white
                )
            )
            holder.tvCountry.setTextColor(ContextCompat.getColor(activity, R.color.black))
        }
        holder.cvParentCountry.setOnClickListener {
            for (i in 0 until listCountry.size) {
                listCountry[i].isSelectedCountry = false
            }
            listCountry[position].isSelectedCountry = true
            callBackRecInt.onItemSelected(it, position)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return listCountry.size
    }
}

interface CallBackRecInt {
    fun onItemSelected(view: View, position: Int)
}
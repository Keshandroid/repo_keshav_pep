package com.app.discountro.ui.firebase_dummy.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.ItemDummyUsersBinding
import com.app.discountro.ui.firebase_dummy.model.DummyUserModel

class AllUsersListAdapter(val usersList : ArrayList<DummyUserModel>, val listener : UpdateUserData) : RecyclerView.Adapter<AllUsersListAdapter.ViewHolder>() {
    
    class ViewHolder(val binding: ItemDummyUsersBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemDummyUsersBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.txtId.text = usersList[position].id
        holder.binding.txtUserName.text = usersList[position].user_name
        holder.binding.txtEmail.text = usersList[position].email
        holder.binding.txtPass.text = usersList[position].password
        holder.binding.ivUpdate.setOnClickListener {
            listener.onItemClick(usersList[position])
        }
        holder.binding.ivDelete.setOnClickListener {

        }
    }

    override fun getItemCount(): Int {
        return usersList.size
    }

    interface UpdateUserData{
        fun onItemClick(data : DummyUserModel)
    }
}
package com.app.discountro.ui.home.view.interfaces

import android.view.View

interface CompareProductInterface {
    fun onItemClicked( view: View?, directLink: String)
}
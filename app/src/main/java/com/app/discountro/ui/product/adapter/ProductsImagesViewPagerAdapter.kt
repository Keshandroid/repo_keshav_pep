package com.app.discountro.ui.product.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.PagerAdapter
import com.app.discountro.R
import com.bumptech.glide.Glide


class ProductsImagesViewPagerAdapter(
    var context: Context,
    var productsImagesArrayList: ArrayList<String>
) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layoutScreen: View = inflater.inflate(R.layout.product_images_items, null)

        val imgSlide: ImageView = layoutScreen.findViewById(R.id.ivProductsImages)

        Glide.with(context).load(productsImagesArrayList[position]).placeholder(ContextCompat.getDrawable(context, R.drawable.ic_no_product_image)).into(imgSlide)


        container.addView(layoutScreen)
        return layoutScreen
    }

    override fun getCount(): Int {
        return productsImagesArrayList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}
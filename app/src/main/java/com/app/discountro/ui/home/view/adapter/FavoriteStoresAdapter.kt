package com.app.discountro.ui.home.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.R
import com.app.discountro.ui.home.view.models.FavoriteStore
import com.bumptech.glide.Glide

class FavoriteStoresAdapter(
    var context: Context,
    var favoriteStores: List<FavoriteStore>,
    var listener: StoresCouponsListener
    ) :RecyclerView.Adapter<FavoriteStoresAdapter.MyViewHold>() {

    inner class MyViewHold(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val rlMain = itemView.findViewById<RelativeLayout>(R.id.rlMain)
        val imgFavStore = itemView.findViewById<ImageView>(R.id.imgFavStore)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHold {
        return MyViewHold(
            LayoutInflater.from(context).inflate(R.layout.item_horizontal_products, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHold, position: Int) {
        val data : FavoriteStore =  favoriteStores.get(holder.adapterPosition)

        Glide.with(context).load(data.logoUrl).into(holder.imgFavStore)

        holder.rlMain.setOnClickListener {
            listener.favoriteStoreClick(favoriteStores[position])
        }


    }


    override fun getItemCount(): Int {
        return favoriteStores.size
    }

    interface StoresCouponsListener{
        fun favoriteStoreClick(data : FavoriteStore)
    }
}
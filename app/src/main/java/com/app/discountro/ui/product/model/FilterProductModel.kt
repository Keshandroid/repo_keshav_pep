package com.app.discountro.ui.product.model

data class FilterProductModel(
    var title: String? = null,
    var icon: Int? = null,
    var backgroundColor: Int? = null,
    var textColor: Int? = null,
    var isSelectedList: Boolean = false,
    var listId: String? = null,
)

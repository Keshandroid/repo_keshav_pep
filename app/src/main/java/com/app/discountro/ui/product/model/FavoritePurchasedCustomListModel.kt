package com.app.discountro.ui.product.model

data class FavoritePurchasedCustomListModel(
    var domain: String? = null,
    var url: String? = null,
    var id_product: String? = null,
    var url_hash: String? = null,
    var notification_min_discount: Int? = null,
    var mute_notification: Boolean? = false,
)

package com.app.discountro.ui.firebase_dummy.model

import java.io.Serializable

data class DummyUserModel(
    var email:String?,
    var id: String? = null,
    var password:String?,
    var user_name:String?,
)

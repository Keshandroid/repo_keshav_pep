package com.app.discountro.ui.notification.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.ItemNotificationsBinding
import com.app.discountro.ui.notification.model.NotificationModel
import com.app.discountro.utils.Utils
import com.bumptech.glide.Glide
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AppNotificationAdapter(val context: Context, val notificationList: ArrayList<NotificationModel>, val appNotificationCallback: AppNotificationCallback) : RecyclerView.Adapter<AppNotificationAdapter.ViewHolder>() {

    class ViewHolder(val binding: ItemNotificationsBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemNotificationsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())

        /*val simpleDateFormat = SimpleDateFormat("yyyy-MMM-dd", Locale.getDefault())
        val formattedDateTime: String = simpleDateFormat.format(notificationList[position].date_add?.toDate())*/

        val notificationDate = notificationList[position].date_add?.toDate()?.let {
            Utils.convertUtcToLocalTime(notificationList[position].date_add?.toDate()!!)
        }


        Glide.with(context).load(notificationList[position].image_url).into(holder.binding.ivNotification)
        Glide.with(context).load(notificationList[position].image_url_secondary).into(holder.binding.ivSecondaryImg)
        holder.binding.txtTitle.text = notificationList[position].title
        holder.binding.txtDescription.text = notificationList[position].description
        holder.binding.txtPrice.text = notificationList[position].subtitle
        holder.binding.txtDate.text = notificationDate
        holder.binding.root.setOnClickListener {
            notificationList[position].redirect_link?.let {
                appNotificationCallback.itemNotificationClick(notificationList[position].redirect_link!!)
            }
        }
    }

    override fun getItemCount(): Int {
        return notificationList.size
    }

    interface AppNotificationCallback{
        fun itemNotificationClick(redirectLink: String)
    }
}
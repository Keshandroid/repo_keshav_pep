package com.app.discountro.ui.account.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.discountro.databinding.MWalletFragmentBinding
import com.app.discountro.ui.account.adapter.WalletTransactionsAdapter
import com.app.discountro.ui.home.view.BrowserActivity

class WalletFragment : Fragment(), WalletTransactionsAdapter.WalletTransactionsListener {
    private lateinit var binding: MWalletFragmentBinding
    private var browserActivity: BrowserActivity? = null
    private lateinit var walletTransactionsAdapter : WalletTransactionsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = MWalletFragmentBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        browserActivity = activity as BrowserActivity?
        browserActivity?.appbarTitle?.text = "Wallet"


        initClick()
        setAdapter()
    }

    private fun initClick() {
        binding.txtSeeAll.setOnClickListener {
            browserActivity?.setFragment(AllTransactionsFragment(), "AllTransactionsFragment", Bundle())
        }
    }

    private fun setAdapter() {
        binding.rvTransactions.layoutManager = LinearLayoutManager(requireContext())
        walletTransactionsAdapter = WalletTransactionsAdapter(requireContext(), arrayListOf(), this)
        binding.rvTransactions.adapter = walletTransactionsAdapter

    }

    override fun transactionOnClick(data: String) {
        browserActivity?.setFragment(TransactionDetailsFragment(), "TransactionDetailsFragment", Bundle())
    }
}
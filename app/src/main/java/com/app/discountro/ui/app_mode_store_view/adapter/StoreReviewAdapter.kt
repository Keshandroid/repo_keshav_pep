package com.app.discountro.ui.app_mode_store_view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.ItemviewStoreReviewsBinding

class StoreReviewAdapter(val context : Context, val reviewList : ArrayList<String>) : RecyclerView.Adapter<StoreReviewAdapter.ViewHolder>() {

    class ViewHolder(val binding: ItemviewStoreReviewsBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemviewStoreReviewsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.root.setOnClickListener { }
    }

    override fun getItemCount(): Int {
        return 10
    }
}
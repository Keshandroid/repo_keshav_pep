package com.app.discountro.ui.splash.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.app.discountro.R
import com.app.discountro.ui.splash.model.LanguageModel
import com.blongho.country_data.World
import com.bumptech.glide.Glide

class LanguageDropDwnAdapter(
    var context: Context,
    private var languageModel: MutableList<LanguageModel>
) : BaseAdapter() {

    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return languageModel.size
    }

    override fun getItem(p0: Int): Any {
        return languageModel[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }


    override fun getView(position: Int, p1: View?, p2: ViewGroup?): View {
        val myView: View
        val viewHolder: ItemHolder
        if (p1 == null) {
            myView = inflater.inflate(R.layout.item_language_select, p2, false)
            viewHolder = ItemHolder(myView)
            myView?.tag = viewHolder
        } else {
            myView = p1
            viewHolder = myView.tag as ItemHolder
        }
        World.init(context)

        if (languageModel[position].lang_iso.isNotEmpty()) {
            if (languageModel[position].lang_iso == "en") {
                Glide.with(context).load(R.drawable.us).fitCenter().into(viewHolder.ivCountryFlag)
            } else {
                val flagImage = World.getFlagOf(languageModel[position].lang_iso)
                Glide.with(context).load(flagImage).fitCenter().into(viewHolder.ivCountryFlag)
            }
        } else {
            Glide.with(context).load(R.drawable.ic_earth).into(viewHolder.ivCountryFlag)
        }

        viewHolder.tvLanguageName.text = languageModel[position].lang_name

        return myView
    }

    private class ItemHolder(row: View?) {
        var ivCountryFlag: ImageView
        var tvLanguageName: TextView

        init {
            ivCountryFlag = row!!.findViewById(R.id.ivCountryFlag)
            tvLanguageName = row.findViewById(R.id.tvLanguageName)
        }


    }


}
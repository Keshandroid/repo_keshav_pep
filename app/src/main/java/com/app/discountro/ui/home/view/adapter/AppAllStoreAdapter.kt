package com.app.discountro.ui.home.view.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.R
import com.app.discountro.ui.home.view.models.AllStoreModel
import com.bumptech.glide.Glide
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView

class AppAllStoreAdapter(
    var context: Context,
    var allStoreList: MutableList<AllStoreModel>,
    private val selectedStoresInterface: SelectedStoresInterface

) : RecyclerView.Adapter<AppAllStoreAdapter.MyViewHold>(), FastScrollRecyclerView.SectionedAdapter {

    inner class MyViewHold(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val rlChecked = itemView.findViewById<RelativeLayout>(R.id.rlChecked)
        val txtTitle = itemView.findViewById<TextView>(R.id.txtTitle)
        val txtCashBack = itemView.findViewById<TextView>(R.id.txtCashBack)
        val chkBox = itemView.findViewById<CheckBox>(R.id.chkBox)
        val imageUrl = itemView.findViewById<ImageView>(R.id.imageUrl)

    }

    interface SelectedStoresInterface {
        fun onCheckBoxClicked()

    }

    @SuppressLint("DefaultLocale")
    @NonNull
    override fun getSectionName(position: Int): String {



        return String.format("%s", allStoreList.get(position).storeName.toString().substring(0,1))

//        return String.format("%d", position + 1) // original
    }


    fun filterList(filteredList: MutableList<AllStoreModel>) {
        this.allStoreList = filteredList
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHold {
        return MyViewHold(
            LayoutInflater.from(context).inflate(R.layout.item_all_stores, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHold, position: Int) {
        val data : AllStoreModel =  allStoreList.get(holder.adapterPosition)

        holder.txtTitle.text = data.storeName
        if(data.cashBack.isNullOrBlank() || data.cashBack == "null"){
            holder.txtCashBack.visibility = INVISIBLE
        }else{
            holder.txtCashBack.visibility = VISIBLE
        }
        holder.txtCashBack.text = data.cashBack
        Glide.with(context).load(data.logoUrl).into(holder.imageUrl)


        if(data.checked == true){
            holder.chkBox.isChecked = true
        }else{
            holder.chkBox.isChecked = false
        }

        holder.rlChecked.setOnClickListener {
            holder.chkBox.isChecked = !holder.chkBox.isChecked
        }

        holder.chkBox.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                if(p1){
                    allStoreList.get(holder.adapterPosition).checked = true
                    //notifyItemChanged(holder.adapterPosition)
                    selectedStoresInterface.onCheckBoxClicked()

//                    Log.d("CHECK_BOX",""+holder.adapterPosition + "  -  " +  p1 +
//                            allStoreList.get(holder.adapterPosition).storeName)
                }else{

                    allStoreList.get(holder.adapterPosition).checked = false
                    //notifyItemChanged(holder.adapterPosition)

                    selectedStoresInterface.onCheckBoxClicked()


//                    Log.d("CHECK_BOX",""+holder.adapterPosition + "  -  " +  p1 +
//                            allStoreList.get(holder.adapterPosition).storeName)
                }
            }

        })

    }



    override fun getItemCount(): Int {
        return allStoreList.size
    }
}
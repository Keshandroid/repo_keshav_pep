package com.app.discountro.ui.splash.view

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.app.discountro.R
import com.app.discountro.base.BaseViewModelFactory
import com.app.discountro.common.constant.AppConstant
import com.app.discountro.databinding.ActivitySplashIntroBinding
import com.app.discountro.ui.splash.model.SplashIntroItemModel
import com.app.discountro.ui.splash.model.SplashModel
import com.app.discountro.ui.splash.view.adapter.SplashIntroViewPagerAdapter
import com.app.discountro.ui.splash.viewModel.LanguageViewModel
import com.app.discountro.ui.welcome.view.WelcomeActivity
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.SharedPrefsUtils
import com.google.android.material.tabs.TabLayout
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase


class SplashIntroActivity : AppCompatActivity() {

    lateinit var binding: ActivitySplashIntroBinding
    private val splashIntroItemModel: ArrayList<SplashIntroItemModel> = arrayListOf()

    //    private val myTranslationVarModel: ArrayList<MyTranslationVarModel> = arrayListOf()
    private val splashModelList: ArrayList<SplashModel> = arrayListOf()
    private lateinit var languageViewModel: LanguageViewModel
    lateinit var langISOValue: String
    lateinit var splashIntroViewPagerAdapter: SplashIntroViewPagerAdapter
    private val appConstant: AppConstant = AppConstant()
    private var MAX_LAYOUTS: Int = 0
    private var isLastPageSwiped = false
    private var counterPageScroll = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        binding = ActivitySplashIntroBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initView()
    }

    private fun initView() {
        langISOValue = SharedPrefsUtils.getStringPreference(KeysUtils.keyLangISO).ifEmpty {
            intent.getStringExtra(KeysUtils.keyLangISO).toString()
        }

        languageViewModel =
            ViewModelProvider(this, BaseViewModelFactory { LanguageViewModel() }).get(
                LanguageViewModel::class.java
            )
        languageViewModel.getFireSplashData(langISOValue)
        // getFireLanguageData(langISOValue)
        setDynamicIntroSplash()
    }

    private fun setDynamicIntroSplash() {
        binding.progressBar.visibility = View.VISIBLE
        languageViewModel.splashModel.observe(this) {
            splashModelList.add(it)
            splashIntroItemModel.add(SplashIntroItemModel(it.image_url, it.title, it.description))
            Log.e("GetImageUrlDetai", "setDynamicIntroSplash: ${it.image_url}")
        }
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            loadMySplashData()
        }, 1500)
    }

    private fun loadMySplashData() {
        MAX_LAYOUTS = splashIntroItemModel.size
        Log.e("MAX_LAYOUTS", "setDynamicIntroSplash: $MAX_LAYOUTS")
        try {
            splashIntroViewPagerAdapter =
                SplashIntroViewPagerAdapter(this@SplashIntroActivity, splashIntroItemModel)
            binding.screenViewpager.adapter = splashIntroViewPagerAdapter

            Log.e(
                "SunnyPageScroll",
                "setDynamicIntroSplash: ${binding.screenViewpager.scrollIndicators}"
            )

            binding.tabIndicator.setupWithViewPager(binding.screenViewpager)

            binding.btnNext.setOnClickListener {
                binding.screenViewpager.setCurrentItem(
                    binding.screenViewpager.currentItem + 1,
                    true
                )
            }
            binding.screenViewpager.addOnPageChangeListener(object : OnPageChangeListener {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                    if (position == MAX_LAYOUTS - 1 && positionOffset.toInt() == 0 && !isLastPageSwiped) {
                        if (counterPageScroll != 0) {
                            isLastPageSwiped = true
                            val intent =
                                Intent(this@SplashIntroActivity, WelcomeActivity::class.java)
                            startActivity(intent)
                            finish()
                            overridePendingTransition(R.anim.left_to_right, R.anim.fadein)
                        }
                        counterPageScroll++
                    } else {
                        counterPageScroll = 0
                    }
                }

                override fun onPageSelected(position: Int) {
                }

                override fun onPageScrollStateChanged(state: Int) {
                }
            })

            binding.tabIndicator.addOnTabSelectedListener(object :
                TabLayout.OnTabSelectedListener {
                override fun onTabReselected(p0: TabLayout.Tab?) {}

                override fun onTabUnselected(p0: TabLayout.Tab?) {}

                override fun onTabSelected(p0: TabLayout.Tab?) {
                    if (p0?.position == splashIntroItemModel.size - 1) {
                        loadLastScreen()
                    }
                }
            })
            binding.progressBar.visibility = View.GONE
        } catch (e: Exception) {
            binding.progressBar.visibility = View.GONE
            Log.e("SunnySplashIntroAct", "setDynamicIntroSplash: ", e)
        }

    }

    private fun getFireLanguageData(langISO: String) {
        binding.progressBar.visibility = View.VISIBLE

        Firebase.firestore.collection(KeysUtils.KEYFIRELANGUAGE_Collection).document(langISO)
            .collection(KeysUtils.KEYFIRESPLASH_Collection).get().addOnSuccessListener {
                splashModelList.clear()
                splashIntroItemModel.clear()
                for (doc in it) {
                    splashModelList.add(doc.toObject())
                    splashIntroItemModel.add(
                        SplashIntroItemModel(
                            doc.data["image_url"].toString(),
                            doc.data["title"].toString(),
                            doc.data["description"].toString()
                        )
                    )
                }
                loadMySplashData()
            }.addOnFailureListener {
                Log.e("errGetFireSplashData", "getFireSplashData: ", it)
            }
        binding.progressBar.visibility = View.GONE
    }

    fun loadLastScreen() {
        binding.btnNext.setOnClickListener {
            val mainActivity = Intent(applicationContext, WelcomeActivity::class.java)
            startActivity(mainActivity)
            finish()
            overridePendingTransition(R.anim.left_to_right, R.anim.fadein)
        }
    }
}
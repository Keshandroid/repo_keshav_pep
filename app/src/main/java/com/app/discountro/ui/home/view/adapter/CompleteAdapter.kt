package com.app.discountro.ui.home.view.adapter

import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import com.app.discountro.common.sqlite.record.Record


class CompleteAdapter(
    recordList: List<Record>
) : BaseAdapter(), Filterable {
    private inner class CompleteFilter : Filter() {
        override fun performFiltering(prefix: CharSequence): FilterResults {
            resultList.clear()
            for (item in originalList) {
                if (item.title!!.contains(prefix) || item.uRL!!.contains(prefix)) {
                    if (item.title.contains(prefix)) {
                        item.index = item.title.indexOf(prefix.toString())
                    } else if (item.uRL!!.contains(prefix)) {
                        item.index = item.uRL.indexOf(prefix.toString())
                    }
                    resultList.add(item)
                }
            }
            resultList.sortWith { first, second ->
                first.index.compareTo(second.index)
            }
            val results = FilterResults()
            results.values = resultList
            results.count = resultList.size
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults) {
            notifyDataSetChanged()
        }
    }

    private inner class CompleteItem(val title: String?, val uRL: String?) {
        var index = Int.MAX_VALUE
        override fun equals(other: Any?): Boolean {
            if (other !is CompleteItem) {
                return false
            }
            return other.title == title && other.uRL == uRL
        }

        override fun hashCode(): Int {
            return if (title == null || uRL == null) {
                0
            } else title.hashCode() and uRL.hashCode()
        }
    }

    private class Holder {
        var titleView: TextView? = null
        var urlView: TextView? = null
    }

    private val originalList: MutableList<CompleteItem>
    private val resultList: MutableList<CompleteItem>
    private val filter = CompleteFilter()
    private fun dedup(recordList: List<Record>) {
        for (record in recordList) {
            if (record.title != null && record.title!!.isNotEmpty()
                && record.uRL != null && record.uRL!!.isNotEmpty()
            ) {
                originalList.add(CompleteItem(record.title, record.uRL))
            }
        }
        val set: Set<CompleteItem> = HashSet(originalList)
        originalList.clear()
        originalList.addAll(set)
    }

    override fun getCount(): Int {
        return resultList.size
    }

    override fun getFilter(): Filter {
        return filter
    }

    override fun getItem(position: Int): Any {
        return resultList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View? {
         val holder: Holder = convertView.tag as Holder
        val item = resultList[position]
        holder.titleView!!.text = item.title
        holder.urlView!!.text = item.uRL
        return convertView
    }


//    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
//        var view = convertView
//        val holder: Holder
//        if (view == null) {
//            holder = Holder()
////            holder.titleView =
////                view?.findViewById<TextView>(com.app.discountro.R.id.complete_item_title)
////            holder.urlView = view?.findViewById<TextView>(com.app.discountro.R.id.complete_item_url)
////            view?.setTag(holder)
//        } else {
//            holder = view.tag as Holder
//        }
//        val item: CompleteItem = resultList[position]
//        Log.e(
//            "kirtanNarola", """getView: ${item.title}
//     Link : ${item.uRL}
//     """.trimIndent()
//        )
//
//        holder.titleView?.setText(item.title)
//        holder.urlView?.setText(item.uRL)
//        return view
//    }

    init {
        originalList = ArrayList()
        resultList = ArrayList()
        dedup(recordList)
    }
}
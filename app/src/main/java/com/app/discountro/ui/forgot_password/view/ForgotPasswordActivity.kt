package com.app.discountro.ui.forgot_password.view

import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.app.discountro.R
import com.app.discountro.base.extentions.encodeToHtml
import com.app.discountro.common.constant.AppConstant
import com.app.discountro.databinding.ActivityForgotPasswordBinding
import com.app.discountro.ui.login.view.LoginActivity
import com.app.discountro.ui.splash.model.MyTranslationVarModel
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.SharedPrefsUtils
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class ForgotPasswordActivity : AppCompatActivity() {

    lateinit var binding: ActivityForgotPasswordBinding
    private lateinit var auth: FirebaseAuth
    val appConstant: AppConstant = AppConstant()

    private lateinit var myTranslationVarModel: MyTranslationVarModel
    private lateinit var myTransDataForgotActivity: MyTranslationVarModel
    private val myTranslationArrayData: ArrayList<MyTranslationVarModel> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getTranslationData()
        initView()
    }

    private fun initView() {
        setTranslationDataFromPref()
        onBackPressListener()
        onResetPwdClickListener()
    }

    private fun setTranslationDataFromPref() {
        myTransDataForgotActivity = myTranslationArrayData[0]
        Log.e(
            "MyDataValue_SunnyPatel",
            "setTranslationDataFromPref: ${myTransDataForgotActivity.welcome_to}"
        )
        binding.tvResetPwdTxt.text = myTransDataForgotActivity.reset_password


        val fireFetchDataString =
            myTransDataForgotActivity.please_enter_your_email_address_to_request_a_password_reset.encodeToHtml()
        val convertFireCDATA: String =
            Html.fromHtml(fireFetchDataString.toString(), Html.FROM_HTML_MODE_LEGACY).toString()



        binding.tvFrgPwdDescTxt.text = convertFireCDATA

        binding.etFireEmailReset.hint = myTransDataForgotActivity.enter_your_e_mail
        binding.cbFireResetPwd.hint = myTransDataForgotActivity.send_new_password

    }

    private fun onResetPwdClickListener() {

        binding.cbFireResetPwd.setOnClickListener {
            if (binding.etFireEmailReset.text.isNotEmpty()) {
                firePwdResetFunction()
            } else {
                Toast.makeText(this, myTransDataForgotActivity.fill_creadential.ifEmpty {
                    "Please fill up the Credentials"
                }, Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun getTranslationData() {
        Log.e(
            "MySharePrefDataValue", "getTranslationData: ${
                SharedPrefsUtils.getModelPreferences(
                    KeysUtils.KeyModelTranslation,
                    MyTranslationVarModel::class.java
                )
            }"
        )
        myTranslationVarModel = SharedPrefsUtils.getModelPreferences(
            KeysUtils.KeyModelTranslation,
            MyTranslationVarModel::class.java
        ) as MyTranslationVarModel
        myTranslationArrayData.add(myTranslationVarModel)
        myTranslationArrayData[0]
    }

    private fun firePwdResetFunction() {
        auth = Firebase.auth
        auth.setLanguageCode(SharedPrefsUtils.getStringPreference(KeysUtils.keyLangISO))
        appConstant.showDiscoRoProgress(this@ForgotPasswordActivity)
        auth.sendPasswordResetEmail(binding.etFireEmailReset.text.toString())
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    Toast.makeText(
                        this,
                        myTransDataForgotActivity.email_success_sent_to.ifEmpty {
                            resources.getString(R.string.email_success_sent_to)
                        } + " " + binding.etFireEmailReset.text,
                        Toast.LENGTH_LONG
                    ).show()
                    val intent = Intent(this@ForgotPasswordActivity, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }
                Log.e("MyPwdResetResult", "firePwdResetFunction: ${it.isSuccessful}")
                Log.e("MyPwdResetResult", "firePwdResetFunction: $it")
                appConstant.dismissDiscRoProgress()
            }.addOnFailureListener {
                Toast.makeText(this@ForgotPasswordActivity, it.message, Toast.LENGTH_LONG)
                    .show()
                appConstant.dismissDiscRoProgress()
            }
    }

    private fun onBackPressListener() {

        binding.cvBack.setOnClickListener {
            onBackPressed()
            finish()
        }
    }
}
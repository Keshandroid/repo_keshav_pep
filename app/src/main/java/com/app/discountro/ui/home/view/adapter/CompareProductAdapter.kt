package com.app.discountro.ui.home.view.adapter

import android.content.Context
import android.graphics.PointF
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.R
import com.app.discountro.fireStore.models.SellerModel
import com.app.discountro.ui.home.view.interfaces.CompareProductInterface
import com.bumptech.glide.Glide
import com.github.marlonlom.utilities.timeago.TimeAgo
import java.sql.Date
import java.sql.Timestamp
import java.text.SimpleDateFormat


class CompareProductAdapter(
    val context: Context,
    private val compareProductInterface: CompareProductInterface,
    private val dataList: ArrayList<SellerModel>
) :
    RecyclerView.Adapter<CompareProductAdapter.MyViewHolder>() {

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgWebsiteLogo = itemView.findViewById<ImageView>(R.id.imgWebsiteLogo)
        val tvUpdateTime = itemView.findViewById<TextView>(R.id.tvUpdateTime)
        val tvUsed = itemView.findViewById<TextView>(R.id.tvUsed)
        val tvPrice = itemView.findViewById<TextView>(R.id.tvPrice)
        val tvFreeShipping = itemView.findViewById<TextView>(R.id.tvFreeShipping)
        val layoutDetail = itemView.findViewById<ConstraintLayout>(R.id.layoutDetail)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(
            LayoutInflater.from(context).inflate(R.layout.compare_item_layout, parent, false)
        )
    }

    private val touchMoveFactor: Short = 10
    private val touchTimeFactor: Short = 200
    private val actionDownPoint = PointF(0f, 0f)
    private var touchDownTime = 0L

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = dataList[position]
//        holder.tvPrice.text=data.price
        Glide.with(context).load(data.store_logo).into(holder.imgWebsiteLogo)



        //NEW AD
        if(!data.timestamp_updated.isNullOrEmpty()){
            try {
                val sdf = SimpleDateFormat("MM/dd/yyyy")
                val netDate = Date(data.timestamp_updated.toLong() * 1000)

                val date = SimpleDateFormat("MM/dd/yyyy").parse(sdf.format(netDate))
                if (date != null) {
                    holder.tvUpdateTime.setText(TimeAgo.using(date.time))
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }


        holder.tvUsed.text = data.condition
        holder.tvPrice.text = data.currency + " " + data.price
        holder.tvFreeShipping.text = data.comment

        //Original
        /*holder.itemView.setOnClickListener {
            val directLink = data.direct_link
            compareProductInterface.onItemClicked(holder.itemView, directLink)
        }*/

        //new added
        holder.layoutDetail.setOnClickListener{
            val directLink = data.direct_link
            compareProductInterface.onItemClicked(holder.itemView, directLink)
        }

        //New
        /*holder.itemView.setOnTouchListener(OnTouchListener { v, event ->
            v.parent.requestDisallowInterceptTouchEvent(true)
            Log.d("ACTION_EVENT","111")
            when (event.action and MotionEvent.ACTION_MASK) {
                MotionEvent.ACTION_UP ->{
                    Log.d("ACTION_EVENT","222")
                    v.parent.requestDisallowInterceptTouchEvent(false)
                }
                MotionEvent.ACTION_DOWN ->{
                    Log.d("ACTION_EVENT","333")
                }
            }


            false
        })*/





    }

    override fun getItemCount(): Int {
        return dataList.size
    }

}
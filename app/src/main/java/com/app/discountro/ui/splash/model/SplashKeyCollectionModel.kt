package com.app.discountro.ui.splash.model

data class SplashKeyCollectionModel(val splashCollectionKeys: String)
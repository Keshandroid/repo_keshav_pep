package com.app.discountro.ui.home.view.adapter
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.R
import com.app.discountro.databinding.StoreItemLayoutBinding
class StoreProductAdapter(
    var context: Context
    ) :RecyclerView.Adapter<StoreProductAdapter.MyViewHold>() {

    inner class MyViewHold(itemView: View) : RecyclerView.ViewHolder(itemView) {

        //val imgWebsiteLogo = itemView.findViewById<ImageView>(R.id.imgWebsiteLogo)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHold {
        return MyViewHold(
            LayoutInflater.from(context).inflate(R.layout.store_item_layout, parent, false)
        )
    }

    override fun onBindViewHolder(holder: MyViewHold, position: Int) {
        //val data = dataList[position]
    }


    override fun getItemCount(): Int {
        return 5
    }
}
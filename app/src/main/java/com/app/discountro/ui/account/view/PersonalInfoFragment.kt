package com.app.discountro.ui.account.view

import android.app.DatePickerDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.app.discountro.Interface.FireStoreUpdateUserDataInterface
import com.app.discountro.Interface.FireStoreUserDataInterface
import com.app.discountro.R
import com.app.discountro.databinding.MPersonalInfoFragmentBinding
import com.app.discountro.fireStore.helper.FireStoreHelper
import com.app.discountro.fireStore.models.UserDataModel
import com.app.discountro.ui.home.view.BrowserActivity
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.SharedPrefsUtils
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random

class PersonalInfoFragment : Fragment() {
    private lateinit var binding: MPersonalInfoFragmentBinding
    private var browserActivity: BrowserActivity? = null
    private lateinit var userId: String
    private var userDataModel: UserDataModel? = null
    private var TAG = "PersonalInfoFragment"
    private var genderList : ArrayList<String> = arrayListOf("Other","Male","Female")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userId = SharedPrefsUtils.getStringPreference(KeysUtils.KeyUUID)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = MPersonalInfoFragmentBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        browserActivity = activity as BrowserActivity?
        browserActivity?.appbarTitle?.text = "Personal Information"

        val genderAdapter = ArrayAdapter(requireContext(), R.layout.item_gender, genderList)
        binding.spGender.adapter = genderAdapter



        initClick()
        firebaseGetUserData()
    }

    private fun initClick() {
        binding.tvDOB.setOnClickListener {
            val today = Calendar.getInstance()
            val dpd = DatePickerDialog(requireContext(), { view, year, monthOfYear, dayOfMonth ->

                // Display Selected date in textbox
                val msg = "$dayOfMonth/${monthOfYear +1}/$year"
                binding.tvDOB.text = msg

            }, today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH))
            dpd.show()
        }

        binding.llSaveData.setOnClickListener {
            if(binding.etFirstName.text.isNotBlank() && binding.etLastName.text.isNotBlank() && binding.tvDOB.text.isNotBlank() && binding.etPhoneNo.text.isNotBlank() && userDataModel != null && binding.txtCcp.selectedCountryNameCode != null){
//                Toast.makeText(requireContext(), "saveData $userId", Toast.LENGTH_SHORT).show()
                binding.llProgressbar.visibility = VISIBLE
                userDataModel?.user_name = binding.etFirstName.text.toString() + " " + binding.etLastName.text.toString()
                userDataModel?.birth_date = binding.tvDOB.text.toString()
                userDataModel?.gender = binding.spGender.selectedItem.toString()
                userDataModel?.phone_no = binding.etPhoneNo.text.toString()
                userDataModel?.country_code = binding.txtCcp.selectedCountryNameCode
                userDataModel?.country_ph_code = binding.txtCcp.selectedCountryCode

                if(userDataModel?.user_public_token.isNullOrBlank()){
                    userDataModel?.user_public_token = generateRandomNumber().toString()
                }else{
                    userDataModel?.user_public_token = userDataModel?.user_public_token!!
                }

                Log.e(TAG, "user details $userDataModel")
                FireStoreHelper().updateUserData(
                    userDataModel!!.id_user,
                    userDataModel!!,
                    object : FireStoreUpdateUserDataInterface{
                        override fun onSuccess(msg: String) {
                            Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
                            binding.llProgressbar.visibility = GONE
//                            requireActivity().onBackPressed()
                        }
                        override fun onError(error: String) {
                            Toast.makeText(requireContext(), "Not update data", Toast.LENGTH_SHORT).show()
                            binding.llProgressbar.visibility = GONE
                        }
                    }
                )
            }else{
                Log.e(TAG, "CCP data : ${binding.txtCcp.selectedCountryCode} & ${binding.txtCcp.selectedCountryNameCode}" )
                Toast.makeText(requireContext(), "please check required field first name, last name, birth date, phone no", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun firebaseGetUserData() {
        Log.e("FireStore", "user id preference: $userId " )
        binding.llProgressbar.visibility = VISIBLE
        FireStoreHelper().getUserData(
            userId,
            object: FireStoreUserDataInterface{
                override fun onComplete(model: UserDataModel) {
                    userDataModel = model

                    if(!model.user_name.isNullOrBlank()){
                        var fullName : MutableList<String> = model.user_name.split(" ") as MutableList<String>
                        fullName.removeAll(listOf(""))


                        if(fullName.isNotEmpty() && fullName.size >= 2 ) {
                            binding.etFirstName.setText(fullName.get(0))
                            binding.etLastName.setText(fullName.get(1))
                        }
                    }

                    binding.etEmail.setText(model.email)
                    binding.etPhoneNo.setText(model.phone_no)
                    binding.tvDOB.text = model.birth_date
                    binding.txtCcp.setCountryForNameCode(model.country_code)
                    for (i in genderList.indices){
                        if(genderList[i] == model.gender){
                            binding.spGender.setSelection(i)
                            break
                        }
                    }
                    binding.llProgressbar.visibility = GONE
                }

                override fun onError(error: String) {
                    binding.llProgressbar.visibility = GONE
                    Log.e(TAG, "onError fetch user data : $error")
                    Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    private fun generateRandomNumber(): Long {
        return Random.nextLong(1000000000, 9999999999)
    }
}
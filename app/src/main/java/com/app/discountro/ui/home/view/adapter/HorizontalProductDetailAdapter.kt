package com.app.discountro.ui.home.view.adapter
import android.content.Context
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.View.*
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.ItemRowCouponsBinding
import com.app.discountro.fireStore.models.RoModel
import com.bumptech.glide.Glide

class HorizontalProductDetailAdapter(
    var context: Context,
    private var listCoupons: ArrayList<RoModel.CouponModel>,
//    private val clickListener: AppCouponsAdapter.AppCouponsClickListener,
    private var logoUrl: String?,
    val selectedStoreCouponsClickListener: SelectedStoreCouponsClickListener
    ) :RecyclerView.Adapter<HorizontalProductDetailAdapter.MyViewHold>() {

    inner class MyViewHold(val binding: ItemRowCouponsBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHold {
        return MyViewHold(ItemRowCouponsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: MyViewHold, position: Int) {
        //val data = dataList[position]
        if (logoUrl != null && logoUrl?.isNotEmpty() == true) {
            holder.binding.ivProfile.visibility = View.VISIBLE
            Glide.with(context).load(logoUrl).into(holder.binding.ivProfile)
        }else{
            holder.binding.ivProfile.visibility = View.INVISIBLE
        }

        if (listCoupons[position].isCopied!!) {
            holder.binding.rlCouponCode.visibility = VISIBLE
            holder.binding.rlCopyCoupon.visibility = GONE
        } else {
            holder.binding.rlCopyCoupon.visibility = VISIBLE
            holder.binding.rlCouponCode.visibility = GONE
        }

        holder.binding.tvTitle.text = listCoupons[position].offerUrl
        holder.binding.tvCouponDesc.text = listCoupons[position].offerName
        holder.binding.tvCouponCode.text = listCoupons[position].code

        holder.binding.rlCopyCoupon.setOnClickListener {
            selectedStoreCouponsClickListener.onCouponClick(it, position)
            hideCoverTimer(60000 * 30, position)
            listCoupons[position].apply { isCopied = true }
            notifyDataSetChanged()
        }

        holder.binding.root.setOnClickListener {
            selectedStoreCouponsClickListener.couponRootClick(listCoupons[position], position, logoUrl!!)
        }
    }


    override fun getItemCount(): Int {
        return listCoupons.size
    }

    fun setNewListAndRefresh(listCoupons: ArrayList<RoModel.CouponModel>, logoUrl: String?) {
        this.listCoupons = listCoupons
        this.logoUrl = logoUrl
        notifyDataSetChanged()
    }

    private fun hideCoverTimer(time: Long, position: Int) {
        val timer = object : CountDownTimer(time, 100) {
            override fun onTick(p0: Long) {
            }

            override fun onFinish() {
                listCoupons[position].isCopied = false
                notifyItemChanged(position)
            }
        }.start()
    }

    interface SelectedStoreCouponsClickListener{
        fun couponRootClick(data: RoModel.CouponModel, position: Int, logoUrl: String)
        fun onCouponClick(view: View, position: Int)
    }
}
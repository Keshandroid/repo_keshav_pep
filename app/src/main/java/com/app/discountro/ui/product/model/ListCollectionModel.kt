package com.app.discountro.ui.product.model

data class ListCollectionModel(
    var listName: String? = null,
    var isSelectedList: Boolean = false,
    var productsList : ArrayList<FavoritePurchasedCustomListModel> = arrayListOf(),
    var description : String? = null,
    var listId : String? = null,
)

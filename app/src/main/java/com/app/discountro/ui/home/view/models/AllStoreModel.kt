package com.app.discountro.ui.home.view.models


data class AllStoreModel(
    var storeName: String?,
    var cashBack: String?,
    var checked: Boolean?,
    var logoUrl: String?
)
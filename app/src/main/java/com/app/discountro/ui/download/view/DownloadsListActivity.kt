package com.app.discountro.ui.download.view
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.app.discountro.databinding.ActivityDownloadsListBinding
import com.app.discountro.ui.downloads.DownloadListAdapter
import com.app.discountro.utils.browser_utils.my_object.HelperUnit
import java.io.File

class DownloadsListActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var mBinding: ActivityDownloadsListBinding
    val TAG: String = javaClass.simpleName
    lateinit var mContext: Activity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityDownloadsListBinding.inflate(layoutInflater)
        HelperUnit.applyTheme(this)
        setContentView(mBinding.root)
        mContext = this

        initClickListeners()
        initDownloadsList(mContext)
    }
    private fun initClickListeners() {
        mBinding.ivBack.setOnClickListener(this)
    }
    private fun initDownloadsList(mContext: Context) {
        val path = Environment.getExternalStorageDirectory()
            .toString() + "/" + Environment.DIRECTORY_DOWNLOADS + "/$DOWNLOAD_FOLDER_NAME"
        val discountRoDir = File(path)
        if (!discountRoDir.exists()) {
            discountRoDir.mkdirs()
        }

        val files = discountRoDir.listFiles()

        if (files != null) {
            Log.e(TAG, "showDownloads: " + files.size)
            for (i in files.indices) {
                Log.e(TAG, "showDownloads:name[$i] " + files[i].name)
            }

            if (files.isEmpty()) {
                showDownList(false)
            } else {

                showDownList(true)
            }
        } else {
            showDownList(false)
        }

        val downloadsListAdapter = DownloadListAdapter(mContext, files)
        mBinding.rvDownloadsList.adapter = downloadsListAdapter

    }

    override fun onClick(view: View?) {
        when (view) {
            mBinding.ivBack -> {
                onBackPressed()
            }
        }
    }

    override fun onBackPressed() {
        finish()
    }

    private fun showDownList(show: Boolean) {
        if (show) {
            mBinding.tvNoDownloads.visibility = View.GONE
            mBinding.rvDownloadsList.visibility = View.VISIBLE
        } else {
            mBinding.tvNoDownloads.visibility = View.VISIBLE
            mBinding.rvDownloadsList.visibility = View.GONE
        }
    }

    companion object {
        const val DOWNLOAD_FOLDER_NAME = "DiscountRo"
    }

}
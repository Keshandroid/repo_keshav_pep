package com.app.discountro.ui.firebase_dummy.firebase_helper

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.app.discountro.ui.firebase_dummy.model.DummyUserModel
import com.app.discountro.utils.FireStorageKeys
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson

object FirebaseHelperDummy {
    val db = Firebase.firestore
    var userListData = MutableLiveData<ArrayList<DummyUserModel>>()
    var userList = ArrayList<DummyUserModel>()
    private val map = mutableMapOf<String,String>()


    fun updateUserData(user: DummyUserModel){
        if(user.id.isNullOrEmpty()){
            db.collection("dummy_user")
                .add(user)
                .addOnSuccessListener {
                    Log.d("firebaseData", "DocumentSnapshot update with ID: ${it}")
                }
                .addOnFailureListener { e ->
                    Log.d("firebaseData", "Error adding document ${e.message}")
                }
        }else {
            db.collection("dummy_user").document(user.id!!)
                .set(user)
                .addOnSuccessListener {
                    Log.d("firebaseData", "DocumentSnapshot update with ID: ${it}")
                }
                .addOnFailureListener { e ->
                    Log.d("firebaseData", "Error adding document ${e.message}")
                }
        }
    }

    fun readDataFirebase(){
        db.collection("dummy_user")
            .get()
            .addOnSuccessListener { result ->
                try {
                    userList.clear()
                    for (document in result) {
                        val jsonData = Gson().toJson(document.data)
                        val modelConverter = Gson().fromJson(jsonData, DummyUserModel::class.java)
                        val userModel = DummyUserModel(modelConverter.email, document.id, modelConverter.password, modelConverter.user_name)
                        userList.add(userModel)
                        Log.d("firebaseAllData", "${document.id} => ${modelConverter}")
                    }
                    userListData.postValue(userList)
                }catch (e: Exception){
                    Log.w("firebaseAllData", "Error getting documents. ${e.message}",)
                }

            }
            .addOnFailureListener { exception ->
                Log.w("firebaseAllData", "Error getting documents.", exception)
            }
    }

    fun getUserDataFirebase(documentID : String){
        db.collection(FireStorageKeys.KEY_USER_TABLE)
            .get()
            .addOnSuccessListener { result ->
                try {
                    for (document in result) {
                        /*map.put(FireStorageKeys.KEYUT_DB_REG, )
                        map.put(FireStorageKeys.KEYUT_EMAIL, )
                        map.put(FireStorageKeys.KEYUT_ID_NAME, )
                        map.put(FireStorageKeys.KEYUT_DB_REG, )*/
                        val jsonData = Gson().toJson(document.data)
                        val modelConverter = Gson().fromJson(jsonData, DummyUserModel::class.java)
                        val userModel = DummyUserModel(modelConverter.email, document.id, modelConverter.password, modelConverter.user_name)
                        userList.add(userModel)
                        Log.d("firebaseAllData", "${document.id} => ${modelConverter}")
                    }
                    userListData.postValue(userList)
                }catch (e: Exception){
                    Log.w("firebaseAllData", "Error getting documents. ${e.message}",)
                }

            }
            .addOnFailureListener { exception ->
                Log.w("firebaseAllData", "Error getting documents.", exception)
            }
    }
}
package com.app.discountro.ui.home.view.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.app.discountro.R
import com.app.discountro.common.sqlite.record.Record
import com.app.discountro.databinding.ListRecentItemBinding
import java.text.SimpleDateFormat
import java.util.*

class AdapterRecentRecord(private val _context: Context, list: MutableList<Record>) :
    ArrayAdapter<Record?>(
        _context, R.layout.list_recent_item, list as List<Record?>
    ) {
    private val list: MutableList<Record>

    private class Holder {
        var title: TextView? = null
        var time: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val view = ListRecentItemBinding.inflate(LayoutInflater.from(_context), parent, false)
        val holder = Holder()
        holder.title =  view.recordItemTitle
        holder.time = view.recordItemTime
        holder.title!!.setTextColor(Color.BLACK)
        holder.time!!.setTextColor(Color.BLACK)

        val record = list[position]
        val sdf = SimpleDateFormat("MMM dd", Locale.getDefault())
        holder.title!!.text = record.title
        holder.time!!.text = sdf.format(record.time)
        return view.root
    }

    init {
        this.list = list
    }
}
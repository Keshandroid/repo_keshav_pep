package com.app.discountro.ui.home.view.models

import com.google.gson.annotations.SerializedName

data class CountryCouponsModel(
    @SerializedName("country_code")
    val countryCode: String? = "",
    @SerializedName("country_name")
    val countryName: String? = "",
    var isSelectedCountry: Boolean = false
)
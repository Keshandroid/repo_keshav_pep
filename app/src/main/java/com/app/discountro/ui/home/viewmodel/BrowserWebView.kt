package com.app.discountro.ui.home.viewmodel
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.net.MailTo
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.preference.PreferenceManager
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.view.ViewConfiguration
import android.webkit.CookieManager
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import androidx.core.view.NestedScrollingChild
import androidx.core.view.NestedScrollingChildHelper
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.app.discountro.R
import com.app.discountro.common.album_service.AlbumController
import com.app.discountro.common.my_interface.BrowserController
import com.app.discountro.common.my_interface.OnScrollableChangeListener
import com.app.discountro.ui.home.model.Album
import com.app.discountro.ui.home.view.BrowserActivity.Companion.webView
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.KeysUtils.Companion.KEY_ON_OFF_SWIPE_MENU
import com.app.discountro.utils.SharedPrefsUtils
import com.app.discountro.utils.browser_utils.*
import com.app.discountro.utils.browser_utils.my_object.BrowserToast.show
import com.app.discountro.utils.browser_utils.my_object.BrowserUnit
import com.app.discountro.utils.browser_utils.my_object.BrowserUnit.queryWrapper
import com.app.discountro.utils.browser_utils.my_object.IntentUnit.getEmailIntent
import com.app.discountro.utils.browser_utils.my_object.ViewUnit.capture
import org.koin.core.component.KoinComponent
import java.net.URISyntaxException
class BrowserWebView : WebView, AlbumController, KoinComponent, NestedScrollingChild {

    public var onScrollChangeListener: OnScrollChangeListener? = null
    private val scrollSlop: Int = ViewConfiguration.get(context).scaledPagingTouchSlop
    private var scrollableHeight: (() -> Int)? = null
    private var lastClampedTopY: Boolean = true
    private var contentAllowsSwipeToRefresh: Boolean = true
    private var enableSwipeRefreshCallback: ((Boolean) -> Unit)? = null
    var enableDisableSwipe_: ((Boolean) -> Unit)? = null
    var boolEnableDisableSwipe: Boolean = true
    private var lastDeltaY: Int = 0
    var scrollableChangeListener: OnScrollableChangeListener? = null
    var mContext: Context? = null
//    var swipeRefreshLayout : SwipeRefreshLayout?=null
    var isIncoWebView: Boolean = false
    private var mChildHelper: NestedScrollingChildHelper? = null
    private var nestedOffsetY: Int = 0
    private var hasGestureFinished = true

    private var lastY: Int = 0
    private val scrollConsumed = IntArray(2)
    private val scrollOffset = IntArray(2)

    constructor(context: Context?, attrs: AttributeSet?, isIncoTab: Boolean = false) : super(
        context!!, attrs
    )

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context!!, attrs, defStyleAttr
    )

    override fun onScrollChanged(l: Int, t: Int, old_l: Int, old_t: Int) {
        super.onScrollChanged(l, t, old_l, old_t)
        Log.e("SunnyPatelScrolling", "l: $l")
        Log.e("SunnyPatelScrolling", "t: $t")
        Log.e("SunnyPatelScrolling", "old_l: $old_l")
        Log.e("SunnyPatelScrolling", "old_t: $old_t")
        if (onScrollChangeListener != null) {
            onScrollChangeListener!!.onScrollChange(t, old_t)
        }
    }

    interface OnScrollChangeListener {
        fun onScrollChange(scrollY: Int, oldScrollY: Int)
    }


    private var context1: Context? = null
    private var dimen144dp = 0
    private var dimen108dp = 0
    private var animTime = 0
    private var album: Album? = null
    private var webViewClient: BrowserWebViewClient? = null
    private var webChromeClient: BrowserWebChromeClient? = null
    private var downloadListener: BrowserDownloadListener? = null
    private var clickHandler: BrowserClickHandler? = null

    //    private var gestureDetector: GestureDetector? = null
    var adBlock: AdBlock? = null
        private set
    private var javaHosts: Javascript? = null
    var cookieHosts: Cookie? = null
        private set
    lateinit var sp: SharedPreferences
    private var webSettings: WebSettings? = null
    var isForeground = false
        private set
    private var browserController: BrowserController? = null

    fun getBrowserController(): BrowserController? {
        return browserController
    }

    fun setBrowserController(browserController: BrowserController?) {
        this.browserController = browserController
        album!!.setBrowserController(browserController!!)

    }
    // add swipe refresh object in secondary constructor
    constructor(
        swipeRefreshLayout: SwipeRefreshLayout?=null,
        context: Context?,
        isIncoTab: Boolean = false,
        scrollableChangeListener: OnScrollableChangeListener? = null
    ) : super(context!!) // Cannot create a dialog, the WebView context is not an Activity
    {
        this.scrollableChangeListener = scrollableChangeListener
//        this.swipeRefreshLayout = swipeRefreshLayout
        isIncoWebView = isIncoTab
        mChildHelper = NestedScrollingChildHelper(this)
        isNestedScrollingEnabled = true


        this.context1 = context
        dimen144dp = resources.getDimensionPixelSize(R.dimen.layout_width_144dp)
        dimen108dp = resources.getDimensionPixelSize(R.dimen.layout_height_108dp)
        animTime = resources.getInteger(android.R.integer.config_shortAnimTime)
        isForeground = false
        adBlock = AdBlock(this.context1!!)
        javaHosts = Javascript(this.context1!!)
        cookieHosts = Cookie(this.context1!!)
        Log.e(
            "browserConrreo",
            ":: \$myContext :: " + getBrowserController() + "::" + browserController
        )
        album = Album(this.context1!!, this, browserController)
        // pass swipe refresh object
        webViewClient = BrowserWebViewClient(swipeRefreshLayout,this, isIncoTab)
        webChromeClient = BrowserWebChromeClient(this)
        downloadListener = BrowserDownloadListener(this.context1!!)
        clickHandler = BrowserClickHandler(this)

        initWebView()
        initWebSettings()
        initPreferences()
        initAlbum()
    }

    @Synchronized
    private fun initWebView() {
        setWebViewClient(webViewClient!!)
        setWebChromeClient(webChromeClient)
        setDownloadListener(downloadListener)

    }

//    @SuppressLint("ClickableViewAccessibility")
//    override fun onTouchEvent(ev: MotionEvent): Boolean {
//        val returnValue: Boolean
//
//        val event = MotionEvent.obtain(ev)
//        val action = event.actionMasked
//        if (action == MotionEvent.ACTION_DOWN) {
//            nestedOffsetY = 0
//        }
//        val eventY = event.y.toInt()
//        event.offsetLocation(0f, nestedOffsetY.toFloat())
//
//        when (action) {
//            MotionEvent.ACTION_UP -> {
//                hasGestureFinished = true
//                returnValue = super.onTouchEvent(event)
//                stopNestedScroll()
//            }
//            MotionEvent.ACTION_MOVE -> {
//                var deltaY = lastY - eventY
//
//                if (deltaY > 0) {
//                    lastClampedTopY = false
//                }
//
////                lastClampedTopY = deltaY <= 0
//
//                if (dispatchNestedPreScroll(0, deltaY, scrollConsumed, scrollOffset)) {
//                    deltaY -= scrollConsumed[1]
//                    lastY = eventY - scrollOffset[1]
//                    event.offsetLocation(0f, (-scrollOffset[1]).toFloat())
//                    nestedOffsetY += scrollOffset[1]
//                }
//
//                returnValue = super.onTouchEvent(event)
//
//
//
//
//                if (scrollY == 0 && lastClampedTopY) {
//
//                    stopNestedScroll()
//                } else if (dispatchNestedScroll(0, scrollOffset[1], 0, deltaY, scrollOffset)) {
//                    event.offsetLocation(0f, scrollOffset[1].toFloat())
//                    nestedOffsetY += scrollOffset[1]
//                    lastY -= scrollOffset[1]
//                }
//
//                lastDeltaY = deltaY
//            }
//
//            MotionEvent.ACTION_DOWN -> {
//                returnValue = super.onTouchEvent(event)
//                lastY = eventY
//                startNestedScroll(ViewCompat.SCROLL_AXIS_VERTICAL)
//            }
//
//            else -> {
//                returnValue = super.onTouchEvent(event)
//            }
//        }

//        return returnValue
//        return true
//    }


    override fun computeVerticalScrollRange(): Int {
        val scrollRange = super.computeVerticalScrollRange()
        val isScrollable = scrollRange > height + scrollSlop + (scrollableHeight?.invoke() ?: 0)
        Log.e("swipeRefreshBool", "onReceive: $isScrollable")
        val intent = Intent("ScrollReceiver").putExtra(
            KEY_ON_OFF_SWIPE_MENU,
            isScrollable
        )
        scrollableChangeListener?.onScrollableChanged(isScrollable)
        context1!!.sendBroadcast(intent)
        return scrollRange
    }


    override fun setNestedScrollingEnabled(enabled: Boolean) {
        mChildHelper!!.isNestedScrollingEnabled = enabled
    }

    override fun stopNestedScroll() {
        mChildHelper!!.stopNestedScroll()
    }

    override fun isNestedScrollingEnabled(): Boolean = mChildHelper!!.isNestedScrollingEnabled

    override fun startNestedScroll(axes: Int): Boolean = mChildHelper!!.startNestedScroll(axes)

    override fun hasNestedScrollingParent(): Boolean = mChildHelper!!.hasNestedScrollingParent()

    override fun dispatchNestedScroll(
        dxConsumed: Int,
        dyConsumed: Int,
        dxUnconsumed: Int,
        dyUnconsumed: Int,
        offsetInWindow: IntArray?,
    ): Boolean =
        mChildHelper!!.dispatchNestedScroll(
            dxConsumed,
            dyConsumed,
            dxUnconsumed,
            dyUnconsumed,
            offsetInWindow
        )

    override fun dispatchNestedPreScroll(
        dx: Int,
        dy: Int,
        consumed: IntArray?,
        offsetInWindow: IntArray?,
    ): Boolean =
        mChildHelper!!.dispatchNestedPreScroll(dx, dy, consumed, offsetInWindow)

    override fun dispatchNestedFling(
        velocityX: Float,
        velocityY: Float,
        consumed: Boolean,
    ): Boolean =
        mChildHelper!!.dispatchNestedFling(velocityX, velocityY, consumed)

    override fun dispatchNestedPreFling(
        velocityX: Float,
        velocityY: Float,
    ): Boolean =
        mChildHelper!!.dispatchNestedPreFling(velocityX, velocityY)

    override fun onOverScrolled(
        scrollX: Int,
        scrollY: Int,
        clampedX: Boolean,
        clampedY: Boolean ,
    ) {
        lastClampedTopY = clampedY && lastDeltaY <= 0
        super.onOverScrolled(scrollX, scrollY, clampedX, clampedY)
    }

    fun setEnableSwipeRefreshCallback(callback: (Boolean) -> Unit) {
        enableSwipeRefreshCallback = callback
    }

    fun detectOverscrollBehavior() {
        evaluateJavascript("(function() { return getComputedStyle(document.querySelector('body')).overscrollBehaviorY; })();") { behavior ->
            setContentAllowsSwipeToRefresh(behavior.replace("\"", "") == "auto")
        }
    }

    private fun enableSwipeRefresh(enable: Boolean) {
        enableSwipeRefreshCallback?.invoke(enable && contentAllowsSwipeToRefresh)
    }

    private fun setContentAllowsSwipeToRefresh(allowed: Boolean) {
        contentAllowsSwipeToRefresh = allowed
        if (!allowed) {
            enableSwipeRefresh(false)
        }
    }

    @Synchronized
    private fun initWebSettings() {

//        var cUserAgent = ""
        val defaultUserAgentString = WebSettings.getDefaultUserAgent(context).replace("wv", "")
        val prefix: String =
            defaultUserAgentString.substring(0, defaultUserAgentString.indexOf(")") + 1)
        val mobileUserAgent = defaultUserAgentString.replace(prefix, BrowserUnit.UA_MOBILE_PREFIX)
        SharedPrefsUtils.setStringPreference(KeysUtils.keyDefaultMobUserAgent, mobileUserAgent)


        /*if(isDesktopModeOn)
            cUserAgent=KeysUtils.DESKTOP_USERAGENT
        else
            cUserAgent=mobileUserAgent*/
        Log.e("myUserAgentValue", "initWebSettings: $mobileUserAgent")

        //webView?.settings?.cacheMode = LOAD_CACHE_ELSE_NETWORK
        //setLayerType(View.LAYER_TYPE_HARDWARE, null)

        with(settings) {
            setSupportMultipleWindows(true)
            javaScriptCanOpenWindowsAutomatically = true
            javaScriptEnabled = true
            domStorageEnabled = true
            loadWithOverviewMode = true
            useWideViewPort = true

            scrollBarStyle = SCROLLBARS_OUTSIDE_OVERLAY
            isScrollbarFadingEnabled = true
            displayZoomControls = false

            builtInZoomControls = true
            defaultTextEncodingName = "utf-8"
            isVerticalScrollBarEnabled = true
            setGeolocationEnabled(true)
            userAgentString = mobileUserAgent
            isHorizontalScrollBarEnabled = false


            mixedContentMode = WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE
            setSupportMultipleWindows(true)

            setSupportZoom(true)
            if (Build.VERSION.SDK_INT >= 26) {
                safeBrowsingEnabled = true
            }
        }
        Log.e("isVerticalScrollBarEnabled", "initWebSettings: $isVerticalScrollBarEnabled")
    }




    @Synchronized
    fun initPreferences() {
        sp = PreferenceManager.getDefaultSharedPreferences(context1)
        webViewClient!!.enableAdBlock(
            sp.getBoolean(
                context1!!.getString(R.string.sp_ad_block),
                true
            )
        )


        with(settings) {
            setAppCacheEnabled(true)
            setAppCachePath("");
            setAppCacheMaxSize(30 * 1024 * 1024)
            allowFileAccessFromFileURLs = sp.getBoolean("sp_remote", true)
            allowUniversalAccessFromFileURLs = sp.getBoolean("sp_remote", true)
            domStorageEnabled = sp.getBoolean("sp_remote", true)
            databaseEnabled = true
            blockNetworkImage = !sp.getBoolean(context!!.getString(R.string.sp_images), true)
            javaScriptEnabled = sp.getBoolean(context!!.getString(R.string.sp_javascript), true)
            javaScriptCanOpenWindowsAutomatically =
                sp.getBoolean(context!!.getString(R.string.sp_javascript), true)
            setSupportMultipleWindows(
                sp.getBoolean(
                    context!!.getString(R.string.sp_javascript),
                    true
                )
            )
            setGeolocationEnabled(sp.getBoolean(context!!.getString(R.string.sp_location), false))
            mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
            setRenderPriority(WebSettings.RenderPriority.HIGH)
        }
        webViewClient?.enableAdBlock(sp.getBoolean(context!!.getString(R.string.sp_ad_block), true))

        //   toggleCookieSupport(config.cookies)


        sp = PreferenceManager.getDefaultSharedPreferences(context1)
        /*   webViewClient!!.enableAdBlock(
               sp.getBoolean(
                   context1!!.getString(R.string.sp_ad_block),
                   true
               )
           )
           webSettings = settings
           webSettings!!.textZoom = sp.getString("sp_fontSize", "100")!!.toInt()
           webSettings!!.allowFileAccessFromFileURLs = sp.getBoolean("sp_remote", false)
           webSettings!!.allowUniversalAccessFromFileURLs = sp.getBoolean("sp_remote", false)
           webSettings!!.blockNetworkImage =
               !sp.getBoolean(context1!!.getString(R.string.sp_images), true)
           webSettings!!.javaScriptEnabled =
               sp.getBoolean(context1!!.getString(R.string.sp_javascript), true)
           webSettings!!.javaScriptCanOpenWindowsAutomatically =
               sp.getBoolean(context1!!.getString(R.string.sp_javascript), true)
           webSettings!!.setGeolocationEnabled(
               sp.getBoolean(
                   context1!!.getString(R.string.sp_location),
                   false
               )
           )*/
        // val manager = CookieManager.getInstance()
        //  manager.setAcceptCookie(sp.getBoolean(context1!!.getString(R.string.sp_cookies), true))

        with(CookieManager.getInstance()) {
            setAcceptCookie(isEnabled)
            setAcceptThirdPartyCookies(this@BrowserWebView, isEnabled)
        }
    }

    @Synchronized
    private fun initAlbum() {
        album!!.setAlbumCover(null)

        album!!.setAlbumTitle(resources.getString(R.string.app_name))

        album!!.setBrowserController(browserController)
    }

    @get:Synchronized
    val requestHeaders: HashMap<String, String>
        get() {
            val requestHeaders = HashMap<String, String>()
            requestHeaders["DNT"] = "1"
            if (sp.getBoolean(context1!!.getString(R.string.sp_savedata), false)) {
                requestHeaders["Save-Data"] = "on"
            }
            return requestHeaders
        }


    @SuppressLint("SetJavaScriptEnabled")
    @Synchronized
    override fun loadUrl(url: String) {
        var url: String? = url
        if (url == null || url.trim { it <= ' ' }.isEmpty()) {
            show(context1!!, R.string.toast_load_error)
            return
        }

        if (!url.contains("://")) {
            Log.e("Load", "loadUrl: 2->  $url")
            url = queryWrapper(context1!!, url.trim { it <= ' ' })
            Log.e("Load", "loadUrl: 3->  $url")
        }

        if (url.startsWith(BrowserUnit.URL_SCHEME_MAIL_TO)) {
            val intent = getEmailIntent(MailTo.parse(url))
            context1!!.startActivity(intent)
            reload()
            return
        } else if (url.startsWith(BrowserUnit.URL_SCHEME_INTENT)) {
            val intent: Intent
            try {
                intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME)
                context1!!.startActivity(intent)
            } catch (u: URISyntaxException) {
                Log.w("Browser", "Error parsing URL")
            }
            return
        }

        if (!sp.getBoolean(context1!!.getString(R.string.sp_javascript), true)) {
            if (javaHosts!!.isWhite(url)) {
                webSettings = settings
                webSettings!!.javaScriptCanOpenWindowsAutomatically = true
                webSettings!!.javaScriptEnabled = true
                webSettings!!.useWideViewPort = true
                webSettings!!.loadsImagesAutomatically = true
            } else {
                webSettings = settings
                webSettings!!.javaScriptCanOpenWindowsAutomatically = false
                webSettings!!.javaScriptEnabled = false
                webSettings!!.useWideViewPort = true
                webSettings!!.loadsImagesAutomatically = true
            }
        }
        if (!sp.getBoolean(context1!!.getString(R.string.sp_cookies), true)) {
            if (cookieHosts!!.isWhite(url)) {
                val manager = CookieManager.getInstance()
                manager.getCookie(url)
//                manager.setAcceptCookie(true)
            } else {
                val manager = CookieManager.getInstance()
                manager.setAcceptCookie(false)
            }
        }
        webViewClient!!.updateWhite(adBlock!!.isWhite(url))

        // added 3/11/22

//        webView!!.setWebChromeClient(WebChromeClient())

//        webView!!.settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
//        webView!!.settings.cacheMode = WebSettings.LOAD_NO_CACHE;
//        webView!!.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY;
//        webView!!.settings.domStorageEnabled = true;
//        webView!!.settings.javaScriptEnabled = true;
//        webView!!.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.NORMAL;
//        // extra settings
//        webView!!.settings.loadWithOverviewMode = false;
//        webView!!.settings.useWideViewPort = true;
//        webView!!.isScrollContainer = true;
//        // setting for lollipop and above
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            webView!!.settings.mixedContentMode = WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE;
//            webView!!.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW;
//        }
//        webView!!.settings.setSavePassword(true);
//        webView!!.settings.setSaveFormData(true);
//        webView!!.settings.setEnableSmoothTransition(false);
//        webView!!.settings.allowFileAccess = true
//        webView!!.settings.allowContentAccess = true
//        webView!!.settings.setAppCacheEnabled(true);
//        webView!!.settings.loadsImagesAutomatically = true;
        super.loadUrl(url, requestHeaders)
        if (browserController != null && isForeground) {
            browserController!!.updateBookmarks()
        }

    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun reload() {
        webViewClient!!.updateWhite(adBlock!!.isWhite(url))
        if (!sp.getBoolean(context1!!.getString(R.string.sp_javascript), true)) {
            if (javaHosts!!.isWhite(url)) {
                webSettings = settings
                webSettings!!.javaScriptCanOpenWindowsAutomatically = true
                webSettings!!.javaScriptEnabled = true
                webSettings!!.useWideViewPort = true
                webSettings!!.loadsImagesAutomatically = true
            } else {
                webSettings = settings
                webSettings!!.javaScriptCanOpenWindowsAutomatically = false
                webSettings!!.javaScriptEnabled = false
                webSettings!!.useWideViewPort = true
                webSettings!!.loadsImagesAutomatically = true
            }
        }
        if (!sp.getBoolean(context1!!.getString(R.string.sp_cookies), true)) {
            if (cookieHosts!!.isWhite(url)) {
                val manager = CookieManager.getInstance()
                manager.getCookie(url)
//                manager.setAcceptCookie(true)
            } else {
                val manager = CookieManager.getInstance()
                manager.setAcceptCookie(false)
            }
        }
        super.reload()
    }

    override val albumView: View?
        get() = album!!.albumView

    override fun setAlbumCover(bitmap: Bitmap?) {
        album!!.setAlbumCover(bitmap)
    }

    override var albumTitle: String?
        get() = album!!.getAlbumTitle()
        set(title) {
            album!!.setAlbumTitle(title)
        }

    @Synchronized
    override fun activate() {
        requestFocus()
        isForeground = true
        album!!.activate()
    }

    @Synchronized
    override fun deactivate() {
        clearFocus()
        isForeground = false
        album!!.deactivate()
    }

    fun update(progress: Int, webView: WebView) {
        browserController!!.updateTabsAdapterData(progress, webView)
        if (isForeground) {
            browserController!!.updateProgress(progress)
        }
//        setAlbumCover(
//            capture(
//                this,
//                dimen144dp.toFloat(),
//                dimen108dp.toFloat(),
//                Bitmap.Config.RGB_565
//            )
//        )
//        if (isLoadFinish) {
//            Handler(Looper.getMainLooper()).postDelayed({
//                setAlbumCover(
//                    capture(
//                        this@BrowserWebView,
//                        dimen144dp.toFloat(),
//                        dimen108dp.toFloat(),
//                        Bitmap.Config.RGB_565
//                    )
//                )
//            }, animTime.toLong())
//            if (prepareRecord()) {
//                browserController!!.updateAutoComplete()
//            }
//        }
    }

    @Synchronized
    fun update(title: String?, url: String?) {
        album!!.setAlbumTitle(title)
        if (isForeground) {
            browserController!!.updateBookmarks()
        }
    }

    @Synchronized
    override fun destroy() {
        stopLoading()
        onPause()
        clearHistory()
        visibility = GONE
        removeAllViews()
        super.destroy()
    }

    val isLoadFinish: Boolean
        get() = progress >= BrowserUnit.PROGRESS_MAX

    private fun prepareRecord(): Boolean {
        val title = title
        val url = url
        return !(title == null || title.isEmpty()
                || url == null || url.isEmpty()
                || url.startsWith(BrowserUnit.URL_SCHEME_ABOUT)
                || url.startsWith(BrowserUnit.URL_SCHEME_MAIL_TO)
                || url.startsWith(BrowserUnit.URL_SCHEME_INTENT))
    }
}



















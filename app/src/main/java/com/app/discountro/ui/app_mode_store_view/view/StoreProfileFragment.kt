package com.app.discountro.ui.app_mode_store_view.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RatingBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.R
import com.app.discountro.databinding.StoreProfileLayoutBinding
import com.app.discountro.ui.app_mode_store_view.adapter.CouponsDealsAdapter
import com.app.discountro.ui.app_mode_store_view.adapter.FeatureProductAdapter
import com.app.discountro.ui.app_mode_store_view.adapter.StoreReviewAdapter
import com.app.discountro.ui.home.view.BrowserActivity
import com.app.discountro.ui.home.view.models.FavoriteStore
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.KeysUtils.Companion.KEY_STORE_MODEL
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog

class StoreProfileFragment : Fragment() {
    private lateinit var binding: StoreProfileLayoutBinding
    private var browserActivity: BrowserActivity? = null
    private var favoriteStore: FavoriteStore? = null
    private lateinit var featureProductAdapter : FeatureProductAdapter
    private lateinit var couponsDealsAdapter : CouponsDealsAdapter
    lateinit var reviewBottomSheet: BottomSheetDialog
    lateinit var leaveReviewBottomSheet: BottomSheetDialog


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = StoreProfileLayoutBinding.inflate(layoutInflater, container, false)
        favoriteStore = arguments?.getSerializable(KEY_STORE_MODEL) as FavoriteStore

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        browserActivity = activity as BrowserActivity?
        Glide.with(requireContext()).load(favoriteStore?.logoUrl).into(binding.ivStoreLogo)
        binding.tvStorename.text = favoriteStore?.storeName
        browserActivity?.appbarTitle?.text = favoriteStore?.storeName ?: "McDonald's"

        setAdapter()
        initClick()
    }

    private fun initClick() {
        binding.llWriteReview.setOnClickListener {
            reviewsBottomSheet()
//            browserActivity?.setFragment(TransactionDetailsFragment(), "TransactionDetailsFragment", "")
        }
    }

    private fun setAdapter() {
        featureProductAdapter = FeatureProductAdapter(requireContext(), arrayListOf())
        binding.rvFeaturedProducts.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.rvFeaturedProducts.adapter = featureProductAdapter

        couponsDealsAdapter = CouponsDealsAdapter(requireContext(), arrayListOf())
        binding.rvCouponsDeals.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.rvCouponsDeals.adapter = couponsDealsAdapter
    }

    private fun reviewsBottomSheet() {
        reviewBottomSheet =
            BottomSheetDialog(requireContext(), R.style.AppBottomSheetDialogTheme)
        reviewBottomSheet.setContentView(R.layout.reviews_bottomsheet_layout)
        val ivClose = reviewBottomSheet.findViewById<ImageView>(R.id.ivClose)
        val writeReview = reviewBottomSheet.findViewById<LinearLayout>(R.id.llWriteReview)
        val rvStoreReview = reviewBottomSheet.findViewById<RecyclerView>(R.id.rvStoreReviews)
        val reviewAdapter = StoreReviewAdapter(reviewBottomSheet.context, arrayListOf())
        reviewBottomSheet.behavior.state = BottomSheetBehavior.STATE_EXPANDED


        rvStoreReview?.layoutManager = LinearLayoutManager(reviewBottomSheet.context)
        rvStoreReview?.adapter = reviewAdapter


        ivClose?.setOnClickListener {
            reviewBottomSheet.dismiss()
        }

        writeReview?.setOnClickListener {
            reviewBottomSheet.dismiss()
            leaveReviewBottomSheet()
        }

        reviewBottomSheet.show()
    }

    private fun leaveReviewBottomSheet() {
        leaveReviewBottomSheet =
            BottomSheetDialog(requireContext(), R.style.AppBottomSheetDialogTheme)
        leaveReviewBottomSheet.setContentView(R.layout.leave_review_bottomsheet)
        val ivClose = leaveReviewBottomSheet.findViewById<ImageView>(R.id.ivClose)
        val submitReview = leaveReviewBottomSheet.findViewById<LinearLayout>(R.id.llSubmitReview)
        val ratingBar = leaveReviewBottomSheet.findViewById<RatingBar>(R.id.ratingBar)
        val llEditReview = leaveReviewBottomSheet.findViewById<LinearLayout>(R.id.llEditReview)
        val tvLeaveReview = leaveReviewBottomSheet.findViewById<TextView>(R.id.tvLeaveReview)
        val ivSuccessFeedback = leaveReviewBottomSheet.findViewById<ImageView>(R.id.ivSuccessFeedback)



        ivClose?.setOnClickListener {
            leaveReviewBottomSheet.dismiss()
        }

        submitReview?.setOnClickListener {
            ratingBar?.visibility = GONE
            llEditReview?.visibility = GONE
            submitReview?.visibility = GONE

            tvLeaveReview?.text = "Thank you for your feedback!"
            tvLeaveReview?.visibility = VISIBLE
            ivSuccessFeedback?.visibility = VISIBLE
        }

        leaveReviewBottomSheet.show()
    }
}
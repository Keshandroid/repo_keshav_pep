package com.app.discountro.ui.home.model

data class ReOpenTabsModel(
    var siteUrl: String?,
    var siteTitle: String?,
//    var siteImgPath: String?,
    var isForeground: Boolean?
)
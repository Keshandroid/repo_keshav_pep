package com.app.discountro.ui.home.view

import android.annotation.SuppressLint
import android.content.Context
import android.view.MotionEvent
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import com.app.discountro.R

class FullscreenHolder(context: Context?) : FrameLayout(context!!) {
    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return true
    }

    init {
        setBackgroundColor(ContextCompat.getColor(context!!, R.color.color_black))
    }
}
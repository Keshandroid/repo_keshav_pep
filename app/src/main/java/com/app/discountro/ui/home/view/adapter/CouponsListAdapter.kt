package com.app.discountro.ui.home.view.adapter

import android.content.Context
import android.os.CountDownTimer
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.R
import com.app.discountro.fireStore.models.RoModel
import com.bumptech.glide.Glide

class CouponsListAdapter(
    private val activity: Context,
    private var listCoupons: ArrayList<RoModel.CouponModel>,
    private val callBackRecInt: CallBackRecInt,
    private val isSimilarCoupons: Boolean,
    private val logoUrl: String?
) : RecyclerView.Adapter<CouponsListAdapter.MyViewHolder>() {
    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val ivBrand: ImageView = view.findViewById(R.id.ivBrand)
        val tvDiscount: TextView = view.findViewById(R.id.tvDiscount)
        val tvCouponCopied: TextView = view.findViewById(R.id.tvCouponCopied)
        val tvCode: TextView = view.findViewById(R.id.tvCode)
        val tvVerified: TextView = view.findViewById(R.id.tvVerified)
        val tvDescriptions: TextView = view.findViewById(R.id.tvDescriptions)
        val tvCodeCopy: TextView = view.findViewById(R.id.tvCodeCopy)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        return MyViewHolder(
            LayoutInflater.from(activity)
                .inflate(R.layout.item_coupons_code_row, parent, false)
        )

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (isSimilarCoupons) {
            if (logoUrl?.isNotEmpty()!!) {
                holder.ivBrand.visibility = View.VISIBLE
                Glide.with(activity).load(logoUrl).into(holder.ivBrand)
            } else {
                holder.ivBrand.visibility = View.INVISIBLE
            }
        } else {
            holder.ivBrand.visibility = View.INVISIBLE
        }

        //holder.tvCouponCopied.visibility = View.GONE
        if (listCoupons[position].isCopied!!) {
            holder.tvCouponCopied.visibility = View.GONE
            holder.tvCodeCopy.gravity = Gravity.CENTER
        } else {
            holder.tvCouponCopied.visibility = View.VISIBLE
            holder.tvCodeCopy.gravity = Gravity.CENTER or Gravity.END
        }
        holder.tvDiscount.text = listCoupons[position].offerText

        holder.tvDescriptions.text = listCoupons[position].offerName

        holder.tvCodeCopy.text = listCoupons[position].code
        holder.tvCodeCopy.setOnClickListener {
            callBackRecInt.onItemSelected(it, position)
            hideCoverTimer(60000 * 30, position)
            listCoupons[position].apply { isCopied = true }
            notifyDataSetChanged()
        }
    }

    fun setNewListAndRefresh(listCoupons: ArrayList<RoModel.CouponModel>) {
        this.listCoupons = listCoupons
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return listCoupons.size
    }

    private fun hideCoverTimer(time: Long, position: Int) {
        val timer = object : CountDownTimer(time, 100) {
            override fun onTick(p0: Long) {
            }

            override fun onFinish() {
                listCoupons[position].isCopied = false
                notifyItemChanged(position)
            }
        }.start()
    }

}

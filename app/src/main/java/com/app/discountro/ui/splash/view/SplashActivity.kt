package com.app.discountro.ui.splash.view

import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.lifecycle.ViewModelProvider
import com.app.discountro.R
import com.app.discountro.base.BaseViewModelFactory
import com.app.discountro.common.constant.AppConstant
import com.app.discountro.common.constant.AppMethods
import com.app.discountro.common.service.CoupanPopService
import com.app.discountro.databinding.ActivitySplashBinding
import com.app.discountro.fireStore.models.OverlayTextsDialogModel
import com.app.discountro.fireStore.services.FireStoreServiceCall
import com.app.discountro.fireStore.services.OverlayTextCallback
import com.app.discountro.ui.home.view.BrowserActivity
import com.app.discountro.ui.splash.model.LanguageModel
import com.app.discountro.ui.splash.model.MyTranslationVarModel
import com.app.discountro.ui.splash.view.adapter.LanguageDropDwnAdapter
import com.app.discountro.ui.splash.viewModel.LanguageViewModel
import com.app.discountro.ui.welcome.view.WelcomeActivity
import com.app.discountro.utils.DialogMyInterface
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.SharedPrefsUtils
import com.app.discountro.utils.showPermissionOverlay
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.net.URL


class SplashActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    companion object {
        private const val TAG = "SplashActivity"
    }

    lateinit var binding: ActivitySplashBinding
    private val languageModelList: MutableList<LanguageModel> = mutableListOf()
    private lateinit var languageViewModel: LanguageViewModel
    private val appConstant: AppConstant = AppConstant()
    private val myTranslationVarModel: ArrayList<MyTranslationVarModel> = arrayListOf()
    private val map = HashMap<String, String>()
    private val gson = Gson()
    var domainFromChrome = ""
    private var sheetBehavior: BottomSheetBehavior<*>? = null

    private var showForFirstTime = false;
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        Log.e(TAG, "onNewIntent")
        if (intent != null && intent.clipData != null && intent.clipData!!.itemCount > 0) {
            try {
                domainFromChrome = intent.clipData!!.getItemAt(0).text.toString()
                if (canDrawOverlays(this)) {
                    val intent = Intent(this, CoupanPopService::class.java)
                    if (getIntent().component?.className?.endsWith(".Queue") == true) {
                        intent.putExtra(
                            "domain",
                            getIntent().clipData!!.getItemAt(0).text.toString()
                        )
                        intent.putExtra("compare", "compare")
                    } else {
                        intent.putExtra("domain", domainFromChrome)
                    }
                    startService(intent)
                    finish()
                    this.moveTaskToBack(true)
                } else {
                    //checkOverlayPermission()
                    openBottomSheetForPermission()
                    /*val intent = Intent(
                            Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse(
                                "package:$packageName"
                            )
                        )
                        startActivityForResult(intent, 0)*/
                }
            } catch (e: Exception) {
                Log.e(TAG, e.message.toString())
            }
        }
    }

    private fun canDrawOverlays(context: Context): Boolean {
        return if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) true else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            Settings.canDrawOverlays(context)
        } else {
            if (Settings.canDrawOverlays(context)) return true
            try {
                val mgr = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
                    ?: return false
                //getSystemService might return null
                val viewToAdd = View(context)
                val params = WindowManager.LayoutParams(
                    0,
                    0,
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY else WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE or WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                    PixelFormat.TRANSPARENT
                )
                viewToAdd.layoutParams = params
                mgr.addView(viewToAdd, params)
                mgr.removeView(viewToAdd)
                return true
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            false
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            super.onCreate(savedInstanceState)
        } else {
            val bundle = Bundle()
            super.onCreate(bundle)
        }


        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        //binding.contentLay.visibility = View.GONE


        sheetBehavior = BottomSheetBehavior.from<CardView>(binding.contentLay)
        sheetBehavior?.isHideable = true
        sheetBehavior?.isDraggable = true
        sheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED
        sheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN

        //openBottomSheetForPermission()
        appearOnTopTrue()  // don't show permission
//        checkOverlayPermission() // show permission for appear on top

    //openBottomSheetForPermission()
    }

    private fun checkOverlayPermission() {
        if (Settings.canDrawOverlays(this)) {
            appearOnTopTrue()
        } else {
            if (SharedPrefsUtils.getBooleanPreference("NEVER_ASK_AGAIN", false)) {
                appearOnTopTrue()
            } else {
                showPermissionOverlay(object : DialogMyInterface {
                    override fun callBack(view: View, buttonAction: Int, neverShowAgain: Boolean) {
                        SharedPrefsUtils.setBooleanPreference("NEVER_ASK_AGAIN", neverShowAgain)
                        if (buttonAction == DialogMyInterface.ACTION_OK) {
                            showForFirstTime = true
                            val intent = Intent(
                                Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse(
                                    "package:$packageName"
                                )
                            )
                            startActivityForResult(intent, 0)
                        } else if (buttonAction == DialogMyInterface.ACTION_CANCEL) {
                            finishAffinity()
                        }
                    }
                }).show()
            }
        }
    }

    private fun openBottomSheetForPermission() {
        binding.pbProgress.visibility = View.VISIBLE
        binding.llShowPermissions.visibility = View.GONE

        binding.ivClose.setOnClickListener { finishAffinity() }
        binding.btnUnderstand.setOnClickListener {
            val intent = Intent(
                Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse(
                    "package:$packageName"
                )
            )
            startActivityForResult(intent, 0)
        }

        CoroutineScope(Dispatchers.Default).launch {
            val selectedLang = SharedPrefsUtils.getStringPreference(KeysUtils.keyLangISO)
            FireStoreServiceCall.getTextAppearOnTop(selectedLang, object : OverlayTextCallback {
                override fun onSuccessOverlay(response: OverlayTextsDialogModel) {
                    runOnUiThread {
                        binding.tvNormal.settings.javaScriptEnabled = true
                        binding.tvNormal.loadData(
                            response.steps,
                            "text/html; charset=utf-8",
                            "UTF-8"
                        )
                        binding.tvAlmostDone.text = response.almost_done
                        binding.btnUnderstand.text = response.i_understand
                        binding.pbProgress.visibility = View.GONE
                        binding.llShowPermissions.visibility = View.VISIBLE
                    }
                }

                override fun onErrorOverlay(error: String) {
                    runOnUiThread {
                        binding.tvNormal.settings.javaScriptEnabled = true
                        binding.tvNormal.loadData(
                            MY_HTML_FORMAT,
                            "text/html; charset=utf-8",
                            "UTF-8"
                        )
                        binding.pbProgress.visibility = View.GONE
                        binding.llShowPermissions.visibility = View.VISIBLE
                    }
                }
            })


            runOnUiThread {
                sheetBehavior?.addBottomSheetCallback(object : BottomSheetCallback() {
                    override fun onStateChanged(bottomSheet: View, newState: Int) {
                        when (newState) {
                            BottomSheetBehavior.STATE_HIDDEN -> {
                                finishAffinity()
                            }
                            BottomSheetBehavior.STATE_COLLAPSED -> {}
                            BottomSheetBehavior.STATE_DRAGGING -> {}
                            BottomSheetBehavior.STATE_EXPANDED -> {}
                            BottomSheetBehavior.STATE_HALF_EXPANDED -> {}
                            BottomSheetBehavior.STATE_SETTLING -> {}
                        }
                    }

                    override fun onSlide(bottomSheet: View, slideOffset: Float) {}
                })


                if (Settings.canDrawOverlays(this@SplashActivity)) {
                    appearOnTopTrue()
                    binding.contentLay.visibility = View.GONE
                } else {
                    sheetBehavior?.setState(BottomSheetBehavior.STATE_EXPANDED)
                }

            }
        }

    }

    private fun appearOnTopTrue() {
        Log.e(TAG, "appearOnTopTrue: ")
        if (intent.clipData != null && intent.clipData!!.itemCount > 0) {
            try {
                domainFromChrome = intent.clipData!!.getItemAt(0).text.toString()
                if (canDrawOverlays(this)) {
                    val intent = Intent(this, CoupanPopService::class.java)
                    if (getIntent().component?.className?.endsWith(".Queue") == true) {
                        intent.putExtra(
                            "domain",
                            getIntent().clipData!!.getItemAt(0).text.toString()
                        )
                        intent.putExtra("compare", "compare")
                    } else {
                        intent.putExtra("domain", domainFromChrome)
                    }
                    startService(intent)
                    finish()
                    this.moveTaskToBack(true)
                } else {
                    //checkOverlayPermission()
                    openBottomSheetForPermission()
                    /*val intent = Intent(
                            Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse(
                                "package:$packageName"
                            )
                        )
                        startActivityForResult(intent, 0)*/
                }
            } catch (e: Exception) {
                Log.e(TAG, e.message.toString())
            }
        } else {
            initView()
        }
    }


    private fun initView() {
        if (AppMethods.isNetworkConnected(this@SplashActivity)) {
            appConstant.getHashKey(this@SplashActivity)
            myIntentScreen()

        } else {
            SharedPrefsUtils
            appConstant.showAlertMessage(
                this@SplashActivity,
                resources.getString(com.app.discountro.R.string.alert),
                resources.getString(com.app.discountro.R.string.alert_no_internet_message),
                resources.getString(com.app.discountro.R.string.alert_setting),
                resources.getString(com.app.discountro.R.string.alert_cancel)
            )
        }
    }

    private fun myIntentScreen() {
        val handler = Handler(Looper.getMainLooper())
        //Input true if work in splashIntro screen
        if (false) {
            userSelectingLanguage()
            //  translationFireData()
        } else {
            when {
                SharedPrefsUtils.getStringPreference(KeysUtils.keyLangISO).isNotEmpty()
                        || SharedPrefsUtils.getStringPreference(KeysUtils.keyLangISO) != ""
                        && SharedPrefsUtils.getStringPreference(KeysUtils.KeyUUID).isEmpty() -> {

                    binding.llSpinnerSelect.visibility = View.GONE

                    getFireLanguageData(SharedPrefsUtils.getStringPreference(KeysUtils.keyLangISO))
                    handler.postDelayed(
                        {
                            navigateToWelcome()
                        },
                        2000
                    )
//                localizeFromStringFile()

                }
                SharedPrefsUtils.getStringPreference(KeysUtils.keyLangISO).isEmpty()
                        && SharedPrefsUtils.getStringPreference(KeysUtils.KeyUUID).isEmpty() -> {
//                    handler.postDelayed(
//                        {
                    userSelectingLanguage()
//                        },
//                        2000
//                    )
                }

                SharedPrefsUtils.getStringPreference(KeysUtils.KeyUUID).isNotEmpty() -> {
                    getFireLanguageData(SharedPrefsUtils.getStringPreference(KeysUtils.keyLangISO))
                    navToMainActivity()
//                  localizeFromStringFile()
                }
                else -> {

                }
            }
        }
    }

    private fun navToMainActivity() {
//        val mainIntent = Intent(this@SplashActivity, BrowserActivity::class.java)
        Log.e("SplashActivity", "BrowserActivity::class.java 1")

        val mainIntent = Intent(this@SplashActivity, BrowserActivity::class.java)
        startActivity(mainIntent)
        finish()
    }

    private fun navigateToWelcome() {
        val mainIntent = Intent(this@SplashActivity, WelcomeActivity::class.java)
        startActivity(mainIntent)
        finish()
    }

    private fun userSelectingLanguage() {
        binding.llSpinnerSelect.visibility = View.VISIBLE
        languageViewModel =
            ViewModelProvider(this, BaseViewModelFactory { LanguageViewModel() }).get(
                LanguageViewModel::class.java
            )
        languageViewModel.getFireLanguage()
        setSelectLangObserver()
    }

    private fun setSelectLangObserver() {
        languageModelList.clear()
        languageModelList.add(LanguageModel(resources.getString(R.string.select_language), ""))
        binding.llSpinnerSelect.visibility = View.GONE
        languageViewModel.languageModel.observe(this) {
            languageModelList.add(it)
            binding.llSpinnerSelect.visibility = View.VISIBLE
        }
        val customDropDownAdapter = LanguageDropDwnAdapter(this, languageModelList)
        binding.spnSelectCountry.adapter = customDropDownAdapter
        binding.spnSelectCountry.onItemSelectedListener = this
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, p3: Long) {
        Log.e("MyLanguageSelection", "onItemSelected: ${languageModelList[position].lang_iso}")
        //   appConstant.setLocal(languageModelList[position].lang_iso, this@SplashActivity)
        if (languageModelList[position].lang_iso.isNotEmpty()) {
            val langISO = languageModelList[position].lang_iso
            SharedPrefsUtils.setStringPreference(KeysUtils.keyLangISO, langISO)
            // localizeFromFireData(langISO)
            getFireLanguageData((langISO))
            navigateToIntroPage(langISO)
        }
    }


    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    private fun getFireLanguageData(langISO: String) {
        appConstant.showDiscoRoProgress(this@SplashActivity)
        Firebase.firestore.collection(KeysUtils.KEYFIRELANGUAGE_Collection).document(langISO)
            .collection(KeysUtils.KEYFIRETRANSLATION_Collection).get().addOnSuccessListener {

                for (doc in it) {
                    Log.e("getMDATA", "ID:: ${doc.id} ::Data:: ${doc.data} ")
                    map[doc.data["string_slug"].toString()] =
                        doc.data["string_translation"].toString()
                }
                val myDataSunny = gson.toJson(map).toString()

                val myConvertData: MyTranslationVarModel =
                    gson.fromJson(myDataSunny, MyTranslationVarModel::class.java)

                /**
                 * ADD SHARE PREFERENCE DATA HERE FOR LANGUAGE_TRANS
                 */

                SharedPrefsUtils.setStringPreference(
                    KeysUtils.KeyPleaseWait,
                    myConvertData.please_wait
                )
                SharedPrefsUtils.setModelPreferences(KeysUtils.KeyModelTranslation, myConvertData)
                myTranslationVarModel.add(myConvertData)

            }.addOnFailureListener {
                //  appConstant.dismissDiscRoProgress()
                Log.e("errGetFireSplashData", "getFireSplashData: ", it)
            }
        appConstant.dismissDiscRoProgress()
    }

    private fun navigateToIntroPage(langISO: String) {
        val intent = Intent(this@SplashActivity, SplashIntroActivity::class.java)
        intent.putExtra(KeysUtils.keyLangISO, langISO)
        startActivity(intent)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (Settings.canDrawOverlays(this)) {
            val intent = Intent(this, CoupanPopService::class.java)

            if (getIntent().component?.className?.endsWith(".Queue") == true) {

                intent.putExtra(
                    "domain",
                    domainFromChrome + " " + URL(getIntent().clipData!!.getItemAt(0).text.toString()).path
                )
                intent.putExtra("compare", "compare")
            } else {
                intent.putExtra("domain", domainFromChrome)
            }
            startService(intent)
        }
        finish()
        this.moveTaskToBack(true)

    }
}


const val MY_HTML_FORMAT = "<p>Just activate the App to appear on top.</p>\n" +
        "\n" +
        "<p>Steps</p>\n" +
        "\n" +
        "<ol style=\"list-style-type:none\" type=\"1\">\n" +
        "\t\t\t<li>After you click the &ldquo;I understand&rdquo; button search for the Discount App in the list of apps</li>" +
        "\t\t\t<li>Toggle the button to be &ldquo;ON&rdquo; for the Discount App</li>" +
        "\t\t\t<li>Close the window</li>" +
        "</ol>\n"
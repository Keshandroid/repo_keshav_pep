package com.app.discountro.ui.home.view.models
import com.app.discountro.fireStore.models.RoModel
import java.util.ArrayList
class CountryNameAndDataModel
{
    var countryName : String = ""
    var countryData : ArrayList<RoModel.CouponModel> = ArrayList()
    var countryListData : CountryCouponsModel ?= null
}
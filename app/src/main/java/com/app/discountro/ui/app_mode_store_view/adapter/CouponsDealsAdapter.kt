package com.app.discountro.ui.app_mode_store_view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.ItemRowCouponsBinding

class CouponsDealsAdapter(val context: Context, val couponsDealsList: ArrayList<String>) : RecyclerView.Adapter<CouponsDealsAdapter.ViewHolder>() {

    class ViewHolder(val binding : ItemRowCouponsBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemRowCouponsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.root.setOnClickListener {  }
    }

    override fun getItemCount(): Int {
        return 6
    }
}
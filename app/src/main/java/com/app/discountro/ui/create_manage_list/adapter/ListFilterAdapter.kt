package com.app.discountro.ui.create_manage_list.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.ItemProductFilterListVerticleLayoutBinding
import com.app.discountro.ui.product.model.ListCollectionModel

class ListFilterAdapter(val context: Context, private var filtersList: ArrayList<ListCollectionModel>, val listener: ManageListListener) : RecyclerView.Adapter<ListFilterAdapter.ViewHolder>() {

    class ViewHolder(val binding: ItemProductFilterListVerticleLayoutBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemProductFilterListVerticleLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.tvFilterName.text = filtersList[position].listName

        holder.binding.ivDelete.setOnClickListener {
            listener.removeList(position, filtersList[position])
        }

        holder.binding.ivEdit.setOnClickListener {
            listener.editList(position, filtersList[position])
        }
    }

    fun updateList(filterList: ArrayList<ListCollectionModel>){
        this.filtersList = filterList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return  filtersList.size
    }

    interface ManageListListener{
        fun removeList(position: Int, listCollectionModel: ListCollectionModel)
        fun editList(position: Int, listCollectionModel: ListCollectionModel)
    }
}
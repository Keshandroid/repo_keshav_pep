package com.app.discountro.ui.account.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.ItemviewTransactionCategoryBinding

class TransactionsCategoryAdapter (val context: Context, val categoryList : ArrayList<String>) : RecyclerView.Adapter<TransactionsCategoryAdapter.ViewHolder>() {

    class ViewHolder(binding : ItemviewTransactionCategoryBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemviewTransactionCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return 4
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }
}
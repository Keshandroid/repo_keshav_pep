package com.app.discountro.ui.firebase_dummy.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.discountro.databinding.ActivityFirebaseCrudeSelectionBinding
import com.app.discountro.ui.firebase_dummy.adapter.AllUsersListAdapter
import com.app.discountro.ui.firebase_dummy.firebase_helper.FirebaseHelperDummy
import com.app.discountro.ui.firebase_dummy.firebase_helper.FirebaseHelperDummy.userListData
import com.app.discountro.ui.firebase_dummy.model.DummyUserModel

class FirebaseCrudeSelectionActivity : AppCompatActivity(), AllUsersListAdapter.UpdateUserData {
    lateinit var binding : ActivityFirebaseCrudeSelectionBinding
    lateinit var usersListAdapter : AllUsersListAdapter
    var userList = ArrayList<DummyUserModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFirebaseCrudeSelectionBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initClick()
        setAdapter()
        initObserver()
        FirebaseHelperDummy.readDataFirebase()
    }

    private fun setAdapter() {
        binding.rvUsers.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
        usersListAdapter = AllUsersListAdapter(userList, this)
        binding.rvUsers.adapter = usersListAdapter
    }

    private fun initObserver() {
        userListData.observe(this){userListFirebase ->
            userList.clear()
            userList.addAll(userListFirebase)
            usersListAdapter.notifyDataSetChanged()
        }
    }

    private fun initClick() {
        binding.btnInsert.setOnClickListener {

        }
    }

    override fun onItemClick(data: DummyUserModel) {
        val intent = Intent(this@FirebaseCrudeSelectionActivity, UpdateDummyUserActivity::class.java)
            startActivity(intent)
    }
}
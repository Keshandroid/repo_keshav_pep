package com.app.discountro.ui.app_mode_store_view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.StoreItemLayoutBinding

class FeatureProductAdapter(val context: Context, val featureProductList: ArrayList<String>) : RecyclerView.Adapter<FeatureProductAdapter.ViewHolder>() {

    class ViewHolder(val binding: StoreItemLayoutBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(StoreItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.root.setOnClickListener {}
    }

    override fun getItemCount(): Int {
        return 6
    }

}
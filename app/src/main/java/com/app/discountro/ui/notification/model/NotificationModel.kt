package com.app.discountro.ui.notification.model

import com.google.firebase.Timestamp
import com.google.firebase.firestore.ServerTimestamp
import java.util.*

data class NotificationModel(

    var date_add: Timestamp?,
    var description: String?,
    var id_notification: String?,
    var id_store: String?,
    var image_url: String?,
    var image_url_secondary: String?,
    var interest: Int?,
    var redirect_link: String?,
    var store_name: String?,
    var subtitle: String?,
    var title: String?,

    )

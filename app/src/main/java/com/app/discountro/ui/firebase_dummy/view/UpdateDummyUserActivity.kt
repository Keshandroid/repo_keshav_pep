package com.app.discountro.ui.firebase_dummy.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.discountro.R
import com.app.discountro.databinding.ActivityUpdateDummyUserBinding

class UpdateDummyUserActivity : AppCompatActivity() {
    lateinit var binding : ActivityUpdateDummyUserBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateDummyUserBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}
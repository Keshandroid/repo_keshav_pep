package com.app.discountro.ui.home.view


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.app.discountro.Interface.FireStoreUserDataInterface
import com.app.discountro.databinding.FragmentAppShopBinding
import com.app.discountro.fireStore.helper.FireStoreHelper
import com.app.discountro.fireStore.models.UserDataModel
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class AppShopFragment : Fragment(){
    val TAG = AppShopFragment::class.java.simpleName
    private lateinit var binding: FragmentAppShopBinding
    private var browserActivity: BrowserActivity? = null
    private var userDataModel: UserDataModel? = null
    private val auth: FirebaseAuth = Firebase.auth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentAppShopBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        browserActivity = activity as BrowserActivity?
        browserActivity?.appbarTitle?.text = "Shop/Coupons"

        //firebaseGetUserData()

    }


    private fun firebaseGetUserData() {
        Log.e("FireStore", "user id preference: ${auth.currentUser?.uid} " )
        FireStoreHelper().getUserData(
            auth.currentUser!!.uid,
            object: FireStoreUserDataInterface {
                override fun onComplete(model: UserDataModel) {
                    userDataModel = model
                    var fullName = model.user_name.split(" ")
                    if(fullName.isNotEmpty() && fullName.size >= 2 ) {
                        browserActivity?.appbarTitle?.text = "Hey ${fullName[0]},"
                    }
                }

                override fun onError(error: String) {
                    Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }
}
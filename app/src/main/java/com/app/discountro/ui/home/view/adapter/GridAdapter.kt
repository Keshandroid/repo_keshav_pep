package com.app.discountro.ui.home.view.adapter

import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.app.discountro.R
import com.app.discountro.common.sqlite.GridItem
import com.app.discountro.utils.browser_utils.my_object.BrowserUnit.file2Bitmap
import java.util.*

class GridAdapter(private val context: Context, val list: List<GridItem>) : BaseAdapter() {
    private class Holder {
        var title: TextView? = null
        var cover: ImageView? = null
        var cardView: CardView? = null
    }

    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        val holder: Holder
        var view = convertView

        Log.e("TAG", "getView: setting grid adapter!!!!!!!!!!>>>>>>>", )
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.grid_item, parent, false)
            holder = Holder()
            holder.title = view.findViewById(R.id.grid_item_title)
            holder.cover = view.findViewById(R.id.grid_item_cover)
            holder.cardView = view.findViewById(R.id.cardView)
            view.tag = holder
        } else {
            holder = view.tag as Holder
        }
        holder.cardView!!.setBackgroundResource(R.drawable.album_shape_gray)
        val item = list[position]
        holder.title!!.text = item.title
        holder.cover!!.setImageBitmap(file2Bitmap(context, item.filename))
        //  holder.bg.setBackgroundColor(getRandomColorCode());
        return view
    }

    val randomColorCode: Int
        get() {
            val random = Random()
            return Color.argb(255, random.nextInt(256), random.nextInt(256), random.nextInt(256))
        }

    override fun getCount(): Int {
        // TODO Auto-generated method stub
        return list.size
        //return 0;
    }

    override fun getItem(arg0: Int): Any {
        // TODO Auto-generated method stub
        return list[arg0]
    }

    override fun getItemId(arg0: Int): Long {
        // TODO Auto-generated method stub
        return arg0.toLong()
    }
}
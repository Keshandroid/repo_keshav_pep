package com.app.discountro.ui.account.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.MItemTransactionsBinding

class AllWalletTransactionsAdapter(val context: Context, val allTransactionList: ArrayList<String>, val listener: TransactionOnClickListener): RecyclerView.Adapter<AllWalletTransactionsAdapter.ViewHolder>()  {

    class ViewHolder(val binding: MItemTransactionsBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(MItemTransactionsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.root.setOnClickListener {
            listener.transactionOnClick("")
        }
        holder.binding.cardTransaction.stateListAnimator = null

    }

    override fun getItemCount(): Int {
        return  50
    }

    interface TransactionOnClickListener{
        fun transactionOnClick(data: String)
    }
}
package com.app.discountro.ui.welcome.view

import android.app.Activity
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Html.FROM_HTML_MODE_LEGACY
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.app.discountro.R
import com.app.discountro.base.extentions.encodeToHtml
import com.app.discountro.common.constant.AppConstant
import com.app.discountro.databinding.ActivityWelcomeBinding
import com.app.discountro.ui.home.view.BrowserActivity
import com.app.discountro.ui.login.view.LoginActivity
import com.app.discountro.ui.splash.model.MyTranslationVarModel
import com.app.discountro.utils.FireStorageKeys
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.SharedPrefsUtils
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase


class WelcomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityWelcomeBinding

    private var auth: FirebaseAuth = Firebase.auth
    private lateinit var googleSignInClient: GoogleSignInClient
    private val appConstant: AppConstant = AppConstant()
    val db = Firebase.firestore
    private var gLoginUserName: String = ""
    private var socialType: String = ""
    private lateinit var callbackManager: CallbackManager

    private lateinit var myTranslationVarModel: MyTranslationVarModel
    private lateinit var myTransDataWelcomeActivity: MyTranslationVarModel
    private val myTranslationArrayData: ArrayList<MyTranslationVarModel> = arrayListOf()


    private val resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            Log.e("Sunny", "ResultCode: ${result.resultCode}")
            Log.e("Sunny", "ActivityResult: ${Activity.RESULT_OK} ")
            if (result.resultCode == Activity.RESULT_OK) {
                val data = result.data
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    val account = task.getResult(ApiException::class.java)!!
                    Toast.makeText(
                        this,
                        myTransDataWelcomeActivity.success_login.ifEmpty {
                            resources.getString(R.string.success_login)
                        },
                        Toast.LENGTH_SHORT
                    ).show()
                    // Google Sign In was successful, authenticate with Firebase
                    Log.e("AccountDetails:-", "Google ID ${account.id}")

                    Log.e("AccountDetails:-", "Google familyName ${account.familyName}")
                    Log.e("AccountDetails:-", "Google LastName ${account.givenName}")
                    gLoginUserName =
                        account.givenName.toString() + " " + account.familyName.toString()
                    firebaseAuthWithGoogle(account.idToken!!)
                } catch (e: ApiException) {
                    appConstant.dismissDiscRoProgress()
                    Log.w("Exception", "Google sign in failed", e)
                }
            } else {
                appConstant.dismissDiscRoProgress()
            }
        }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWelcomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getTranslationData()

        initView()
        // binding.textView3.text = tFireVar?.welcome_to
    }

    private fun getTranslationData() {
        Log.e(
            "MySharePrefDataValue", "getTranslationData: ${
                SharedPrefsUtils.getModelPreferences(
                    KeysUtils.KeyModelTranslation,
                    MyTranslationVarModel::class.java
                )
            }"
        )
        myTranslationVarModel = SharedPrefsUtils.getModelPreferences(
            KeysUtils.KeyModelTranslation,
            MyTranslationVarModel::class.java
        ) as MyTranslationVarModel
        myTranslationArrayData.add(myTranslationVarModel)
        myTranslationArrayData[0]
    }

    private fun initView() {

        setTranslationDataFromPref()
        facebookFireLogin()
        firebaseGoogleLogin()
        buttonIntent()

    }

    private fun setTranslationDataFromPref() {
        myTransDataWelcomeActivity = myTranslationArrayData[0]

        binding.tvWelcomeTo.text = myTransDataWelcomeActivity.welcome_to


        val fireFetchDataString =
            myTransDataWelcomeActivity.your_favourite_foods_delivered_fast_at_your_door.encodeToHtml()
        val convertFireCDATA: String =
            Html.fromHtml(fireFetchDataString.toString(), FROM_HTML_MODE_LEGACY).toString()

        binding.tvTitleDesc.text = convertFireCDATA

        binding.tvSignInWith.text = myTransDataWelcomeActivity.sign_in_with
        binding.clFacebookLogin.text = myTransDataWelcomeActivity.facebook
        binding.clGoogleLogin.text = myTransDataWelcomeActivity.google
        binding.tvStartWithEmail.text = myTransDataWelcomeActivity.start_with_email_or_phone
        binding.tvAlreadyAc.text = myTransDataWelcomeActivity.already_have_an_account

    }


    private fun facebookFireLogin() {
        FacebookSdk.fullyInitialize()
//        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(application)
        fireFacebookLogin()
        onFbButtonClickListener()
    }

    private fun onFbButtonClickListener() {
        binding.clFacebookLogin.setOnClickListener {
            appConstant.showDiscoRoProgress(this@WelcomeActivity)
            binding.lbFacebookLoginBtn.performClick()
        }
    }

    private fun fireFacebookLogin() {
        callbackManager = CallbackManager.Factory.create()
        binding.lbFacebookLoginBtn.setPermissions("email", "public_profile")
        binding.lbFacebookLoginBtn.registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult) {
                    Log.d("FacebookSunny", "facebook:onSuccess:$result")
                    handleFacebookAccessToken(result.accessToken)
                }

                override fun onCancel() {
                    appConstant.dismissDiscRoProgress()
                    Log.d("FacebookSunny", "facebook:onCancel")
                }

                override fun onError(error: FacebookException) {
                    appConstant.dismissDiscRoProgress()
                    Log.d("FacebookSunny", "facebook:onError", error)
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Pass the activity result back to the Facebook SDK
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d("FacebookAccessToken", "handleFacebookAccessToken:$token")
        val credential = FacebookAuthProvider.getCredential(token.token)
        auth.setLanguageCode(SharedPrefsUtils.getStringPreference(KeysUtils.keyLangISO))
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Toast.makeText(
                        this,
                        myTransDataWelcomeActivity.success_login.ifEmpty {
                            resources.getString(R.string.success_login)
                        },
                        Toast.LENGTH_SHORT
                    ).show()
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("facebookAccessToken", "signInWithCredential:success")
                    val user = auth.currentUser
                    socialType = KeysUtils.KeyFirefacebook
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("facebookAccessToken", "signInWithCredential:failure", task.exception)
                    Toast.makeText(
                        baseContext,
                        myTransDataWelcomeActivity.authantication_fail.ifEmpty {
                            resources.getString(R.string.authantication_fail)
                        },
                        Toast.LENGTH_SHORT
                    ).show()
                    updateUI(null)
                }
            }.addOnFailureListener {
                LoginManager.getInstance().logOut()
                Toast.makeText(
                    this@WelcomeActivity,
                    it.localizedMessage?.toString() ?: it.message,
                    Toast.LENGTH_LONG
                ).show()
                Log.e("MyFbErrorsss", "handleFacebookAccessToken: ${it.message}")
                appConstant.dismissDiscRoProgress()
            }
    }

    private fun firebaseGoogleLogin() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)
    }


    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)

        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    socialType = KeysUtils.KeyFireGoogle
                    // Sign in success, update UI with the signed-in user's information
                    Log.e("SunnyPatel", "signInWithCredential:success")
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.e("SunnyPatel", "signInWithCredential:failure", task.exception)
                    updateUI(null)
                }
            }.addOnFailureListener {
                appConstant.dismissDiscRoProgress()
            }
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            Log.e("FireAuAccountDetails:-", "Google ID ${user.uid}")
            Log.e("FireAuAccountDetails:-", "Google LastName ${user.email}")
            Log.e("FireAuAccountDetails:-", "Google ProfilePicURL ${user.photoUrl}")
            Log.e("FireAuAccountDetails:-", "Google IDToken ${user.providerId}")
            try {
                fireStoreCreateUsers(user)
            } catch (e: Exception) {
                Log.e("fireStoreCreateUsers", "updateUI: ", e)
            }
        }
        appConstant.dismissDiscRoProgress()
    }

    private fun fireStoreCreateUsers(user: FirebaseUser?) {

        gLoginUserName.ifEmpty {
            gLoginUserName = ""
        }
        gLoginUserName.ifBlank {
            gLoginUserName = ""
        }
        if (user != null) {

            val userTable = hashMapOf(
                FireStorageKeys.KEYUT_ID_USER to user.uid,
                //FireStorageKeys.KEYUT_ID_NAME to gLoginUserName,
                FireStorageKeys.KEYUT_P_TOKEN to "",
                FireStorageKeys.KEYUT_EMAIL to user.email,
                FireStorageKeys.KEYUT_PWD to "",
                //FireStorageKeys.KEYUT_DB_REG to "",
                FireStorageKeys.KEYUT_ACTIVE to "",
                FireStorageKeys.KEYUT_SOCIAL_TYPE to socialType
            )


            //OLD CHANGE
            /*db.collection(FireStorageKeys.KEY_USER_TABLE).document(user.uid).set(userTable)
                .addOnSuccessListener {
                    Log.e("userAuthFireStorage", "Success: $it")
                }.addOnFailureListener {
                    LoginManager.getInstance().logOut()
                    Log.e("userAuthFireStorage", "Fail: ${it.message}")
                }*/

            //NEW CHANGE AD
            db.collection(FireStorageKeys.KEY_USER_TABLE).document(user.uid).set(userTable, SetOptions.merge())
                .addOnSuccessListener {
                    Log.e("userAuthFireStorage", "Success: $it")
                }.addOnFailureListener {
                    LoginManager.getInstance().logOut()
                    Log.e("userAuthFireStorage", "Fail: ${it.message}")
                }
        }
        intentToMainScreen(user)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    private fun intentToMainScreen(user: FirebaseUser?) {
        if (user != null) {
            SharedPrefsUtils.setStringPreference(KeysUtils.KeyUUID, user.uid)
            Log.e("welcomeActivity", "BrowserActivity::class.java 4")

            val intent = Intent(this@WelcomeActivity, BrowserActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.

        val currentUser = auth.currentUser
        if (currentUser != null) {
            updateUI(currentUser)
        }
        //   revokeAccess()
    }

    private fun buttonIntent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            binding.tvSignIn.text = Html.fromHtml(
                myTransDataWelcomeActivity.sign_in.encodeToHtml().toString(),
                FROM_HTML_MODE_LEGACY
            )
            binding.tvSignIn.setOnClickListener {
                intentLoginActivity()
            }
        }
        binding.clStartWthEmail.setOnClickListener {
            intentLoginActivity()
        }
        binding.clGoogleLogin.setOnClickListener {
            appConstant.showDiscoRoProgress(this@WelcomeActivity)
            val signInIntent = googleSignInClient.signInIntent
            resultLauncher.launch(signInIntent)
            revokeAccess()
        }

    }

    private fun revokeAccess() {
        googleSignInClient.revokeAccess().addOnCompleteListener(this) {}
    }

    private fun intentLoginActivity() {
        val signUpIntent = Intent(this@WelcomeActivity, LoginActivity::class.java)
        startActivity(signUpIntent)
        finish()
    }

}
package com.app.discountro.ui.product.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.CompareItemLayoutBinding
import com.app.discountro.fireStore.models.SellerModel
import com.app.discountro.ui.home.view.interfaces.CompareProductInterface
import com.app.discountro.ui.product.model.ProductModel
import com.bumptech.glide.Glide
import com.github.marlonlom.utilities.timeago.TimeAgo
import java.sql.Date
import java.sql.Timestamp
import java.text.SimpleDateFormat

class CompareProductsAdapter(
    val context: Context,
    private val compareProductInterface: CompareProductInterface,
    private val dataList: ArrayList<ProductModel.SellerList?>?
) :
    RecyclerView.Adapter<CompareProductsAdapter.ViewHolder>() {

    class ViewHolder(val mBinding: CompareItemLayoutBinding) : RecyclerView.ViewHolder(mBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CompareItemLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = dataList!!.get(position)

        Glide.with(context).load(data!!.store_logo).into(holder.mBinding.imgWebsiteLogo)



        if(!data.timestamp_updated.isNullOrEmpty()){
            try {
                val sdf = SimpleDateFormat("MM/dd/yyyy")
                val netDate = Date(data.timestamp_updated.toLong() * 1000)

                val date = SimpleDateFormat("MM/dd/yyyy").parse(sdf.format(netDate))
                if (date != null) {
                    holder.mBinding.tvUpdateTime.setText(TimeAgo.using(date.time))
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }


        holder.mBinding.tvUsed.text = data.condition
        holder.mBinding.tvPrice.text = data.currency + " " + data.price
        holder.mBinding.tvFreeShipping.text = data.comment


        holder.mBinding.layoutDetail.setOnClickListener{
            val directLink = data!!.direct_link
            compareProductInterface.onItemClicked(holder.itemView, directLink!!)
        }


    }
    override fun getItemCount(): Int {
        return dataList!!.size
    }
}
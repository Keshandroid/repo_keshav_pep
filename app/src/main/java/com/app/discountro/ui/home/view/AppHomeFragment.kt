package com.app.discountro.ui.home.view

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.telephony.TelephonyManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.discountro.Interface.FireStoreUserDataInterface
import com.app.discountro.R
import com.app.discountro.base.extentions.showSnackBar
import com.app.discountro.common.sqlite.offerCoupons.CopiedCouponsModel
import com.app.discountro.common.sqlite.offerCoupons.DBHelperOffer
import com.app.discountro.databinding.FragmentAppHomeBinding
import com.app.discountro.fireStore.helper.FireStoreHelper
import com.app.discountro.fireStore.interfaces.FireStoreListener
import com.app.discountro.fireStore.models.CouponsAllModel
import com.app.discountro.fireStore.models.RoModel
import com.app.discountro.fireStore.models.UserDataModel
import com.app.discountro.fireStore.services.FireStoreServiceCall
import com.app.discountro.ui.home.model.FeaturedModel
import com.app.discountro.ui.home.model.RoModelBackupModel
import com.app.discountro.ui.home.view.adapter.*
import com.app.discountro.ui.home.view.models.AllStoreModel
import com.app.discountro.ui.home.view.models.CountryCouponsModel
import com.app.discountro.ui.home.view.models.FavoriteStore
import com.app.discountro.ui.home.view.models.UserModel
import com.app.discountro.utils.FireStorageKeys
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView
import java.util.*


class AppHomeFragment : Fragment(), AppFavoriteStoresAdapter.StoresCouponsListener,
    AppCouponsAdapter.AppCouponsClickListener,
    AppAllCouponsAdapter.AppCouponsClickListener,
    AppAllStoreAdapter.SelectedStoresInterface {
    val TAG = AppHomeFragment::class.java.simpleName
    private lateinit var binding: FragmentAppHomeBinding
    private var browserActivity: BrowserActivity? = null
    private lateinit var appFavoriteStoresAdapter : AppFavoriteStoresAdapter
    private lateinit var couponsAdapter : AppCouponsAdapter

    private lateinit var allCouponsAdapter: AppAllCouponsAdapter

    private lateinit var appAllStoreAdapter: AppAllStoreAdapter
    private lateinit var tooHotToMissAdapter : TooHotToMissAdapter
    private lateinit var homeCouponsDealsAdapter : HomeCouponsDealAdapter
    val allStores: MutableList<AllStoreModel> = mutableListOf()
    //filter list for all stores is that item already checked
    val filterAllStores: MutableList<AllStoreModel> = mutableListOf()
    lateinit var favoriteStoreBottomSheet: BottomSheetDialog
    lateinit var couponBottomSheet: BottomSheetDialog
    var rvAllStores: FastScrollRecyclerView?=null
    private var favoriteStores: ArrayList<FavoriteStore> = ArrayList()
    private val auth: FirebaseAuth = Firebase.auth
    private var couponsAllModel: CouponsAllModel? = null
    private var couponsDealsAllModel: CouponsAllModel? = null
    private val countryList = ArrayList<CountryCouponsModel>()
    private var selectedCountry = ""
    private var isSimilarCoupons = false
    var roBackUpObjectsList = ArrayList<RoModelBackupModel>()
    private var roModel: RoModel? = null
    private var roModelAllCouponsDeal: RoModel? = null
    private var finalList: ArrayList<RoModel.CouponModel> = ArrayList()
    private var couponsDealList: ArrayList<RoModel.CouponModel> = ArrayList()
    private lateinit var offersDBHelper: DBHelperOffer
    private var userDataModel: UserDataModel? = null
    private var tooHotToMissList : ArrayList<FeaturedModel> = ArrayList()


    //ALL COUPONS
    private var newRoModel: RoModel? = null
    private var newroBackUpObjectsList: ArrayList<RoModelBackupModel> = ArrayList()

    private var allFinalList: ArrayList<RoModel.CouponModel> = ArrayList()




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        offersDBHelper = DBHelperOffer(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentAppHomeBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        browserActivity = activity as BrowserActivity?

        initClick()
        firebaseGetUserData()
        setAdapter()
        getFavoriteStores()
        getAllStores()
        getTooHotToMiss()

        //AD CHANGE
        getAllStoreCoupons()

        // Coupons Deals store collection to some data to get the error for not proper dataFiled
//        allCouponsDeals()
    }

    private fun initClick() {
        binding.ivAdd.setOnClickListener {
            favoriteStoreBottomSheet()
        }
        binding.rlAddStore.setOnClickListener {
            favoriteStoreBottomSheet()
        }
    }

    private fun firebaseGetUserData() {
        Log.e("FireStore", "user id preference: ${auth.currentUser?.uid} " )
        FireStoreHelper().getUserData(
            auth.currentUser!!.uid,
            object: FireStoreUserDataInterface {
                override fun onComplete(model: UserDataModel) {
                    userDataModel = model
                    var fullName = model.user_name.split(" ")
                    if(fullName.isNotEmpty() && fullName.size >= 2 ) {
                        browserActivity?.appbarTitle?.text = "Hey ${fullName[0]},"
                    }
                }

                override fun onError(error: String) {
                    Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    private fun setAdapter() {
        appFavoriteStoresAdapter = AppFavoriteStoresAdapter(requireContext(), favoriteStores, this)
        binding.rvStores.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.rvStores.adapter = appFavoriteStoresAdapter

        tooHotToMissAdapter = TooHotToMissAdapter(requireContext(), tooHotToMissList)
        binding.rvTooHotToMiss.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.rvTooHotToMiss.adapter = tooHotToMissAdapter

        couponsAdapter = AppCouponsAdapter(requireContext(), finalList, this, couponsAllModel?.logoUrl)
        binding.rvCoupons.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.rvCoupons?.adapter = couponsAdapter

        allCouponsAdapter = AppAllCouponsAdapter(requireContext(), allFinalList, this, couponsAllModel?.logoUrl)
        binding.rvCouponsDeals.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        binding.rvCouponsDeals?.adapter = allCouponsAdapter

    }

    override fun favoriteStoreClick(data: FavoriteStore) {
//        val bundle = Bundle()
//        bundle.putSerializable(KEY_STORE_MODEL, data)
//        browserActivity?.setFragment(StoreProfileFragment(), "StoreProfileFragment", bundle)

        getCoupons(data)


    }

    fun getAllStoreCoupons(){

        Firebase.firestore.collection(FireStoreHelper.PARENT_STORE_KEY).get()
            .addOnSuccessListener { documents ->

                allFinalList.clear()
                for (document in documents) {

                    val jsonData = convertToJson(document.data)
                    couponsAllModel = Gson().fromJson(
                        jsonData.toString(),
                        CouponsAllModel::class.java
                    )


                    if (couponsAllModel != null) {

                        for (i in 0 until couponsAllModel?.countries?.size!!) {

                            val countryName = couponsAllModel!!.countries?.get(i).toString()
                            selectedCountry = couponsAllModel!!.countries?.get(i).toString()
                            val couponsListDD = couponsAllModel?.coupons?.get(selectedCountry)
                            addNewToRoModel(couponsListDD, isSimilarCoupons, countryName,
                                couponsAllModel!!.logoUrl)

                        }
                    }




                }

                setNewCouponRecycler()


            }





    }

    fun getCoupons(favoriteStoreData: FavoriteStore){
        FireStoreHelper().getDataListTemp(favoriteStoreData.storeName!!, object : FireStoreListener {
            override fun onComplete(response: String) {
                couponsAllModel = Gson().fromJson(
                    response,
                    CouponsAllModel::class.java
                )
                if (couponsAllModel != null) {
                    showCouponsRecycleView(true, favoriteStoreData.logoUrl!!, favoriteStoreData.storeName!!)

                    countryList.clear()
                    for (i in 0 until couponsAllModel?.countries?.size!!) {

                        val countryName = (couponsAllModel!!.countries?.get(i).toString())
                        if (i == 0) {
                            countryList.add(
                                CountryCouponsModel(
                                    countryName, couponsAllModel!!.countries?.get(i).toString(), true
                                )
                            )
                            selectedCountry = couponsAllModel!!.countries?.get(i).toString()
                            val couponsListDD = couponsAllModel?.coupons?.get(selectedCountry)
                            addToRoModel(couponsListDD, isSimilarCoupons, countryName)
                            setCouponRecycler(
                                roModel?.couponList!!,
                                isSimilarCoupons,
                                favoriteStoreData.storeName!!
                            )
                        } else {
                            countryList.add(
                                CountryCouponsModel(
                                    countryName, couponsAllModel!!.countries?.get(i).toString(), false
                                )
                            )
                        }
                    }
                } else {
                    showCouponsRecycleView(false, favoriteStoreData.logoUrl!!, favoriteStoreData.storeName!!)
                }
            }

            override fun onError(error: String) {
                FireStoreServiceCall.responseError.postValue(error)
                showCouponsRecycleView(false, favoriteStoreData.logoUrl!!, favoriteStoreData.storeName!!)
            }
        })
    }

    fun showCouponsRecycleView(isShowCouponsList: Boolean, logoUrl: String, title: String){
        if(isShowCouponsList){
            binding.rvCoupons.visibility = VISIBLE
            binding.includeNoCoupons.cvNoCouponsRoot.visibility = GONE
        }else{
            binding.includeNoCoupons.cvNoCouponsRoot.visibility = VISIBLE
            binding.rvCoupons.visibility = GONE
            Glide.with(requireContext()).load(logoUrl).into(binding.includeNoCoupons.ivProfile)
            binding.includeNoCoupons.tvTitle.text = title
        }
    }

    fun newShowCouponsRecycleView(isShowCouponsList: Boolean){
        if(isShowCouponsList){
            binding.rvCoupons.visibility = VISIBLE
            binding.includeNoCoupons.cvNoCouponsRoot.visibility = GONE
        }else{
            binding.includeNoCoupons.cvNoCouponsRoot.visibility = VISIBLE
            binding.rvCoupons.visibility = GONE
            //Glide.with(requireContext()).load(logoUrl).into(binding.includeNoCoupons.ivProfile)
            //binding.includeNoCoupons.tvTitle.text = title
        }
    }

    private fun addToRoModel(couponsListDD: Any?, isSimilarCoupan: Boolean, countryName: String) {

        /*val listData =
            roBackUpObjectsList.filter { it.isSimilarCoupon == isSimilarCoupan && it.countryName == countryName }

        if (listData != null && listData.isNotEmpty()) {
            roModel = listData[0].data
            return
        }*/

        roModel = if (couponsListDD != null) {
            val gson = Gson()
            val toJson = gson.toJson(couponsListDD)
            gson.fromJson(toJson, RoModel::class.java)

        } else {
            null
        }

        if (roModel != null) {
            val roModelBackupModel = RoModelBackupModel()
            roModelBackupModel.data = roModel
            roModelBackupModel.countryName = countryName
            roModelBackupModel.isSimilarCoupon = isSimilarCoupan
            roBackUpObjectsList.clear()
            roBackUpObjectsList.add(roModelBackupModel)
        }

    }

    private fun addNewToRoModel(
        couponsListDD: Any?,
        isSimilarCoupan: Boolean,
        countryName: String,
        logoUrl: String?
    ) {

        /*val listData =
            roBackUpObjectsList.filter { it.isSimilarCoupon == isSimilarCoupan && it.countryName == countryName }

        if (listData != null && listData.isNotEmpty()) {
            roModel = listData[0].data
            return
        }*/

        newRoModel = if (couponsListDD != null) {
            val gson = Gson()
            val toJson = gson.toJson(couponsListDD)
            gson.fromJson(toJson, RoModel::class.java)


        } else {
            null
        }

//        Log.d("DataModel",""+GsonBuilder().setPrettyPrinting().create().toJson(newRoModel))

        if (newRoModel != null) {
            val roModelBackupModel = RoModelBackupModel()
            roModelBackupModel.data = newRoModel
            roModelBackupModel.countryName = countryName
            roModelBackupModel.isSimilarCoupon = isSimilarCoupan
            newroBackUpObjectsList.add(roModelBackupModel)
            newSetDataToList(newRoModel?.couponList!!,logoUrl)

        }


    }

    private fun newSetDataToList(couponList: ArrayList<RoModel.CouponModel?>, logoUrl: String?){


        try {
            @Suppress("UNCHECKED_CAST") val list = couponList as ArrayList<RoModel.CouponModel>


            for (mList in list){
                val cModel = RoModel.CouponModel()
                cModel.logo_url = logoUrl
                cModel.offerUrl = mList.offerUrl
                cModel.offerText = mList.offerText
                cModel.code = mList.code
                cModel.idOffer = mList.idOffer
                cModel.affiliateUrl = mList.affiliateUrl

                cModel.offerDetails = mList.offerDetails
                cModel.offerName = mList.offerName
                cModel.isCopied = mList.isCopied
                cModel.offerType = mList.offerType


                allFinalList.add(cModel)
            }




        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    private fun setCouponRecycler(
        couponList: ArrayList<RoModel.CouponModel?>,
        isSimilarCoupon: Boolean,
        websiteNameIs: String
    ) {
        @Suppress("UNCHECKED_CAST") val list = couponList as ArrayList<RoModel.CouponModel>

        try {

//            if (websiteNameIs == oldWebsite && coupanAdapter != null) {
            if (false) {
//                coupanAdapter!!.notifyDataSetChanged()
                binding.rvCoupons.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
                binding.rvCoupons.adapter = couponsAdapter
            } else {

//                finalList = checkCopiedValues(list, offersList)

                finalList.clear()
                finalList.addAll(list)
                Log.e(TAG, "setCouponRecycler finallist: $finalList")
                couponsAdapter.setNewListAndRefresh(finalList, couponsAllModel?.logoUrl!!)

            }

        } catch (ex: Exception) {
        }

    }


    private fun setNewCouponRecycler() {

        try {
            Log.e(TAG, "setCouponRecycler finallist: $allFinalList")
            binding.rvCouponsDeals.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            binding.rvCouponsDeals.adapter = allCouponsAdapter
            allCouponsAdapter.setNewListAndRefresh(allFinalList, "ALL_COUPONS")


        } catch (ex: Exception) {
            Log.e(TAG, ""+ex.printStackTrace())
        }

    }


    private fun getAllStores(){
        //GET DATA FROM FIREBASE
        allStores.clear()
        FireStoreHelper.db?.collection(FireStoreHelper.PARENT_STORE_KEY)
            ?.get()
            ?.addOnSuccessListener { firstResult ->
                try {

                    for (document in firstResult) {
//                        Log.d("ALL_ST",""+document.id)

                        val allStoreModel = AllStoreModel(
                            document.id,
                            document.get("cashback").toString(),
                            false,
                            document.get("logo_url").toString()
                        )


                        allStores.add(allStoreModel) // All stores names eg. admin.ro, airbnb.com etc...

                        //ALL STORES adapter
//                        allStoresAdapter = AllStoreAdapter(this,allStores,this)
//                        rvAllStores!!.adapter = allStoresAdapter
//                        rvAllStores!!.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)


                    }
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }?.addOnFailureListener {
                Log.d("onFailed",""+it.message.toString())
            }
    }

     private fun couponBottomSheet(data: RoModel.CouponModel, position: Int) {
         couponBottomSheet = BottomSheetDialog(requireContext(), R.style.AppBottomSheetDialogTheme)
         couponBottomSheet.setContentView(R.layout.bottomsheet_coupon)
         val ivClose = couponBottomSheet.findViewById<ImageView>(R.id.ivClose)
         val ivProfile = couponBottomSheet.findViewById<ImageView>(R.id.ivProfile)
         val tvTitle = couponBottomSheet.findViewById<TextView>(R.id.tvTitle)
         val tvDescription = couponBottomSheet.findViewById<TextView>(R.id.tvDescription)
         val btnCopyCoupon = couponBottomSheet.findViewById<AppCompatButton>(R.id.btnCopyCoupon)

         ivProfile?.let {
             Glide.with(couponBottomSheet.context).load(couponsAllModel?.logoUrl!!).into(ivProfile)
         }

         tvTitle?.text = data.offerUrl
         tvDescription?.text = data.offerName

         ivClose?.setOnClickListener {
             couponBottomSheet.dismiss()
         }

         btnCopyCoupon?.setOnClickListener {
             val codeCopy = finalList[position].code
             val clipboard =
                 requireActivity().getSystemService(AppCompatActivity.CLIPBOARD_SERVICE) as ClipboardManager
             val clip = ClipData.newPlainText("Coupon", codeCopy)
             clipboard.setPrimaryClip(clip)

//                    view.mySnack("Coupon Copied"){}


             val snackBar = showSnackBar(
                 requireActivity(),
                 browserActivity?.cvParentLayout!!,
                 Snackbar.LENGTH_SHORT,
                 "Coupon Copied",
                 finalList[position].offerUrl ?: "url not available",
                 couponsAllModel?.logoUrl!!
             )
             snackBar.show()

             offersDBHelper.insertOffer(
                 CopiedCouponsModel(
                     finalList[position].idOffer.toString(),
                     finalList[position].code.toString(),
                     finalList[position].offerUrl ?: "",
                     System.currentTimeMillis().toString()
                 )
             )

             val viewIntent = Intent(
                 "android.intent.action.VIEW",
                 Uri.parse(data.affiliateUrl)
             )
             startActivity(viewIntent)
         }

         couponBottomSheet.show()
     }

    private fun couponBottomSheetAll(data: RoModel.CouponModel, position: Int) {
        couponBottomSheet = BottomSheetDialog(requireContext(), R.style.AppBottomSheetDialogTheme)
        couponBottomSheet.setContentView(R.layout.bottomsheet_coupon)
        val ivClose = couponBottomSheet.findViewById<ImageView>(R.id.ivClose)
        val ivProfile = couponBottomSheet.findViewById<ImageView>(R.id.ivProfile)
        val tvTitle = couponBottomSheet.findViewById<TextView>(R.id.tvTitle)
        val tvDescription = couponBottomSheet.findViewById<TextView>(R.id.tvDescription)
        val btnCopyCoupon = couponBottomSheet.findViewById<AppCompatButton>(R.id.btnCopyCoupon)

        ivProfile?.let {
            Glide.with(couponBottomSheet.context).load(allFinalList[position].logo_url).into(ivProfile)
        }

        tvTitle?.text = data.offerUrl
        tvDescription?.text = data.offerName

        ivClose?.setOnClickListener {
            couponBottomSheet.dismiss()
        }

        btnCopyCoupon?.setOnClickListener {
            val codeCopy = allFinalList[position].code
            val clipboard =
                requireActivity().getSystemService(AppCompatActivity.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("Coupon", codeCopy)
            clipboard.setPrimaryClip(clip)

//                    view.mySnack("Coupon Copied"){}


            val snackBar = showSnackBar(
                requireActivity(),
                browserActivity?.cvParentLayout!!,
                Snackbar.LENGTH_SHORT,
                "Coupon Copied",
                allFinalList[position].offerUrl ?: "url not available",
                couponsAllModel?.logoUrl!!
            )
            snackBar.show()

            offersDBHelper.insertOffer(
                CopiedCouponsModel(
                    allFinalList[position].idOffer.toString(),
                    allFinalList[position].code.toString(),
                    allFinalList[position].offerUrl ?: "",
                    System.currentTimeMillis().toString()
                )
            )

            val viewIntent = Intent(
                "android.intent.action.VIEW",
                Uri.parse(data.affiliateUrl)
            )
            startActivity(viewIntent)
        }

        couponBottomSheet.show()
    }

    private fun favoriteStoreBottomSheet() {
        favoriteStoreBottomSheet =
            BottomSheetDialog(requireContext(), R.style.AppBottomSheetDialogTheme)
        favoriteStoreBottomSheet.setContentView(R.layout.app_store_bottomsheet_layout)
        val ivClose = favoriteStoreBottomSheet.findViewById<ImageView>(R.id.ivClose)
        val etSearchStores = favoriteStoreBottomSheet.findViewById<EditText>(R.id.etSearch)!!
        rvAllStores = favoriteStoreBottomSheet.findViewById<FastScrollRecyclerView>(R.id.rvAllStores)
        val btnSaveStore: AppCompatButton? = favoriteStoreBottomSheet.findViewById<AppCompatButton>(R.id.btnSaveStore)
        favoriteStoreBottomSheet.behavior.state = BottomSheetBehavior.STATE_EXPANDED

        // filter list of remove already favorite list from all store listing.
        filterAllStores.clear()

        for (i in allStores) {
            for (j in favoriteStores) {
                if (i.storeName == j.storeName) {
                    filterAllStores.add(i)
                }
            }
        }

        // remove from all store in already to added
        /*for(i in filterAllStores){
            allStores.remove(i)
        }*/

        // Checked the already favorite store in all stores.
        for(i in filterAllStores){
            if(i in allStores){
                allStores[allStores.indexOf(i)].checked = true
            }
        }


        appAllStoreAdapter = AppAllStoreAdapter(requireContext(), allStores,this)
        rvAllStores!!.adapter = appAllStoreAdapter
        rvAllStores!!.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)


        etSearchStores.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                filter(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        ivClose?.setOnClickListener {
            favoriteStoreBottomSheet.dismiss()
        }

        btnSaveStore?.setOnClickListener{

            if(!allStores.isNullOrEmpty()){
                val selectedStores: ArrayList<AllStoreModel> = ArrayList()

                for (i in 0 until allStores.size) {
                    if(allStores[i].checked == true) {
                        selectedStores.add(allStores[i])
                    }
                }

                if (!selectedStores.isNullOrEmpty()){
                    Log.d("SEL_STORE", GsonBuilder().setPrettyPrinting().create().toJson(selectedStores))
                    favoriteStoreBottomSheet.dismiss()
                    addToFavoriteFirebase(selectedStores)


                }else{
                    Toast.makeText(requireContext(),"Please select at least one store", Toast.LENGTH_SHORT).show()
                }

            }

        }

        favoriteStoreBottomSheet.show()
    }

    private fun filter(text: String) {
        val filteredArray: ArrayList<AllStoreModel> = ArrayList()
        for (i in 0 until allStores.size) {
            if(allStores.get(i).storeName.toString().toLowerCase(Locale.getDefault())
                    .contains(text.lowercase(Locale.getDefault()))){
                filteredArray.add(allStores.get(i))
            }
        }

        //calling a method of the adapter class and passing the filtered list
        appAllStoreAdapter.filterList(filteredArray)
    }

    private fun getTooHotToMiss(){
        //GET DATA FROM FIREBASE
        binding.llProgressbar.visibility = VISIBLE

        FireStoreHelper.db?.collection(FireStorageKeys.KEYUT_FEATURED)
            ?.get()
            ?.addOnSuccessListener { result ->
                try {
                    if(!tooHotToMissList.isNullOrEmpty()){
                        tooHotToMissList.clear()
                        tooHotToMissAdapter.notifyDataSetChanged()
                    }

                    for (document in result) {
                        val jsonData = Gson().toJson(document.data)
                        val featuredModel = Gson().fromJson(jsonData, FeaturedModel::class.java)
                        userDataModel?.let {

                            /** current device country code get but it not proper. It is get ("US") */
                            /*val locale = context!!.resources.configuration.locale.country
                            Log.d(TAG, "getTooHotToMiss device current county code: $locale")*/


                            /** get current country code using telephony manager it is get ok ("in") */
                            val tm = requireContext().getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                            val countryCodeValue = tm.networkCountryIso

                            Log.d(TAG, "getTooHotToMiss device current county code 1: $countryCodeValue")

                            if(!countryCodeValue.isNullOrBlank()){

                                /**
                                 * TODO : -  When creating real build to unComment this code
                                 */
                                /*if(countryCodeValue.lowercase() == featuredModel.country_code!!.lowercase()){
                                    tooHotToMissList.add(featuredModel)
                                }*/


                                /**
                                 * TODO :- Only when testing the get the too hot too miss data
                                 */
                                if("ro" == featuredModel.country_code!!.lowercase()){
                                    tooHotToMissList.add(featuredModel)
                                }
                            }
                        }
                    }

                    tooHotToMissAdapter.notifyDataSetChanged()
                    if(tooHotToMissList.isEmpty()){
                        binding.rvTooHotToMiss.visibility = GONE
                        binding.llNoTooHotToMiss.visibility = VISIBLE
                    }else{
                        binding.llNoTooHotToMiss.visibility = GONE
                        binding.rvTooHotToMiss.visibility = VISIBLE
                    }


                    binding.llProgressbar.visibility = GONE

//                    Log.d(TAG,""+ GsonBuilder().setPrettyPrinting().create().toJson(tooHotToMissList))


                }catch (ex:Exception){
                    binding.llProgressbar.visibility = GONE
                    ex.printStackTrace()
                }
            }?.addOnFailureListener {
                binding.llProgressbar.visibility = GONE
                Log.d("onFailed",""+it.message.toString())
            }
    }

    private fun allCouponsDeals(){
/*
        FireStoreHelper.db?.collection(FireStoreHelper.PARENT_STORE_KEY)
            ?.get()
            ?.addOnSuccessListener { result ->
                try {
                    couponsDealList.clear()

                    Log.d(TAG, "CouponsDeals AllStore Name total size" + result.size())
                    for (documentIndex in 0 until result.size()) {
                        Log.d(TAG,"CouponsDeals AllStore Name"+result.documents[documentIndex].id)

                        val jsonData = convertToJson(result.documents[documentIndex].data)

                        couponsDealsAllModel = Gson().fromJson(
                            jsonData,
                            CouponsAllModel::class.java
                        )

//                        Log.d(TAG,"CouponsDeals couponsDealsAllModel :-  $couponsDealsAllModel")

                        if (couponsDealsAllModel != null) {

                            userDataModel?.country_code?.let {

                                val couponsListDD = couponsDealsAllModel?.coupons?.get(userDataModel!!.country_code.lowercase())
//                                Log.e(TAG, "CouponsDeals DD: $couponsListDD")


                                roModelAllCouponsDeal = if (couponsListDD != null) {
                                    val gson = Gson()
                                    val toJson = gson.toJson(couponsListDD)
                                    gson.fromJson(toJson, RoModel::class.java)
                                } else {
                                    null
                                }

//                                Log.e(TAG, "CouponsDeals RoModelAllCouponsDeals: $roModelAllCouponsDeal")

                                roModelAllCouponsDeal?.couponList?.let {
                                    for (i in roModelAllCouponsDeal?.couponList!!){
                                        couponsDealList.add(i!!)
//                                        Log.e(TAG, "CouponsDeals for loop: $i")
                                    }
                                }
                            }

                        } else {
                            showCouponsDealsRecycleView(false)
                        }
                    }
                    Log.e(TAG, "allCouponsDeals List: $couponsDealList")
                    homeCouponsDealsAdapter.notifyDataSetChanged()

                    if (couponsDealList.isNotEmpty()) {
                        showCouponsDealsRecycleView(true)
                    } else {
                        showCouponsDealsRecycleView(false)
                    }

                }catch (ex:Exception){
                    Log.d(TAG, "CouponsDeals catch exception ${ex.message}")
                    showCouponsDealsRecycleView(false)
                }
            }?.addOnFailureListener {
                Log.d(TAG, "CouponsDeals onFail ${it.message}")
                showCouponsDealsRecycleView(false)
            }*/
    }

    fun showCouponsDealsRecycleView(isShowCouponsDealsList: Boolean){
        if(isShowCouponsDealsList){
            binding.rvCoupons.visibility = VISIBLE
            binding.includeNoCoupons.cvNoCouponsRoot.visibility = GONE
        }else{
            binding.includeNoCoupons.cvNoCouponsRoot.visibility = VISIBLE
            binding.rvCoupons.visibility = GONE
        }

    }

    fun getFavoriteStores(){
        //GET DATA FROM FIREBASE
        binding.llProgressbar.visibility = VISIBLE
        FireStoreHelper.db?.collection(FireStorageKeys.KEY_USER_TABLE)!!.document(auth.currentUser!!.uid)
            ?.get()
            ?.addOnSuccessListener { firstResult ->
                try {

                    val jsonData = convertToJson(firstResult.data)
                    val gsn = Gson()
                    val userModel: UserModel = gsn.fromJson<UserModel>(
                        jsonData.toString(),
                        UserModel::class.java
                    )

                    if(!favoriteStores.isNullOrEmpty()){
                        favoriteStores.clear()
                        appFavoriteStoresAdapter.notifyDataSetChanged()
                    }

                    favoriteStores.addAll(userModel.favorite_stores!!)
                    appFavoriteStoresAdapter.notifyDataSetChanged()
                    if(favoriteStores.isEmpty()){
                        binding.rlAddStoreMain.visibility = VISIBLE
                    }else{
                        binding.rlAddStoreMain.visibility = GONE
                    }

                    Log.d("FAV_STORES",""+ GsonBuilder().setPrettyPrinting().create().toJson(userModel.favorite_stores))

                    binding.llProgressbar.visibility = GONE

                }catch (ex:Exception){
                    binding.llProgressbar.visibility = GONE
                    ex.printStackTrace()
                }
            }?.addOnFailureListener {
                binding.llProgressbar.visibility = GONE
                Log.d("onFailed",""+it.message.toString())
            }
    }

    fun addToFavoriteFirebase(selectedStores: ArrayList<AllStoreModel>) {

        Log.d("C_ID",""+auth.currentUser!!.uid)
        Log.d(TAG,"selected stores  :- $selectedStores")
        val addUserToArrayMap: MutableMap<String, Any> = HashMap()
        /*for(i in 0 until selectedStores.size){
            addUserToArrayMap[FireStorageKeys.KEYUT_FAVORITE_STORES] = FieldValue.arrayUnion(
                selectedStores[i]
            )

            FireStoreHelper.db?.collection(FireStorageKeys.KEY_USER_TABLE)!!.document(auth.currentUser!!.uid)
                .update(addUserToArrayMap).addOnSuccessListener {
                    getFavoriteStores()
                }
        }*/

        addUserToArrayMap[FireStorageKeys.KEYUT_FAVORITE_STORES] = selectedStores
        FireStoreHelper.db?.collection(FireStorageKeys.KEY_USER_TABLE)!!
            .document(auth.currentUser!!.uid)
            .update(addUserToArrayMap).addOnSuccessListener {
                Toast.makeText(context,"Added to Favorite",Toast.LENGTH_SHORT).show()
                getFavoriteStores()
            }


    }

    override fun onCheckBoxClicked() {
        Log.d("CHECK_BOX",""+ GsonBuilder().setPrettyPrinting().create().toJson(allStores))
    }

    private fun convertToJson(mapData: Map<String, Any>?): String? {
        val gsn = Gson()
        return gsn.toJson(mapData)
    }

    override fun couponRootClick(data: RoModel.CouponModel, position: Int) {
        Log.e(TAG, "couponRootClick: $data")
        couponBottomSheet(data, position)
    }

    override fun onCopyCouponClick(view: View, position: Int) {
        val codeCopy = finalList[position].code
        val clipboard =
            requireActivity().getSystemService(AppCompatActivity.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Coupon", codeCopy)
        clipboard.setPrimaryClip(clip)

//                    view.mySnack("Coupon Copied"){}


        //SHOW IN SNACKBAR
        /*val snackBar = showSnackBar(
            requireActivity(),
            browserActivity?.cvParentLayout!!,
            Snackbar.LENGTH_SHORT,
            "Coupon Copied",
            finalList[position].offerUrl ?: "url not available",
            couponsAllModel?.logoUrl!!
        )
        snackBar.show()*/

        offersDBHelper.insertOffer(
            CopiedCouponsModel(
                finalList[position].idOffer.toString(),
                finalList[position].code.toString(),
                finalList[position].offerUrl ?: "",
                System.currentTimeMillis().toString()
            )
        )
    }

    override fun couponRootClickAll(data: RoModel.CouponModel, position: Int) {
        Log.e(TAG, "couponRootClick: $data")
        couponBottomSheetAll(data, position)
    }

    override fun onCopyCouponClickAll(view: View, position: Int) {
        val codeCopy = allFinalList[position].code
        val clipboard =
            requireActivity().getSystemService(AppCompatActivity.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Coupon", codeCopy)
        clipboard.setPrimaryClip(clip)

//                    view.mySnack("Coupon Copied"){}


        //SHOW IN SNACKBAR
        /*val snackBar = showSnackBar(
            requireActivity(),
            browserActivity?.cvParentLayout!!,
            Snackbar.LENGTH_SHORT,
            "Coupon Copied",
            finalList[position].offerUrl ?: "url not available",
            couponsAllModel?.logoUrl!!
        )
        snackBar.show()*/

        offersDBHelper.insertOffer(
            CopiedCouponsModel(
                allFinalList[position].idOffer.toString(),
                allFinalList[position].code.toString(),
                allFinalList[position].offerUrl ?: "",
                System.currentTimeMillis().toString()
            )
        )
    }
}
package com.app.discountro.ui.account.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.discountro.databinding.AllTransactionsFragmentBinding
import com.app.discountro.ui.account.adapter.AllWalletTransactionsAdapter
import com.app.discountro.ui.account.adapter.TransactionsCategoryAdapter
import com.app.discountro.ui.home.view.BrowserActivity

class AllTransactionsFragment : Fragment(), AllWalletTransactionsAdapter.TransactionOnClickListener {
    private lateinit var binding: AllTransactionsFragmentBinding
    private var browserActivity: BrowserActivity? = null
    private lateinit var transactionsCategoryAdapter : TransactionsCategoryAdapter
    private lateinit var allWalletTransactionsAdapter : AllWalletTransactionsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = AllTransactionsFragmentBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        browserActivity = activity as BrowserActivity?
        browserActivity?.appbarTitle?.text = "All transactions"

        setAdapter()
    }

    private fun setAdapter() {
        binding.rvTransactionCategory.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        transactionsCategoryAdapter = TransactionsCategoryAdapter(requireContext(), arrayListOf())
        binding.rvTransactionCategory.adapter = transactionsCategoryAdapter

        binding.rvAllWalletTransactions.layoutManager = LinearLayoutManager(requireContext())
        allWalletTransactionsAdapter = AllWalletTransactionsAdapter(requireContext(), arrayListOf(), this)
        binding.rvAllWalletTransactions.adapter = allWalletTransactionsAdapter

    }

    override fun transactionOnClick(data: String) {
        browserActivity?.setFragment(TransactionDetailsFragment(), "TransactionDetailsFragment", Bundle())
    }

}
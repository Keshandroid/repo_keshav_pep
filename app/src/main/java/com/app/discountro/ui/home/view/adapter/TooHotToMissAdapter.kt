package com.app.discountro.ui.home.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.ItemRowHotMissBinding
import com.app.discountro.ui.home.model.FeaturedModel
import com.bumptech.glide.Glide

class TooHotToMissAdapter(val context: Context, private val tooHotToMissList: ArrayList<FeaturedModel>) : RecyclerView.Adapter<TooHotToMissAdapter.ViewHolder>() {

    class ViewHolder(val binding: ItemRowHotMissBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemRowHotMissBinding.inflate(LayoutInflater.from(parent.context),parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context).load(tooHotToMissList[position].image_url).into(holder.binding.ivImage)
        holder.binding.tvTitle.text = tooHotToMissList[position].title
        holder.binding.tvSubTitle.text = tooHotToMissList[position].subtitle
        holder.binding.root.setOnClickListener {  }
    }

    override fun getItemCount(): Int {
        return  tooHotToMissList.size
    }
}
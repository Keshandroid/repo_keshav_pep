package com.app.discountro.ui.home.view

import android.content.Context
import android.content.ContextWrapper
import android.content.res.Resources.Theme
import com.app.discountro.utils.browser_utils.my_object.HelperUnit.applyTheme

class BrowserContextWrapper(private val context: Context) : ContextWrapper(
    context
) {
    override fun getTheme(): Theme {
        return context.theme
    }

    init {
        applyTheme(context)
    }
}
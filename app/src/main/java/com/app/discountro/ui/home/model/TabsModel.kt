package com.app.discountro.ui.home.model

import android.graphics.Bitmap
import com.app.discountro.common.album_service.AlbumController

data class TabsModel(
    var cAlbumCon: AlbumController?,
    var albumTitle: String?,
    var albumImg: Bitmap?,
    var isIncoWeb: Boolean?,
)
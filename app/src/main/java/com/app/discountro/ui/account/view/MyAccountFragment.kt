package com.app.discountro.ui.account.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.app.discountro.Interface.FireStoreUserDataInterface
import com.app.discountro.databinding.MMyAccountFragmentBinding
import com.app.discountro.fireStore.helper.FireStoreHelper
import com.app.discountro.fireStore.models.UserDataModel
import com.app.discountro.ui.create_manage_list.view.CreateManageListFragment
import com.app.discountro.ui.home.view.BrowserActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class MyAccountFragment : Fragment(){
    private lateinit var binding: MMyAccountFragmentBinding
    private var browserActivity: BrowserActivity? = null
    private val auth: FirebaseAuth = Firebase.auth
    private var userDataModel: UserDataModel? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = MMyAccountFragmentBinding.inflate(layoutInflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        browserActivity = activity as BrowserActivity?

        initClick()
        firebaseGetUserData()
    }

    private fun initClick() {
        binding.txtPersonalInfo.setOnClickListener {
            browserActivity?.setFragment(PersonalInfoFragment(), "PersonalInfoFragment", Bundle())
        }
        binding.txtWalletCash.setOnClickListener {
            browserActivity?.setFragment(WalletFragment(), "WalletFragment", Bundle())
        }
        binding.txtCreateManageList.setOnClickListener {
            browserActivity?.setFragment(CreateManageListFragment(), "CreateManageListFragment", Bundle())
        }
    }

    private fun firebaseGetUserData() {
        Log.e("FireStore", "user id preference: ${auth.currentUser?.uid} " )
        FireStoreHelper().getUserData(
            auth.currentUser!!.uid,
            object: FireStoreUserDataInterface {
                override fun onComplete(model: UserDataModel) {
                    userDataModel = model
                    var fullName = model.user_name.split(" ")
                    if(fullName.isNotEmpty() && fullName.size >= 2 ) {
                        browserActivity?.appbarTitle?.text = "${fullName[0]} account"
                    }
                }

                override fun onError(error: String) {
                    Toast.makeText(requireContext(), "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }
}
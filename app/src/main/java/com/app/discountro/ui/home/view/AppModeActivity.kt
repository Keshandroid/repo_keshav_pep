package com.app.discountro.ui.home.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import com.app.discountro.R
import com.app.discountro.ui.account.view.MyAccountFragment
import com.app.discountro.ui.notification.view.AppModeNotificationFragment
import com.app.discountro.ui.product.view.ProductsFragment


/*
class AppModeActivity : AppCompatActivity() {
    private var notification: RelativeLayout? = null
    private var layoutProduct: RelativeLayout? = null
    var ivBack: ImageView? = null
    var ivUser: ImageView? = null
    var appbarTitle: TextView? = null
    var clSwitch: ConstraintLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            super.onCreate(Bundle())
        } else {
            super.onCreate(savedInstanceState)
        }
        setContentView(R.layout.activity_app)

        notification = findViewById(R.id.rlNotification)
        layoutProduct = findViewById(R.id.layoutProducts)
        ivBack = findViewById(R.id.ivBack)
        ivUser = findViewById(R.id.ivUser)
        appbarTitle = findViewById(R.id.tvAppBarTitle)
        clSwitch = findViewById(R.id.clSwitch)


        setFragment(AppHomeFragment(), "AppHomeFragment","", false)
        initClick()
    }

    fun setFragment(
        fragment: Fragment,
        tag: String,
        bundledata: String,
        addToBackstack:Boolean = true
    ) {
        val transaction = supportFragmentManager.beginTransaction()
        if(addToBackstack){
            transaction.addToBackStack(tag)
        }

        Log.e("setFragment1=", "setFragment")
        Log.e("bundledata1=", bundledata)

        transaction.replace(R.id.flContainer, fragment).commit()
    }

    private fun removeFragment(){

        */
/*for (fragment in supportFragmentManager.fragments) {
            if (fragment is RulesFragment) {
                supportFragmentManager.beginTransaction().remove(fragment).commit()
            } else {
                continue
            }
        }
        imgBack.visibility = View.GONE
        llContainer.visibility = View.VISIBLE*//*

    }

    private fun initClick() {

        layoutProduct?.setOnClickListener {
            val fragmentInstance = supportFragmentManager.findFragmentById(R.id.flContainer)
            if(fragmentInstance !is ProductsFragment){
                setFragment(ProductsFragment(), "ProductFragment","")
            }
        }

        ivUser?.setOnClickListener {
            val fragmentInstance = supportFragmentManager.findFragmentById(R.id.flContainer)
            if(fragmentInstance !is MyAccountFragment){
                setFragment(MyAccountFragment(), "MyAccountFragment","")
            }
        }
        notification?.setOnClickListener {
            val fragmentInstance = supportFragmentManager.findFragmentById(R.id.flContainer)
            if(fragmentInstance !is AppModeNotificationFragment){
                setFragment(AppModeNotificationFragment(), "AppModeNotificationFragment","")
            }
        }

        ivBack?.setOnClickListener {
            onBackPressed()
        }

        clSwitch?.setOnClickListener {
//            val intent = Intent(this@AppModeActivity, BrowserActivity::class.java)
//            startActivity(intent)
            finish()
        }
    }

}*/

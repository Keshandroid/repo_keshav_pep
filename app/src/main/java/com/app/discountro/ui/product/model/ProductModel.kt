package com.app.discountro.ui.product.model

import com.google.firebase.Timestamp
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ProductModel(
    @SerializedName("date_add")
    var date_add: Timestamp?,
    @SerializedName("description")
    var description: String?,
    @SerializedName("domain")
    var domain: String?,
    @SerializedName("image")
    var image: ArrayList<String>?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("price")
    var price: String?,
    @SerializedName("url")
    var url: String?,
    @SerializedName("currency")
    val currency: String? = null,
    @SerializedName("id_product")
    val id_product: String? = null,
    @SerializedName("max_price")
    val max_price: String? = null,
    @SerializedName("min_price")
    val min_price: String? = null,
    @SerializedName("product_urls")
    val product_urls: ArrayList<String?>? = arrayListOf(),
    @SerializedName("seller_count")
    val seller_count: Int? = null,
    @SerializedName("seller_list")
    val seller_list: ArrayList<SellerList?>? = arrayListOf(),
    @SerializedName("similar_count")
    val similar_count: Int? = null,
    @SerializedName("similar_products")
    val similar_products: ArrayList<String?>? = arrayListOf(),
    @SerializedName("sku")
    val sku: ArrayList<String?>? = arrayListOf(),
    @SerializedName("sku_source")
    val sku_source: ArrayList<String?>? = arrayListOf(),

    @SerializedName("product_brand")
    val product_brand: String? = null,
    @SerializedName("product_image")
    val product_image: String? = null,
    @SerializedName("product_name")
    val product_name: String? = null,


    var isFavorite: Boolean = false,
    var hash_url: String?
    ): Serializable {

    data class SellerList(
        @SerializedName("background_link")
        val background_link: Boolean? = false,
        @SerializedName("comment")
        val comment: String? = null,
        @SerializedName("condition")
        val condition: String? = null,
        @SerializedName("currency")
        val currency: String? = null,
        @SerializedName("direct_link")
        val direct_link: String? = null,
        @SerializedName("hours_updated_ago")
        val hours_updated_ago: String? = null,
        @SerializedName("price")
        val price: String? = null,
        @SerializedName("status")
        val status: String? = null,
        @SerializedName("store_logo")
        val store_logo: String? = null,

        @SerializedName("timestamp_added")
        val timestamp_added: String? = null,
        @SerializedName("timestamp_updated")
        val timestamp_updated: String? = null


    ): Serializable
}
package com.app.discountro.ui.home.view.adapter

import android.view.LayoutInflater
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.ItemNewListBinding
import com.app.discountro.ui.product.model.ListCollectionModel

class CreateListBrowserAdapter(private var listItems: ArrayList<ListCollectionModel>, val listener: CreateListBrowserListener) : RecyclerView.Adapter<CreateListBrowserAdapter.ViewHolder>() {

    var itemSelectedPosition:Int? = null

    class ViewHolder(val binding: ItemNewListBinding): RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ItemNewListBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.tvListName.text = listItems[position].listName

        holder.binding.root.setOnClickListener {
            itemSelectedPosition = position
            listener.onItemClick(listItems[position])
            notifyDataSetChanged()
        }

        if(itemSelectedPosition == position){
            listItems[position].isSelectedList = true
            holder.binding.ivCheck.visibility = VISIBLE
        }else{
            listItems[position].isSelectedList = false
            holder.binding.ivCheck.visibility = GONE
        }
    }

    override fun getItemCount(): Int {
        return listItems.size
    }

    fun updateList(filterList: ArrayList<ListCollectionModel>){
        itemSelectedPosition = null
        this.listItems = filterList
        notifyDataSetChanged()
    }

    interface CreateListBrowserListener {
        fun onItemClick(data: ListCollectionModel)
    }
}
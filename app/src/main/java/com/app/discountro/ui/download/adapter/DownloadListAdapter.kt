package com.app.discountro.ui.downloads

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.ListItemDownloadsBinding
import com.app.discountro.utils.browser_utils.my_object.BrowserUnit.sizeInGb
import com.app.discountro.utils.browser_utils.my_object.BrowserUnit.sizeInMb
import java.io.File
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*

class DownloadListAdapter(val mContext: Context, private val files: Array<File>?) :
    RecyclerView.Adapter<DownloadListAdapter.ViewHolder>() {
    val TAG = javaClass.simpleName

    class ViewHolder(val mBinding: ListItemDownloadsBinding) :
        RecyclerView.ViewHolder(mBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(ListItemDownloadsBinding.inflate(LayoutInflater.from(mContext),
            parent,
            false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var fileSize = DecimalFormat("##.##").format(files!![position].sizeInMb)

        fileSize = if (fileSize.toDouble().toInt() < 999)
            DecimalFormat("##.##").format(files[position].sizeInMb) + " MB"
        else
            DecimalFormat("##.##").format(files[position].sizeInGb) + " GB"

        holder.mBinding.tvDownloadTitle.text = files!![position].name
        holder.mBinding.tvDownloadSize.text = fileSize

        Log.e("GetDetailsFile", "onBindViewHolder: ${files[position]}" )
        holder.mBinding.tvDownloadTime.text =
            convertToLocalDateViaInstant(Date(files[position].lastModified()))
    }

    override fun getItemCount(): Int {
        return files?.size ?: 0
    }

    private fun convertToLocalDateViaInstant(dateToConvert: Date): String {
        val dateFormatter = SimpleDateFormat("dd MMM YYYY", Locale.getDefault())
        Log.e(TAG,
            "convertToLocalDateViaInstant:dateFormatter: " + dateFormatter.format(dateToConvert))
        return dateFormatter.format(dateToConvert)
//        return dateToConvert.toInstant()
//            .atZone(ZoneId.systemDefault())
//            .toLocalDate().toString()
    }
}
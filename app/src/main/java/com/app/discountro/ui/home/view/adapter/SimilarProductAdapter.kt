package com.app.discountro.ui.home.view.adapter
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.SimilarItemLayoutBinding
import com.app.discountro.fireStore.models.SimilarProductModel
import com.app.discountro.ui.home.view.interfaces.CompareProductInterface
import com.bumptech.glide.Glide
class SimilarProductAdapter(val context: Context,
                            private val compareProductInterface: CompareProductInterface,
                            val dataList: List<SimilarProductModel>) : RecyclerView.Adapter<SimilarProductAdapter.ViewHold>()
{
    class ViewHold(var bind : SimilarItemLayoutBinding) : RecyclerView.ViewHolder(bind.root){
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHold {
        return ViewHold(SimilarItemLayoutBinding.inflate(LayoutInflater.from(context),parent,false))
    }
    override fun onBindViewHolder(holder: ViewHold, position: Int) {
        val model = dataList[holder.adapterPosition]
        holder.bind.apply {
            Glide.with(context).load(model.product_logo).into(imgItem)
            tvItemName.text = model.title
            tvItemDescription.text = model.comment
            tvItemPrice.text = model.currency + " "+model.price
//            Glide.with(context).load(model.).into(imgSiteLogo)

            holder.itemView.setOnClickListener {
                val directLink=model.direct_link
                compareProductInterface.onItemClicked(holder.itemView,directLink)
            }
        }
    }
    override fun getItemCount(): Int {
        return dataList.size
    }
}
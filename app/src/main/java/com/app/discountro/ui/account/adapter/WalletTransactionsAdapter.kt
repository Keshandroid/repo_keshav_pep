package com.app.discountro.ui.account.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.databinding.MItemTransactionsBinding

class WalletTransactionsAdapter(val context: Context, val transactionList: ArrayList<String>, val listener: WalletTransactionsListener ) : RecyclerView.Adapter<WalletTransactionsAdapter.ViewHolder>() {

    class ViewHolder(val binding: MItemTransactionsBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(MItemTransactionsBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.root.setOnClickListener {
            listener.transactionOnClick("")
        }
        holder.binding.cardTransaction.stateListAnimator = null
//        holder.binding.cardTransaction.cardElevation = 10f
//        holder.binding.cardTransaction.elevation = 10f
//        holder.binding.cardTransaction.maxCardElevation = 10f

    }

    override fun getItemCount(): Int {
        return 5
    }

    interface WalletTransactionsListener{
        fun transactionOnClick(data: String)
    }
}
package com.app.discountro.ui.login.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import com.app.discountro.R
import com.app.discountro.common.constant.AppConstant
import com.app.discountro.databinding.ActivityLoginBinding
import com.app.discountro.ui.forgot_password.view.ForgotPasswordActivity
import com.app.discountro.ui.home.view.BrowserActivity
import com.app.discountro.ui.sign_up.view.SignUpActivity
import com.app.discountro.ui.splash.model.MyTranslationVarModel
import com.app.discountro.ui.welcome.view.WelcomeActivity
import com.app.discountro.utils.FireStorageKeys
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.SharedPrefsUtils
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class LoginActivity : AppCompatActivity() {

    lateinit var binding: ActivityLoginBinding
    private val auth: FirebaseAuth = Firebase.auth
    private lateinit var googleSignInClient: GoogleSignInClient
    private val appConstant: AppConstant = AppConstant()
    val db = Firebase.firestore
    private var gLoginUserName: String = ""
    var bPassword = false
    private var socialType: String = ""
    private lateinit var callbackManager: CallbackManager

    private lateinit var myTranslationVarModel: MyTranslationVarModel
    private lateinit var myTranslationLoginAct: MyTranslationVarModel
    private val myTranslationArrayData: ArrayList<MyTranslationVarModel> = arrayListOf()

    private val resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            Log.e("Sunny", "ResultCode: ${result.resultCode}")
            Log.e("Sunny", "ActivityResult: ${Activity.RESULT_OK} ")
            if (result.resultCode == Activity.RESULT_OK) {
                val data = result.data
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    val account = task.getResult(ApiException::class.java)!!
                    Toast.makeText(
                        this,
                        if (myTranslationLoginAct.equals(null)) {
                            resources.getString(R.string.success_login)
                        } else {
                            myTranslationLoginAct.success_login
                        },
                        Toast.LENGTH_SHORT
                    ).show()
                    // Google Sign In was successful, authenticate with Firebase
                    Log.e("AccountDetails:-", "Google ID ${account.id}")
                    Log.e("AccountDetails:-", "Google familyName ${account.familyName}")
                    Log.e("AccountDetails:-", "Google LastName ${account.givenName}")
                    gLoginUserName =
                        account.givenName.toString() + " " + account.familyName.toString()
                    firebaseAuthWithGoogle(account.idToken!!)
                } catch (e: ApiException) {
                    appConstant.dismissDiscRoProgress()
                    Log.w("Exception", "Google sign in failed", e)
                }
            } else {
                appConstant.dismissDiscRoProgress()
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        getTranslationData()
        initView()
    }

    private fun initView() {
        setTranslationDataFromPref()
        onPwdEyeClickListener()
        onSignUpActivityClickListener()
        onBackClickListener()
        onForgotPwdIntentClick()
        onLoginClickListener()
        firebaseGoogleLogin()
        onGButtonPressListener()
        facebookFireLogin()
    }

    private fun setTranslationDataFromPref() {
        myTranslationLoginAct = myTranslationArrayData[0]
        Log.e(
            "MyDataValue_SunnyPatel",
            "setTranslationDataFromPref: ${myTranslationLoginAct.welcome_to}"
        )
        binding.tvLoginTxt.text = myTranslationLoginAct.login
        binding.tvEmailTxt.text = myTranslationLoginAct.e_mail
        binding.etFireEmail.hint = myTranslationLoginAct.enter_your_e_mail
        binding.tvPwdTxt.text = myTranslationLoginAct.password
        binding.etFirePassword.hint = myTranslationLoginAct.enter_your_password
        binding.tvForgotPwd.text = myTranslationLoginAct.forgot_password
        binding.cbFireLogin.text = myTranslationLoginAct.login
        binding.tvNotHaveAcTxt.text = myTranslationLoginAct.don_t_have_an_account
        binding.tvSignUp.text = myTranslationLoginAct.sign_up
        binding.tvSignInWithTxt.text = myTranslationLoginAct.sign_in_with
        binding.clFacebookLogin.text = myTranslationLoginAct.facebook
        binding.clGoogleLogin.text = myTranslationLoginAct.google
    }

    private fun getTranslationData() {
        Log.e(
            "MySharePrefDataValue", "getTranslationData: ${
                SharedPrefsUtils.getModelPreferences(
                    KeysUtils.KeyModelTranslation,
                    MyTranslationVarModel::class.java
                )
            }"
        )
        myTranslationVarModel = SharedPrefsUtils.getModelPreferences(
            KeysUtils.KeyModelTranslation,
            MyTranslationVarModel::class.java
        ) as MyTranslationVarModel
        myTranslationArrayData.add(myTranslationVarModel)
        myTranslationArrayData[0]
    }

    private fun facebookFireLogin() {
//        FacebookSdk.sdkInitialize(applicationContext)
        FacebookSdk.fullyInitialize()
        AppEventsLogger.activateApp(application)
        fireFacebookLogin()
        onFbButtonClickListener()
    }

    private fun onFbButtonClickListener() {
        binding.clFacebookLogin.setOnClickListener {
            appConstant.showDiscoRoProgress(this@LoginActivity)
            binding.lbFacebookLoginBtn.performClick()
        }
    }

    private fun fireFacebookLogin() {
        callbackManager = CallbackManager.Factory.create()
        binding.lbFacebookLoginBtn.setPermissions("email", "public_profile","user_friends")
        binding.lbFacebookLoginBtn.registerCallback(callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult) {
                    Log.d("FacebookSunny", "facebook:onSuccess:$result")
                    handleFacebookAccessToken(result.accessToken)
                }

                override fun onCancel() {
                    appConstant.dismissDiscRoProgress()
                    Log.d("FacebookSunny", "facebook:onCancel")
                }

                override fun onError(error: FacebookException) {
                    appConstant.dismissDiscRoProgress()
                    Log.d("FacebookSunny", "facebook:onError", error)
                }
            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Pass the activity result back to the Facebook SDK
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleFacebookAccessToken(token: AccessToken) {
        Log.d("FacebookAccessToken", "handleFacebookAccessToken:$token")

        val credential = FacebookAuthProvider.getCredential(token.token)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    Toast.makeText(
                        this,
                        myTranslationLoginAct.success_login.ifEmpty {
                            resources.getString(R.string.success_login)
                        },
                        Toast.LENGTH_SHORT
                    ).show()
                    // Sign in success, update UI with the signed-in user's information
                    Log.d("facebookAccessToken", "signInWithCredential:success")
                    val user = auth.currentUser
                    socialType = KeysUtils.KeyFirefacebook
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.w("facebookAccessToken", "signInWithCredential:failure", task.exception)
                    Toast.makeText(
                        baseContext, myTranslationLoginAct.authantication_fail.ifEmpty {
                            resources.getString(R.string.authantication_fail)
                        },
                        Toast.LENGTH_SHORT
                    ).show()
                    updateUI(null)
                }
            }.addOnFailureListener {
                LoginManager.getInstance().logOut()
                Toast.makeText(this@LoginActivity, it.message, Toast.LENGTH_LONG).show()
                Log.e("MyFbErrorsss", "handleFacebookAccessToken: ${it.message}")
                appConstant.dismissDiscRoProgress()
            }
    }


    override fun onBackPressed() {
        super.onBackPressed()
        val signUpIntent = Intent(this@LoginActivity, WelcomeActivity::class.java)
        startActivity(signUpIntent)
        finish()
    }

    private fun onPwdEyeClickListener() {
        binding.clHideUnHide.setOnClickListener {
            pwdHideUnHideFunction()
        }
    }

    private fun pwdHideUnHideFunction() {
        if (bPassword) {
            binding.etFirePassword.transformationMethod =
                PasswordTransformationMethod.getInstance()
            bPassword = false
            binding.ivHideUnHide.background = resources.getDrawable(R.drawable.ic_eye)
        } else {
            binding.etFirePassword.transformationMethod =
                HideReturnsTransformationMethod.getInstance()
            bPassword = true
            binding.ivHideUnHide.background = resources.getDrawable(R.drawable.ic_eye_close)
        }
    }


    private fun onGButtonPressListener() {
        binding.clGoogleLogin.setOnClickListener {
            appConstant.showDiscoRoProgress(this@LoginActivity)
            val signInIntent = googleSignInClient.signInIntent
            resultLauncher.launch(signInIntent)
            revokeAccess()
        }
    }

    private fun firebaseGoogleLogin() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_ID))
            .requestEmail()
            .build()
        googleSignInClient = GoogleSignIn.getClient(this, gso)
    }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    socialType = KeysUtils.KeyFireGoogle
                    // Sign in success, update UI with the signed-in user's information
                    Log.e("SunnyPatel", "signInWithCredential:success")
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    // If sign in fails, display a message to the user.
                    Log.e("SunnyPatel", "signInWithCredential:failure", task.exception)
                    updateUI(null)
                }
            }.addOnFailureListener {
                appConstant.dismissDiscRoProgress()
            }
    }

    private fun onLoginClickListener() {
        binding.cbFireLogin.setOnClickListener {
            if (binding.etFireEmail.text.isNotEmpty() &&
                binding.etFirePassword.text.isNotEmpty()
            ) {
                functionFireLogin()
            } else {
                Toast.makeText(
                    this,
                    myTranslationLoginAct.fill_creadential.ifEmpty { resources.getString(R.string.fill_creadential) },
                    Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        if (currentUser != null) {
            updateUI(currentUser)
        }
    }

    private fun functionFireLogin() {
        appConstant.showDiscoRoProgress(this@LoginActivity)
        auth.signInWithEmailAndPassword(
            binding.etFireEmail.text.toString(),
            binding.etFirePassword.text.toString()
        ).addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                val user = auth.currentUser
                socialType = KeysUtils.KeyFireEmail
                updateUI(user)
            } else {
                Log.e("FireLogin", "failure", task.exception)
            }
            appConstant.dismissDiscRoProgress()
        }.addOnFailureListener {
            Toast.makeText(this@LoginActivity, it.message, Toast.LENGTH_SHORT).show()
            appConstant.dismissDiscRoProgress()
        }
    }

    private fun updateUI(user: FirebaseUser?) {
        if (user != null) {
            SharedPrefsUtils.setStringPreference(KeysUtils.KeyUUID, user.uid)
           /* Toast.makeText(
                this@LoginActivity,
                "You are login with ${user.email}",
                Toast.LENGTH_SHORT
            ).show()*/

            Log.e("FireAuAccountDetails:-", "Google ID ${user.uid}")
            Log.e("FireAuAccountDetails:-", "Google LastName ${user.email}")
            Log.e("FireAuAccountDetails:-", "Google ProfilePicURL ${user.photoUrl}")
            Log.e("FireAuAccountDetails:-", "Google IDToken ${user.providerId}")
            fireStoreCreateUsers(user)
        }
        appConstant.dismissDiscRoProgress()
    }

    private fun fireStoreCreateUsers(user: FirebaseUser?) {
        gLoginUserName.ifEmpty {
            gLoginUserName = ""
        }
        gLoginUserName.ifBlank {
            gLoginUserName = ""
        }
        if (user != null) {

            val userTable = hashMapOf(
                FireStorageKeys.KEYUT_ID_USER to user.uid,
                FireStorageKeys.KEYUT_ID_NAME to gLoginUserName,
                FireStorageKeys.KEYUT_P_TOKEN to "",
                FireStorageKeys.KEYUT_EMAIL to user.email,
                FireStorageKeys.KEYUT_PWD to "",
                FireStorageKeys.KEYUT_DB_REG to "",
                FireStorageKeys.KEYUT_ACTIVE to "",
                FireStorageKeys.KEYUT_SOCIAL_TYPE to socialType
            )
            db.collection(FireStorageKeys.KEY_USER_TABLE).document(user.uid).set(userTable)
                .addOnSuccessListener {
                    Log.e("userAuthFireStorage", "Success: $it")
                }.addOnFailureListener {
                    Log.e("userAuthFireStorage", "Fail: ${it.message}")
                    LoginManager.getInstance().logOut()
                }
        }
        intentToMainScreen(user)
    }

    private fun intentToMainScreen(user: FirebaseUser?) {
        if (user != null) {
            SharedPrefsUtils.setStringPreference(KeysUtils.KeyUUID, user.uid)
            Log.e("LoginActivity", "BrowserActivity::class.java 2")

            val intent = Intent(this@LoginActivity, BrowserActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun onForgotPwdIntentClick() {
        binding.tvForgotPwd.setOnClickListener {
            val intentForgotPwd = Intent(this@LoginActivity, ForgotPasswordActivity::class.java)
            startActivity(intentForgotPwd)
        }
    }

    private fun onBackClickListener() {
        binding.cvBack.setOnClickListener {
            val signUpIntent = Intent(this@LoginActivity, WelcomeActivity::class.java)
            startActivity(signUpIntent)
            finish()
        }
    }

    private fun revokeAccess() {
        googleSignInClient.revokeAccess().addOnCompleteListener(this) {}
    }


    private fun onSignUpActivityClickListener() {

        binding.tvSignUp.setOnClickListener {
            val intentSignUp = Intent(this@LoginActivity, SignUpActivity::class.java)
            startActivity(intentSignUp)
        }
    }
}
package com.app.discountro.ui.home.view

import android.Manifest
import android.annotation.SuppressLint
import android.app.*
import android.content.*
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.*
import android.media.MediaPlayer
import android.media.MediaPlayer.OnCompletionListener
import android.net.Uri
import android.os.*
import android.os.StrictMode.VmPolicy
import android.preference.PreferenceManager
import android.print.PrintAttributes
import android.print.PrintManager
import android.provider.MediaStore
import android.speech.RecognizerIntent
import android.text.Editable
import android.text.Html
import android.text.TextWatcher
import android.util.Base64
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.view.View.*
import android.view.ViewTreeObserver.OnScrollChangedListener
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.*
import android.webkit.WebChromeClient.CustomViewCallback
import android.webkit.WebView.GONE
import android.webkit.WebView.HitTestResult
import android.widget.*
import android.widget.AdapterView.*
import android.widget.TextView.OnEditorActionListener
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.SwitchCompat
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.transition.ChangeBounds
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.app.discountro.Firebase.RetrieveDataFromFirebase
import com.app.discountro.Interface.ComparableProductInterface
import com.app.discountro.Interface.CouponListInterface
import com.app.discountro.Interface.FireStoreAddFavoriteProductInterface
import com.app.discountro.Interface.FireStoreAddListInterface
import com.app.discountro.Interface.FireStoreAddPurchasedProductInterface
import com.app.discountro.Interface.FireStoreGetFilterListListener
import com.app.discountro.Interface.FireStoreGetProductFromHashUrl
import com.app.discountro.Interface.FireStoreRemoveFavoriteProductInterface
import com.app.discountro.Interface.FirestoreProductMatchingInterface
import com.app.discountro.R
import com.app.discountro.R.drawable.ic_notification
import com.app.discountro.R.drawable.ic_oval
import com.app.discountro.base.extentions.showSnackBar
import com.app.discountro.common.album_service.AlbumController
import com.app.discountro.common.my_interface.BrowserController
import com.app.discountro.common.my_interface.OnScrollableChangeListener
import com.app.discountro.common.sqlite.GridItem
import com.app.discountro.common.sqlite.browser_db.BookmarkList
import com.app.discountro.common.sqlite.browser_db.MyDbHandler
import com.app.discountro.common.sqlite.browser_db.RecordAction
import com.app.discountro.common.sqlite.browser_db.myDbHandlerBook
import com.app.discountro.common.sqlite.offerCoupons.CopiedCouponsModel
import com.app.discountro.common.sqlite.offerCoupons.DBHelperOffer
import com.app.discountro.common.sqlite.record.Record
import com.app.discountro.common.urlConverter.ConvertUrlToSHA
import com.app.discountro.databinding.TabMenuDialogLayoutBinding
import com.app.discountro.fireStore.helper.FireStoreHelper
import com.app.discountro.fireStore.interfaces.FireStoreListener
import com.app.discountro.fireStore.models.CompareProductModel
import com.app.discountro.fireStore.models.CouponsAllModel
import com.app.discountro.fireStore.models.RoModel
import com.app.discountro.fireStore.services.FireStoreServiceCall
import com.app.discountro.network.ApiController
import com.app.discountro.network.ResponseInterface
import com.app.discountro.network.SearchModel
import com.app.discountro.network.StringRecord
import com.app.discountro.ui.account.view.MyAccountFragment
import com.app.discountro.ui.download.view.DownloadsListActivity
import com.app.discountro.ui.home.model.ReOpenTabsModel
import com.app.discountro.ui.home.model.RoModelBackupModel
import com.app.discountro.ui.home.model.TabsModel
import com.app.discountro.ui.home.view.adapter.*
import com.app.discountro.ui.home.view.adapter.CompleteAdapter
import com.app.discountro.ui.home.view.interfaces.CompareProductInterface
import com.app.discountro.ui.home.view.models.AllStoreModel
import com.app.discountro.ui.home.view.models.CountryCouponsModel
import com.app.discountro.ui.home.view.models.FavoriteStore
import com.app.discountro.ui.home.view.models.UserModel
import com.app.discountro.ui.home.viewmodel.BrowserWebView
import com.app.discountro.ui.notification.model.NotificationModel
import com.app.discountro.ui.notification.view.AppModeNotificationFragment
import com.app.discountro.ui.product.model.FavoritePurchasedCustomListModel
import com.app.discountro.ui.product.model.ListCollectionModel
import com.app.discountro.ui.product.model.ProductModel
import com.app.discountro.ui.product.view.ProductsFragment
import com.app.discountro.utils.FireStorageKeys
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.SharedPrefsUtils
import com.app.discountro.utils.Utils
import com.app.discountro.utils.Utils.Companion.toast
import com.app.discountro.utils.browser_utils.AdBlock
import com.app.discountro.utils.browser_utils.Cookie
import com.app.discountro.utils.browser_utils.HolderService
import com.app.discountro.utils.browser_utils.Javascript
import com.app.discountro.utils.browser_utils.my_object.*
import com.bumptech.glide.Glide
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.mobapphome.mahencryptorlib.MAHEncryptor
import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView
import kotlinx.coroutines.*
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.xmlpull.v1.XmlPullParser
import org.xmlpull.v1.XmlPullParserException
import org.xmlpull.v1.XmlPullParserFactory
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.StringReader
import java.net.URL
import java.util.*
import kotlin.math.floor
import kotlin.math.roundToInt


class BrowserActivity : AppCompatActivity(), BrowserController,
    OnScrollableChangeListener,
    OnClickListener,AllStoreAdapter.SelectedStoresInterface {

    private val auth: FirebaseAuth = Firebase.auth


    var apiController: ApiController? = null
    var countDownTimer: CountDownTimer? = null
    var cvParentLayout: ConstraintLayout? = null
    var rl_inner_layout: RelativeLayout? = null
    var rlRecentHistory: RelativeLayout? = null
    var editToolContainer: LinearLayout? = null
    var listRecentContainer: LinearLayout? = null
    var tvRecentSearchText: LinearLayout? = null
    var rvCompareProducts: RecyclerView? = null
    var progressShadow: View? = null
    lateinit var btnClose: ImageView
    var checkwebedit: Boolean? = false
    var onclickEdit: Boolean? = false
    var firstSearch: Boolean? = false
    var appBarHeight = 0
    var appBarBehavior: androidx.coordinatorlayout.widget.CoordinatorLayout.Behavior<AppBarLayout>? =
        null
    val tabsDetailsArr: MutableList<TabsModel> = mutableListOf()
    var tabAdapter: TabOverViewAdapter? = null
    var incoTabAdapter: IncoTabOverViewAdapter? = null
    lateinit var rvTabs: RecyclerView
    lateinit var rvIncoTabs: RecyclerView
    val TAG = "BrowserActivity"
    private var url: String? = null
    var back = false
    lateinit var menu_tabPreview: LinearLayout
    lateinit var menu_newTabOpen: LinearLayout
    lateinit var menu_closeTab: LinearLayout
    lateinit var menu_quit: LinearLayout
    lateinit var menu_shareScreenshot: LinearLayout
    lateinit var menu_shareLink: LinearLayout
    lateinit var menu_sharePDF: LinearLayout
    lateinit var menu_openWith: LinearLayout
    lateinit var listRecent: ListView

    lateinit var ll_share: LinearLayout
    lateinit var ll_share_1: LinearLayout
    lateinit var menu_searchSite: LinearLayout
    lateinit var menu_settings: LinearLayout
    lateinit var menu_download: LinearLayout
    lateinit var menu_saveScreenshot: LinearLayout
    lateinit var menu_saveBookmark: LinearLayout
    lateinit var menu_savePDF: LinearLayout
    lateinit var menu_saveStart: LinearLayout
    lateinit var menu_help: LinearLayout
    lateinit var ll_save: LinearLayout
    lateinit var ll_save_1: LinearLayout
    lateinit var ll_more: LinearLayout
    lateinit var ll_more_1: LinearLayout
    lateinit var menu_fav: LinearLayout
    lateinit var menu_sc: LinearLayout
    lateinit var menu_openFav: LinearLayout
    lateinit var menu_shareCLipboard: LinearLayout
    lateinit var floatButton_tabView: View
    lateinit var floatButton_saveView: View
    lateinit var floatButton_shareView: View
    lateinit var floatButton_moreView: View

    // Views
    lateinit var fab_tab: ImageButton
    lateinit var fab_share: ImageButton
    lateinit var fab_save: ImageButton
    lateinit var fab_more: ImageButton
    lateinit var tab_plus: ImageButton
    lateinit var tab_plus_bottom: ImageButton
    lateinit var tvNewTab: TextView
    lateinit var tvOverViewMenuTabCount: TextView
    lateinit var btnIncoIcon: ImageButton
    lateinit var hlIncoTabs: View
    lateinit var hlStdTabs: View
    lateinit var rlOverViewDialog: RelativeLayout
    lateinit var searchUp: ImageButton


    lateinit var omniboxRefresh: ImageButton

    lateinit var open_startPage: ImageButton
    lateinit var open_bookmark: ImageButton
    lateinit var open_history: ImageButton
    lateinit var open_menu: ImageButton
    lateinit var ivTabMenu: ImageButton

    lateinit var inputBox: AutoCompleteTextView
    lateinit var progressBar: ProgressBar

    // TODO: searchbar:
    lateinit var mainSearchBar: CollapsingToolbarLayout
    lateinit var searchView2: SearchView
    lateinit var tvTabCount: TextView
    lateinit var ivMenu: ImageView
    lateinit var ivNewTabPlus: ImageView

    // lateinit var image_google: ImageView
//    lateinit var search: LinearLayout
    lateinit var bottomSheetDialog: BottomSheetDialog
    lateinit var bottomSheetDialog_hist: BottomSheetDialog
    lateinit var bottomSheetDialog_OverView: BottomSheetDialog
    lateinit var bottomSheetDialog_OverView_hist: BottomSheetDialog
    lateinit var compareProductsBottomSheetDialog: BottomSheetDialog

    //Store Bottomsheet
    lateinit var storeProductsBottomSheetDialog: BottomSheetDialog


    lateinit var listView: ListView
    lateinit var titleTV: TextView
    lateinit var currentTitle: TextView
    lateinit var currentIcon: ImageView
    lateinit var view3: View
    lateinit var omniboxTitle: TextView
    lateinit var dialogTitle: TextView

    lateinit var recentTxt: TextView

    // Layouts
    private var customView: View? = null
    private var videoView: VideoView? = null
    lateinit var tab_ScrollView: ScrollView
    lateinit var overview_top: LinearLayout
    lateinit var overview_topButtons: LinearLayout
    lateinit var omnibox: RelativeLayout

    //   lateinit var searchPanel: RelativeLayout
    lateinit var contentFrame: FrameLayout

    // create variable for swipe refresh
    lateinit var swipeRefreshLayout: SwipeRefreshLayout

    // create variable for scroll listener
    private var mOnScrollChangedListener: OnScrollChangedListener? = null

    //    lateinit var tab_container: LinearLayout
    private var fullscreenHolder: FrameLayout? = null
    lateinit var open_startPageView: View

    // Others
    lateinit var open_bookmarkView: View
    lateinit var open_historyView: View
    lateinit var overview_titleIcons_startView: View
    lateinit var overview_titleIcons_bookmarksView: View
    lateinit var overview_titleIcons_historyView: View
    lateinit var title: String
    lateinit var overViewTab: String
    lateinit var downloadReceiver: BroadcastReceiver
    lateinit var mBehavior: BottomSheetBehavior<*>
    lateinit var mBehavior_hist: BottomSheetBehavior<*>
    lateinit var context: Activity
    lateinit var sp: SharedPreferences
    lateinit var mahEncryptor: MAHEncryptor
    lateinit var gridAdapter: GridAdapter
    private var originalOrientation = 0
    private var shortAnimTime = 0
    private var vibrantColor = 0
    private var dimen156dp = 0f
    private var dimen144dp = 0f
    private var dimen117dp = 0f
    private var dimen108dp = 0f
    private var customViewCallback: CustomViewCallback? = null
    private var filePathCallback: ValueCallback<Array<Uri>>? = null
    private var currentAlbumController: AlbumController? = null
    private var mFilePathCallback: ValueCallback<Array<Uri>>? = null
    lateinit var mCameraPhotoPath: String
    private val REQUEST_CODE_SPEECH_INPUT = 1
    private lateinit var sharedPreferences: SharedPreferences
    private var editor: SharedPreferences.Editor? = null

    var roBackUpObjectsList = ArrayList<RoModelBackupModel>()
    // for only favorites store selection purpose
    var selectedStoreRoBackUpObjectsList = ArrayList<RoModelBackupModel>()

    // bottom sheet view
    private var clSwitch: ConstraintLayout? = null
    public var clSwitchApp: ConstraintLayout? = null
    private var rlCoupons: RelativeLayout? = null
    private var layoutCompare: RelativeLayout? = null
    private var layoutStore: RelativeLayout? = null

    private var layoutAppShop: RelativeLayout? = null


    private var rlNotification: RelativeLayout? = null
    private var rlTest: RelativeLayout? = null
    private var llBottomNav: LinearLayout? = null
    private var cvCouponCode: CardView? = null
    private var compareBadge: CardView? = null
    private var tvCountCouponBottom: TextView? = null
    private var compareBadgeText: TextView? = null
    private var imCheck: ImageView? = null
    private var imPhone: ImageView? = null
    private var isBrowserOn = true

    // all Coupon code
    private var roModel: RoModel? = null

    // for only favorites store selection purpose
    private var selectedRoModel: RoModel? = null

    private val countryList = ArrayList<CountryCouponsModel>()
    private var compareProductModel: CompareProductModel? = null

    private var couponsAllModel: CouponsAllModel? = null

    // for only favorites store selection purpose
    private var selectedStoresCouponsAllModel: CouponsAllModel? = null

    private var isSimilarCoupons = false
    private var selectedCountry = ""
    private var websiteNameIs = ""
    private var oldWebAddress = ""
    private lateinit var offersDBHelper: DBHelperOffer
    private var wbLoad: WebView? = null
    private var finalList: ArrayList<RoModel.CouponModel> = ArrayList()

    // for only favorites store selection purpose
    private var selectedStorefinalList: ArrayList<RoModel.CouponModel> = ArrayList()

    private var coupanAdapter: CouponsListAdapter? = null

    lateinit var rlBrowserView : RelativeLayout
    private var llAppModeToolbar: LinearLayout? = null
    lateinit var flAppView : FrameLayout
    private var llBottomAppNav: LinearLayout? = null
    private var layoutProduct: RelativeLayout? = null
    private var layoutHome: RelativeLayout? = null

    //BOTTOMBAR
    private var imStoreApp: ImageView? = null
    private var imHome: ImageView? = null
    private var imProduct: ImageView? = null
    private var imNotificationsApp: ImageView? = null


    private var ivUser: ImageView? = null
    private var ivBack: ImageView? = null
    private var notificationApp: RelativeLayout? = null
    var appbarTitle: TextView? = null

    // for only favorites store selection purpose
    lateinit var horizontalProductDetailAdapter: HorizontalProductDetailAdapter

    //FAVORITE STORES
    var rvFavoriteStores: RecyclerView?= null
    var rvAllStores: FastScrollRecyclerView?=null
    lateinit var allStoresAdapter: AllStoreAdapter
    lateinit var favoriteStoresAdapter: FavoriteStoresAdapter

    // for the all stores in checked the items
    val filterAllStores: MutableList<AllStoreModel> = mutableListOf()
    //All Stores names
    val allStores: MutableList<AllStoreModel> = mutableListOf()
    //Favorite stores array
    private var favoriteStores: ArrayList<FavoriteStore> = ArrayList()
    //coupon Bottomsheet
     lateinit var couponBottomSheet: BottomSheetDialog

     // Notification
    lateinit var notificationAdapter: NotificationAdapter
    private var notificationList: ArrayList<NotificationModel> = ArrayList()

    // editProductBottomsheet
    lateinit var editItemSaveBottomSheet: BottomSheetDialog
    var isListItemSelected = false
    private var filterList: ArrayList<ListCollectionModel> = arrayListOf()
    lateinit var createListBrowserAdapter: CreateListBrowserAdapter
    private var productData: ProductModel? = null
    var selectedListId: String? = null
    var selectedListProducts: ArrayList<FavoritePurchasedCustomListModel> = arrayListOf()
    lateinit var ivFavorite : ImageView
    private var productList: ArrayList<ProductModel> = arrayListOf()


    // new create list 
    lateinit var createNewProductListBottomSheet: BottomSheetDialog




    var oldWebsite = ""
    private val MOBILE_VIEW =
        "Mozilla/5.0 (Linux; U; Android 4.4; en-us; Nexus 4 Build/JOP24G) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"

    private fun prepareRecord(): Boolean {
        val webView = currentAlbumController as BrowserWebView?
        val title = webView!!.title
        val url = webView.url
        return (title == null || title.isEmpty()
                || url == null || url.isEmpty()
                || url.startsWith(BrowserUnit.URL_SCHEME_ABOUT)
                || url.startsWith(BrowserUnit.URL_SCHEME_MAIL_TO)
                || url.startsWith(BrowserUnit.URL_SCHEME_INTENT))
    }


    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("MissingPermission")
    public override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            super.onCreate(Bundle())
        } else {
            super.onCreate(savedInstanceState)
        }
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        WebView.enableSlowWholeDocumentDraw()
        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())

        context = this@BrowserActivity
        dbHandler = MyDbHandler(context, null, null, 1)
        dbHandlerbook = myDbHandlerBook(context, null)
        sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putInt("restart_changed", 0).apply()

        sp.edit().putInt("vibrantcolor", resources.getColor(R.color.colorAccent)).apply()
        HelperUnit.applyTheme(context)
        setContentView(R.layout.activity_browser)

        // find views of bottom navigation
        tvCountCouponBottom = findViewById(R.id.tvCountCouponBottom)
        cvCouponCode = findViewById(R.id.cvCouponCode)
        imPhone = findViewById(R.id.imPhone)
        imCheck = findViewById(R.id.imCheck)

        rlBrowserView = findViewById(R.id.rlBrowserView)
        flAppView = findViewById(R.id.flAppView)
        llBottomAppNav = findViewById(R.id.llBottomAppNav)
        llAppModeToolbar = findViewById(R.id.llAppModeToolbar)
        notificationApp = findViewById(R.id.rlNotificationApp)
        layoutProduct = findViewById(R.id.layoutProducts)

        layoutHome= findViewById(R.id.layoutHome)
        ivBack = findViewById(R.id.ivBack)
        ivUser = findViewById(R.id.ivUser)
        appbarTitle = findViewById(R.id.tvAppBarTitle)

        imStoreApp= findViewById(R.id.imStoreApp)
        imHome = findViewById(R.id.imHome)
        imProduct = findViewById(R.id.imProduct)
        imNotificationsApp = findViewById(R.id.imNotificationsApp)






        setUpBottomNavigation()
        cvParentLayout = findViewById(R.id.cvParentLayout)
        rl_inner_layout = findViewById(R.id.rl_inner_layout)
//        rlRecentHistory = findViewById(R.id.RlRecentHistory)
        editToolContainer = findViewById(R.id.editToolContainer)
        recentTxt = findViewById(R.id.recentTxt)
        listRecentContainer = findViewById(R.id.listRecentContainer)
        tvRecentSearchText = findViewById(R.id.tvRecentSearchText)
        currentTitle = findViewById(R.id.currentTitle)
        currentIcon = findViewById(R.id.currentIcon)

        compareBadgeText = findViewById(R.id.compareBadgeText)
        compareBadge = findViewById(R.id.compareBadge)

//        swipeRefreshContainer = findViewById(R.id.swipeRefreshContainer)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        editor = sharedPreferences.edit()
        wbLoad = findViewById(R.id.wbLoad)
        setupWebView()
        offersDBHelper = DBHelperOffer(this)
        //startLoading("https://www.amazon.com/")

        if (/*SharedPrefsUtils.getStringPreferenceBrowser(KeysUtils.SAVED_KEY_OK,"no")*/ sp.getString(
                "saved_key_ok",
                "no"
            ) == "no"
        ) {
            val chars =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!§$%&/()=?;:_-.,+#*<>".toCharArray()
            val sb = StringBuilder()
            val random = Random()
            for (i in 0..24) {
                val c = chars[random.nextInt(chars.size)]
                sb.append(c)
            }
            //SharedPrefsUtils.setStringPreference(KeysUtils.SAVED_KEY, sb.toString())
            sp.edit().putString("saved_key", sb.toString()).apply()
            //SharedPrefsUtils.setStringPreference(KeysUtils.SAVED_KEY_OK, "yes")
            sp.edit().putString("saved_key_ok", "yes").apply()
            //SharedPrefsUtils.setStringPreference(KeysUtils.SETTING_GESTURE_TB_UP, "08")
            sp.edit().putString("setting_gesture_tb_up", "08").apply()
            //SharedPrefsUtils.setStringPreference(KeysUtils.SETTING_GESTURE_TB_DOWN, "01")
            sp.edit().putString("setting_gesture_tb_down", "01").apply()
            //SharedPrefsUtils.setStringPreference(KeysUtils.SETTING_GESTURE_TB_LEFT, "07")
            sp.edit().putString("setting_gesture_tb_left", "07").apply()
            //SharedPrefsUtils.setStringPreference(KeysUtils.SETTING_GESTURE_TB_RIGHT, "06")
            sp.edit().putString("setting_gesture_tb_right", "06").apply()
            //SharedPrefsUtils.setStringPreference(KeysUtils.SETTING_GESTURE_NAV_UP, "04")
            sp.edit().putString("setting_gesture_nav_up", "04").apply()
            //SharedPrefsUtils.setStringPreference(KeysUtils.SETTING_GESTURE_NAV_DOWN, "05")
            sp.edit().putString("setting_gesture_nav_down", "05").apply()
            //SharedPrefsUtils.setStringPreference(KeysUtils.SETTING_GESTURE_NAV_LEFT, "03")
            sp.edit().putString("setting_gesture_nav_left", "03").apply()
            //SharedPrefsUtils.setStringPreference(KeysUtils.SETTING_GESTURE_NAV_RIGHT, "02")
            sp.edit().putString("setting_gesture_nav_right", "02").apply()
            //SharedPrefsUtils.setBooleanPreference(KeysUtils.SP_LOCATION_9,false)
            sp.edit().putBoolean(KeysUtils.SP_LOCATION_9, false).apply()
        }
        try {

            mahEncryptor = MAHEncryptor.newInstance(sp.getString("saved_key", ""))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        contentFrame = findViewById(R.id.main_content)
        // initialize swipe refresh
        swipeRefreshLayout = findViewById(R.id.swipe)

//        swipeRefreshLayout.setDistanceToTriggerSync(300)

        // swipe distance

        // define a distance
        // define a distance

//        val metrics = resources.displayMetrics
//        var mDistanceToTriggerSync = Math.min(
//            (parent as View).height * 0.6,
//            120 * metrics.density
//        ) as Int.toFloat()
//
//        val mDistanceToTriggerSync: Float = yourCalculation()
//
//        try {
//            // Set the internal trigger distance using reflection.
//            val field: Field =
//                SwipeRefreshLayout::class.java.getDeclaredField("mDistanceToTriggerSync")
//            field.setAccessible(true)
//            field.setFloat(swipeRefreshLayout, mDistanceToTriggerSync)
//        } catch (e: java.lang.Exception) {
//            e.printStackTrace()
//        }

//        val vto: ViewTreeObserver = swipeRefreshLayout.getViewTreeObserver()
//        vto.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
//            override fun onGlobalLayout() {
//                // Calculate the trigger distance.
//                val metrics = resources.displayMetrics
//                val mDistanceToTriggerSync = 1000f
//                try {
//                    // Set the internal trigger distance using reflection.
//                    val field =
//                        SwipeRefreshLayout::class.java.getDeclaredField("mDistanceToTriggerSync")
//                    field.isAccessible = true
//                    field.setFloat(swipeRefreshLayout, mDistanceToTriggerSync)
//                } catch (e: java.lang.Exception) {
//                    e.printStackTrace()
//                }
//
//                // Only needs to be done once so remove listener.
//                val obs: ViewTreeObserver = swipeRefreshLayout.getViewTreeObserver()
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
//                    obs.removeOnGlobalLayoutListener(this)
//                } else {
//                    obs.removeGlobalOnLayoutListener(this)
//                }
//            }
//        })

        //

        shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime)
        dimen156dp = resources.getDimensionPixelSize(R.dimen.layout_width_156dp).toFloat()
        dimen144dp = resources.getDimensionPixelSize(R.dimen.layout_width_144dp).toFloat()
        dimen117dp = resources.getDimensionPixelSize(R.dimen.layout_height_117dp).toFloat()
        dimen108dp = resources.getDimensionPixelSize(R.dimen.layout_height_108dp).toFloat()

        webView = currentAlbumController as BrowserWebView?

        // TODO: turn Incognito mode by-default-off
        SharedPrefsUtils.setBooleanPreference(KeysUtils.keyIsIncoModeOn, false)
        // implement refresh event on swipe
        swipeRefreshLayout.setOnRefreshListener {
            onRefreshRequested()
        }
//        contentFrame.setOnTouchListener { view, me ->
//            webView?.dispatchTouchEvent(me)
//            false
//        }

        webView?.apply {
            this.enableDisableSwipe_ = { position: Boolean ->
                Log.e("bjvdskjfnbgvdslkj", "onCreate: $position")
            }
        }

        /*webView?.setOnScrollChangeListener { view, i, i2, i3, i4 ->
            Log.e("MyScrollingBehaviour", "onCreate:i :: $i \n i2 :: $i2 \n i3 :: $i3 \n i4 :: $i4")
        }*/

        /*  if (`$_SERVER`.get('HTTP_X_REQUESTED_WITH'.code) === "your.app.id") {
              //webview
          } else {
              //browser
          }*/

        //Swipe to Refresh

        configureSwipeRefresh()
//        webView.isdrawer
        /**//*  swipeRefreshLayout = findViewById(R.id.srlRefreshWebPage)*/
        /*  swipeRefreshLayout.setColorSchemeResources(
              R.color.colorPrimary,
              android.R.color.holo_green_dark,
              android.R.color.holo_orange_dark,
              android.R.color.holo_blue_dark
          )*/

//        swipeRefreshContainer.viewTreeObserver.removeOnScrollChangedListener(
//            mOnScrollChangedListener
//        )
//        swipeRefreshContainer.viewTreeObserver.addOnScrollChangedListener(
//            OnScrollChangedListener {
//                /*else {
//                     swipeRefreshContainer.isEnabled = false
//                 }*/
////                 swipeRefreshContainer.isEnabled = webView?.scrollY == 0
//            }.also { mOnScrollChangedListener = it })
//
//
//        swipeRefreshContainer.setOnRefreshListener { webView?.reload() }
//
//        swipeRefreshContainer.canChildScrollUp()

        initOmnibox()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            initSearchPanel()
        }

        initOverview()
        initOverviewHist()
        AdBlock(context)
        Javascript(context)
        Cookie(context)

        downloadReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                bottomSheetDialog = BottomSheetDialog(context, R.style.SheetDialog)
                val dialogView = inflate(context, R.layout.dialog_action, null)
                val textView = dialogView.findViewById<TextView>(R.id.dialog_text)
                textView.setText(R.string.toast_downloadComplete)
                val action_ok = dialogView.findViewById<Button>(R.id.action_ok)
                action_ok.setOnClickListener {
                    startActivity(Intent(context, DownloadsListActivity::class.java))
                    hideBottomSheetDialog()
                }
                val action_cancel = dialogView.findViewById<Button>(R.id.action_cancel)
                action_cancel.setOnClickListener { hideBottomSheetDialog() }
                bottomSheetDialog.setContentView(dialogView)
                bottomSheetDialog.show()
                HelperUnit.setBottomSheetBehavior(
                    bottomSheetDialog,
                    dialogView,
                    BottomSheetBehavior.STATE_EXPANDED
                )
            }
        }
        val filter = IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        registerReceiver(downloadReceiver, filter)
        dispatchIntent(intent)
        if (sp.getBoolean("start_tabStart", false)) {
            showOverview()
            Handler(Looper.getMainLooper()).postDelayed(
                { mBehavior.state = BottomSheetBehavior.STATE_EXPANDED },
                shortAnimTime.toLong()
            )
        }

        findViewById<ImageView>(R.id.ivHomeSearch).setOnClickListener {
            webView!!.loadUrl("www.google.com")
        }

        findViewById<ImageView>(R.id.copyURL).setOnClickListener {
            Log.e("KirtanNarola", "onCreate: kn = >" + webView?.url)
            CopyURL(findViewById<TextView>(R.id.currentURL).text.toString())
        }

        findViewById<ImageButton>(R.id.shareURL).setOnClickListener {
            ShareURL(webView?.url)
        }

        findViewById<ImageButton>(R.id.editURL).setOnClickListener {
            onclickEdit = true
//            findViewById<ImageView>(R.id.ivWebMic1).setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_close));
            findViewById<ImageView>(R.id.ivWebMic1).visibility = GONE
            findViewById<ImageView>(R.id.ivClose).visibility = VISIBLE
            findViewById<SearchView>(R.id.etSearchWebBrowser).setQuery(webView?.url, false)
            val manager = getSystemService(
                INPUT_METHOD_SERVICE
            ) as InputMethodManager
            manager.toggleSoftInput(
                InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY
            );
//            openKeyboard()
        }

        findViewById<ImageView>(R.id.ivClose).setOnClickListener {
            findViewById<SearchView>(R.id.etSearchWebBrowser).setQuery("", false)
        }

        findViewById<LinearLayout>(R.id.tvRecentSearchText).setBackgroundColor(
            resources.getColor(
                R.color.white_color
            )
        );

        val searchEditText =
            searchView2.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)

        context.let {
            KeyboardVisibilityEvent.setEventListener(it, object : KeyboardVisibilityEventListener {
                override fun onVisibilityChanged(isOpen: Boolean) {
                    if (isOpen) {
//                        Toast.makeText(context,"Keyboard is opened",Toast.LENGTH_SHORT).show()
                    } else {
                        if (editToolContainer?.visibility == VISIBLE && firstSearch == false && onclickEdit == true) {
                            val webAddress: String =
                                sharedPreferences.getString("webAddress", "").toString()
                            findViewById<SearchView>(R.id.etSearchWebBrowser).setQuery(
                                webAddress,
                                false
                            )
                            searchEditText.setSelection(0)
                            editToolContainer?.visibility = GONE
                            tvRecentSearchText?.visibility = GONE
                            listRecentContainer!!.visibility = GONE
//                            listRecent.visibility = GONE
                            findViewById<TextView>(R.id.tvTabCount).visibility = VISIBLE
                            findViewById<ImageView>(R.id.ivNewTabPlus).visibility = VISIBLE
                            findViewById<ImageView>(R.id.ivMenu).visibility = VISIBLE
                            findViewById<ImageView>(R.id.ivWebMic1).visibility = GONE
                            findViewById<ImageView>(R.id.ivHomeSearch).visibility = VISIBLE
                            rl_inner_layout!!.visibility = VISIBLE
                            mainSearchBar.visibility = VISIBLE
                            searchView2.visibility = VISIBLE
                            mainSearchBar.visibility = VISIBLE
                        } else {
                            firstSearch = false
                            onclickEdit = true
                        }
//                        Toast.makeText(context,"Keyboard is closed",Toast.LENGTH_SHORT).show()
                    }
                }
            })
        }

        editToolContainer!!.setOnClickListener {
            findViewById<ImageView>(R.id.ivClose).visibility = GONE
            listRecentContainer!!.visibility = GONE
            editToolContainer?.visibility = GONE
            tvRecentSearchText?.visibility = GONE
            closeKeyboard()
            findViewById<ImageView>(R.id.ivWebMic1).visibility = GONE
            findViewById<TextView>(R.id.tvTabCount).visibility = VISIBLE
            findViewById<ImageView>(R.id.ivNewTabPlus).visibility = VISIBLE
            findViewById<ImageView>(R.id.ivMenu).visibility = VISIBLE
            findViewById<ImageView>(R.id.ivHomeSearch).visibility = VISIBLE
            findViewById<TextView>(R.id.currentURL).text.toString()
                .let { it1 -> webView?.loadUrl(it1) }
            setSearchBarData(webView!!)
        }

        searchEditText.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                firstSearch = true
//                if (!TextUtils.isEmpty(v.text.toString())) {
                searchFor(searchEditText.text.toString())
                if (checkwebedit == false) {
//                    listRecentContainer!!.visibility = GONE
//                    editToolContainer?.visibility = GONE
//                    tvRecentSearchText?.visibility = GONE
                    webView?.loadUrl(searchView2.query.toString())
                    closeKeyboard()
//                    findViewById<ImageView>(R.id.ivWebMic1).visibility = GONE
//                    findViewById<TextView>(R.id.tvTabCount).visibility = VISIBLE
//                    findViewById<ImageView>(R.id.ivMenu).visibility = VISIBLE
//                    findViewById<ImageView>(R.id.ivHomeSearch).visibility = VISIBLE
                    checkwebedit = false

                    Handler(Looper.getMainLooper()).postDelayed(Runnable {
                        updateTabsAdapterData(30, webView!!)
                        updateProgress(30)

                        updateTabsAdapterData(50, webView!!)
                        updateProgress(50)

                        setSearchBarData(webView!!, false)

                        //

                        editToolContainer?.visibility = GONE
                        tvRecentSearchText?.visibility = GONE
                        listRecentContainer?.visibility = GONE
                        rl_inner_layout?.visibility = VISIBLE
//                findViewById<LinearLayout>(R.id.llVoiceOption).visibility = VISIBLE
                        findViewById<ImageView>(R.id.ivWebMic1).visibility = GONE
                        findViewById<TextView>(R.id.tvTabCount).visibility = VISIBLE
                        findViewById<ImageView>(R.id.ivNewTabPlus).visibility = VISIBLE
                        findViewById<ImageView>(R.id.ivMenu).visibility = VISIBLE
                        findViewById<ImageView>(R.id.ivHomeSearch).visibility = VISIBLE
                        //

                    }, 1000)

                }
//                    findViewById<SearchView>(R.id.etSearchWebBrowser).setQuery(searchView2.query.toString(),false)
            }
            true
        }

        //recent list
        listRecent = findViewById(R.id.home_list_2)
        val action = RecordAction(context)
        action.open(false)
        val list = action.listHistory()
        list.reverse()
        action.close()
        val adapter = AdapterRecentRecord(context, list)
        listRecent.adapter = adapter
        adapter.notifyDataSetChanged()

        listRecent?.visibility = VISIBLE
        listRecent.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            Log.e("kirtanSnap", "onCreate: List => " + list[position].uRL)
            updateAlbum(list[position].uRL)
//            hideOverview()
            editToolContainer?.visibility = GONE
            tvRecentSearchText?.visibility = GONE
            listRecentContainer?.visibility = GONE
            listRecent?.visibility = GONE

            findViewById<ImageView>(R.id.ivWebMic1).visibility = GONE
            findViewById<TextView>(R.id.tvTabCount).visibility = VISIBLE
            findViewById<ImageView>(R.id.ivNewTabPlus).visibility = VISIBLE
            findViewById<ImageView>(R.id.ivMenu).visibility = VISIBLE
            findViewById<ImageView>(R.id.ivHomeSearch).visibility = VISIBLE
            closeKeyboard()
            //
        }

        findViewById<ImageView>(R.id.ivWebMic1).setOnClickListener {

            // on below line we are calling speech recognizer intent.
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)

            // on below line we are passing language model
            // and model free form in our intent
            intent.putExtra(
                RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
            )
            Log.e("kirtannn", "onCreate: kn" + Locale.getDefault())
            // on below line we are passing our
            // language as a default language.
            intent.putExtra(
                RecognizerIntent.EXTRA_LANGUAGE,
                Locale.getDefault()
            )

            // on below line we are specifying a prompt
            // message as speak to text on below line.
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Speak to text")

            try {
                startActivityForResult(intent, REQUEST_CODE_SPEECH_INPUT)
            } catch (e: Exception) {
                // on below line we are displaying error message in toast
                Toast
                    .makeText(
                        this@BrowserActivity, " " + e.message,
                        Toast.LENGTH_SHORT
                    )
                    .show()
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            webView!!.setOnScrollChangeListener { view, i, i2, i3, i4 ->
                Log.d(TAG, "onScroll: ")
                val appBarLayout: AppBarLayout = findViewById(R.id.ablSearchAppBar)
                //            appBarLayout.setExpanded((i2 < i3), true)
                appBarLayout.elevation = 0F;
            }
        }

        val titleEdit: String = sharedPreferences.getString("EditTitle", "").toString()
        val titleurl: String = sharedPreferences.getString("EditFavicon", "").toString()
        val EditUrl: String = sharedPreferences.getString("EditUrl", "").toString()
        findViewById<TextView>(R.id.currentURL).text = EditUrl
//        if (decodeBase64(titleurl) != null) {
//            currentIcon.setImageBitmap(decodeBase64(titleurl))
//        }else{
//            findViewById<ImageView>(R.id.currentIcon).setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_web_icon));
//        }
        currentTitle.text = titleEdit

    }

    private var isBottomVisible = false
    private fun setUpBottomNavigation() {
        rlCoupons = findViewById(R.id.rlCoupons)
        layoutCompare = findViewById(R.id.layoutCompare)
        layoutStore = findViewById(R.id.layoutStore)
        layoutAppShop = findViewById(R.id.layoutAppShop)
        rlNotification = findViewById(R.id.rlNotification)
//        rlTest = findViewById(R.id.rlTest)
        llBottomNav = findViewById(R.id.llBottomNav)
        clSwitch = findViewById(R.id.clSwitch)
        clSwitchApp = findViewById(R.id.clSwitchApp)


//        rlTest?.setOnClickListener {
//            if(isBottomVisible){
//                isBottomVisible=false
//                llBottomNav?.visibility=View.GONE
//            }else{
//                isBottomVisible=true
//                llBottomNav?.visibility=View.VISIBLE
//            }
//        }

        rlCoupons?.setOnClickListener { openBottomSheetCoupons() }
        layoutCompare?.setOnClickListener { openCompareBottomSheet() }
        layoutStore?.setOnClickListener {
            openStoreBottomSheet()

        }

        rlNotification?.setOnClickListener {
            openBottomSheetNotification()
        }

        //old
        /*clSwitch?.setOnClickListener {
            if (isBrowserOn) {
                isBrowserOn = false
                imCheck?.background = null
                imPhone?.background = ContextCompat.getDrawable(this, R.drawable.switch_card_bg)
                imCheck?.setColorFilter(
                    ContextCompat.getColor(context, R.color.card_switch_bg),
                    PorterDuff.Mode.MULTIPLY
                )
                imPhone?.setColorFilter(
                    ContextCompat.getColor(context, R.color.white),
                    PorterDuff.Mode.SRC_IN
                )
            } else {

                isBrowserOn = true
                imCheck?.background = ContextCompat.getDrawable(this, R.drawable.switch_card_bg)
                imPhone?.background = null
                imCheck?.setColorFilter(
                    ContextCompat.getColor(context, R.color.white),
                    PorterDuff.Mode.MULTIPLY
                )
                imPhone?.setColorFilter(
                    ContextCompat.getColor(context, R.color.card_switch_bg),
                    PorterDuff.Mode.MULTIPLY
                )
            }
        }*/

        //new
        clSwitch?.setOnClickListener {
//            if (isBrowserOn) {
                Log.e(TAG, "Executed first block")
//                val appModeIntent = Intent(this@BrowserActivity, AppModeActivity::class.java)
//                startActivity(appModeIntent)
//                finish()

                llAppModeToolbar?.visibility = VISIBLE
                flAppView.visibility = VISIBLE
                llBottomAppNav?.visibility = VISIBLE

                rlBrowserView.visibility = GONE
                llBottomNav?.visibility = GONE
                isBrowserOn = false
                imCheck?.background = null
                imPhone?.background = ContextCompat.getDrawable(this, R.drawable.switch_card_bg)
                imCheck?.setColorFilter(
                    ContextCompat.getColor(context, R.color.card_switch_bg),
                    PorterDuff.Mode.MULTIPLY
                )
                imPhone?.setColorFilter(ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.SRC_IN)
                setFragment(AppHomeFragment(), "AppHomeFragment", Bundle(), false)

            //set last value of APP MODE/BROWSER MODE
            SharedPrefsUtils.setStringPreference(KeysUtils.KEY_MODE_TYPE, "APP")

        }

        clSwitchApp?.setOnClickListener {
            llAppModeToolbar?.visibility = GONE
            flAppView.visibility = GONE
            llBottomAppNav?.visibility = GONE

            rlBrowserView.visibility = VISIBLE
            llBottomNav?.visibility = VISIBLE
            isBrowserOn = true
            imCheck?.background = ContextCompat.getDrawable(this, R.drawable.switch_card_bg)
            imPhone?.background = null
            imCheck?.setColorFilter(
                ContextCompat.getColor(context, R.color.white),
                PorterDuff.Mode.MULTIPLY
            )
            imPhone?.setColorFilter(
                ContextCompat.getColor(context, R.color.card_switch_bg),
                PorterDuff.Mode.MULTIPLY
            )

            //set last value of APP MODE/BROWSER MODE
            SharedPrefsUtils.setStringPreference(KeysUtils.KEY_MODE_TYPE, "BROWSER")


        }

        layoutAppShop?.setOnClickListener{
            val fragmentInstance = supportFragmentManager.findFragmentById(R.id.flAppView)
            if(fragmentInstance !is AppShopFragment){
                setFragment(AppShopFragment(), "AppShopFragment", Bundle())
            }

            imHome!!.setColorFilter(
                ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
            imStoreApp!!.setColorFilter(
                ContextCompat.getColor(context, R.color.red_primary), android.graphics.PorterDuff.Mode.SRC_IN);
            imProduct!!.setColorFilter(
                ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
            imNotificationsApp!!.setColorFilter(
                ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
        }

        layoutHome?.setOnClickListener {
            val fragmentInstance = supportFragmentManager.findFragmentById(R.id.flAppView)
            if(fragmentInstance !is AppHomeFragment){
                setFragment(AppHomeFragment(), "AppHomeFragment", Bundle())
            }

            imHome!!.setColorFilter(
                ContextCompat.getColor(context, R.color.red_primary), android.graphics.PorterDuff.Mode.SRC_IN);
            imStoreApp!!.setColorFilter(
                ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
            imProduct!!.setColorFilter(
                ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
            imNotificationsApp!!.setColorFilter(
                ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);

        }

        layoutProduct?.setOnClickListener {
            val fragmentInstance = supportFragmentManager.findFragmentById(R.id.flAppView)
            if(fragmentInstance !is ProductsFragment){
                setFragment(ProductsFragment(), "ProductFragment", Bundle())
            }

            imHome!!.setColorFilter(
                ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
            imStoreApp!!.setColorFilter(
                ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
            imProduct!!.setColorFilter(
                ContextCompat.getColor(context, R.color.red_primary), android.graphics.PorterDuff.Mode.SRC_IN);
            imNotificationsApp!!.setColorFilter(
                ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
        }

        notificationApp?.setOnClickListener {
            val fragmentInstance = supportFragmentManager.findFragmentById(R.id.flAppView)
            if(fragmentInstance !is AppModeNotificationFragment){
                setFragment(AppModeNotificationFragment(), "AppModeNotificationFragment", Bundle())
            }

            imHome!!.setColorFilter(
                ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
            imStoreApp!!.setColorFilter(
                ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
            imProduct!!.setColorFilter(
                ContextCompat.getColor(context, R.color.black), android.graphics.PorterDuff.Mode.SRC_IN);
            imNotificationsApp!!.setColorFilter(
                ContextCompat.getColor(context, R.color.red_primary), android.graphics.PorterDuff.Mode.SRC_IN);

        }

        ivUser?.setOnClickListener {
            val fragmentInstance = supportFragmentManager.findFragmentById(R.id.flAppView)
            if(fragmentInstance !is MyAccountFragment){
                setFragment(MyAccountFragment(), "MyAccountFragment", Bundle())
            }
        }

        ivBack?.setOnClickListener {
            this.onBackPressed()
        }

        //CHECK to open BROWSER MODE or APP MODE
        if(SharedPrefsUtils.getStringPreference(KeysUtils.KEY_MODE_TYPE).equals("APP")){
            llAppModeToolbar?.visibility = VISIBLE
            flAppView.visibility = VISIBLE
            llBottomAppNav?.visibility = VISIBLE

            rlBrowserView.visibility = GONE
            llBottomNav?.visibility = GONE
            isBrowserOn = false
            imCheck?.background = null
            imPhone?.background = ContextCompat.getDrawable(this, R.drawable.switch_card_bg)
            imCheck?.setColorFilter(
                ContextCompat.getColor(context, R.color.card_switch_bg),
                PorterDuff.Mode.MULTIPLY
            )
            imPhone?.setColorFilter(ContextCompat.getColor(context, R.color.white), PorterDuff.Mode.SRC_IN)
            setFragment(AppHomeFragment(), "AppHomeFragment", Bundle(), false)
        }else{
            //DO NOTHING because of browser mode is already openend
        }


    }

    fun setFragment(
        fragment: Fragment,
        tag: String,
        bundle: Bundle,
        addToBackstack:Boolean = true
    ) {
        val transaction = supportFragmentManager.beginTransaction()
        fragment.arguments = bundle
        if(addToBackstack){
            transaction.addToBackStack(tag)
        }

        Log.e("setFragment1=", "setFragment")
        Log.e("bundledata1=", bundle.toString())

        transaction.replace(R.id.flAppView, fragment).commit()
    }

    private fun getCouponCode(websiteName: String, webView: WebView, urlSha: String) {
        if (webView.url == null) {
            Log.d("CHECK_BROWSER", "getCouponCode: 1")
            return
        }

        compareBadge!!.visibility = GONE
        compareProductModel = null
        rvCompareProducts?.adapter = null
        offersDBHelper = DBHelperOffer(this)
        //////////////////////////////////////////////////////////////////////////////////////////////
//        val offer = offersDBHelper.readSearchedOffers(websiteName)
//        if (offer.isNotEmpty()) {
//            val resultTime = System.currentTimeMillis() - offer[0].searchTime.toLong()
//            // showToast( TimeUnit.MILLISECONDS.toHours(resultTime).toString())
//            if (TimeUnit.MILLISECONDS.toHours(resultTime).toString() != "0") {
//                offersDBHelper.deleteSearchedOffer(websiteName)
//                offer.clear()
//            }
//
//        }
        ///////////////////////////////////////////////////////////////////////////////////
        Log.d("CHECK_BROWSER", "getCouponCode: 2 : "+webView.url)
        RetrieveDataFromFirebase(offersDBHelper).getCouponCode(
            webView.url,
            object : CouponListInterface {
                override fun getCouponList(model: CouponsAllModel?) {
                    couponsAllModel = model
                    Log.d("CHECK_BROWSER", "getCouponCode: 3")

                    tvCountCouponBottom?.visibility = VISIBLE
                    cvCouponCode?.visibility = VISIBLE
                    tvCountCouponBottom?.text = couponsAllModel?.totalOffers.toString()

                    countryList.clear()
                    for (i in 0 until couponsAllModel?.countries?.size!!) {

                        val countryName = (couponsAllModel!!.countries?.get(i).toString())
                        if (i == 0) {
                            countryList.add(
                                CountryCouponsModel(
                                    countryName, couponsAllModel!!.countries?.get(i).toString(), true
                                )
                            )
                            selectedCountry = couponsAllModel!!.countries?.get(i).toString()
                            val couponsListDD = couponsAllModel?.coupons?.get(selectedCountry)
                            addToRoModel(couponsListDD, isSimilarCoupons, countryName)
                        } else {
                            countryList.add(
                                CountryCouponsModel(
                                    countryName, couponsAllModel!!.countries?.get(i).toString(), false
                                )
                            )
                        }
                    }
                }

                override fun errorOnCouponList(error: String?) {
                    Log.d("CHECK_BROWSER", "getCouponCode: 4: $error")
                    couponsAllModel = null
                    roModel = null
                    tvCountCouponBottom?.visibility = INVISIBLE
                    cvCouponCode?.visibility = INVISIBLE
                }

            });

        //FireStoreServiceCall.getAllCoupons(websiteName, offersDBHelper)

//        if (FireStoreServiceCall.status) {
//            FireStoreServiceCall.status = false
////            FireStoreServiceCall.responseError.observe(this) {
////                couponsAllModel = null
////                roModel = null
////                tvCountCouponBottom?.visibility = View.INVISIBLE
////                cvCouponCode?.visibility = View.INVISIBLE
////            }
//
//            FireStoreServiceCall.responseData.observe(this) {
//                // get Coupons
//                couponsAllModel = it
//
//                // search for sku
//
//                Log.d("CHECK_BROWSER", "getCouponCode: 1")
//
//                if (it.sku_info != null) {
//                    Log.d("CHECK_BROWSER", "getCouponCode: 2")
//                    val skuObject = Gson().fromJson(
//                        (it.sku_info as Map<*, *>?)?.let { it1 -> JSONObject(it1).toString() },
//                        SkuObject::class.java
//                    )
//                    getProductId(
//                        webView.url!!,
//                        skuObject.jquery_path,
//                        skuObject.remove_strings
//                    )
//                } else {
//                    getProductId(
//                        webView.url!!,
//                        "NULL",
//                        ArrayList()
//                    )
//                    Log.d("CHECK_BROWSER", "getCouponCode: 3 : " + webView.url!!)
//                }
//
//                // add Countries Data
//
//            }
//        }

    }


    private fun addToRoModel(couponsListDD: Any?, isSimilarCoupan: Boolean, countryName: String) {

        val listData =
            roBackUpObjectsList.filter { it.isSimilarCoupon == isSimilarCoupan && it.countryName == countryName }

        if (listData != null && listData.isNotEmpty()) {
            roModel = listData[0].data
            return
        }

        roModel = if (couponsListDD != null) {
            val gson = Gson()
            val toJson = gson.toJson(couponsListDD)
            gson.fromJson(toJson, RoModel::class.java)

        } else {
            null
        }

        if (roModel != null) {
            val roModelBackupModel = RoModelBackupModel()
            roModelBackupModel.data = roModel
            roModelBackupModel.countryName = countryName
            roModelBackupModel.isSimilarCoupon = isSimilarCoupan
            roBackUpObjectsList.add(roModelBackupModel)
        }

    }

    private fun getCountryName(lng: String): String {
        val loc = Locale(lng)
        return loc.getDisplayLanguage(loc)
    }

    private fun openBottomSheetNotification() {
        val bottomSheetDialog = BottomSheetDialog(this, R.style.AppBottomSheetDialogTheme)
        bottomSheetDialog.setContentView(R.layout.bottom_notification)

//          BottomSheetBehavior.STATE_COLLAPSED
        //     bottomSheetDialog. behavior.isDraggable = false

//        bottomSheetDialog.behavior.isHideable = true;
        val rvNotification = bottomSheetDialog.findViewById<RecyclerView>(R.id.rvNotification)
        val llEmpty = bottomSheetDialog.findViewById<LinearLayout>(R.id.llEmpty)
        val tvCountInterest = bottomSheetDialog.findViewById<TextView>(R.id.tvCountInterest)
        val llAllNotifications = bottomSheetDialog.findViewById<LinearLayout>(R.id.llAllNotifications)
        val llInterest = bottomSheetDialog.findViewById<LinearLayout>(R.id.llInterest)
        val ivClose = bottomSheetDialog.findViewById<ImageView>(R.id.ivClose)


        rvNotification?.layoutManager = LinearLayoutManager(this@BrowserActivity)
        notificationAdapter = NotificationAdapter(
            this@BrowserActivity, notificationList,
            object: NotificationAdapter.NotificationCallback{
                override fun itemClick(redirectLink: String) {
                    bottomSheetDialog.dismiss()
                    openNewTabForNotification(redirectLink)
                }
            }
        )
        rvNotification?.adapter = notificationAdapter

        getNotification(rvNotification!!, tvCountInterest!!, llEmpty!!)

        ivClose?.setOnClickListener {
            bottomSheetDialog.dismiss()
        }

        llAllNotifications?.setOnClickListener {
            llAllNotifications.backgroundTintList = ContextCompat.getColorStateList(this, android.R.color.white)
            llInterest?.backgroundTintList = ContextCompat.getColorStateList(this, android.R.color.transparent)
        }

        llInterest?.setOnClickListener {
            llAllNotifications?.backgroundTintList = ContextCompat.getColorStateList(this, android.R.color.transparent)
            llInterest.backgroundTintList = ContextCompat.getColorStateList(this, android.R.color.white)
        }

        val bottom_dialog = bottomSheetDialog as BottomSheetDialog

        val bottomSheet = (bottom_dialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?)!!
        //BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
        BottomSheetBehavior.from(bottomSheet).state = BottomSheetBehavior.STATE_EXPANDED
//        val displayMetrics: DisplayMetrics = resources.displayMetrics
//        val height = displayMetrics.heightPixels
//        val maxHeight = (height * 0.80).toInt()
//        BottomSheetBehavior.from(bottomSheet).peekHeight = maxHeight
//        BottomSheetBehavior.from(bottomSheet).maxHeight = maxHeight
        bottomSheetDialog.show()

        /*bottomSheetDialog.behavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    (bottomSheetDialog.behavior).isDraggable = true
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                if (slideOffset < -0.05) {
                    (bottomSheetDialog.behavior).state = BottomSheetBehavior.STATE_HIDDEN
                    (bottomSheetDialog.behavior).isDraggable = false
                }

            }
        })*/
    }

    private fun getNotification(notificationRecyclerView : RecyclerView, tvCountInterest: TextView, llEmpty: LinearLayout){
        //GET DATA FROM FIREBASE
        FireStoreHelper.db?.collection(FireStorageKeys.KEY_USER_TABLE)!!.document(auth.currentUser!!.uid)
            ?.collection(FireStorageKeys.KEYUT_NOTIFICATION)
            ?.get()
            ?.addOnSuccessListener { result ->
                try {

                    if(!notificationList.isNullOrEmpty()){
                        notificationList.clear()
                        notificationAdapter.notifyDataSetChanged()
                    }

                    for (document in result) {
                        val jsonData = Gson().toJson(document.data)
                        val notificationModel = Gson().fromJson(jsonData, NotificationModel::class.java)
                        notificationList.add(notificationModel)
                    }

                    tvCountInterest.text = notificationList.size.toString()
                    notificationAdapter.notifyDataSetChanged()
                    if(notificationList.isEmpty()){
                        notificationRecyclerView.visibility = GONE
                        llEmpty.visibility = VISIBLE
                    }else{
                        notificationRecyclerView.visibility = VISIBLE
                        llEmpty.visibility = GONE
                    }

                    Log.d(TAG,""+ GsonBuilder().setPrettyPrinting().create().toJson(notificationList))


                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }?.addOnFailureListener {
                Log.d("onFailed",""+it.message.toString())
            }
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun openBottomSheetCoupons() {
        try {
            var offersList = offersDBHelper.readAllOffers()
            val bottomSheetDialog = BottomSheetDialog(this, R.style.AppBottomSheetDialogTheme)
            bottomSheetDialog.setContentView(R.layout.bottom_coupons)

//          BottomSheetBehavior.STATE_COLLAPSED
            //     bottomSheetDialog. behavior.isDraggable = false

//        bottomSheetDialog.behavior.isHideable = true;
            val tvCoupons = bottomSheetDialog.findViewById<TextView>(R.id.tvCoupons)
            val tvCountCoupon = bottomSheetDialog.findViewById<TextView>(R.id.tvCountCoupon)
            val tvCountCouponSim = bottomSheetDialog.findViewById<TextView>(R.id.tvCountCouponSim)
            val ivClose = bottomSheetDialog.findViewById<ImageView>(R.id.ivClose)
            val llCoupons = bottomSheetDialog.findViewById<LinearLayout>(R.id.llCoupons)
            val llSimilarCoupons =
                bottomSheetDialog.findViewById<LinearLayout>(R.id.llSimilarCoupons)
            val rvCountry = bottomSheetDialog.findViewById<RecyclerView>(R.id.rvCountry)
            val rvCoupons = bottomSheetDialog.findViewById<RecyclerView>(R.id.rvCoupons)
            val imgCoupon = bottomSheetDialog.findViewById<ImageView>(R.id.imgCoupon)
            val tvNoCoupon = bottomSheetDialog.findViewById<TextView>(R.id.tvNoCoupon)
            val cvAmazonCoupon = bottomSheetDialog.findViewById<CardView>(R.id.cvAmazonCoupon)
            val cvSimilarCoupon = bottomSheetDialog.findViewById<CardView>(R.id.cvSimilarCoupon)
            val llEmpty = bottomSheetDialog.findViewById<LinearLayout>(R.id.llEmpty)

            rvCoupons!!.setOnTouchListener(OnTouchListener { v, event ->
                val action = event.action
                when (action) {
                    MotionEvent.ACTION_DOWN -> {
                        // Disallow NestedScrollView to intercept touch events.
                        Log.d("MotionEvent","ACTION_DOWN")

                        v.parent.requestDisallowInterceptTouchEvent(true)
                    }
                    MotionEvent.ACTION_UP -> {
                        // Allow NestedScrollView to intercept touch events.
                        Log.d("MotionEvent","ACTION_UP")
                        v.parent.requestDisallowInterceptTouchEvent(false)
                    }
                }

                // Handle RecyclerView touch events.
                v.onTouchEvent(event)
                true
            })

            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                llCoupons!!.performClick()
            }, 300)

            ivClose?.setOnClickListener {
                bottomSheetDialog.dismiss()
            }

            if (roModel != null) {
                llEmpty?.visibility = GONE
                rvCountry?.visibility = VISIBLE
                rvCoupons?.visibility = VISIBLE


                //AD CHANGE
                //tvCoupons?.text = "$websiteNameIs Coupons" //ADDED PREVIOUSLY


                cvAmazonCoupon?.visibility = VISIBLE
                cvSimilarCoupon?.visibility = VISIBLE

                if (countryList.size > 1) {
                    rvCountry?.adapter =
                        CountriesCouponAdapter(this, countryList, object : CallBackRecInt {
                            override fun onItemSelected(view: View, position: Int) {
                                val couponsListDD = if (isSimilarCoupons) {
                                    couponsAllModel?.similarCoupons?.get(countryList[position].countryCode)
                                } else {
                                    couponsAllModel?.coupons?.get(countryList[position].countryCode)
                                }
                                offersList = offersDBHelper.readAllOffers()
                                selectedCountry = countryList[position].countryCode.toString()
                                addToRoModel(couponsListDD, isSimilarCoupons, selectedCountry)
                                if (roModel != null) {
                                    llEmpty?.visibility = GONE
                                    //rvCountry?.visibility = View.GONE
                                    rvCoupons?.visibility = VISIBLE
                                    setCouponRecycler(
                                        rvCoupons,
                                        roModel?.couponList!!,
                                        isSimilarCoupons,
                                        offersList,
                                        bottomSheetDialog
                                    )
                                } else {
                                    if (isSimilarCoupons) {
                                        llEmpty?.visibility = VISIBLE
                                        //rvCountry?.visibility = View.GONE
                                        rvCoupons?.visibility = GONE
                                        tvNoCoupon?.text = "There are no similar offers available"
                                        imgCoupon?.setImageDrawable(
                                            ContextCompat.getDrawable(
                                                this@BrowserActivity,
                                                R.drawable.ic_bell_ic
                                            )
                                        )
                                    } else {
                                        llEmpty?.visibility = VISIBLE
                                        //rvCountry?.visibility = View.GONE
                                        rvCoupons?.visibility = GONE
                                        tvNoCoupon?.text = "There are no coupons available"
                                        imgCoupon?.setImageDrawable(
                                            ContextCompat.getDrawable(
                                                this@BrowserActivity,
                                                ic_oval
                                            )
                                        )
                                    }
                                }

                            }
                        })
                    rvCountry?.visibility = VISIBLE
                } else {
                    rvCountry?.visibility = GONE
                }

                // this is for recyclerview to scroll view in bottom sheet
                /*  rvCoupons?.setOnTouchListener { v, event ->
                      v.parent.requestDisallowInterceptTouchEvent(true)
                      v.onTouchEvent(event)
                      true
                  }*/
                tvCountCoupon?.text = couponsAllModel?.couponCount?.toString()
                tvCountCouponSim?.text = couponsAllModel?.similarCouponsCount?.toString()

                setCouponRecycler(
                    rvCoupons,
                    roModel?.couponList!!,
                    isSimilarCoupons,
                    offersList,
                    bottomSheetDialog
                )

            } else {
                llEmpty?.visibility = VISIBLE
                //rvCountry?.visibility = View.GONE
                rvCoupons?.visibility = GONE
            }

            llCoupons?.setOnClickListener {
                llSimilarCoupons?.backgroundTintList =
                    ContextCompat.getColorStateList(this, android.R.color.transparent)
                llCoupons.backgroundTintList =
                    ContextCompat.getColorStateList(this, android.R.color.white)
                isSimilarCoupons = false
                val couponsListDD = couponsAllModel?.coupons?.get(selectedCountry)
                addToRoModel(couponsListDD, isSimilarCoupons, selectedCountry)
                if (roModel != null) {
                    if (!roModel?.couponList?.isNullOrEmpty()!!) {
                        llEmpty?.visibility = GONE
                        rvCountry?.visibility = VISIBLE
                        rvCoupons?.visibility = VISIBLE

                        Log.e(TAG, "openBottomSheetCoupons: $selectedCountry")
                        offersList = offersDBHelper.readAllOffers()
                        setCouponRecycler(
                            rvCoupons,
                            roModel?.couponList!!,
                            isSimilarCoupons,
                            offersList,
                            bottomSheetDialog
                        )
                    } else {
                        llEmpty?.visibility = VISIBLE
                        //rvCountry?.visibility = View.GONE
                        rvCoupons?.visibility = GONE
                        tvNoCoupon?.text = "There are no coupons available"
                        imgCoupon?.setImageDrawable(ContextCompat.getDrawable(this, ic_oval))
                    }
                } else {
                    llEmpty?.visibility = VISIBLE
                    //rvCountry?.visibility = View.GONE
                    rvCoupons?.visibility = GONE
                    tvNoCoupon?.text = "There are no coupons available"
                    imgCoupon?.setImageDrawable(ContextCompat.getDrawable(this, ic_oval))
                }
            }
            llSimilarCoupons?.setOnClickListener {
                llCoupons?.backgroundTintList =
                    ContextCompat.getColorStateList(this, android.R.color.transparent)
                llSimilarCoupons.backgroundTintList =
                    ContextCompat.getColorStateList(this, android.R.color.white)
                isSimilarCoupons = true
                val couponsListDD = couponsAllModel?.similarCoupons?.get(selectedCountry)
                addToRoModel(couponsListDD, isSimilarCoupons, selectedCountry)
                if (roModel != null) {
                    if (!roModel?.couponList?.isNullOrEmpty()!!) {
                        llEmpty?.visibility = GONE
                        rvCountry?.visibility = VISIBLE
                        rvCoupons?.visibility = VISIBLE

                        Log.e(TAG, "openBottomSheetCoupons: $selectedCountry")

                        offersList = offersDBHelper.readAllOffers()
                        setCouponRecycler(
                            rvCoupons,
                            roModel?.couponList!!,
                            isSimilarCoupons,
                            offersList,
                            bottomSheetDialog
                        )
                    } else {
                        llEmpty?.visibility = VISIBLE
                        //rvCountry?.visibility = View.GONE
                        rvCoupons?.visibility = GONE
                        tvNoCoupon?.text = "There are no similar offers available"
                        imgCoupon?.setImageDrawable(
                            ContextCompat.getDrawable(
                                this,
                                R.drawable.ic_bell_ic
                            )
                        )
                    }
                } else {
                    llEmpty?.visibility = VISIBLE
                    //rvCountry?.visibility = View.GONE
                    rvCoupons?.visibility = GONE
                    tvNoCoupon?.text = "There are no similar offers available"
                    imgCoupon?.setImageDrawable(
                        ContextCompat.getDrawable(
                            this,
                            R.drawable.ic_bell_ic
                        )
                    )
                }
            }

//            if (isSimilarCoupons) {
//                llSimilarCoupons?.performClick()
//            } else {
//                llCoupons?.performClick()
//            }

            val bottom_dialog = bottomSheetDialog as BottomSheetDialog

            val bottomSheet =
                (bottom_dialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?)!!
            //BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            //BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED);
            val displayMetrics: DisplayMetrics =
                resources.displayMetrics
            val height = displayMetrics.heightPixels
            val maxHeight = (height * 0.80).toInt()
            BottomSheetBehavior.from(bottomSheet).peekHeight = maxHeight
            BottomSheetBehavior.from(bottomSheet).maxHeight = maxHeight
            bottomSheetDialog.show()



            bottomSheetDialog.behavior.addBottomSheetCallback(object :
                BottomSheetBehavior.BottomSheetCallback() {
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                        (bottomSheetDialog.behavior).isDraggable = true
                    }
                }

                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                    if (slideOffset < -0.05) {
                        (bottomSheetDialog.behavior).state = BottomSheetBehavior.STATE_HIDDEN
                        (bottomSheetDialog.behavior).isDraggable = false
                    }

                }
            })

        } catch (ex: Exception) {
            Log.e("BottomSheetFill", "openBottomSheetCoupons: ${ex.message}")
        }
    }

    private fun setCouponRecycler(
        rvCoupons: RecyclerView?,
        couponList: ArrayList<RoModel.CouponModel?>,
        isSimilarCoupon: Boolean,
        offersList: ArrayList<CopiedCouponsModel>,
        bottomSheetDialog: BottomSheetDialog
    ) {
        @Suppress("UNCHECKED_CAST") val list = couponList as ArrayList<RoModel.CouponModel>

        try {

//            if (websiteNameIs == oldWebsite && coupanAdapter != null) {
            if (false) {
//                coupanAdapter!!.notifyDataSetChanged()
                rvCoupons?.adapter = coupanAdapter
            } else {

//                finalList = checkCopiedValues(list, offersList)

                finalList.clear()
                finalList.addAll(list)

                coupanAdapter = CouponsListAdapter(
                    this,
                    finalList,
                    object : CallBackRecInt {
                        override fun onItemSelected(view: View, position: Int) {
                            val codeCopy = finalList[position].code
                            val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
                            val clip = ClipData.newPlainText("Coupon", codeCopy)
                            clipboard.setPrimaryClip(clip)

//                    view.mySnack("Coupon Copied"){}



                            //ORIGINAL AD SNACKBAR ADDED (08-05-2023)
                            /*val snackBar = showSnackBar(
                                this@BrowserActivity,
                                cvParentLayout!!,
                                Snackbar.LENGTH_SHORT,
                                "Coupon Copied",
                                websiteNameIs,
                                couponsAllModel?.logoUrl!!
                            )
                            snackBar.show()*/



                            offersDBHelper.insertOffer(
                                CopiedCouponsModel(
                                    finalList[position].idOffer.toString(),
                                    finalList[position].code.toString(),
                                    websiteNameIs,
                                    System.currentTimeMillis().toString()
                                )
                            )
                            if (!finalList[position].affiliateUrl.isNullOrEmpty()) {
                                startLoading(finalList[position].affiliateUrl.toString())
                                //wbLoad?.visibility = View.VISIBLE
                            }
                            bottomSheetDialog.dismiss()
                        }
                    }, isSimilarCoupon, couponsAllModel?.logoUrl!!
                )

                rvCoupons?.adapter = coupanAdapter
            }

            oldWebsite = websiteNameIs

        } catch (ex: Exception) {
        }

    }

    private fun setupWebView() {
        CookieManager.getInstance().setAcceptThirdPartyCookies(wbLoad, true)
        CookieManager.getInstance().setAcceptCookie(true)
        val settings = wbLoad!!.settings
        settings.userAgentString = MOBILE_VIEW
        settings.useWideViewPort = true
        settings.allowContentAccess = true
        settings.domStorageEnabled = true
        settings.javaScriptEnabled = true
        settings.loadWithOverviewMode = true

        settings.setSupportZoom(false)
        settings.builtInZoomControls = false

        val headers: MutableMap<String, String> = HashMap()
        headers["User-Agent"] = MOBILE_VIEW

        wbLoad!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                view: WebView,
                request: WebResourceRequest
            ): Boolean {
                view.loadUrl(request.url.toString(), headers)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                //showToast("Loaded $url")
                Handler(Looper.getMainLooper()).postDelayed({
                    //wbLoad?.visibility = View.GONE
                }, 2000)
                super.onPageFinished(view, url)
            }
        }
    }

    fun startLoading(websiteName: String) {
        val headers: MutableMap<String, String> = HashMap()
        headers["User-Agent"] = MOBILE_VIEW
        //wbLoad?.visibility = View.VISIBLE
        wbLoad!!.loadUrl(websiteName, headers)
    }

    private fun checkCopiedValues(
        list: ArrayList<RoModel.CouponModel>, offersList: ArrayList<CopiedCouponsModel>
    ): ArrayList<RoModel.CouponModel> {
        for (i in 0 until offersList.size) {
            val copied = offersList[i]
            for (j in 0 until list.size) {
                if (copied.offerID == list[j].idOffer) {
                    list[j].apply {
//                        isCopied = true
                    }
                }
            }
        }
        return list
    }

    private fun configureSwipeRefresh() {
//        swipeRefreshContainer.setColorSchemeColors(
//            ContextCompat.getColor(
//                this@BrowserActivity,
//                R.color.red_primary
//            )
//        )
//
//        swipeRefreshContainer.setOnRefreshListener {
//            onRefreshRequested()
//        }
//
//        swipeRefreshContainer.setCanChildScrollUpCallback {
//            webView?.canScrollVertically(-1) ?: false
//        }
//
//        // avoids progressView from showing under toolbar
//        swipeRefreshContainer.progressViewStartOffset =
//            swipeRefreshContainer.progressViewStartOffset - 15
    }

    fun onEditorAction(v: TextView): Boolean {
        return if (v.id == EditorInfo.IME_ACTION_SEARCH) {
            true
        } else {
            onBackPressed()
            false
        }
    }

    private fun searchFor(input: String) {
        if (input.startsWith("https")) {
//                findViewById<ImageView>(R.id.ivWebImageTags).getLayoutParams().height = 38; //can change the size according to you requirements
//                findViewById<ImageView>(R.id.ivWebImageTags).getLayoutParams().width = 38; //--
            findViewById<ImageView>(R.id.ivWebImageTags).setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_lock_kn
                )
            );
        } else if (input.startsWith("http")) {
//                findViewById<ImageView>(R.id.ivWebImageTags).getLayoutParams().height = 62; //can change the size according to you requirements
//                findViewById<ImageView>(R.id.ivWebImageTags).getLayoutParams().width = 62;
            findViewById<ImageView>(R.id.ivWebImageTags).setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.ic_noun_information
                )
            );
        } else {
            if (input.isEmpty()) {

                recentTxt.text = "Recent Search"

                val action = RecordAction(context)
                action.open(false)
                val list = action.listHistory()
                list.reverse()
                action.close()

                listRecent.adapter = null

                val adapter = AdapterRecentRecord(context, list)
                listRecent.adapter = adapter
                adapter.notifyDataSetChanged()

                listRecent?.visibility = VISIBLE
                listRecent.onItemClickListener = OnItemClickListener { parent, view, position, id ->
                    Log.e("kirtanSnap", "onCreate: List => " + list[position].uRL)
                    updateAlbum(list[position].uRL)
//            hideOverview()
                    editToolContainer?.visibility = GONE
                    tvRecentSearchText?.visibility = GONE
                    listRecentContainer?.visibility = GONE
                    listRecent?.visibility = GONE

                    findViewById<ImageView>(R.id.ivWebMic1).visibility = GONE
                    findViewById<TextView>(R.id.tvTabCount).visibility = VISIBLE
                    findViewById<ImageView>(R.id.ivNewTabPlus).visibility = VISIBLE
                    findViewById<ImageView>(R.id.ivMenu).visibility = VISIBLE
                    findViewById<ImageView>(R.id.ivHomeSearch).visibility = VISIBLE
                    closeKeyboard()
                }
            } else {
                recentTxt.text = "Suggestions"

                if (countDownTimer != null) {
                    countDownTimer!!.cancel()
                    countDownTimer = null
                }

                countDownTimer = object : CountDownTimer(500, 500) {
                    override fun onTick(millisUntilFinished: Long) {

                    }

                    override fun onFinish() {
                        apiController =
                            ApiController(this@BrowserActivity, object : ResponseInterface {
                                override fun onSuccess(tag: String, response: String) {
                                    Log.e("Data", response)

                                    if (searchView2.query == null || searchView2.query == "") {
                                        return
                                    }

                                    if (findViewById<LinearLayout>(R.id.editToolContainer).visibility == GONE) {
                                        return
                                    }

                                    val data = Gson().fromJson(
                                        response,
                                        SearchModel::class.java
                                    )[1] as ArrayList<String>
                                    val adapter = StringRecord(context, data)

                                    listRecent.adapter = null
                                    listRecent.adapter = adapter
                                    listRecent?.visibility = VISIBLE
                                    adapter.notifyDataSetChanged()
                                    listRecent.onItemClickListener =
                                        OnItemClickListener { parent, view, position, id ->
                                            updateAlbum("https://www.google.com/search?q=" + data[position])
                                            hideOverview()
                                            closeKeyboard()
                                            listRecent?.visibility = GONE
                                            searchView2.visibility = GONE
                                            searchView2.visibility = VISIBLE
                                        }
                                }

                                override fun onError(tag: String, error: String) {

                                }

                                override fun onFailure(tag: String, error: String) {

                                }

                                override fun onNoConnection(tag: String, msg: String) {

                                }

                                override fun onInvalidAuth(tag: String, response: String) {

                                }

                            }
                            )
                        val map = HashMap<String, String>()
                        map["q"] = input
                        map["client"] = "firefox"
                        map["hl"] = "en"
                        apiController!!.getData("search", map)
                        countDownTimer!!.cancel()
                        countDownTimer = null
                    }
                }.start()

            }
        }

    }

    fun CopyURL(url: String?) {
        val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("url", url)
        clipboard.setPrimaryClip(clip)
        Toast.makeText(this, "Url Copied", Toast.LENGTH_SHORT).show()
    }

    fun ShareURL(url: String?) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, "Sharing URL")
        intent.putExtra(Intent.EXTRA_TEXT, url)
        startActivity(Intent.createChooser(intent, "Share URL"))
    }

    private fun closeKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val manager = getSystemService(
                INPUT_METHOD_SERVICE
            ) as InputMethodManager
            manager
                .hideSoftInputFromWindow(
                    view.windowToken, 0
                )
        }
    }

    private fun openKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val manager = getSystemService(
                INPUT_METHOD_SERVICE
            ) as InputMethodManager
            manager
                .toggleSoftInput(
                    InputMethodManager.SHOW_FORCED, 0
                )
        }
    }

    private val mSwipeBoolCallReceiver: BroadcastReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent) {
            //Extract your data - better to use constants...
            val swipeRefreshBool = intent.getBooleanExtra(KeysUtils.KEY_ON_OFF_SWIPE_MENU, true)
            Log.e("swipeRefreshBool", "onReceive: $swipeRefreshBool")
//            swipeRefreshContainer.isEnabled = swipeRefreshBool

        }
    }

    // create funtion for webview reload on swipe refresh
    private fun onRefreshRequested() {
        Log.e("SunnyRefreshLayout", "onRefreshRequested: $")
        webView?.reload()
        //  viewModel.onRefreshRequested()
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (requestCode) {
            REQUEST_CODE_SPEECH_INPUT -> if (resultCode == RESULT_OK &&
                data != null
            ) {
                val result =
                    data.getStringArrayListExtra(
                        RecognizerIntent.EXTRA_RESULTS
                    )
                editToolContainer?.visibility = GONE
                tvRecentSearchText?.visibility = GONE
                listRecentContainer?.visibility = GONE
                rl_inner_layout?.visibility = VISIBLE
//                findViewById<LinearLayout>(R.id.llVoiceOption).visibility = VISIBLE
                findViewById<ImageView>(R.id.ivWebMic1).visibility = GONE
                findViewById<TextView>(R.id.tvTabCount).visibility = VISIBLE
                findViewById<ImageView>(R.id.ivNewTabPlus).visibility = VISIBLE
                findViewById<ImageView>(R.id.ivMenu).visibility = VISIBLE
                findViewById<ImageView>(R.id.ivHomeSearch).visibility = VISIBLE
                Log.e("knvpn", "onActivityResult: 1 => " + result.toString())
                Log.e("FirstValue", "onActivityResult: => " + result?.get(0))
                Log.e(
                    "knvpn",
                    "onActivityResult: => " + result.toString().replace("[", "").replace("]", "")
                )
//                webView?.loadUrl(result.toString().replace("[", "").replace("]", ""))
                if (result!!.size.equals(1)) {
                    webView?.loadUrl(result.toString().replace("[", "").replace("]", ""))
                } else {
                    webView?.loadUrl(result.get(0))
                }
            }
        }

        if (requestCode != INPUT_FILE_REQUEST_CODE || mFilePathCallback == null) {
            super.onActivityResult(requestCode, resultCode, data)
            return
        }

        var results: Array<Uri>? = null
        if (resultCode == RESULT_OK) {
            if (data == null) {
                results = arrayOf(Uri.parse(mCameraPhotoPath))
            } else {
                val dataString = data.dataString
                if (dataString != null) {
                    results = arrayOf(Uri.parse(dataString))
                }
            }
        }

        mFilePathCallback!!.onReceiveValue(results)
        mFilePathCallback = null
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setIntent(intent)
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public override fun onResume() {
        super.onResume()

        IntentUnit.context = context
        dispatchIntent(intent)

        val filter = IntentFilter()
        filter.addAction("ScrollReceiver")
        registerReceiver(mSwipeBoolCallReceiver, filter)
    }

    public override fun onPause() {
        val toHolderService = Intent(context, HolderService::class.java)
        IntentUnit.isClear = false
        stopService(toHolderService)
        inputBox.clearFocus()
        IntentUnit.context = context
        super.onPause()

        try {
            unregisterReceiver(mSwipeBoolCallReceiver)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
    }

    override fun showAlbum(albumController: AlbumController?, isIncoTab: Boolean) {

        if (currentAlbumController != null) {
            currentAlbumController!!.deactivate()
            val av = albumController as View
            contentFrame.removeAllViews()
            contentFrame.addView(av)
        } else {
            contentFrame.removeAllViews()
            contentFrame.addView(albumController as View)
        }
        currentAlbumController = albumController
        currentAlbumController!!.activate()
        updateOmnibox()

        Log.e(
            TAG,
            "showAlbum:_filtered_Incognito-tabs" + tabsDetailsArr.filter { it.isIncoWeb == true })

        val tabsList = (tabsDetailsArr.filter { it.isIncoWeb == false }) as MutableList<TabsModel>
        Log.e(TAG, "showAlbum:tabsList: $tabsList")
        tabAdapter = TabOverViewAdapter(context, tabsList)
        rvTabs.adapter = tabAdapter

        val incoTabsList =
            (tabsDetailsArr.filter { it.isIncoWeb == true }) as MutableList<TabsModel>
        Log.e(TAG, "showAlbum:tabsList:incoTabsList: $incoTabsList")

        incoTabAdapter = IncoTabOverViewAdapter(context, incoTabsList)
        rvIncoTabs.adapter = incoTabAdapter

        setSearchBarData(albumController as WebView, isIncoTab)
        saveOpenTabs("showAlbum")

    }

    private fun saveOpenTabs(from: String) {
        val openTabs = ArrayList<ReOpenTabsModel?>()
        val gson = Gson()

        val normalTabsList = tabsDetailsArr.filter { it.isIncoWeb == false }
        for (i in normalTabsList.indices) {

            val getFocusedTab = normalTabsList.filter { it.cAlbumCon == currentAlbumController }
            if (getFocusedTab.isNotEmpty()) {
                // TODO: we have one focused tab available in normal mode
                if (currentAlbumController == normalTabsList[i].cAlbumCon) {
                    Log.e(
                        TAG,
                        "saveOpenTabs: focused tab1: " + (normalTabsList[i].cAlbumCon as WebView).title
                    )

                    openTabs.add(
                        ReOpenTabsModel(
                            (normalTabsList[i].cAlbumCon as WebView).url,
                            (normalTabsList[i].cAlbumCon as WebView).title,
                            true
                        )
                    )
                } else {
                    openTabs.add(
                        ReOpenTabsModel(
                            (normalTabsList[i].cAlbumCon as WebView).url,
                            (normalTabsList[i].cAlbumCon as WebView).title,
                            false
                        )
                    )
                }

            } else {

                // TODO: if we are in incognito mode then we don't have focused tab.
                //  So we are setting "LAST" Normal tab as focused tab.
                if (i == normalTabsList.size - 1) {
                    Log.e(
                        TAG,
                        "saveOpenTabs: focused tab2: " + (normalTabsList[i].cAlbumCon as WebView).title
                    )
                    openTabs.add(
                        ReOpenTabsModel(
                            (normalTabsList[i].cAlbumCon as WebView).url,
                            (normalTabsList[i].cAlbumCon as WebView).title,
                            true
                        )
                    )
                } else {
                    openTabs.add(
                        ReOpenTabsModel(
                            (normalTabsList[i].cAlbumCon as WebView).url,
                            (normalTabsList[i].cAlbumCon as WebView).title,
                            false
                        )
                    )
                }
            }
        }

        val rejson = gson.toJson(openTabs)
        Log.e(TAG, "saveOpenTabs:$from: Open Tabs::$rejson")
        sp.edit().putString("openTabs", rejson).apply()
    }

    private fun getClosedTabs(): ArrayList<ReOpenTabsModel?>? {
        val gson = Gson()
        val json: String? = sp.getString("openTabs", "")
        val type = object : TypeToken<ArrayList<ReOpenTabsModel?>?>() {}.type
        return gson.fromJson(json, type)
    }

    private fun addTabDataToArr(
        isFirstTab: Boolean,
        webView: BrowserWebView?,
        currentAlbumController: AlbumController?,
        isIncoTab: Boolean,
    ): MutableList<TabsModel> {


        Log.e("Tab added ", "tab")
        val slideUpAnimation: Animation
        val slideDownAnimation: Animation

        slideUpAnimation = AnimationUtils.loadAnimation(
            applicationContext,
            R.anim.slide_up
        );

        slideDownAnimation = AnimationUtils.loadAnimation(
            applicationContext,
            R.anim.slide_down
        );

//        webView!!.getSettings().setRenderPriority(WebSettings.RenderPriority.LOW);
//        webView!!.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
//        webView!!.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//        webView!!.settings.setDomStorageEnabled(true);
//        webView!!.settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
//        // extra settings
//        webView.getSettings().setLoadWithOverviewMode(false);
//        webView.getSettings().setUseWideViewPort(true);
//        webView.setScrollContainer(true);
//        // setting for lollipop and above
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
//            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
//        }
//        webView!!.settings.setSavePassword(true);
//        webView!!.settings.setSaveFormData(true);
//        webView!!.settings.setEnableSmoothTransition(true);
//        webView.settings.allowFileAccess = true
//        webView.settings.allowContentAccess = true
//
////        val newUA = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0"
////        val newUA = "Android"
//        val newUA =
//            "Mozilla/5.0 (Linux; U; Android 10; SM-G960F Build/QP1A.190711.020; wv)"
//        webView.getSettings().setUserAgentString(newUA)

//        webView?.webViewClient = object : WebViewClient() {
//            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
//                view?.loadUrl(url!!)
//                setSearchBarData(view!!)
//                val uri = Uri.parse(url)
//                handleUri(webView!!,uri)
//                return false
//            }
//        }

        appBarHeight = findViewById<AppBarLayout>(R.id.ablSearchAppBar).layoutParams.height
        appBarBehavior = findViewById<AppBarLayout>(R.id.ablSearchAppBar).behavior


        // logic with coordinator

//        var job: Job? = null
//        var currentScroll = 0
//        var oldScroll = 0
//
//        val params =
//            findViewById<AppBarLayout>(R.id.ablSearchAppBar).layoutParams as CoordinatorLayout.LayoutParams
//        if (params.behavior == null)
//            params.behavior = AppBarLayout.Behavior()
//        val behaviour = params.behavior as AppBarLayout.Behavior
//        behaviour.setDragCallback(object : AppBarLayout.Behavior.DragCallback() {
//            override fun canDrag(appBarLayout: AppBarLayout): Boolean {
//                return false
//            }
//        })

//        webView?.onScrollChangeListener = object : BrowserWebView.OnScrollChangeListener {
//            override fun onScrollChange(scrollY: Int, oldScrollY: Int) {
//                currentScroll = scrollY
//                if (scrollY < 500) {
//                    if(findViewById<AppBarLayout>(R.id.ablSearchAppBar).behavior != null){
//                        findViewById<AppBarLayout>(R.id.ablSearchAppBar).behavior = null
//                    }
////                    setVisibleAppBar(true)
//
//                } else {
//
////                    CoroutineScope(Dispatchers.Default).launch {
////
////                        if (job == null) {
////                            job = CoroutineScope(Dispatchers.Default).launch {
////                                oldScroll = scrollY
////                                Log.e("Marginn", "$oldScroll")
////                                delay(500)
////                                Log.e("Margin", "$oldScroll $currentScroll")
////                                if (oldScroll - currentScroll > 50) {
////                                    runOnUiThread {
////                                        setVisibleAppBar(true)
//////                                        findViewById<AppBarLayout>(R.id.ablSearchAppBar).visibility =
//////                                            View.VISIBLE
////                                    }
////                                } else {
////                                    runOnUiThread {
////                                        setVisibleAppBar(false)
//////                                        findViewById<AppBarLayout>(R.id.ablSearchAppBar).visibility =
//////                                            View.GONE
////                                    }
////                                }
////                            }
////
////                            job!!.join()
////                            job = null
////                        }
////
////
////                    }
//                }
//            }
//        }

//        val params: CoordinatorLayout.LayoutParams = findViewById<AppBarLayout>(R.id.coordinatorLay).layoutParams as CoordinatorLayout.LayoutParams
//        params.behavior = AppBarLayout.ScrollingViewBehavior()

//        webView!!.setOnTouchListener(object : View.OnTouchListener {
//            private var startClickTime: Long = 0
//            lateinit var downEvent: MotionEvent
//            lateinit var upEvent: MotionEvent
//            override fun onTouch(v: View?, event: MotionEvent): Boolean {
//
//                if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                    Log.e("Event", "down " + event.y)
//                    downEvent = event
//                    startClickTime = System.currentTimeMillis();
//                } else if (event.getAction() == MotionEvent.ACTION_UP) {
//                    upEvent = event
//                    Log.e("Event", "Up " + event.y)
//                }
//
//            return false
//
//        }
//
//    })

//        findViewById<AppBarLayout>(R.id.ablSearchAppBar).addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener{
//            override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
//                Log.e("Offset", "$verticalOffset ")
//            }
//        })

//logic without coordinator


        var isExpanded = true
        var job: Job? = null
        var currentScroll = 0
        var oldScroll = 0
        var startX = 0f
        var startY = 0f
        webView?.setOnTouchListener(object : OnTouchListener {
            override fun onTouch(p0: View?, event: MotionEvent): Boolean {
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        startX = event.x
                        startY = event.y
                    }

                    MotionEvent.ACTION_MOVE -> {
                        if (event.y - startY > 0) {
                            Log.e("onTouchSwipe: ", (event.y - startY).toString())
                            swipeRefreshLayout.isEnabled =
                                event.y - startY > 200 && webView.scrollY == 0
                        } else {
                            Log.e("onTouchSwipe: ", (startY - event.y).toString())
                            swipeRefreshLayout.isEnabled =
                                startY - event.y > 200 && webView.scrollY == 0
                        }
                    }

//                     MotionEvent.ACTION_UP -> {
//                        if (event.y - startY > 0) {
//                            Log.e( "onTouchSwipe: ",(event.y - startY).toString() )
//                            swipeRefreshLayout.isEnabled =
//                                event.y - startY > 300 && webView.scrollY == 0
//                        } else {
//                            Log.e( "onTouchSwipe: ",(startY-event.y).toString() )
//                            swipeRefreshLayout.isEnabled =
//                                startY - event.y > 300 && webView.scrollY == 0
//                        }
//                    }

                }
                return false
            }

        })

        webView?.onScrollChangeListener = object : BrowserWebView.OnScrollChangeListener {
            override fun onScrollChange(scrollY: Int, oldScrollY: Int) {
                currentScroll = scrollY
                if (scrollY < 200) {
                    setVisibleAppBar(true)
//                    findViewById<AppBarLayout>(R.id.ablSearchAppBar).visibility = View.VISIBLE
                } else {

                    CoroutineScope(Dispatchers.Default).launch {

                        if (job == null) {
                            job = CoroutineScope(Dispatchers.Default).launch {
                                oldScroll = scrollY
                                Log.e("Marginn", "$oldScroll")
                                delay(500)
                                Log.e("Margin", "$oldScroll $currentScroll")
                                if (currentScroll > 400) {
                                    if (oldScroll - currentScroll > 50) {
                                        runOnUiThread {
                                            setVisibleAppBar(true)
//                                        findViewById<AppBarLayout>(R.id.ablSearchAppBar).visibility =
//                                            View.VISIBLE
                                        }
                                    }
                                    if (currentScroll - oldScroll > 50) {
                                        runOnUiThread {
                                            setVisibleAppBar(false)
//                                        findViewById<AppBarLayout>(R.id.ablSearchAppBar).visibility =
//                                            View.GONE
                                        }
                                    }
                                }
                            }

                            job!!.join()
                            job = null
                        }


                    }
                }

            }
        }

        if (isFirstTab) {

            val img = ViewUnit.capture(
                webView as View,
                dimen144dp,
                dimen108dp,
                Bitmap.Config.RGB_565
            )

            val tabData = TabsModel(
                webView,
                webView.title,
                img,
                isIncoTab
            )
            tabsDetailsArr.add(tabData)

        } else {
            val img = ViewUnit.capture(
                currentAlbumController as View,
                dimen144dp,
                dimen108dp,
                Bitmap.Config.RGB_565
            )
            val tabData = TabsModel(
                webView,
                webView?.title,
                img,
                isIncoTab
            )
            tabsDetailsArr.add(tabData)
        }
        return tabsDetailsArr
    }

    fun setVisibleAppBar(status: Boolean) {

        CoroutineScope(Dispatchers.Main).launch {
            if (status) {
//                findViewById<AppBarLayout>(R.id.ablSearchAppBar).setExpanded(true)
//                findViewById<View>(R.id.compansationView).visibility = View.VISIBLE
//                slideUp(findViewById<View>(R.id.compansationView))
                //        setConstraintVisible(findViewById<ConstraintLayout>(R.id.rootConstraintLay),findViewById<AppBarLayout>(R.id.ablSearchAppBar),findViewById<View>(R.id.compansationView))
            } else {
//                findViewById<AppBarLayout>(R.id.ablSearchAppBar).setExpanded(false)
//                findViewById<View>(R.id.compansationView).visibility = View.GONE
//                slideDown(findViewById<View>(R.id.compansationView))
                //       setConstraintInVisible(findViewById<ConstraintLayout>(R.id.rootConstraintLay),findViewById<AppBarLayout>(R.id.ablSearchAppBar),findViewById<View>(R.id.compansationView))
            }
        }

    }

    private fun updateRecyclerViewAdapter() {
        val recyclerViewState = rvTabs.layoutManager?.onSaveInstanceState()
        val tabsList = (tabsDetailsArr.filter { it.isIncoWeb == false }) as MutableList<TabsModel>
        tabAdapter!!.updateTabData(tabsList)
        tabAdapter!!.notifyDataSetChanged()
        rvTabs.layoutManager?.onRestoreInstanceState(recyclerViewState)

        val incoRecyclerViewState = rvIncoTabs.layoutManager?.onSaveInstanceState()
        val incoTabsList =
            (tabsDetailsArr.filter { it.isIncoWeb == true }) as MutableList<TabsModel>
        incoTabAdapter!!.updateTabData(incoTabsList)
        incoTabAdapter!!.notifyDataSetChanged()
        rvIncoTabs.layoutManager?.onRestoreInstanceState(incoRecyclerViewState)
    }

    fun setConstraintVisible(
        rootLayout: ConstraintLayout,
        appBarLayout: AppBarLayout,
        customView: View
    ) {
        //rootLayout is the Id of the parent layout
        val constraintSet = ConstraintSet()
        constraintSet.clone(rootLayout)
        constraintSet.clear(appBarLayout.id, ConstraintSet.BOTTOM)
        constraintSet.clear(appBarLayout.id, ConstraintSet.TOP)
        constraintSet.connect(
            appBarLayout.id,
            ConstraintSet.BOTTOM,
            customView.id,
            ConstraintSet.BOTTOM
        )
        constraintSet.connect(appBarLayout.id, ConstraintSet.TOP, customView.id, ConstraintSet.TOP)
        val transition: Transition = ChangeBounds()
        transition.interpolator = LinearInterpolator()
//        transition.interpolator = AnticipateOvershootInterpolator(1.0f)
        transition.duration = 200
        TransitionManager.beginDelayedTransition(rootLayout, transition)
        constraintSet.applyTo(rootLayout)
    }

    fun setConstraintInVisible(
        rootLayout: ConstraintLayout,
        appBarLayout: AppBarLayout,
        customView: View
    ) {
        //rootLayout is the Id of the parent layout
        val constraintSet = ConstraintSet()
        constraintSet.clone(rootLayout)
        constraintSet.clear(appBarLayout.id, ConstraintSet.BOTTOM)
        constraintSet.clear(appBarLayout.id, ConstraintSet.TOP)
        constraintSet.connect(
            appBarLayout.id,
            ConstraintSet.BOTTOM,
            customView.id,
            ConstraintSet.TOP
        )
        val transition: Transition = ChangeBounds()
        transition.interpolator = LinearInterpolator()
//        transition.interpolator = AnticipateOvershootInterpolator(1.0f)
        transition.duration = 200
        TransitionManager.beginDelayedTransition(rootLayout, transition)
        constraintSet.applyTo(rootLayout)
    }

    private fun slideUp(child: View) {
        child.clearAnimation()
        child.animate().translationY(0f).duration = 200
    }

    private fun slideDown(child: View) {
        child.clearAnimation()
        child.animate().translationY(child.height.toFloat()).duration = 200
    }

    private fun saveToInternalStorage(bitmapImage: Bitmap): String? {
        val cw = ContextWrapper(applicationContext)

        val imagePreFix = "webViewImg_"
        val directory = cw.getDir(WEBVIEW_IMG_DIR, MODE_PRIVATE)
        val mypath = File(directory, imagePreFix + "name.jpg")
        var fos: FileOutputStream? = null
        try {
            fos = FileOutputStream(mypath)
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos)
            0
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            try {
                fos?.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return directory.absolutePath
    }


    private fun showTabMenuDialog() {
        val dialog = Dialog(context)
        dialog.setCancelable(true)
        val mBinding = TabMenuDialogLayoutBinding.inflate(layoutInflater)
        dialog.setContentView(mBinding.root)


//        mBinding.topControlBar.visibility = GONE
        mBinding.tvOpenBookmarks.visibility = GONE
        mBinding.ivOpenBookmarks.visibility = GONE

        val window = dialog.window
        val wlp = window!!.attributes
        wlp.gravity = Gravity.TOP or Gravity.END
        wlp.flags = wlp.flags and WindowManager.LayoutParams.FLAG_DIM_BEHIND.inv()
        window.attributes = wlp

        val width = (resources.displayMetrics.widthPixels * 0.70).toInt()
        val height = WindowManager.LayoutParams.WRAP_CONTENT
        window.setLayout(width, height)

        mBinding.tvNewTabFromDialog.setOnClickListener {
            dialog.dismiss()
            // TODO: new normal tab
            SharedPrefsUtils.setBooleanPreference(KeysUtils.keyIsIncoModeOn, false)
            hideBottomSheetDialog()
            hideOverview()
            addAlbum(
                "New tab",
                sp.getString("favoriteURL", "https://google.com"),
                true
            )
        }

        mBinding.tvNewIcoTab.setOnClickListener {
            // TODO: new incognito tab
            dialog.dismiss()

            SharedPrefsUtils.setBooleanPreference(KeysUtils.keyIsIncoModeOn, true)

            hideBottomSheetDialog()
            hideOverview()
            addAlbum(
                "New tab",
                sp.getString("favoriteURL", "https://google.com"),
                true, isIncoTab = true
            )
        }

        mBinding.tvDownloads.setOnClickListener {
            if (if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                } else {
                    TODO("VERSION.SDK_INT < M")
                }
            ) {
                HelperUnit.grantPermissionsStorage(context)
            } else {
                startActivity(Intent(context, DownloadsListActivity::class.java))
            }
            dialog.dismiss()
        }

        mBinding.ivDownloadIcon.setOnClickListener {
            if (if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                } else {
                    TODO("VERSION.SDK_INT < M")
                }
            ) {
                HelperUnit.grantPermissionsStorage(context)
            } else {
                startActivity(Intent(context, DownloadsListActivity::class.java))
            }
            dialog.dismiss()
        }

        mBinding.tvHistory.setOnClickListener {
            setHistoryData()
            showOverview_hist()
            dialog.dismiss()
        }


        mBinding.tvOpenBookmarks.setOnClickListener {

            dialog.dismiss()
        }

        mBinding.ivMoveBack.setOnClickListener {
            onBackWordPressed()
            rl_inner_layout!!.visibility = VISIBLE
            dialog.dismiss()
        }

        mBinding.ivMoveForward.setOnClickListener {
            onForwardPressed()
            searchView2.visibility = VISIBLE

            dialog.dismiss()
        }

        mBinding.ivBookmark.setOnClickListener {

            dialog.dismiss()
        }

        mBinding.ivRefresh.setOnClickListener {
            webView?.reload()
            dialog.dismiss()
        }

        mBinding.tvRefreshes.setOnClickListener {

            dialog.dismiss()
            webView?.reload()
        }

        mBinding.tvShareLink.setOnClickListener {
            val shareIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, webView?.url)
                type = "text/plain"
            }
            startActivity(Intent.createChooser(shareIntent, null))
            dialog.dismiss()
        }

        if (!dialog.isShowing)
            dialog.show()

    }

    private fun updateTabCount(isIncoTab: Boolean) {

        // TODO: for updating Tab count of standard/incognito Tabs
        tvTabCount.text = tabsDetailsArr.filter { it.isIncoWeb == isIncoTab }.size.toString()
        // TODO: for updating Tab overview menu count
        tvOverViewMenuTabCount.text =
            tabsDetailsArr.filter { it.isIncoWeb == false }.size.toString()

    }

    fun updateTopHttpIcons(currentWebView: WebView) {

        var tempUrl = currentWebView.url
        if (tempUrl != null) {

            if (tempUrl.startsWith("https")) {
//                findViewById<ImageView>(R.id.ivWebImageTags).getLayoutParams().height = 38; //can change the size according to you requirements
//                findViewById<ImageView>(R.id.ivWebImageTags).getLayoutParams().width = 38; //--
                findViewById<ImageView>(R.id.ivWebImageTags).setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_lock_kn
                    )
                );
            } else if (tempUrl.startsWith("http")) {
//                findViewById<ImageView>(R.id.ivWebImageTags).getLayoutParams().height = 62; //can change the size according to you requirements
//                findViewById<ImageView>(R.id.ivWebImageTags).getLayoutParams().width = 62;
                findViewById<ImageView>(R.id.ivWebImageTags).setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_noun_information
                    )
                );
            }
        }
    }

    fun setSearchBarData(currentWebView: WebView, isIncoTab: Boolean? = false) {
        val searchEditText =
            searchView2.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
        Log.e(TAG, "setSearchBarData:url::- " + currentWebView.url)
        var tempUrl = currentWebView.url
        if (tempUrl != null) {

            if (tempUrl.startsWith("https")) {
//                findViewById<ImageView>(R.id.ivWebImageTags).getLayoutParams().height = 38; //can change the size according to you requirements
//                findViewById<ImageView>(R.id.ivWebImageTags).getLayoutParams().width = 38; //--
                findViewById<ImageView>(R.id.ivWebImageTags).setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_lock_kn
                    )
                );
            } else if (tempUrl.startsWith("http")) {
//                findViewById<ImageView>(R.id.ivWebImageTags).getLayoutParams().height = 62; //can change the size according to you requirements
//                findViewById<ImageView>(R.id.ivWebImageTags).getLayoutParams().width = 62;
                findViewById<ImageView>(R.id.ivWebImageTags).setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_noun_information
                    )
                );
            }

            if (tempUrl.contains("https://")) {
                tempUrl = tempUrl.replace("https://", "")
            }
            if (tempUrl.contains("http://")) {
                tempUrl = tempUrl.replace("http://", "")
            }
            if (tempUrl.contains("www.")) {
                tempUrl = tempUrl.replace("www.", "")
            }

            if (tempUrl.length > 0 && tempUrl.get(tempUrl.length - 1).toString().equals("/")) {
                tempUrl = tempUrl.substring(0, tempUrl.length - 1)
            }

        }
        searchView2.setQuery(tempUrl, false)
        searchEditText.setSelection(0)

        Log.e(
            TAG,
            "setSearchBarData:incoTabs: " + tabsDetailsArr.filter { it.isIncoWeb == true }.size
        )
        Log.e(
            TAG,
            "setSearchBarData:stdTabs: " + tabsDetailsArr.filter { it.isIncoWeb == false }.size
        )

        if (isIncoTab!!) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mainSearchBar.setBackgroundColor(context.getColor(R.color.black))
            }
            findViewById<ImageView>(R.id.ivHomeSearch).setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.white_color
                ), PorterDuff.Mode.SRC_IN
            )
            tvTabCount.setTextColor(Color.parseColor("#FFFFFF"))
            tvTabCount.background = ContextCompat.getDrawable(this, R.drawable.bg_tab_border_light)
            ivMenu.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.white_color
                ), PorterDuff.Mode.SRC_IN
            )
            tvTabCount.text = tabsDetailsArr.filter { it.isIncoWeb == true }.size.toString()
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mainSearchBar.setBackgroundColor(context.getColor(R.color.white_color))
            }
            findViewById<ImageView>(R.id.ivHomeSearch).setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.black
                ), PorterDuff.Mode.SRC_IN
            )
            tvTabCount.setTextColor(Color.parseColor("#FF000000"))
            tvTabCount.background =
                ContextCompat.getDrawable(this, R.drawable.ic_btn_tabswitcher_modern)
            ivMenu.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.black
                ), PorterDuff.Mode.SRC_IN
            )
            tvTabCount.text = tabsDetailsArr.filter { it.isIncoWeb == false }.size.toString()
        }


//        searchView2.clearFocus()
    }


    override fun updateAutoComplete() {
        val action = RecordAction(this)
        action.open(false)
        val list = action.listHistory()
        list.addAll(action.listHistory())
        action.close()
        val adapter = CompleteAdapter(list)
        inputBox.setAdapter(adapter)
        adapter.notifyDataSetChanged()
        inputBox.threshold = 1
        inputBox.dropDownVerticalOffset = -16
        inputBox.dropDownWidth = ViewUnit.getWindowWidth(this)
        inputBox.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            val url = (view.findViewById<View>(R.id.complete_item_url) as TextView).text.toString()
            updateAlbum(url)

            hideKeyboard(context)
        }
    }

    override fun updateBookmarks() {
        val action = RecordAction(context)
        action.open(false)
        action.close()
    }

    private fun showOverview(isIncoTab: Boolean? = false) {

        setDialogColors(isIncoTab!!)

        overview_top.visibility = VISIBLE
        mBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        if (currentAlbumController != null) {
            currentAlbumController!!.deactivate()
            currentAlbumController!!.activate()
        }

        open_startPageView.setBackgroundColor(vibrantColor)
        open_historyView.setBackgroundColor(vibrantColor)
        open_bookmarkView.setBackgroundColor(vibrantColor)
        overview_titleIcons_startView.setBackgroundColor(vibrantColor)
        overview_titleIcons_bookmarksView.setBackgroundColor(vibrantColor)
        overview_titleIcons_historyView.setBackgroundColor(vibrantColor)
        bottomSheetDialog_OverView.show()
        Handler(Looper.getMainLooper()).postDelayed({
            tab_ScrollView.smoothScrollTo(
                currentAlbumController!!.albumView!!.left,
                0
            )
        }, shortAnimTime.toLong())

    }

    private fun showOverview_hist() {

        overview_top.visibility = VISIBLE
        mBehavior_hist.state = BottomSheetBehavior.STATE_EXPANDED
        if (currentAlbumController != null) {
            currentAlbumController!!.deactivate()
            currentAlbumController!!.activate()
        }

        open_historyView.setBackgroundColor(vibrantColor)
        overview_titleIcons_historyView.setBackgroundColor(vibrantColor)
        bottomSheetDialog_OverView_hist.show()
        Handler(Looper.getMainLooper()).postDelayed({
            tab_ScrollView.smoothScrollTo(
                currentAlbumController!!.albumView!!.left,
                0
            )
        }, shortAnimTime.toLong())

    }

    override fun hideOverview() {
        bottomSheetDialog_OverView.cancel()
    }

    private fun hideBottomSheetDialog() {
        bottomSheetDialog.cancel()
    }

    private fun onBackWordPressed() {
        if (webView!!.canGoBack()) {
            webView!!.goBack()
        } else {
            if (!back) {
                rl_inner_layout!!.visibility = GONE
                progressBar.visibility = INVISIBLE
                progressShadow!!.visibility = GONE
                mainSearchBar.visibility = VISIBLE
                searchView2.visibility = VISIBLE
//                mainSearchBar.visibility = VISIBLE
                back = true
                Toast.makeText(this, "Cannot Go Back", Toast.LENGTH_SHORT).show()
            } else {
                try {
                    unregisterReceiver(downloadReceiver)
                } catch (e: IllegalArgumentException) {
                    e.printStackTrace()
                }
                doubleTapsQuit()
            }
        }
    }

    fun onForwardPressed() {
        if (webView!!.canGoForward()) {
            webView!!.goForward()
            rl_inner_layout!!.visibility = VISIBLE
            progressBar.visibility = VISIBLE
            progressShadow!!.visibility = VISIBLE
            searchView2.visibility = GONE
            back = false
        } else {
            back = false
            Toast.makeText(this, "Cannot Go Forward", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onClick(v: View) {
        val action = RecordAction(context)
        webView = currentAlbumController as BrowserWebView?
        try {
            title = webView!!.title!!.trim { it <= ' ' }
            url = webView!!.url!!.trim { it <= ' ' }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        when (v.id) {
            R.id.tab_plus -> {
                hideBottomSheetDialog()
                hideOverview()
                addAlbum(
                    getString(R.string.app_name),
                    sp.getString("favoriteURL", "https://google.com"),
                    true
                )
            }
            R.id.tab_plus_bottom, R.id.tvNewTab -> {
                hideBottomSheetDialog()
                hideOverview()
                val isIncoOn =
                    SharedPrefsUtils.getBooleanPreference(KeysUtils.keyIsIncoModeOn, false)
                addAlbum(
                    getString(R.string.app_name),
                    sp.getString("favoriteURL", "https://google.com"),
                    true,
                    isIncoTab = isIncoOn
                )
            }

            /*R.id.btnIncoIcon -> {
                rvIncoTabs.visibility = VISIBLE
                rvTabs.visibility = GONE
                fdaf
                findViewById<View>(R.id.hlStdTabs).visibility = INVISIBLE
                findViewById<View>(R.id.hlIncoTabs).visibility = VISIBLE
                findViewById<View>(R.id.rlOverViewDialog).setBackgroundColor(myContext.getColor(R.color.black))
                tab_plus_bottom
            }
            R.id.tvStdTabCount -> {
                rvTabs.visibility = VISIBLE
                rvIncoTabs.visibility = GONE
                findViewById<View>(R.id.hlStdTabs).visibility = VISIBLE
                findViewById<View>(R.id.hlIncoTabs).visibility = INVISIBLE
                findViewById<View>(R.id.rlOverViewDialog).setBackgroundColor(myContext.getColor(R.color.white))

            }*/


            R.id.menu_newTabOpen,
            -> {
                hideBottomSheetDialog()
                hideOverview()
                addAlbum(
                    getString(R.string.app_name),
                    sp.getString("favoriteURL", "https://google.com"),
                    true
                )
            }
            R.id.menu_closeTab -> {
                hideBottomSheetDialog()
                removeAlbum(currentAlbumController!!)
            }
            R.id.menu_tabPreview -> {
                hideBottomSheetDialog()
                showOverview()
            }
            R.id.menu_quit -> {
                hideBottomSheetDialog()
                doubleTapsQuit()
            }
            R.id.menu_shareScreenshot -> if (Build.VERSION.SDK_INT >= 23) {
                val hasWRITE_EXTERNAL_STORAGE =
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (hasWRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
                    HelperUnit.grantPermissionsStorage(context)
                } else {
                    hideBottomSheetDialog()
                    sp.edit().putInt("screenshot", 1).apply()
                }
            } else {
                hideBottomSheetDialog()
                sp.edit().putInt("screenshot", 1).apply()
            }
            R.id.menu_shareLink -> {
                hideBottomSheetDialog()
                if (prepareRecord()) {
                    BrowserToast.show(context, getString(R.string.toast_share_failed))
                } else {
                    IntentUnit.share(context, title, url)
                }
            }
            R.id.menu_sharePDF -> {
                hideBottomSheetDialog()
                printPDF(true)
            }
            R.id.menu_openWith -> {
                hideBottomSheetDialog()
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(url)
                val chooser = Intent.createChooser(intent, getString(R.string.menu_open_with))
                startActivity(chooser)
            }
            R.id.menu_saveScreenshot -> if (Build.VERSION.SDK_INT >= 23) {
                val hasWRITE_EXTERNAL_STORAGE =
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                if (hasWRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
                    HelperUnit.grantPermissionsStorage(context)
                } else {
                    hideBottomSheetDialog()
                    sp.edit().putInt("screenshot", 0).apply()
                }
            } else {
                hideBottomSheetDialog()
                sp.edit().putInt("screenshot", 0).apply()
            }
            R.id.menu_saveBookmark -> {
                hideBottomSheetDialog()
                try {
                    val mahEncryptor = MAHEncryptor.newInstance(
                        Objects.requireNonNull(
                            sp.getString("saved_key", "")
                        )
                    )
                    val encrypted_userName = mahEncryptor.encode("")
                    val encrypted_userPW = mahEncryptor.encode("")
                    val db = BookmarkList(context)
                    db.open()
                    if (db.isExist(url!!)) {
                        BrowserToast.show(context, R.string.toast_newTitle)
                    } else {
                        db.insert(
                            HelperUnit.secString(webView!!.title),
                            url!!,
                            encrypted_userName,
                            encrypted_userPW,
                            "01"
                        )
                        BrowserToast.show(context, R.string.toast_edit_successful)
                        initBookmarkList()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    BrowserToast.show(context, R.string.toast_error)
                }
            }
            R.id.menu_saveStart -> {
                hideBottomSheetDialog()
                action.open(true)
                if (action.checkGridItem(url)) {
                    BrowserToast.show(context, getString(R.string.toast_already_exist_in_home))
                } else {
                    var counter = sp.getInt("counter", 0)
                    counter += 1
                    sp.edit().putInt("counter", counter).apply()
                    val bitmap =
                        ViewUnit.capture(webView!!, dimen156dp, dimen117dp, Bitmap.Config.ARGB_8888)
                    val filename = counter.toString() + BrowserUnit.SUFFIX_PNG
                    val itemAlbum = GridItem(title, url, filename, counter)
                    if (BrowserUnit.bitmap2File(context, bitmap, filename) && action.addGridItem(
                            itemAlbum
                        )
                    ) {
                        BrowserToast.show(context, getString(R.string.toast_add_to_home_successful))
                        gridAdapter.notifyDataSetChanged()
                    } else {
                        BrowserToast.show(context, getString(R.string.toast_add_to_home_failed))
                    }
                }
                action.close()
            }
            R.id.menu_searchSite -> {
                hideBottomSheetDialog()
                hideKeyboard(context)
                showSearchPanel()
            }
            R.id.contextLink_saveAs -> {
                hideBottomSheetDialog()
                printPDF(false)
            }
            R.id.menu_settings -> {
                hideBottomSheetDialog()

            }
            R.id.menu_help -> {
                hideBottomSheetDialog()
            }
            R.id.menu_download -> {
                hideBottomSheetDialog()
                startActivity(Intent(DownloadManager.ACTION_VIEW_DOWNLOADS))
            }
            R.id.floatButton_tab -> {
                menu_newTabOpen.visibility = VISIBLE
                menu_closeTab.visibility = VISIBLE
                menu_tabPreview.visibility = VISIBLE
                menu_quit.visibility = VISIBLE
                ll_share.visibility = GONE
                ll_save.visibility = GONE
                ll_save_1.visibility = GONE
                floatButton_tabView.visibility = VISIBLE
                floatButton_saveView.visibility = INVISIBLE
                floatButton_shareView.visibility = INVISIBLE
                floatButton_moreView.visibility = INVISIBLE
                ll_more.visibility = GONE
                ll_more_1.visibility = GONE
                menu_openFav.visibility = VISIBLE
                ll_share_1.visibility = GONE
            }
            R.id.floatButton_share -> {
                menu_newTabOpen.visibility = GONE
                menu_closeTab.visibility = GONE
                menu_tabPreview.visibility = GONE
                menu_quit.visibility = GONE
                ll_share.visibility = VISIBLE
                menu_shareLink.visibility = VISIBLE

                ll_save.visibility = GONE
                ll_save_1.visibility = GONE

                floatButton_tabView.visibility = INVISIBLE
                floatButton_saveView.visibility = INVISIBLE
                floatButton_shareView.visibility = VISIBLE
                floatButton_moreView.visibility = INVISIBLE
                ll_more.visibility = GONE
                ll_more_1.visibility = GONE
                menu_openFav.visibility = GONE
                ll_share_1.visibility = VISIBLE
            }
            R.id.floatButton_save -> {
                menu_newTabOpen.visibility = GONE
                menu_closeTab.visibility = GONE
                menu_tabPreview.visibility = GONE
                menu_quit.visibility = GONE
                ll_share.visibility = GONE
                ll_save.visibility = VISIBLE
                ll_save_1.visibility = VISIBLE
                ll_more.visibility = GONE
                floatButton_tabView.visibility = INVISIBLE
                floatButton_saveView.visibility = VISIBLE
                floatButton_shareView.visibility = INVISIBLE
                floatButton_moreView.visibility = INVISIBLE
                ll_more_1.visibility = GONE
                menu_openFav.visibility = GONE
                ll_share_1.visibility = GONE
            }
            R.id.floatButton_more -> {
                menu_newTabOpen.visibility = GONE
                menu_closeTab.visibility = GONE
                menu_tabPreview.visibility = GONE
                menu_quit.visibility = GONE
                ll_share.visibility = GONE
                ll_save.visibility = GONE
                ll_save_1.visibility = GONE
                floatButton_tabView.visibility = INVISIBLE
                floatButton_saveView.visibility = INVISIBLE
                floatButton_shareView.visibility = INVISIBLE
                floatButton_moreView.visibility = VISIBLE
                ll_more.visibility = VISIBLE
                ll_more_1.visibility = VISIBLE
                menu_openFav.visibility = GONE
                ll_share_1.visibility = GONE
            }
            R.id.omnibox_overview -> showOverview()
            R.id.omnibox_refresh -> if (url != null && webView!!.isLoadFinish) {
                if (!url!!.startsWith("https://")) {
                    bottomSheetDialog = BottomSheetDialog(context, R.style.SheetDialog)
                    val dialogView = inflate(context, R.layout.dialog_action, null)
                    val textView = dialogView.findViewById<TextView>(R.id.dialog_text)
                    textView.setText(R.string.toast_unsecured)
                    val action_ok = dialogView.findViewById<Button>(R.id.action_ok)
                    action_ok.setOnClickListener {
                        hideBottomSheetDialog()
                        webView!!.loadUrl(url!!.replace("http://", "https://"))
                    }
                    val action_cancel2 = dialogView.findViewById<Button>(R.id.action_cancel)
                    action_cancel2.setOnClickListener {
                        hideBottomSheetDialog()
                        webView!!.reload()
                    }
                    bottomSheetDialog.setContentView(dialogView)
                    bottomSheetDialog.show()
                    HelperUnit.setBottomSheetBehavior(
                        bottomSheetDialog,
                        dialogView,
                        BottomSheetBehavior.STATE_EXPANDED
                    )
                } else {
                    webView!!.reload()
                }
            } else if (url == null) {
                val text = getString(R.string.toast_load_error) + ": " + url
                BrowserToast.show(context, text)
            } else {
                webView!!.stopLoading()
            }
            R.id.img_backword -> onBackWordPressed()
            R.id.img_forward -> onForwardPressed()
            else -> {}
        }
    }

    private fun printPDF(share: Boolean) {
        try {
            sp.edit().putBoolean("pdf_create", true).apply()
            if (share) {
                sp.edit().putBoolean("pdf_share", true).apply()
            } else {
                sp.edit().putBoolean("pdf_share", false).apply()
            }
            val title = HelperUnit.fileName(webView!!.url)
            val dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
            val file = File(dir, "$title.pdf")
            sp.edit().putString("pdf_path", file.path).apply()
            val pdfTitle = file.name.replace(".pdf", "")
            val printManager = getSystemService(PRINT_SERVICE) as PrintManager
            val printAdapter = webView!!.createPrintDocumentAdapter(title!!)
            Objects.requireNonNull(printManager)
                .print(pdfTitle, printAdapter, PrintAttributes.Builder().build())
        } catch (e: Exception) {
            sp.edit().putBoolean("pdf_create", false).apply()
            e.printStackTrace()
        }
    }


    private fun dispatchIntent(intent: Intent) {
        val toHolderService = Intent(context, HolderService::class.java)
        IntentUnit.isClear = false
        stopService(toHolderService)
        val action = intent.action
        if (intent.action != null && intent.action == Intent.ACTION_WEB_SEARCH) {
            // From ActionMode and some others
            pinAlbums(intent.getStringExtra(SearchManager.QUERY))
        } else if (filePathCallback != null) {
            filePathCallback = null
        } else if ("sc_history" == action) {
            pinAlbums(null)
            showOverview()
            Handler(Looper.getMainLooper()).postDelayed(
                { open_history.performClick() },
                shortAnimTime.toLong()
            )
        } else if ("sc_bookmark" == action) {
            pinAlbums(null)
            showOverview()
            Handler(Looper.getMainLooper()).postDelayed(
                { open_bookmark.performClick() },
                shortAnimTime.toLong()
            )
        } else if ("sc_startPage" == action) {
            pinAlbums(null)
            showOverview()
            Handler(Looper.getMainLooper()).postDelayed(
                { open_startPage.performClick() },
                shortAnimTime.toLong()
            )
        } else if (Intent.ACTION_SEND == action) {
            pinAlbums(intent.getStringExtra(Intent.EXTRA_TEXT))
        } else if ("" == action) {
        } else {
            pinAlbums(null)
        }
        getIntent().action = ""
    }

    private fun initRendering(view: View?) {
        if (sp.getBoolean("sp_invert", false)) {
            val paint = Paint()
            val matrix = ColorMatrix()
            matrix.set(NEGATIVE_COLOR)
            val gcm = ColorMatrix()
            gcm.setSaturation(0f)
            val concat = ColorMatrix()
            concat.setConcat(matrix, gcm)
            val filter = ColorMatrixColorFilter(concat)
            paint.colorFilter = filter
            // maybe sometime LAYER_TYPE_NONE would better?
            view!!.setLayerType(LAYER_TYPE_HARDWARE, paint)
        } else {
            view!!.setLayerType(LAYER_TYPE_HARDWARE, null)
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("ClickableViewAccessibility")
    private fun initOmnibox() {

        omnibox = findViewById(R.id.main_omnibox)
        inputBox = findViewById(R.id.main_omnibox_input)
        omniboxRefresh = findViewById(R.id.omnibox_refresh)
        omniboxTitle = findViewById(R.id.omnibox_title)
        progressBar = findViewById(R.id.main_progress_bar)
        progressShadow = findViewById(R.id.progressShadow)

        inputBox.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            val query = inputBox.text.toString().trim { it <= ' ' }
            if (query.isEmpty()) {
                BrowserToast.show(context, getString(R.string.toast_input_empty))
                return@OnEditorActionListener true
            }
            if (actionId == EditorInfo.IME_ACTION_GO) {
                rl_inner_layout!!.visibility = VISIBLE
                progressBar.visibility = VISIBLE
                progressShadow!!.visibility = VISIBLE
                back = false
            }
            updateAlbum(query)
            hideKeyboard(context)
            showOmnibox()
            false
        })

        inputBox.setOnFocusChangeListener { _, _ ->
            if (inputBox.hasFocus()) {
                webView!!.stopLoading()
                omniboxTitle.visibility = GONE
                inputBox.setSelection(0, inputBox.text.toString().length)
            } else {
                omniboxTitle.visibility = VISIBLE
                omniboxTitle.text = webView!!.title
                hideKeyboard(context)
            }
        }

        updateBookmarks()
        updateAutoComplete()
        omniboxRefresh.setOnClickListener(this)
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("ClickableViewAccessibility")
    private fun initOverview() {
        bottomSheetDialog_OverView = BottomSheetDialog(context, R.style.SheetDialog)
        bottomSheetDialog = BottomSheetDialog(context, R.style.SheetDialog)
        val dialogView = inflate(context, R.layout.dialog_overiew, null)

        rvTabs = dialogView.findViewById(R.id.rvTabs)
        rvIncoTabs = dialogView.findViewById(R.id.rvIncoTabs)

        open_startPage = dialogView.findViewById(R.id.open_newTab_2)
        open_bookmark = dialogView.findViewById(R.id.open_bookmark_2)
        open_history = dialogView.findViewById(R.id.open_history_2)
        open_menu = dialogView.findViewById(R.id.open_menu)
        ivTabMenu = dialogView.findViewById(R.id.ivTabMenu)
        hlStdTabs = dialogView.findViewById(R.id.hlStdTabs)
        hlIncoTabs = dialogView.findViewById(R.id.hlIncoTabs)
        rlOverViewDialog = dialogView.findViewById(R.id.rlOverViewDialog)

//      tab_container = dialogView.findViewById(R.id.tab_container)
        tab_plus = dialogView.findViewById(R.id.tab_plus)
        tab_plus.setOnClickListener(this)
        tab_plus_bottom = dialogView.findViewById(R.id.tab_plus_bottom)
        tvNewTab = dialogView.findViewById(R.id.tvNewTab)
        btnIncoIcon = dialogView.findViewById(R.id.btnIncoIcon)
        tvOverViewMenuTabCount = dialogView.findViewById(R.id.tvStdTabCount)
        btnIncoIcon.setOnClickListener(this)
        tvOverViewMenuTabCount.setOnClickListener(this)
        tab_plus_bottom.setOnClickListener(this)
        tvNewTab.setOnClickListener(this)
        tab_ScrollView = dialogView.findViewById(R.id.tab_ScrollView)
        overview_top = dialogView.findViewById(R.id.overview_top)
        overview_topButtons = dialogView.findViewById(R.id.overview_topButtons)
        listView = dialogView.findViewById(R.id.home_list_2)
        titleTV = dialogView.findViewById(R.id.titleTV)
        view3 = dialogView.findViewById(R.id.view3)
        open_startPageView = dialogView.findViewById(R.id.open_newTabView)
        open_bookmarkView = dialogView.findViewById(R.id.open_bookmarkView)
        open_historyView = dialogView.findViewById(R.id.open_historyView)
        overview_titleIcons_startView = dialogView.findViewById(R.id.overview_titleIcons_startView)
        overview_titleIcons_bookmarksView =
            dialogView.findViewById(R.id.overview_titleIcons_bookmarksView)
        overview_titleIcons_historyView =
            dialogView.findViewById(R.id.overview_titleIcons_historyView)
        val gridView = dialogView.findViewById<GridView>(R.id.home_grid_2)
        val overview_titleIcons_start =
            dialogView.findViewById<ImageButton>(R.id.overview_titleIcons_start)
        val overview_titleIcons_bookmarks =
            dialogView.findViewById<ImageButton>(R.id.overview_titleIcons_bookmarks)
        val overview_titleIcons_history =
            dialogView.findViewById<ImageButton>(R.id.overview_titleIcons_history)
        gridView.visibility = GONE
        listView.visibility = GONE

        bottomSheetDialog_OverView.window?.setWindowAnimations(-1)
        bottomSheetDialog.window?.setWindowAnimations(-1)

        listView.setOnTouchListener { v, event ->
            val action = event.action
            when (action) {
                MotionEvent.ACTION_DOWN ->
                    if (listView.canScrollVertically(-1)) {
                        v.parent.requestDisallowInterceptTouchEvent(true)
                    }
            }
            v.onTouchEvent(event)
            true
        }
        gridView.setOnTouchListener { v, event ->
            val action = event.action
            if (action == MotionEvent.ACTION_DOWN) {
                if (gridView.canScrollVertically(-1)) {
                    v.parent.requestDisallowInterceptTouchEvent(true)
                }
            }
            v.onTouchEvent(event)
            true
        }
        open_menu.setOnClickListener {
            bottomSheetDialog = BottomSheetDialog(context, R.style.SheetDialog)
            val dialogView = inflate(context, R.layout.dialog_menu_overview, null)
            val bookmark_sort = dialogView.findViewById<LinearLayout>(R.id.bookmark_sort)
            val bookmark_filter = dialogView.findViewById<LinearLayout>(R.id.bookmark_filter)
            val bookmark_blank = dialogView.findViewById<LinearLayout>(R.id.bookmark_blank)
            if (overViewTab == getString(R.string.album_title_bookmarks)) {
                bookmark_filter.visibility = VISIBLE
                bookmark_sort.visibility = VISIBLE
            } else if (overViewTab == getString(R.string.album_title_home)) {
                bookmark_filter.visibility = GONE
                bookmark_sort.visibility = VISIBLE
            } else if (overViewTab == getString(R.string.album_title_history)) {
                bookmark_filter.visibility = GONE
                bookmark_sort.visibility = GONE
            }
            bookmark_blank.setOnClickListener {
                hideBottomSheetDialog()
                HelperUnit.setFavorite(context, "about:blank")
            }

            bottomSheetDialog.setContentView(dialogView)
            bottomSheetDialog.show()
            HelperUnit.setBottomSheetBehavior(
                bottomSheetDialog,
                dialogView,
                BottomSheetBehavior.STATE_EXPANDED
            )
        }
        bottomSheetDialog_OverView.setContentView(dialogView)
        mBehavior = BottomSheetBehavior.from(dialogView.parent as View)
        mBehavior.addBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {

                if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    mBehavior.state =
                        BottomSheetBehavior.STATE_EXPANDED
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })

        ivTabMenu.setOnClickListener {
            showTabMenuDialog()
        }

        btnIncoIcon.setOnClickListener {
            SharedPrefsUtils.setBooleanPreference(KeysUtils.keyIsIncoModeOn, true)
            changeFocusedTab()
            setDialogColors(true)
        }

        tvOverViewMenuTabCount.setOnClickListener {
            SharedPrefsUtils.setBooleanPreference(KeysUtils.keyIsIncoModeOn, false)
            changeFocusedTab()
            setDialogColors(false)
        }


        tvTabCount.setOnClickListener {

            val isIncoTabsAvailable = tabsDetailsArr.filter { it.isIncoWeb == true }
            tvOverViewMenuTabCount.text =
                tabsDetailsArr.filter { it.isIncoWeb == false }.size.toString()
            Log.e(TAG, "initOverview:isIncoTabsAvailable: " + isIncoTabsAvailable.size)
            if (isIncoTabsAvailable.isEmpty()) {
                // TODO: normals tabs only
                showHideIncogTabIcon(false)
                showOverview()
            } else {
                // TODO: at-least one incotab available
                showHideIncogTabIcon(true)
                val tabData = tabsDetailsArr.filter { it.cAlbumCon == currentAlbumController }[0]
                showOverview(tabData.isIncoWeb)
            }

        }

        ivMenu.setOnClickListener {
            showTabMenuDialog()
        }

        ivNewTabPlus.setOnClickListener {
            // TODO: new normal tab
            SharedPrefsUtils.setBooleanPreference(KeysUtils.keyIsIncoModeOn, false)
            hideBottomSheetDialog()
            hideOverview()
            addAlbum(
                "New tab",
                sp.getString("favoriteURL", "https://google.com"),
                true
            )
        }

        open_startPage.setOnClickListener {
            titleTV.text = resources.getString(R.string.home)
            mBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            gridView.visibility = VISIBLE
            listView.visibility = GONE
            open_startPageView.visibility = VISIBLE
            open_bookmarkView.visibility = INVISIBLE
            open_historyView.visibility = INVISIBLE
            overview_titleIcons_startView.visibility = VISIBLE
            overview_titleIcons_bookmarksView.visibility = INVISIBLE
            overview_titleIcons_historyView.visibility = INVISIBLE
            overViewTab = getString(R.string.album_title_home)
            val action = RecordAction(context)
            action.open(false)
            val gridList = action.listGrid(context)
            action.close()
            gridAdapter = GridAdapter(context, gridList!!)
            gridView.adapter = gridAdapter
            gridAdapter.notifyDataSetChanged()
            gridView.onItemClickListener = OnItemClickListener { parent, view, position, id ->
                updateAlbum(gridList[position].uRL)
                hideOverview()
            }
            gridView.onItemLongClickListener =
                OnItemLongClickListener { _, _, position, _ ->
                    show_contextMenu_list(
                        gridList[position].title!!, gridList[position].uRL!!,
                        null, null, 0,
                        null, gridList[position]
                    )
                    true
                }
        }
        open_bookmark.setOnClickListener {
            titleTV.text = resources.getString(R.string.book_mark)
            titleTV.visibility = VISIBLE
            view3.visibility = VISIBLE
            mBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            gridView.visibility = GONE
            listView.visibility = VISIBLE
            open_startPageView.visibility = INVISIBLE
            open_bookmarkView.visibility = VISIBLE
            open_historyView.visibility = INVISIBLE
            overview_titleIcons_startView.visibility = INVISIBLE
            overview_titleIcons_bookmarksView.visibility = VISIBLE
            overview_titleIcons_historyView.visibility = INVISIBLE
            overViewTab = getString(R.string.album_title_bookmarks)
            sp.edit().putString("filter_passBY", "00").apply()
            initBookmarkList()
        }
        open_bookmark.setOnLongClickListener {
            false
        }
        open_history.setOnClickListener {
            Log.e("open_history", "initOverview: PerformHistClisk")
            titleTV.text = resources.getString(R.string.history)

            titleTV.visibility = VISIBLE
            mBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            gridView.visibility = GONE
            listView.visibility = VISIBLE
            open_startPageView.visibility = INVISIBLE
            open_bookmarkView.visibility = INVISIBLE
            open_historyView.visibility = VISIBLE
            overview_titleIcons_startView.visibility = INVISIBLE
            overview_titleIcons_bookmarksView.visibility = INVISIBLE
            overview_titleIcons_historyView.visibility = VISIBLE
            overViewTab = getString(R.string.album_title_history)
            val action = RecordAction(context)
            action.open(false)
            val list = action.listHistory()
            action.close()
            val adapter = AdapterRecord(context, list)
            listView.adapter = adapter
            adapter.notifyDataSetChanged()
            listView.onItemClickListener = OnItemClickListener { parent, view, position, id ->
                updateAlbum(list[position].uRL)
                hideOverview()
            }
            listView.onItemLongClickListener =
                OnItemLongClickListener { parent, view, position, id ->
                    show_contextMenu_list(
                        list[position].title.toString(),
                        list[position].uRL.toString(), adapter, list, position,
                        null, null
                    )
                    true
                }
        }
        overview_titleIcons_start.setOnClickListener { open_startPage.performClick() }
        overview_titleIcons_bookmarks.setOnClickListener { open_bookmark.performClick() }
        overview_titleIcons_bookmarks.setOnLongClickListener {
            if (overViewTab != getString(R.string.album_title_bookmarks)) {
                open_bookmark.performClick()
            }
            false
        }
        overview_titleIcons_history.setOnClickListener { open_history.performClick() }
        when (sp.getString("start_tab", "0")) {
            "0" -> {
                overview_top.visibility = GONE
                open_startPage.performClick()
            }
            "3" -> {
                overview_top.visibility = GONE
                open_bookmark.performClick()
            }
            "4" -> {
                overview_top.visibility = GONE
                open_history.performClick()
            }
            else -> {
                overview_top.visibility = GONE
                open_startPage.performClick()
            }
        }
    }

    private fun changeFocusedTab() {

        val isIncoOn = SharedPrefsUtils.getBooleanPreference(KeysUtils.keyIsIncoModeOn, false)
        val tabData = tabsDetailsArr.filter { it.isIncoWeb == isIncoOn }
        val lastTab = tabData[tabData.size - 1]
        showAlbum(lastTab.cAlbumCon, isIncoOn)
    }

    private fun showHideIncogTabIcon(show: Boolean) {
        if (show) {
            tvNewTab.visibility = GONE
            tvOverViewMenuTabCount.visibility = VISIBLE
            hlStdTabs.visibility = VISIBLE
            btnIncoIcon.visibility = VISIBLE
            hlIncoTabs.visibility = VISIBLE
        } else {
            tvNewTab.visibility = VISIBLE
            tvOverViewMenuTabCount.visibility = GONE
            hlStdTabs.visibility = GONE
            btnIncoIcon.visibility = GONE
            hlIncoTabs.visibility = GONE
        }
    }

    private fun setDialogColors(isIncoModeOn: Boolean) {
        if (isIncoModeOn) {
            rvIncoTabs.visibility = VISIBLE
            rvTabs.visibility = GONE
            hlStdTabs.visibility = INVISIBLE
            hlIncoTabs.visibility = VISIBLE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                rlOverViewDialog.setBackgroundColor(context.getColor(R.color.black))
            }
            tab_plus_bottom.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.white_color
                ), PorterDuff.Mode.SRC_IN
            )
            btnIncoIcon.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.white_color
                ), PorterDuff.Mode.SRC_IN
            )
            ivTabMenu.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.white_color
                ), PorterDuff.Mode.SRC_IN
            )
            tvNewTab.setTextColor(Color.parseColor("#FFFFFFFF"))
            tvOverViewMenuTabCount.setTextColor(Color.parseColor("#FFFFFFFF"))
            tvOverViewMenuTabCount.background =
                ContextCompat.getDrawable(this, R.drawable.bg_tab_border_light)

        } else {
            rvTabs.visibility = VISIBLE
            rvIncoTabs.visibility = GONE
            hlStdTabs.visibility = VISIBLE
            hlIncoTabs.visibility = INVISIBLE
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                rlOverViewDialog.setBackgroundColor(context.getColor(R.color.white))
            }

            tab_plus_bottom.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.black
                ), PorterDuff.Mode.SRC_IN
            )
            btnIncoIcon.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.black
                ), PorterDuff.Mode.SRC_IN
            )
            ivTabMenu.setColorFilter(
                ContextCompat.getColor(
                    context,
                    R.color.black
                ), PorterDuff.Mode.SRC_IN
            )
            tvNewTab.setTextColor(Color.parseColor("#FF000000"))
            tvOverViewMenuTabCount.setTextColor(Color.parseColor("#FF000000"))
            tvOverViewMenuTabCount.background =
                ContextCompat.getDrawable(this, R.drawable.bg_tab_border)

        }
    }

/* override fun onTouchEvent(event: MotionEvent?): Boolean {

     var returnValue = false

     val event = MotionEvent.obtain(event)
     val action = MotionEventCompat.getActionMasked(event)
     if (action == MotionEvent.ACTION_DOWN) {
         mNestedOffsetY = 0
     }
     val eventY = event.y.toInt()
     event.offsetLocation(0f, mNestedOffsetY.toFloat())
     when (action) {
         MotionEvent.ACTION_MOVE -> {
             var deltaY = mLastY - eventY
             // NestedPreScroll
             if (dispatchNestedPreScroll(0, deltaY, mScrollConsumed, mScrollOffset)) {
                 deltaY -= mScrollConsumed[1]
                 mLastY = eventY - mScrollOffset[1]
                 event.offsetLocation(0f, (-mScrollOffset[1]).toFloat())
                 mNestedOffsetY += mScrollOffset[1]
             }
             returnValue = super.onTouchEvent(event)

             // NestedScroll
             if (dispatchNestedScroll(0, mScrollOffset[1], 0, deltaY, mScrollOffset)) {
                 event.offsetLocation(0f, mScrollOffset[1].toFloat())
                 mNestedOffsetY += mScrollOffset[1]
                 mLastY -= mScrollOffset[1]
             }
         }
         MotionEvent.ACTION_DOWN -> {
             returnValue = super.onTouchEvent(event)
             mLastY = eventY
             // start NestedScroll
             startNestedScroll(ViewCompat.SCROLL_AXIS_VERTICAL)
         }
         MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
             returnValue = super.onTouchEvent(event)
             // end NestedScroll
             stopNestedScroll()
         }
     }

     return returnValue


//        return super.onTouchEvent(event)


 }*/

    private fun initOverviewHist() {
        bottomSheetDialog_OverView_hist = BottomSheetDialog(context, R.style.SheetDialog)
        bottomSheetDialog_hist = BottomSheetDialog(context, R.style.SheetDialog)
        val dialogView = inflate(context, R.layout.dialog_overiew_history, null)

        open_history = dialogView.findViewById(R.id.open_history_2)
        val home_buttons: LinearLayout = dialogView.findViewById(R.id.home_buttons)
        open_history.visibility = GONE
        home_buttons.visibility = GONE
        listView = dialogView.findViewById(R.id.home_list_2)

        listView.setOnTouchListener { v, event ->
            val action = event.action
            when (action) {
                MotionEvent.ACTION_DOWN ->
                    if (listView.canScrollVertically(-1)) {
                        v.parent.requestDisallowInterceptTouchEvent(true)
                    }
            }
            // Handle ListView touch events.
            v.onTouchEvent(event)
            true
        }

        bottomSheetDialog_OverView_hist.setContentView(dialogView)
        mBehavior_hist = BottomSheetBehavior.from(dialogView.parent as View)
        val peekHeight = (255 * resources.displayMetrics.density).roundToInt()
        mBehavior_hist.peekHeight = peekHeight
        mBehavior_hist.addBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    hideOverview()
                } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    if (sp.getBoolean("overView_hide", false)) {
                        overview_top.visibility = GONE
                        view3.visibility = GONE
                    } else {
                        overview_topButtons.visibility = GONE
                    }
                } else {
                    if (sp.getBoolean("overView_hide", false)) {
                        overview_top.visibility = VISIBLE
                        view3.visibility = VISIBLE
                    } else {
                        overview_topButtons.visibility = VISIBLE
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
        setHistoryData()

    }

    private fun setHistoryData() {

        mBehavior_hist.state = BottomSheetBehavior.STATE_EXPANDED
        listView.visibility = VISIBLE
        open_historyView.visibility = VISIBLE
        overview_titleIcons_historyView.visibility = VISIBLE
        val action = RecordAction(context)
        action.open(false)
        val list = action.listHistory()
        list.reverse()
        action.close()
        Log.e("Shreyas", "List=> " + list.size)
        val adapter = AdapterRecord(context, list)
        listView.adapter = adapter
        adapter.notifyDataSetChanged()
        listView.onItemClickListener = OnItemClickListener { parent, view, position, id ->
            updateAlbum(list[position].uRL)
//            hideOverview()
            bottomSheetDialog_OverView_hist.dismiss()
        }
        listView.onItemLongClickListener =
            OnItemLongClickListener { parent, view, position, id ->
                show_contextMenu_list(
                    list[position].title.toString(),
                    list[position].uRL.toString(), adapter, list, position,
                    null, null
                )
                true
            }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    private fun initSearchPanel() {

        findViewById<TextView>(R.id.currentTitle).text = webView?.title
        mainSearchBar = findViewById(R.id.main_search_1)
        searchView2 = findViewById(R.id.etSearchWebBrowser)
        btnClose = searchView2.findViewById(R.id.search_close_btn)
        btnClose.isEnabled = false

        tvTabCount = findViewById(R.id.tvTabCount)
        ivMenu = findViewById(R.id.ivMenu)
        ivNewTabPlus = findViewById(R.id.ivNewTabPlus)

        searchView2.onActionViewExpanded()
        searchView2.setIconifiedByDefault(false)
        val searchEditText =
            searchView2.findViewById<EditText>(androidx.appcompat.R.id.search_src_text)
        searchEditText.onFocusChangeListener = OnFocusChangeListener { view, b ->
            Log.e("Test101", "focus got $b")
            if (b) {

//                if((currentAlbumController as BrowserWebView?) != null){
//                    (currentAlbumController as BrowserWebView?)!!.stopLoading()
//                }
                val webAddress = findViewById<SearchView>(R.id.etSearchWebBrowser).query.toString()
                editor!!.putString("webAddress", webAddress)
                editor!!.apply()
                findViewById<SearchView>(R.id.etSearchWebBrowser).setQuery("", false)
                findViewById<LinearLayout>(R.id.llVoiceOption).visibility = GONE
                findViewById<TextView>(R.id.tvTabCount).visibility = GONE
                findViewById<ImageView>(R.id.ivNewTabPlus).visibility = GONE
                findViewById<ImageView>(R.id.ivMenu).visibility = GONE
                findViewById<ImageView>(R.id.ivHomeSearch).visibility = GONE
                findViewById<ImageView>(R.id.ivWebMic1).visibility = VISIBLE
                findViewById<ImageView>(R.id.ivClose).visibility = GONE

                findViewById<ImageView>(R.id.ivWebImageTags).setImageDrawable(
                    ContextCompat.getDrawable(
                        this,
                        R.drawable.ic_google
                    )
                )
                //   findViewById<SearchView>(R.id.etSearchWebBrowser).clearFocus()
                if (checkwebedit == true) {
                    listRecentContainer!!.visibility = GONE
                    editToolContainer?.visibility = GONE
                    tvRecentSearchText?.visibility = GONE
                } else if (checkwebedit == false) {
                    listRecentContainer!!.visibility = VISIBLE
                    editToolContainer?.visibility = VISIBLE
                    tvRecentSearchText?.visibility = VISIBLE
                    checkwebedit = false
                }

                //recent list
                val listRecent = findViewById<ListView>(R.id.home_list_2)
                val action = RecordAction(context)
                action.open(false)
                val list = action.listHistory()
                list.reverse()
                action.close()
                val adapter = AdapterRecentRecord(context, list)
                listRecent.adapter = adapter
                listRecent?.visibility = VISIBLE
                adapter.notifyDataSetChanged()
                listRecent.onItemClickListener = OnItemClickListener { parent, view, position, id ->
                    Log.e("kirtanSnap", "onCreate: List => " + list[position].uRL)
                    updateAlbum(list[position].uRL)
                    editToolContainer?.visibility = GONE
                    tvRecentSearchText?.visibility = GONE
                    listRecentContainer?.visibility = GONE
                    listRecent?.visibility = GONE
                    findViewById<ImageView>(R.id.ivWebMic1).visibility = GONE
                    findViewById<TextView>(R.id.tvTabCount).visibility = VISIBLE
                    findViewById<ImageView>(R.id.ivNewTabPlus).visibility = VISIBLE
                    findViewById<ImageView>(R.id.ivMenu).visibility = VISIBLE
                    findViewById<ImageView>(R.id.ivHomeSearch).visibility = VISIBLE
                    closeKeyboard()
                    searchEditText.visibility = GONE
                    searchEditText.visibility = VISIBLE
                }


                val titleEdit: String = sharedPreferences.getString("EditTitle", "").toString()
                val titleFavicon: String = sharedPreferences.getString("EditFavicon", "").toString()
                val EditUrl: String = sharedPreferences.getString("EditUrl", "").toString()
                findViewById<TextView>(R.id.currentURL).text = EditUrl
                currentTitle.text = titleEdit
            } else {
                Handler(Looper.getMainLooper()).postDelayed(Runnable {
                    updateTopHttpIcons((currentAlbumController as BrowserWebView?)!!)
                }, 300)

            }
        }

        searchEditText.setTextColor(resources.getColor(R.color.color_black))
        val iv = searchView2.findViewById<ImageView>(androidx.appcompat.R.id.search_button)
        iv.setImageDrawable(
            ResourcesCompat.getDrawable(
                resources,
                R.drawable.icon_search,
                null
            )
        )

        searchView2.queryHint =
            Html.fromHtml("<font color = #3C4349 >" + resources.getString(R.string.search_hint) + "</font>")

        var isSearchedClicked = false
        searchView2.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            override fun onQueryTextSubmit(s: String): Boolean {
                isSearchedClicked = true
                webView!!.loadUrl(s)
                searchFor(searchEditText.text.toString())

//                editToolContainer?.visibility = GONE
//                rl_inner_layout!!.visibility = VISIBLE
                progressBar.visibility = VISIBLE
                progressShadow!!.visibility = VISIBLE

                listRecent?.visibility = GONE
                back = false
                Log.e("onQueryTextSubmit : ", s)
                return false
            }

            override fun onQueryTextChange(s: String): Boolean {
                if (s.isEmpty()) {
                    if (!isSearchedClicked) {
                        searchFor(searchEditText.text.toString())
                        listRecent?.visibility = VISIBLE
                    } else {
                        listRecent?.visibility = GONE
                    }
                } else {
                    searchFor(searchEditText.text.toString())
                    listRecent?.visibility = VISIBLE
                }
                /*     searchFor(searchEditText.text.toString())
                     listRecent?.visibility = VISIBLE
                     Log.e("onQueryTextChange : ", s)*/
                return false
            }

        })
    }

    fun decodeBase64(input: String?): Bitmap? {
        val decodedBytes: ByteArray = Base64.decode(input, 0)
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
    }

    fun getRGB(rgb: String): IntArray? {
        val r = rgb.substring(0, 2).toInt(16) // 16 for hex
        val g = rgb.substring(2, 4).toInt(16) // 16 for hex
        val b = rgb.substring(4, 6).toInt(16) // 16 for hex
        return intArrayOf(r, g, b)
    }

    private fun initBookmarkList() {
        val db = BookmarkList(context)
        val row: Cursor
        db.open()
        val layoutStyle = R.layout.list_item_bookmark
        val xml_id = intArrayOf(
            R.id.record_item_title
        )
        val column = arrayOf(
            "pass_title"
        )
        val search = sp.getString("filter_passBY", "00")
        row = if (search == "00") {
            db.fetchAllData(context)
        } else {
            db.fetchDataByFilter(search, "pass_creation")
        }!!
        Log.e(
            "convertView",
            "context: ${context} \nlayoutStyle:: $layoutStyle \nrow:: ${row.columnCount} \n column:: ${column.size} \n xml_id :: $xml_id"
        )
        val adapter: SimpleCursorAdapter = object :
            SimpleCursorAdapter(context, layoutStyle, row, column, xml_id, 0) {
            override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

                val v = super.getView(position, convertView, parent)

                listView.getItemAtPosition(position) as Cursor
                v.findViewById<ImageView>(R.id.ib_icon)

                return v
            }
        }

        listView.adapter = adapter
        listView.onItemClickListener = OnItemClickListener { adapterView, view, position, id ->
            val pass_content = row.getString(row.getColumnIndexOrThrow("pass_content"))
            val pass_icon = row.getString(row.getColumnIndexOrThrow("pass_icon"))
            val pass_attachment = row.getString(row.getColumnIndexOrThrow("pass_attachment"))
            updateAlbum(pass_content)
            toastLogin(pass_icon, pass_attachment)
            hideOverview()
        }

        listView.onItemLongClickListener = OnItemLongClickListener { parent, view, position, id ->
            val rowListView = listView.getItemAtPosition(position) as Cursor
            val _id = rowListView.getString(rowListView.getColumnIndexOrThrow("_id"))
            val pass_title = rowListView.getString(rowListView.getColumnIndexOrThrow("pass_title"))
            val pass_content =
                rowListView.getString(rowListView.getColumnIndexOrThrow("pass_content"))
            show_contextMenu_list(
                pass_title, pass_content, null, null, 0,
                _id, null
            )
            true
        }
    }


    private fun toastLogin(userName: String, passWord: String) {
        try {
            val decrypted_userName = mahEncryptor.decode(userName)
            val decrypted_userPW = mahEncryptor.decode(passWord)
            val clipboard = (getSystemService(CLIPBOARD_SERVICE) as ClipboardManager)
            val unCopy: BroadcastReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {
                    val clip = ClipData.newPlainText("text", decrypted_userName)
                    clipboard.setPrimaryClip(clip)
                    BrowserToast.show(context, R.string.toast_copy_successful)
                }
            }
            val pwCopy: BroadcastReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {
                    val clip = ClipData.newPlainText("text", decrypted_userPW)
                    clipboard.setPrimaryClip(clip)
                    BrowserToast.show(context, R.string.toast_copy_successful)
                }
            }
            val intentFilter = IntentFilter("unCopy")
            registerReceiver(unCopy, intentFilter)
            val copy = Intent("unCopy")
            val copyUN: PendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent.getBroadcast(
                    context,
                    0,
                    copy,
                    PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
                )
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    PendingIntent.getBroadcast(
                        context,
                        0,
                        copy,
                        PendingIntent.FLAG_CANCEL_CURRENT or PendingIntent.FLAG_IMMUTABLE
                    )
                } else {
                    TODO("VERSION.SDK_INT < M")
                }
            }

            val intentFilter2 = IntentFilter("pwCopy")
            registerReceiver(pwCopy, intentFilter2)
            val copy2 = Intent("pwCopy")

            val copyPW: PendingIntent = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
                PendingIntent.getBroadcast(
                    context,
                    1,
                    copy2,
                    PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_CANCEL_CURRENT
                )
            } else {
                PendingIntent.getBroadcast(context, 1, copy2, PendingIntent.FLAG_CANCEL_CURRENT)
            }

            val builder: NotificationCompat.Builder
            val mNotificationManager =
                (getSystemService(NOTIFICATION_SERVICE) as NotificationManager)
            builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val CHANNEL_ID = "browser_not" // The id of the channel.
                val name: CharSequence =
                    getString(R.string.app_name) // The user-visible name of the channel.
                val mChannel =
                    NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_HIGH)
                mNotificationManager.createNotificationChannel(mChannel)
                NotificationCompat.Builder(context, CHANNEL_ID)
            } else {
                NotificationCompat.Builder(context)
            }
            val action_UN = NotificationCompat.Action.Builder(
                R.drawable.icon_earth,
                getString(R.string.toast_titleConfirm_pasteUN),
                copyUN
            ).build()
            val action_PW = NotificationCompat.Action.Builder(
                R.drawable.icon_earth,
                getString(R.string.toast_titleConfirm_pastePW),
                copyPW
            ).build()
            val n = builder
                .setCategory(Notification.CATEGORY_MESSAGE)
                .setSmallIcon(ic_notification)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.toast_titleConfirm_paste))
                .setColor(ContextCompat.getColor(context, R.color.colorAccent))
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(LongArray(0))
                .addAction(action_UN)
                .addAction(action_PW)
                .build()
            val notificationManager =
                (getSystemService(NOTIFICATION_SERVICE) as NotificationManager)
            if (decrypted_userName.isNotEmpty() || decrypted_userPW.isNotEmpty()) {
                notificationManager.notify(0, n)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            BrowserToast.show(context, R.string.toast_error)
        }
    }

    @Synchronized
    private fun addAlbum(
        title: String? = "",
        url: String? = "",
        foreground: Boolean = true,
        isIncoTab: Boolean = false,
    ) {
        Log.e(TAG, "addAlbum: isIncoTab:$isIncoTab")
        showOmnibox()

        webView = BrowserWebView(null, context, isIncoTab, null)
        webView!!.setBrowserController(this@BrowserActivity)



        webView!!.albumTitle = title
        ViewUnit.bound(context, webView!!)

        if (currentAlbumController != null) {
            val index = BrowserContainer.indexOf(currentAlbumController!!) + 1
            BrowserContainer.add(webView!!, index) // change

            Log.e(TAG, "pinAlbums:addAlbum: cAlbum-NOT-null")

            addTabDataToArr(false, webView, currentAlbumController, isIncoTab)

        } else {
            // TODO: if no tabs available AKA 1st Tab:
            Log.e(TAG, "pinAlbums:addAlbum: cAlbum-Null")

            BrowserContainer.add(webView!!)

            addTabDataToArr(true, webView, null, isIncoTab)

        }
        if (!foreground) {
            ViewUnit.bound(context, webView!!)
            webView!!.loadUrl(url!!)
            webView!!.deactivate()
            return
        }
        showAlbum(webView!!, isIncoTab)
        if (url != null && url.isNotEmpty()) {
            webView!!.loadUrl(url)
        }
    }


    @Synchronized
    private fun pinAlbums(url: String?) {
        showOmnibox()
        hideSearchPanel()
        /* tab_container.removeAllViews()*/
        // pass swipe refresh object in  constructor
        webView = BrowserWebView(null, context)

        webView!!.settings.useWideViewPort = true
        val settings = webView!!.settings
        settings.domStorageEnabled = true

        Log.e(TAG, "pinAlbums: getClosedTabs:" + getClosedTabs())
//        Log.e(TAG, "pinAlbums: getClosedTabs.size:" + getClosedTabs()!!.size)

        for (controller in BrowserContainer.list()) {
            (controller as BrowserWebView).setBrowserController(this)
            /* tab_container.addView(
                 controller.albumView,
                 LinearLayout.LayoutParams.WRAP_CONTENT,
                 LinearLayout.LayoutParams.MATCH_PARENT
             )*/
            controller.albumView?.visibility = VISIBLE
            controller.deactivate()
        }

        val closedTabsArr = getClosedTabs()
        if (!closedTabsArr.isNullOrEmpty()) {
            Log.e(TAG, "pinAlbums:1 closedTabsArr:$closedTabsArr")
            Log.e(TAG, "pinAlbums:1 closedTabsArr.size:" + closedTabsArr.size)
            val foreGroundTab = closedTabsArr.filter { it!!.isForeground == true }[0]
            closedTabsArr.removeAt(closedTabsArr.indexOf(foreGroundTab))
            closedTabsArr.add(0, foreGroundTab)

            for (i in closedTabsArr.indices) {
                addAlbum(
                    closedTabsArr[i]?.siteTitle,
                    closedTabsArr[i]?.siteUrl,
                    closedTabsArr[i]?.isForeground!!
                )
                if (Build.VERSION.SDK_INT < 28) {
                    Handler(Looper.getMainLooper()).postDelayed(
                        { omniboxRefresh.performClick() },
                        500
                    )
                }
            }

            Handler(Looper.getMainLooper()).postDelayed(
                { showAlbum(currentAlbumController!!) },
                shortAnimTime.toLong()
            )
            return
        }

        if (BrowserContainer.size() < 1 && url == null) {
            addAlbum("", sp.getString("favoriteURL", "https://google.com"), true)
            if (Build.VERSION.SDK_INT < 28) {
                Handler(Looper.getMainLooper()).postDelayed({ omniboxRefresh.performClick() }, 500)
            }

            // TODO: IMP step:
            Log.e(TAG, "pinAlbums:2 getClosedTabs:" + getClosedTabs())
            Log.e(TAG, "pinAlbums:2 getClosedTabs.size:" + getClosedTabs()!!.size)
        }

        // TODO: not needed right now:
        else if (BrowserContainer.size() >= 1 && url == null) {
            val index = BrowserContainer.size() - 1
            currentAlbumController = BrowserContainer.get(index)
            contentFrame.removeAllViews()
            contentFrame.addView(currentAlbumController as View?)
            currentAlbumController!!.activate()

            Log.e(TAG, "pinAlbums:3 getClosedTabs:" + getClosedTabs())
            Log.e(TAG, "pinAlbums:3 getClosedTabs.size:" + getClosedTabs()!!.size)
        }

        // TODO: not needed right now:
        else if (url != null) { // When url != null
            webView!!.setBrowserController(this@BrowserActivity)
            webView!!.albumTitle = getString(R.string.app_name)
            //      webView?.setLayerType(View.LAYER_TYPE_HARDWARE, null)

            ViewUnit.bound(context, webView!!)
            webView!!.loadUrl(url)
            BrowserContainer.add(webView!!)
            //  val albumView = webView!!.albumView
            /* tab_container.addView(
                 albumView,
                 LinearLayout.LayoutParams.WRAP_CONTENT,
                 LinearLayout.LayoutParams.WRAP_CONTENT
             )*/
            contentFrame.removeAllViews()
            contentFrame.addView(webView)
            currentAlbumController = webView as BrowserWebView
            currentAlbumController!!.activate()

            Log.e(TAG, "pinAlbums:4 getClosedTabs:" + getClosedTabs())
            Log.e(TAG, "pinAlbums:4 getClosedTabs.size:" + getClosedTabs()!!.size)
        }
        Handler(Looper.getMainLooper()).postDelayed(
            { showAlbum(currentAlbumController!!) },
            shortAnimTime.toLong()
        )
    }

    @Synchronized
    private fun updateAlbum(url: String?) {
        (currentAlbumController as BrowserWebView?)!!.loadUrl(url!!)
        // added
        setSearchBarData(currentAlbumController as BrowserWebView)
        updateOmnibox()
    }

    private fun closeTabConfirmation(okAction: Runnable) {
        if (!sp.getBoolean("sp_close_tab_confirm", false)) {
            okAction.run()
        } else {
            bottomSheetDialog = BottomSheetDialog(context, R.style.SheetDialog)
            val dialogView = inflate(context, R.layout.dialog_action, null)
            val textView = dialogView.findViewById<TextView>(R.id.dialog_text)
            textView.setText(R.string.toast_close_tab)
            val action_ok = dialogView.findViewById<Button>(R.id.action_ok)
            action_ok.setOnClickListener {
                okAction.run()
                hideBottomSheetDialog()
            }
            val action_cancel = dialogView.findViewById<Button>(R.id.action_cancel)
            action_cancel.setOnClickListener { hideBottomSheetDialog() }
            bottomSheetDialog.setContentView(dialogView)
            bottomSheetDialog.show()
            HelperUnit.setBottomSheetBehavior(
                bottomSheetDialog,
                dialogView,
                BottomSheetBehavior.STATE_EXPANDED
            )
        }
    }


    @Synchronized
    override fun removeAlbum(albumController: AlbumController?, isIncoTab: Boolean) {
        Log.e(TAG, "removeAlbum: removeeeeeeee")

//        if (tabsDetailsArr.filter { it.isIncoWeb == false }.size <= 1) {
//
//        }else if(){}
//        if (BrowserContainer.size() <= 1) {
        if (tabsDetailsArr.filter { it.isIncoWeb == false }.size <= 1 && !isIncoTab) {
            if (!sp.getBoolean("sp_reopenLastTab", false)) {
                //    doubleTapsQuit();
            } else {
                updateAlbum(sp.getString("favoriteURL", "https://google.com"))
                hideOverview()
            }
        } else {
            closeTabConfirmation {

                val indexOfClosedTab =
                    tabsDetailsArr.indexOf(tabsDetailsArr.filter { it.cAlbumCon == albumController }[0])
                Log.e(TAG, "removeAlbum:@ indexof:$indexOfClosedTab")

                var tempIncoArr = tabsDetailsArr
                tempIncoArr =
                    tempIncoArr.filter { it.isIncoWeb == isIncoTab } as MutableList<TabsModel>

                Log.e(TAG, "removeAlbum:2 b4 tempIncoArr$tempIncoArr")
                var indexofRemovingTab =
                    tempIncoArr.indexOf(tempIncoArr.filter { it.cAlbumCon == albumController }[0])

                tempIncoArr.removeAt(indexofRemovingTab)
                Log.e(TAG, "removeAlbum:2 af tempIncoArr$tempIncoArr")
                if (tempIncoArr.isNotEmpty()) {
                    if (indexofRemovingTab >= tempIncoArr.size) {
                        indexofRemovingTab = tempIncoArr.size - 1
                    }
                    showAlbum(tempIncoArr[indexofRemovingTab].cAlbumCon, isIncoTab)
                    /*if (isIncoTab) {
                        SharedPrefsUtils.setStringPreference(
                            KeysUtils.keyFocusedWebView,
                            tempIncoArr[indexofRemovingTab].cAlbumCon.toString()
                        )
                    }*/

                } else {
                    SharedPrefsUtils.setBooleanPreference(KeysUtils.keyIsIncoModeOn, false)
                    showHideIncogTabIcon(false)
                    setDialogColors(false)
                    changeFocusedTab()
                    context.toast("All incognito tabs closed!")

                    // TODO: save last focused normal tab
//                    val test=SharedPrefsUtils.getStringPreference(KeysUtils.keyFocusedWebView)
//                     test as AlbumController)

                }

//                tab_container.removeView(albumController!!.albumView)
                var index = BrowserContainer.indexOf(albumController!!)
                BrowserContainer.remove(albumController)
                if (index >= BrowserContainer.size()) {
                    index = BrowserContainer.size() - 1
                }
                tabsDetailsArr.removeAt(indexOfClosedTab)
                updateRecyclerViewAdapter()
//              showAlbum(BrowserContainer[index])
            }
        }

        updateTabCount(isIncoTab)
    }

    private fun updateOmnibox() {
        if (webView === currentAlbumController) {
            omniboxTitle.text = currentAlbumController!!.albumTitle
            //    setColor();
        } else {
            webView = currentAlbumController as BrowserWebView?
            updateProgress(webView!!.progress)
        }
    }

    private fun scrollChange() {
        if (Objects.requireNonNull(
                sp.getString(
                    "sp_hideToolbar",
                    "0"
                )
            ) == "0" || Objects.requireNonNull(
                sp.getString("sp_hideToolbar", "0")
            ) == "1"
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                webView!!.setOnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
                    Log.e(
                        "MyScrollingBehaviour", "2onCreate:" +
                                "\n scrollY :: $scrollY \n oldScrollY :: $oldScrollY "
                    )

                    val height =
                        floor((webView!!.contentHeight * webView!!.resources.displayMetrics.density).toDouble())
                            .toInt()
                    val webViewHeight = webView!!.height
                    val cutoff =
                        height - webViewHeight - 112 * resources.displayMetrics.density.roundToInt()
                    if (Objects.requireNonNull(sp.getString("sp_hideToolbar", "0")) == "0") {
                        @Suppress("UnclearPrecedenceOfBinaryExpression")
                        if (scrollY in (oldScrollY + 1)..cutoff) {
                            hideOmnibox()
                        } else if (scrollY < oldScrollY) {
                            showOmnibox()
                        }
                    } else if (Objects.requireNonNull(sp.getString("sp_hideToolbar", "0")) == "1") {
                        hideOmnibox()
                    }
                }
            }
        }
    }

    override fun updateProgressBar(progress: Int) {
        Log.e("Test101", "Progress at " + progress)
//        updateOmnibox()
//        //    setColor();
//        scrollChange()
//        initRendering(contentFrame)
//        if (webView!!.url != null && webView!!.url != "")
        updateTabsAdapterData(progress, webView!!)
//        webView!!.requestFocus()
        if (progress < BrowserUnit.PROGRESS_MAX) {
            Log.e(TAG, "updateProgress 1: $progress" )
            progressBar.progress = progress
            progressBar.visibility = VISIBLE
        } else {
            progressBar.visibility = INVISIBLE
            swipeRefreshLayout.isRefreshing = false
        }

        updateTopHttpIcons(webView!!)

    }

    override fun hideProgressBar(progress: Int) {
        progressBar.visibility = INVISIBLE
    }

    @Synchronized
    override fun updateProgress(progress: Int) {
        Log.e(TAG, "updateProgress 2: $progress" )
        progressBar.progress = progress
        updateOmnibox()
        //    setColor();
        scrollChange()
        initRendering(contentFrame)
        webView!!.requestFocus()
        if (progress < BrowserUnit.PROGRESS_MAX) {
            updateRefresh(true)
            progressBar.visibility = VISIBLE
            progressShadow!!.visibility = VISIBLE
        } else {
            updateBookmarks()
            updateRefresh(false)
            // this is for the close the refresh process ...
            swipeRefreshLayout.isRefreshing = false

//            if (!progress.isLoading) {
//            swipeRefreshContainer.isRefreshing = false
            webView?.detectOverscrollBehavior()
//            }

            progressBar.visibility = GONE

            val img = ViewUnit.capture(
                currentAlbumController as View,
                dimen144dp,
                dimen108dp,
                Bitmap.Config.RGB_565
            )

            currentAlbumController!!.setAlbumCover(
                img
            )

            val tabData = tabsDetailsArr.filter { it.cAlbumCon!! == currentAlbumController }[0]

            setSearchBarData(currentAlbumController as WebView, tabData.isIncoWeb)

        }

    }

    override fun updateTabsAdapterData(progress: Int, webView: WebView) {
        if (progress < BrowserUnit.PROGRESS_MAX) {
            try {

                // coupon

                // Find website and fetch all coupons

                val url = URL(webView.url) // https://www.amazon.com
                val str = url.host.split(".") // www amazon com
                val webSiteName = str[1] + "." + str[2] // amazon.com
//              val webSiteName = str[1] // amazon

//                if (websiteNameIs.isEmpty() || websiteNameIs != webSiteName) {
                if (URL(webView.url).host!! + URL(webView.url).path!! != oldWebAddress && progress > 20) {
                    websiteNameIs = webSiteName
                    oldWebAddress = URL(webView.url).host!! + URL(webView.url).path!!
                    var urlForMatching = (url.host + url.path).replace("www.", "")
                    if (urlForMatching[urlForMatching.length - 1].toString() == "/") {
                        urlForMatching = urlForMatching.substring(0, urlForMatching.length - 1)
                    }
///////////////////////////////////////////////////////////////////////////////// COUPON CODE ////////////////////////////////////
                    getCouponCode(webSiteName, webView, urlForMatching) // amazon.com
                    getComparableProduct(webSiteName, webView, urlForMatching)
//                    getProductsForMatching(convertStringToSha1(urlForMatching)!!)
//                    getProductSkuInfo(websiteNameIs)


                    getFavoriteStores() // Already Added Favorite stores


                    Log.e("inner match url", urlForMatching);
                    roBackUpObjectsList.clear()
                    finalList.clear()
                }

                editToolContainer?.visibility = GONE
                tvRecentSearchText?.visibility = GONE
                listRecentContainer?.visibility = GONE
                closeKeyboard()
            } catch (ex: Exception) {
            }
            Log.e(TAG, "updateTabsAdapterData: ")
        } else {
            Log.e(TAG, "updateTabsAdapterData:progress: $progress & url:${webView.url}")

            val img = ViewUnit.capture(
                webView as View,
                dimen144dp,
                dimen108dp,
                Bitmap.Config.RGB_565
            )
            Log.e(TAG, "updateTabsAdapterData:tabsDetailsArr.size: " + tabsDetailsArr.size)

            tabsDetailsArr.find { it.cAlbumCon == webView }?.albumImg = img
            tabsDetailsArr.find { it.cAlbumCon == webView }?.albumTitle = webView.title

            //update data here
            updateRecyclerViewAdapter()

            editToolContainer?.visibility = GONE
            tvRecentSearchText?.visibility = GONE
            listRecentContainer?.visibility = GONE
            findViewById<ImageView>(R.id.ivWebMic1).visibility = GONE
            findViewById<TextView>(R.id.tvTabCount).visibility = VISIBLE
            findViewById<ImageView>(R.id.ivNewTabPlus).visibility = VISIBLE
            findViewById<ImageView>(R.id.ivMenu).visibility = VISIBLE
            findViewById<ImageView>(R.id.ivHomeSearch).visibility = VISIBLE
            saveOpenTabs("updateTabsAdapterData")
            closeKeyboard()
        }
    }

    private fun getComparableProduct(webSiteName: String, webView: WebView, urlForMatching: String) {
        Log.d("CHECK_BROWSER", "getComparableProduct: 5")
        RetrieveDataFromFirebase(offersDBHelper).findComparableProduct(webView.url
        ) { SHA, productId ->
            Log.d("CHECK_BROWSER", "getComparableProduct: 6")
            RetrieveDataFromFirebase(offersDBHelper).comparableProducts(
                SHA,
                productId,
                object : ComparableProductInterface {
                    override fun getComparableProduct(model: CompareProductModel?) {
                        Log.d("CHECK_BROWSER", "getComparableProduct: 7")
                        compareProductModel = model
                        if (compareProductModel != null) {
                            Log.d("CHECK_BROWSER", "getComparableProduct: 8")
                            Log.d(TAG, "onComplete: 5: NOT NULL")
                            compareBadge!!.visibility = VISIBLE
                            compareBadgeText!!.setText(
                                (compareProductModel!!.seller_count
                                        + compareProductModel!!.listOfSimilarProducts.size).toString()
                            )
                        } else {
                            Log.d("CHECK_BROWSER", "getComparableProduct: 9")
                            Log.d(TAG, "onComplete: 10: NULL")
                            compareBadge!!.visibility = GONE
                        }
                    }

                    override fun errorOnComparableProduct(error: String?) {
                        Log.d("CHECK_BROWSER", "getComparableProduct: 10")
                        compareProductModel = null
                    }

                });
        };

    }

    override fun addNewTab(url: String) {
        addAlbum(url = url)
    }


    private fun updateRefresh(running: Boolean) {
        if (running) {
            omniboxRefresh.setImageDrawable(
                ViewUnit.getDrawable(
                    context,
                    R.drawable.ic_action_close
                )
            )
        } else {
            try {
                if (webView!!.url!!.contains("https://")) {
                    omniboxRefresh.setImageDrawable(
                        ViewUnit.getDrawable(
                            context,
                            R.drawable.ic_action_refresh
                        )
                    )
                } else {
                    omniboxRefresh.setImageDrawable(
                        ViewUnit.getDrawable(
                            context,
                            R.drawable.icon_alert
                        )
                    )
                }
            } catch (e: Exception) {
                omniboxRefresh.setImageDrawable(
                    ViewUnit.getDrawable(
                        context,
                        R.drawable.ic_action_refresh
                    )
                )
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    override fun showFileChooser(filePathCallback: ValueCallback<Array<Uri>>) {
        if (mFilePathCallback != null) {
            mFilePathCallback!!.onReceiveValue(null)
        }
        mFilePathCallback = filePathCallback
        var takePictureIntent: Intent? = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent!!.resolveActivity(packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
                takePictureIntent.putExtra("PhotoPath", mCameraPhotoPath)
            } catch (ex: Exception) {
                ex.printStackTrace()
                Log.e(ContentValues.TAG, "Unable to create Image File", ex)
            }

            if (photoFile != null) {
                mCameraPhotoPath = "file:" + photoFile.absolutePath
                takePictureIntent.putExtra(
                    MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(photoFile)
                )
            } else {
                takePictureIntent = null
            }
        }
        val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
        contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
        contentSelectionIntent.type = "*/*"
        val intentArray: Array<Intent?> = takePictureIntent?.let { arrayOf(it) } ?: arrayOfNulls(0)
        val chooserIntent = Intent(Intent.ACTION_CHOOSER)
        chooserIntent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
        chooserIntent.putExtra(Intent.EXTRA_TITLE, "Image Chooser")
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        startActivityForResult(chooserIntent, INPUT_FILE_REQUEST_CODE)
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun createImageFile(): File {
        val time = Objects.toString(System.currentTimeMillis(), null.toString())
        val storageDir =
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
        val tempDir = File("$storageDir/RoTempFile")
        tempDir.mkdirs()
        val extension = "$time.jpg"
        return File(tempDir, extension)
    }

    override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {
        if (view == null) {
            return
        }
        if (customView != null && callback != null) {
            callback.onCustomViewHidden()
            return
        }
        customView = view
        originalOrientation = requestedOrientation
        fullscreenHolder = FullscreenHolder(context)
        (fullscreenHolder as FullscreenHolder).addView(
            customView,
            FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
            )
        )
        val decorView = window.decorView as FrameLayout
        decorView.addView(
            fullscreenHolder,
            FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT
            )
        )
        customView!!.keepScreenOn = true
        (currentAlbumController as View?)!!.visibility = GONE
        setCustomFullscreen(true)
        if (view is FrameLayout) {
            if (view.focusedChild is VideoView) {
                videoView = view.focusedChild as VideoView
                videoView!!.setOnErrorListener(VideoCompletionListener())
                videoView!!.setOnCompletionListener(VideoCompletionListener())
            }
        }
        customViewCallback = callback
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
    }

    override fun onHideCustomView(): Boolean {
        if (customView == null || customViewCallback == null || currentAlbumController == null) {
            return false
        }
        val decorView = window.decorView as FrameLayout
        decorView.removeView(fullscreenHolder)
        customView!!.keepScreenOn = false
        (currentAlbumController as View).visibility = VISIBLE
        setCustomFullscreen(false)
        fullscreenHolder = null
        customView = null
        if (videoView != null) {
            videoView!!.setOnErrorListener(null)
            videoView!!.setOnCompletionListener(null)
            videoView = null
        }
        requestedOrientation = originalOrientation
//        webView!!.reload()
        return true
    }

    private fun showContextMenuLink(url: String?) {
        bottomSheetDialog = BottomSheetDialog(context, R.style.SheetDialog)
        val dialogView = inflate(context, R.layout.dialog_menu_context_link, null)
        dialogTitle = dialogView.findViewById(R.id.dialog_title)
        dialogTitle.text = url
        val contextLink_newTab = dialogView.findViewById<LinearLayout>(R.id.contextLink_newTab)
        contextLink_newTab.setOnClickListener {

            addAlbum(getString(R.string.app_name), url, false)

            BrowserToast.show(context, getString(R.string.toast_new_tab_successful))
            hideBottomSheetDialog()
        }
        val contextLink__shareLink =
            dialogView.findViewById<LinearLayout>(R.id.contextLink__shareLink)
        contextLink__shareLink.setOnClickListener {
            if (prepareRecord()) {
                BrowserToast.show(context, getString(R.string.toast_share_failed))
            } else {
                IntentUnit.share(context, "", url)
            }
            hideBottomSheetDialog()
        }
        val contextLink_openWith = dialogView.findViewById<LinearLayout>(R.id.contextLink_openWith)
        contextLink_openWith.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            val chooser = Intent.createChooser(intent, getString(R.string.menu_open_with))
            startActivity(chooser)
            hideBottomSheetDialog()
        }
        val contextLink_newTabOpen =
            dialogView.findViewById<LinearLayout>(R.id.contextLink_newTabOpen)
        contextLink_newTabOpen.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                addAlbum(getString(R.string.app_name), url, true)
            }
            hideBottomSheetDialog()
        }
        val contextMenu_saveStart =
            dialogView.findViewById<LinearLayout>(R.id.contextMenu_saveStart)
        contextMenu_saveStart.setOnClickListener {
            hideBottomSheetDialog()
            val action = RecordAction(context)
            action.open(true)
            if (action.checkGridItem(url)) {
                BrowserToast.show(context, getString(R.string.toast_already_exist_in_home))
            } else {
                var counter = sp.getInt("counter", 0)
                counter += 1
                sp.edit().putInt("counter", counter).apply()
                val bitmap = ViewUnit.createImage(3, 3, vibrantColor)
                val filename = counter.toString() + BrowserUnit.SUFFIX_PNG
                val itemAlbum = GridItem(
                    Uri.parse(url).host?.replace("www.", "")
                        ?.trim { it <= ' ' },
                    url, filename, counter
                )
                if (BrowserUnit.bitmap2File(context, bitmap, filename) && action.addGridItem(
                        itemAlbum
                    )
                ) {
                    BrowserToast.show(context, getString(R.string.toast_add_to_home_successful))
                } else {
                    BrowserToast.show(context, getString(R.string.toast_add_to_home_failed))
                }
            }
            action.close()
        }
        val contextLink_sc = dialogView.findViewById<LinearLayout>(R.id.contextLink_sc)
        contextLink_sc.setOnClickListener {
            hideBottomSheetDialog()
            HelperUnit.createShortcut(
                context,
                Uri.parse(url).host?.replace("www.", "")?.trim { it <= ' ' },
                url
            )
        }

        bottomSheetDialog.setContentView(dialogView)
        bottomSheetDialog.show()
        HelperUnit.setBottomSheetBehavior(
            bottomSheetDialog,
            dialogView,
            BottomSheetBehavior.STATE_EXPANDED
        )
    }

    override fun onLongPress(url: String?) {
        val result = webView!!.hitTestResult
        if (url != null) {
            showContextMenuLink(url)
        } else if (result.type == HitTestResult.IMAGE_TYPE || result.type == HitTestResult.SRC_IMAGE_ANCHOR_TYPE || result.type == HitTestResult.SRC_ANCHOR_TYPE) {
            showContextMenuLink(result.extra)
        }
    }

    private fun doubleTapsQuit() {
        if (!sp.getBoolean("sp_close_browser_confirm", true)) {
//            finish()
        } else {
            saveOpenTabs("doubleTapsQuit")
            moveTaskToBack(true)
//            bottomSheetDialog = BottomSheetDialog(context, R.style.SheetDialog)
//            val dialogView = inflate(context, R.layout.dialog_action, null)
//            val textView = dialogView.findViewById<TextView>(R.id.dialog_text)
//            textView.setText(R.string.toast_quit)
//            val action_ok = dialogView.findViewById<Button>(R.id.action_ok)
//            action_ok.setOnClickListener {
//                saveOpenTabs("doubleTapsQuit")
//                bottomSheetDialog.dismiss()
////                finish()
//                moveTaskToBack(true)
//            }
//            val action_cancel = dialogView.findViewById<Button>(R.id.action_cancel)
//            action_cancel.setOnClickListener { hideBottomSheetDialog() }
//            bottomSheetDialog.setContentView(dialogView)
//            bottomSheetDialog.show()
//            HelperUnit.setBottomSheetBehavior(
//                bottomSheetDialog,
//                dialogView,
//                BottomSheetBehavior.STATE_EXPANDED
//            )
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("RestrictedApi")
    private fun showOmnibox() {
        if (omnibox.visibility == GONE) {

            if (omnibox.visibility == GONE) {

                omnibox.visibility = GONE
                omniboxTitle.visibility = GONE
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("RestrictedApi")
    private fun hideOmnibox() {


        if (omnibox.visibility == VISIBLE) {
            omnibox.visibility = GONE
        }
    }

    private fun hideSearchPanel() {
        hideKeyboard(context)
        omniboxTitle.visibility = VISIBLE
        omnibox.visibility = VISIBLE
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private fun showSearchPanel() {
        omnibox.visibility = GONE
        omniboxTitle.visibility = GONE
    }

    private fun showOverflow(): Boolean {
        bottomSheetDialog = BottomSheetDialog(context, R.style.SheetDialog)
        val dialogView = inflate(context, R.layout.dialog_menu, null)
        dialogView.setBackgroundColor(Color.TRANSPARENT)
        fab_tab = dialogView.findViewById(R.id.floatButton_tab)
        fab_tab.setOnClickListener(this@BrowserActivity)
        fab_share = dialogView.findViewById(R.id.floatButton_share)
        fab_share.setOnClickListener(this@BrowserActivity)
        fab_save = dialogView.findViewById(R.id.floatButton_save)
        fab_save.setOnClickListener(this@BrowserActivity)
        fab_more = dialogView.findViewById(R.id.floatButton_more)
        fab_more.setOnClickListener(this@BrowserActivity)
        floatButton_tabView = dialogView.findViewById(R.id.floatButton_tabView)
        floatButton_saveView = dialogView.findViewById(R.id.floatButton_saveView)
        floatButton_shareView = dialogView.findViewById(R.id.floatButton_shareView)
        floatButton_moreView = dialogView.findViewById(R.id.floatButton_moreView)
        dialogTitle = dialogView.findViewById(R.id.dialog_title)
        dialogTitle.text = webView!!.title
        menu_newTabOpen = dialogView.findViewById(R.id.menu_newTabOpen)
        menu_newTabOpen.setOnClickListener(this@BrowserActivity)
        menu_closeTab = dialogView.findViewById(R.id.menu_closeTab)
        menu_closeTab.setOnClickListener(this@BrowserActivity)
        menu_tabPreview = dialogView.findViewById(R.id.menu_tabPreview)
        menu_tabPreview.setOnClickListener(this@BrowserActivity)
        menu_quit = dialogView.findViewById(R.id.menu_quit)
        menu_quit.setOnClickListener(this@BrowserActivity)
        menu_shareScreenshot = dialogView.findViewById(R.id.menu_shareScreenshot)
        menu_shareScreenshot.setOnClickListener(this@BrowserActivity)
        menu_shareLink = dialogView.findViewById(R.id.menu_shareLink)
        menu_shareLink.setOnClickListener(this@BrowserActivity)
        menu_sharePDF = dialogView.findViewById(R.id.menu_sharePDF)
        menu_sharePDF.setOnClickListener(this@BrowserActivity)
        menu_openWith = dialogView.findViewById(R.id.menu_openWith)
        menu_openWith.setOnClickListener(this@BrowserActivity)
        ll_share = dialogView.findViewById(R.id.ll_share)
        ll_share.setOnClickListener(this)
        ll_share_1 = dialogView.findViewById(R.id.ll_share_1)
        ll_share_1.setOnClickListener(this)
        menu_saveScreenshot = dialogView.findViewById(R.id.menu_saveScreenshot)
        menu_saveScreenshot.setOnClickListener(this@BrowserActivity)
        menu_saveBookmark = dialogView.findViewById(R.id.menu_saveBookmark)
        menu_saveBookmark.setOnClickListener(this@BrowserActivity)
        menu_savePDF = dialogView.findViewById(R.id.contextLink_saveAs)
        menu_savePDF.setOnClickListener(this@BrowserActivity)
        menu_saveStart = dialogView.findViewById(R.id.menu_saveStart)
        menu_saveStart.setOnClickListener(this@BrowserActivity)
        ll_save = dialogView.findViewById(R.id.ll_save)
        ll_save.setOnClickListener(this)
        ll_save_1 = dialogView.findViewById(R.id.ll_save_1)
        ll_save_1.setOnClickListener(this)
        menu_searchSite = dialogView.findViewById(R.id.menu_searchSite)
        menu_searchSite.setOnClickListener(this@BrowserActivity)
        menu_settings = dialogView.findViewById(R.id.menu_settings)
        menu_settings.setOnClickListener(this@BrowserActivity)
        menu_download = dialogView.findViewById(R.id.menu_download)
        menu_download.setOnClickListener(this@BrowserActivity)
        menu_help = dialogView.findViewById(R.id.menu_help)
        menu_help.setOnClickListener(this@BrowserActivity)
        ll_more = dialogView.findViewById(R.id.ll_more)
        ll_more.setOnClickListener(this)
        ll_more_1 = dialogView.findViewById(R.id.ll_more_1)
        ll_more_1.setOnClickListener(this)
        menu_shareCLipboard = dialogView.findViewById(R.id.menu_shareCLipboard)
        menu_shareCLipboard.setOnClickListener {
            hideBottomSheetDialog()
            val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("text", url)
            clipboard.setPrimaryClip(clip)
            BrowserToast.show(context, R.string.toast_copy_successful)
        }
        menu_openFav = dialogView.findViewById(R.id.menu_openFav)
        menu_openFav.setOnClickListener {
            hideBottomSheetDialog()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                updateAlbum(sp.getString("favoriteURL", "https://google.com"))
            }
        }
        menu_sc = dialogView.findViewById(R.id.menu_sc)
        menu_sc.setOnClickListener {
            hideBottomSheetDialog()
            HelperUnit.createShortcut(context, webView!!.title, webView!!.url)
        }
        menu_fav = dialogView.findViewById(R.id.menu_fav)
        menu_fav.setOnClickListener {
            hideBottomSheetDialog()
            HelperUnit.setFavorite(context, url!!)
        }
        floatButton_tabView.setBackgroundColor(vibrantColor)
        floatButton_saveView.setBackgroundColor(vibrantColor)
        floatButton_shareView.setBackgroundColor(vibrantColor)
        floatButton_moreView.setBackgroundColor(vibrantColor)
        bottomSheetDialog.setContentView(dialogView)
        bottomSheetDialog.show()
        HelperUnit.setBottomSheetBehavior(
            bottomSheetDialog,
            dialogView,
            BottomSheetBehavior.STATE_EXPANDED
        )
        return true
    }

    private fun show_contextMenu_list(
        title: String, url: String,
        adapterRecord: AdapterRecord?, recordList: MutableList<Record>?, location: Int,
        _id: String?, gridItem: GridItem?,
    ) {
        bottomSheetDialog = BottomSheetDialog(context, R.style.SheetDialog)
        val dialogView = inflate(context, R.layout.dialog_menu_context_list, null)
        val db = BookmarkList(context)
        db.open()
        val contextList_edit = dialogView.findViewById<LinearLayout>(R.id.menu_contextList_edit)
        val contextList_fav = dialogView.findViewById<LinearLayout>(R.id.menu_contextList_fav)
        val contextList_sc = dialogView.findViewById<LinearLayout>(R.id.menu_contextLink_sc)
        val contextList_newTab = dialogView.findViewById<LinearLayout>(R.id.menu_contextList_newTab)
        val contextList_newTabOpen =
            dialogView.findViewById<LinearLayout>(R.id.menu_contextList_newTabOpen)
        val contextList_delete = dialogView.findViewById<LinearLayout>(R.id.menu_contextList_delete)
        if (overViewTab == getString(R.string.album_title_history)) {
            contextList_edit.visibility = GONE
        } else {
            contextList_edit.visibility = VISIBLE
        }
        contextList_fav.setOnClickListener {
            hideBottomSheetDialog()
            HelperUnit.setFavorite(context, url)
        }
        contextList_sc.setOnClickListener {
            hideBottomSheetDialog()
            HelperUnit.createShortcut(context, title, url)
        }
        contextList_newTab.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                addAlbum(getString(R.string.app_name), url, false)
            }
            BrowserToast.show(context, getString(R.string.toast_new_tab_successful))
            hideBottomSheetDialog()
        }
        contextList_newTabOpen.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                addAlbum(getString(R.string.app_name), url, true)
            }
            hideBottomSheetDialog()
            hideOverview()
        }
        contextList_delete.setOnClickListener {
            hideBottomSheetDialog()
            bottomSheetDialog = BottomSheetDialog(context, R.style.SheetDialog)
            val dialogAction = inflate(context, R.layout.dialog_action, null)
            val textView = dialogAction.findViewById<TextView>(R.id.dialog_text)
            textView.setText(R.string.toast_titleConfirm_delete)
            val actionOk = dialogAction.findViewById<Button>(R.id.action_ok)
            actionOk.setOnClickListener {
                if (overViewTab == getString(R.string.album_title_home)) {
                    val action = RecordAction(context)
                    action.open(true)
                    action.deleteGridItem(gridItem)
                    action.close()
                    deleteFile(gridItem?.filename)
                    open_startPage.performClick()
                    hideBottomSheetDialog()
                } else if (overViewTab == getString(R.string.album_title_bookmarks)) {
                    db.delete(_id!!.toInt())
                    initBookmarkList()
                    hideBottomSheetDialog()
                } else if (overViewTab == getString(R.string.album_title_history)) {
                    val record = recordList!![location]
                    val action = RecordAction(context)
                    action.open(true)
                    action.deleteHistory(record)
                    action.close()
                    recordList.removeAt(location)
                    adapterRecord!!.notifyDataSetChanged()
                    updateBookmarks()
                    updateAutoComplete()
                    hideBottomSheetDialog()
                }
            }
            val actionCancel = dialogAction.findViewById<Button>(R.id.action_cancel)
            actionCancel.setOnClickListener { hideBottomSheetDialog() }
            bottomSheetDialog.setContentView(dialogAction)
            bottomSheetDialog.show()
            HelperUnit.setBottomSheetBehavior(
                bottomSheetDialog,
                dialogAction,
                BottomSheetBehavior.STATE_EXPANDED
            )
        }
        bottomSheetDialog.setContentView(dialogView)
        bottomSheetDialog.show()
        HelperUnit.setBottomSheetBehavior(
            bottomSheetDialog,
            dialogView,
            BottomSheetBehavior.STATE_EXPANDED
        )
    }

    private fun setCustomFullscreen(fullscreen: Boolean) {
        val decorView = window.decorView
        if (fullscreen) {
            decorView.systemUiVisibility = (SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                    or SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                    or SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
        } else {
            decorView.systemUiVisibility = SYSTEM_UI_FLAG_VISIBLE
        }
    }

    override fun onBackPressed() {
        if(isBrowserOn) {
            Log.e("onBackPressed", ": log1")
            if (webView!!.canGoBack()) {
                Log.e("onBackPressed", ": log2")
                if (checkwebedit == false) {
                    listRecentContainer!!.visibility = GONE
                    editToolContainer?.visibility = GONE
                    tvRecentSearchText?.visibility = GONE
//                    webView?.loadUrl(searchView2.query.toString())
                    setSearchBarData(webView!!)
                }
                webView!!.goBack()
            } else {
                if (!back) {
                    editToolContainer?.visibility = GONE
                    tvRecentSearchText?.visibility = GONE
                    listRecentContainer!!.visibility = GONE
                    Log.e("onBackPressed", ": log3")
                    findViewById<ImageView>(R.id.ivNewTabPlus).visibility = VISIBLE
                    findViewById<TextView>(R.id.tvTabCount).visibility = VISIBLE
                    findViewById<ImageView>(R.id.ivMenu).visibility = VISIBLE
                    findViewById<ImageView>(R.id.ivHomeSearch).visibility = VISIBLE
                    rl_inner_layout!!.visibility = VISIBLE
                    mainSearchBar.visibility = VISIBLE
                    searchView2.visibility = VISIBLE
                    mainSearchBar.visibility = VISIBLE
                    setSearchBarData(webView!!)
                    back = true
                } else {
                    try {
                        unregisterReceiver(downloadReceiver)
                    } catch (e: IllegalArgumentException) {
                        e.printStackTrace()
                    }
                    doubleTapsQuit()
                }
            }
        }
        else{
            Log.e("onBackPressed", ": log4")
            super.onBackPressed()
        }
    }

    /*private var scrollableChangeListener = object : OnScrollableChangeListener {
        override fun onScrollableChanged(scrollable: Boolean) {
            Log.e("hkjvfhvdsbafhkb", "onScrollableChanged: ${scrollable}")
            *//*   val tab = currentTabData ?: return
               adjustBrowserPadding(tab)*//*
        }
    }*/

    private inner class VideoCompletionListener : OnCompletionListener,
        MediaPlayer.OnErrorListener {
        override fun onError(mp: MediaPlayer, what: Int, extra: Int): Boolean {
            return false
        }

        override fun onCompletion(mp: MediaPlayer) {
            onHideCustomView()
        }
    }

    companion object {

        var vpnScroll: Boolean? = null

        const val WEBVIEW_IMG_DIR = "webViewTabImagesDir"

        private val NEGATIVE_COLOR = floatArrayOf(
            -1.0f,
            0f,
            0f,
            0f, 255f, 0f, -1.0f,
            0f,
            0f,
            255f,
            0f,
            0f,
            -1.0f,
            0f,
            255f,
            0f,
            0f,
            0f,
            1.0f,
            0f
        )
        private const val INPUT_FILE_REQUEST_CODE = 1
        var webView: BrowserWebView? = null
        var dbHandler: MyDbHandler? = null
        var dbHandlerbook: myDbHandlerBook? = null

        @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
        fun hideKeyboard(activity: Activity?) {
            val imm = activity!!.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            var view = activity.currentFocus
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

    }

    override fun onScrollableChanged(scrollable: Boolean) {

    }

    //     add scroll event for detect scroll position y = 0
    override fun onStart() {
        super.onStart()
        swipeRefreshLayout.viewTreeObserver.addOnScrollChangedListener(OnScrollChangedListener {
            swipeRefreshLayout.isEnabled = webView?.scrollY == 0
        }
            .also { mOnScrollChangedListener = it })
    }

    // remove scroll event on
    override fun onStop() {
        swipeRefreshLayout.viewTreeObserver.removeOnScrollChangedListener(mOnScrollChangedListener);
        super.onStop()
    }

    fun parseXml(str: String) {
        try {
            val factory = XmlPullParserFactory.newInstance()
            factory.isNamespaceAware = true
            val xpp = factory.newPullParser()
            xpp.setInput(StringReader(str)) // pass input whatever xml you have
            var eventType = xpp.eventType
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_DOCUMENT) {
                    Log.e(TAG, "Start document")
                } else if (eventType == XmlPullParser.START_TAG) {
                    Log.e(TAG, "Start tag " + xpp.name)
                } else if (eventType == XmlPullParser.END_TAG) {
                    Log.e(TAG, "End tag " + xpp.name)
                } else if (eventType == XmlPullParser.TEXT) {
                    Log.e(TAG, "Text " + xpp.text) // here you get the text from xml
                }
                eventType = xpp.next()
            }
            Log.e(TAG, "End document")
        } catch (e: XmlPullParserException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     *  Old
     */
//    private fun getProductsForMatching(sha: String, productId: String) {
//        compareBadge!!.visibility = View.GONE
//        FireStoreHelper().getProductsForMatching(sha, productId, object : FireStoreListener {
//            override fun onComplete(response: String) {
//                Log.e("ConvertMyUrl", "onComplete $response")
//                compareProductModel = Gson().fromJson(response, CompareProductModel::class.java)
//                try {
//                    if (response != "") {
//
//                        // TODO : NEW
////                        compareBadgeText!!.setText((compareProductModel!!.listOfSeller?.size?.plus(
////                            compareProductModel!!.listOfSimilarProducts?.size!!
////                        )).toString())
//                        // TODO : OLD
//                        compareBadgeText!!.setText((compareProductModel!!.listOfSeller.size + compareProductModel!!.listOfSimilarProducts.size).toString())
//                        compareBadge!!.visibility = View.VISIBLE
//                    } else {
//                        compareBadge!!.visibility = View.GONE
//                    }
//                } catch (e: Exception) {
//
//                }
//            }
//
//            override fun onError(error: String) {
//                Log.e(TAG, "response------->$error")
//                compareProductModel = null
//            }
//
//        })
//    }

    /**
     *  New
     */
    private fun getProductsForMatching(sha: String, productId: String) {
        compareBadge!!.visibility = GONE
        Log.d(TAG, "getProductsForMatching: " + sha + " : " + productId)

        FireStoreHelper().getProductsToCompare(
            sha,
            productId,
            object : FirestoreProductMatchingInterface {
                override fun onComplete(model: CompareProductModel) {
                    Log.d(TAG, "onComplete: 1: " + compareProductModel!!.listOfSeller.size)
                    compareProductModel = model
                    if (compareProductModel != null) {
                        Log.d(TAG, "onComplete: 5: NOT NULL")
                        compareBadge!!.visibility = VISIBLE
                        compareBadgeText!!.setText(
                            (compareProductModel!!.seller_count
                                    + compareProductModel!!.listOfSimilarProducts.size).toString()
                        )
                    } else {
                        Log.d(TAG, "onComplete: 10: NULL")
                        compareBadge!!.visibility = GONE
                    }
                }

                override fun onError(error: String) {
                    compareProductModel = null
                }

            })
    }

    fun openNewTabForNotification(directLink: String) {
        SharedPrefsUtils.setBooleanPreference(KeysUtils.keyIsIncoModeOn, false)
        hideOverview()
        addAlbum(
            "New tab",
            sp.getString("favoriteURL", directLink),
            true
        )
    }

    fun openNewTabForCompare(directLink: String) {
        SharedPrefsUtils.setBooleanPreference(KeysUtils.keyIsIncoModeOn, false)
        hideCompareBottomSheet()
        hideOverview()
        addAlbum(
            "New tab",
            sp.getString("favoriteURL", directLink),
            true
        )
    }

    fun openDirectLinkFromAppMode(directLink: String){
        addAlbum(
            "New tab",
            sp.getString("favoriteURL", directLink),
            true
        )
    }

    private fun getProductId(
        url: String,
        className: String?,
        removeStrings: ArrayList<String>,
    ): String {
        Log.d("CHECK_BROWSER", "getCouponCode: 4")
        try {
            CoroutineScope(Dispatchers.Default).launch {
                try {
                    var hit = ""
                    if (className != "NULL") {
                        Log.d("CHECK_BROWSER", "getCouponCode: 7")
                        val document: Document = Jsoup.connect(url).get()
//                document.allElements.forEach {
//                if (it.className().lowercase().contains(className)) {

                        hit = document.select(className).text()
                        if (!hit.isNullOrEmpty()) {
                            for (item in removeStrings) {
                                hit = hit.replace(item, "")
                            }
                            hit = hit.trim().toLowerCase().toString()
                                .replace(("[^A-Za-z0-9]").toRegex(), "")
                            Log.e(TAG, "getProductId: $hit")

                        }
                    } else {
                        hit = ""
                    }


                    CoroutineScope(Dispatchers.Main).launch {
                        Log.d("CHECK_BROWSER", "getCouponCode: 8")
                        Log.e("ConvertMyUrl", "Activity: $url")
                        val convertUrlReal = url.split("/?")[0]
                        val convertUrl = ConvertUrlToSHA().getConvertedSHA(url)
                        Log.e("ConvertMyUrl", "onStartCommand: $convertUrl")
                        getProductsForMatching(convertUrl.toString(), hit)
                    }
                } catch (e: Exception) {
                    Log.d("CHECK_BROWSER", "getCouponCode: 6 : " + e.message)
                    e.printStackTrace()
                }

            }
        } catch (e: Exception) {
            Log.d("CHECK_BROWSER", "getCouponCode: 5 : " + e.message)
            e.printStackTrace()
        }

        return ""

    }

    private fun openCompareBottomSheet() {
        compareProductsBottomSheetDialog =
            BottomSheetDialog(this, R.style.AppBottomSheetDialogTheme)
        compareProductsBottomSheetDialog.setContentView(R.layout.compare_bottomsheet_layout)
        rvCompareProducts =
            compareProductsBottomSheetDialog.findViewById<RecyclerView>(R.id.rvCompareProducts)
        val llCoupons = compareProductsBottomSheetDialog.findViewById<LinearLayout>(R.id.llCoupons)
        val llSimilarCoupons =
            compareProductsBottomSheetDialog.findViewById<LinearLayout>(R.id.llSimilarCoupons)!!
        val llEmpty = compareProductsBottomSheetDialog.findViewById<LinearLayout>(R.id.llEmpty)!!
        val tvCountCoupon: TextView =
            compareProductsBottomSheetDialog.findViewById<TextView>(R.id.tvCountCoupon)!!
        val tvNoSimilarProduct: TextView =
            compareProductsBottomSheetDialog.findViewById<TextView>(R.id.tvNoSimilarProduct)!!
        val tvComment: TextView =
            compareProductsBottomSheetDialog.findViewById<TextView>(R.id.tvComment)!!
        val tvRange: TextView =
            compareProductsBottomSheetDialog.findViewById<TextView>(R.id.tvRange)!!
        val tvCountCouponSim: TextView =
            compareProductsBottomSheetDialog.findViewById<TextView>(R.id.tvCountCouponSim)!!
        val ivClose: ImageView =
            compareProductsBottomSheetDialog.findViewById<ImageView>(R.id.ivClose)!!
        val imgSimilarProduct: ImageView =
            compareProductsBottomSheetDialog.findViewById<ImageView>(R.id.imgSimilarProduct)!!
        val rlSaveProduct: RelativeLayout =
            compareProductsBottomSheetDialog.findViewById<RelativeLayout>(R.id.rlSaveProduct)!!


        ivClose.setOnClickListener {
            compareProductsBottomSheetDialog.dismiss()
        }




        //new here
        rvCompareProducts!!.setOnTouchListener(OnTouchListener { v, event ->
            val action = event.action
            when (action) {
                MotionEvent.ACTION_DOWN -> {
                    // Disallow NestedScrollView to intercept touch events.
                    Log.d("MotionEvent","ACTION_DOWN")
                    v.parent.requestDisallowInterceptTouchEvent(true)
                }
                MotionEvent.ACTION_UP -> {
                    // Allow NestedScrollView to intercept touch events.
                    Log.d("MotionEvent","ACTION_UP")
                    v.parent.requestDisallowInterceptTouchEvent(false)
                }

            }
            // Handle RecyclerView touch
            v.onTouchEvent(event)
            true
        })


        if (compareProductModel != null) {

            tvCountCoupon.text = compareProductModel!!.listOfSeller.size.toString()
            tvCountCouponSim.text = compareProductModel!!.listOfSimilarProducts.size.toString()
            tvComment.text = compareProductModel!!.product_name
            tvRange.text =
                compareProductModel!!.currency + " " + compareProductModel!!.min_price + " - " + compareProductModel!!.max_price
            tvComment.visibility = VISIBLE
            tvRange.visibility = VISIBLE
            llEmpty.visibility = INVISIBLE
            rlSaveProduct.visibility = VISIBLE
            val adapter =
                CompareProductAdapter(this, object : CompareProductInterface {
                    override fun onItemClicked(view: View?, directLink: String) {
                        // do something here
                        openNewTabForCompare(directLink)
                    }
                }, compareProductModel!!.listOfSeller)
            rvCompareProducts!!.adapter = adapter
            rvCompareProducts?.layoutManager = LinearLayoutManager(this)
        } else {
            llEmpty.visibility = VISIBLE
            tvComment.visibility = GONE
            rlSaveProduct.visibility = GONE
            tvRange.visibility = GONE
            imgSimilarProduct.setImageDrawable(resources.getDrawable(R.drawable.ic_compare))
            tvNoSimilarProduct.text = "There are no other offers for this product"
        }

        llCoupons!!.setOnClickListener {

            if (compareProductModel != null) {
                tvComment.visibility = VISIBLE
                tvRange.visibility = VISIBLE
            } else {
                tvComment.visibility = GONE
                tvRange.visibility = GONE
            }
            llSimilarCoupons.backgroundTintList =
                ContextCompat.getColorStateList(
                    this,
                    android.R.color.transparent
                )
            llCoupons!!.backgroundTintList =
                ContextCompat.getColorStateList(this, android.R.color.white)

            if (compareProductModel != null) {
                llEmpty.visibility = INVISIBLE
                rlSaveProduct.visibility = VISIBLE

                val adapter =
                    CompareProductAdapter(
                        this,
                        object : CompareProductInterface {
                            override fun onItemClicked(view: View?, directLink: String) {
                                // do something here
                                openNewTabForCompare(directLink)
                            }
                        },
                        compareProductModel!!.listOfSeller
                    )
                rvCompareProducts!!.adapter = adapter
                rvCompareProducts?.layoutManager = LinearLayoutManager(this)

            } else {
                llEmpty.visibility = VISIBLE
                rlSaveProduct.visibility = GONE
                imgSimilarProduct.setImageDrawable(resources.getDrawable(R.drawable.ic_compare))
                tvNoSimilarProduct.text = "There are no other offers for this product"
            }
        }

        llSimilarCoupons.setOnClickListener {
            tvComment.visibility = GONE
            tvRange.visibility = GONE
            llSimilarCoupons.backgroundTintList =
                ContextCompat.getColorStateList(this, android.R.color.white)
            llCoupons!!.backgroundTintList =
                ContextCompat.getColorStateList(
                    this,
                    android.R.color.transparent
                )

            if (compareProductModel != null) {
                llEmpty.visibility = INVISIBLE
                val adapter = SimilarProductAdapter(
                    this, object : CompareProductInterface {
                        override fun onItemClicked(view: View?, directLink: String) {
                            // do your code for Similar compare product
                            openNewTabForCompare(directLink)

                        }

                    },
                    compareProductModel!!.listOfSimilarProducts.toList()
                )
                rvCompareProducts!!.adapter = adapter
                rvCompareProducts?.layoutManager = GridLayoutManager(this, 2)
            } else {
                llEmpty.visibility = VISIBLE
                imgSimilarProduct.setImageDrawable(resources.getDrawable(R.drawable.ic_similar_products))
                tvNoSimilarProduct.text = "No similar products were found"
            }

        }

        rlSaveProduct.setOnClickListener {
            val sha = ConvertUrlToSHA().getConvertedSHA(webView?.url!!)
            openEditSaveItemBottomSheet(sha)
            compareProductsBottomSheetDialog.dismiss()
        }

        //original here
        /*rvCompareProducts!!.setOnTouchListener(OnTouchListener { v, event ->
            val action = event.action
            when (action) {
                MotionEvent.ACTION_DOWN ->                         // Disallow NestedScrollView to intercept touch events.
                    v.parent.requestDisallowInterceptTouchEvent(true)
                MotionEvent.ACTION_UP ->                         // Allow NestedScrollView to intercept touch events.
                    v.parent.requestDisallowInterceptTouchEvent(false)
            }

            // Handle RecyclerView touch events.
            v.onTouchEvent(event)
            true
        })*/



        //originally added
        val bottom_dialog = compareProductsBottomSheetDialog as BottomSheetDialog
        val bottomSheet = (bottom_dialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?)!!

        val displayMetrics: DisplayMetrics = resources.displayMetrics
        val height = displayMetrics.heightPixels
        val maxHeight = (height * 0.80).toInt()
        BottomSheetBehavior.from(bottomSheet).peekHeight = maxHeight
        BottomSheetBehavior.from(bottomSheet).maxHeight = maxHeight
        compareProductsBottomSheetDialog.show()

        compareProductsBottomSheetDialog.behavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    (compareProductsBottomSheetDialog.behavior).isDraggable = true
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                if (slideOffset < -0.05) {
                    (compareProductsBottomSheetDialog.behavior).state = BottomSheetBehavior.STATE_HIDDEN
                    (compareProductsBottomSheetDialog.behavior).isDraggable = false
                }
            }
        })






    }

    private fun hideCompareBottomSheet() {
        compareProductsBottomSheetDialog.cancel()
    }


    private fun openEditSaveItemBottomSheet(hashUrl: String) {

        editItemSaveBottomSheet = BottomSheetDialog(this@BrowserActivity, R.style.AppBottomSheetDialogTheme)
        editItemSaveBottomSheet.setContentView(R.layout.edit_product_item)
        var isPurchased = false
        val ivClose = editItemSaveBottomSheet.findViewById<ImageView>(R.id.ivClose)
        val cvCreateList = editItemSaveBottomSheet.findViewById<CardView>(R.id.cvCreateList)
        val tvNoListAvailable = editItemSaveBottomSheet.findViewById<TextView>(R.id.tvNoListAvailable)
        val tvPrice = editItemSaveBottomSheet.findViewById<TextView>(R.id.tvPrice)
        val tvDetails = editItemSaveBottomSheet.findViewById<TextView>(R.id.tvDetails)
        val tvTitle = editItemSaveBottomSheet.findViewById<TextView>(R.id.tvTitle)
        val switchNotification = editItemSaveBottomSheet.findViewById<Switch>(R.id.switchNotification)
        val ivProduct = editItemSaveBottomSheet.findViewById<ImageView>(R.id.ivProduct)
        val iv25Per = editItemSaveBottomSheet.findViewById<ImageView>(R.id.iv25Per)
        val iv50Per = editItemSaveBottomSheet.findViewById<ImageView>(R.id.iv50Per)
        val rvList = editItemSaveBottomSheet.findViewById<RecyclerView>(R.id.rvList)
        val ll25Per = editItemSaveBottomSheet.findViewById<LinearLayout>(R.id.ll25Per)
        val ll50Per = editItemSaveBottomSheet.findViewById<LinearLayout>(R.id.ll50Per)
        ivFavorite = editItemSaveBottomSheet.findViewById<ImageView>(R.id.ivFavorite)!!
        val ivMarkFullFilled = editItemSaveBottomSheet.findViewById<ImageView>(R.id.ivMarkFullFilled)
        val llUpdate = editItemSaveBottomSheet.findViewById<LinearLayout>(R.id.llUpdate)
        editItemSaveBottomSheet.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        isListItemSelected = false
        var notificationDiscount = 25

        FireStoreHelper().getProductFromHashUrl(
            hashUrl,
            object : FireStoreGetProductFromHashUrl {
                override fun onSuccess(productData: QuerySnapshot) {

                    productList.clear()

                    for (document in productData) {
                        val jsonData = Utils.convertToJson(document.data)
                        val gsn = Gson()
                        val productModel: ProductModel = gsn.fromJson<ProductModel>(
                            jsonData.toString(),
                            ProductModel::class.java
                        )

                        if (productModel != null) {
                            /*if (productModel.id_product in favoriteProductIdList) {
                                productModel.isFavorite = true
                            }*/
                            productList.add(productModel)
                        }
                    }

                    Log.e(TAG, "getHashProduct: $productList" )

                    val hashProductGetIndex = productList[0].product_urls!!.indexOf(hashUrl)

                    productList[0].product_urls?.get(hashProductGetIndex)



                    if (productList.isNotEmpty()) {
                        if (!productList[0].seller_list.isNullOrEmpty()) {
                            tvPrice?.text =
                                "${productList[0].currency} ${productList[0]!!.seller_list!![hashProductGetIndex]!!.price}"
                            tvTitle?.text = productList[0].product_name
                            Glide.with(editItemSaveBottomSheet.context)
                                .load(productList[0]!!.seller_list!![hashProductGetIndex]!!.store_logo!!)
                                .into(ivProduct!!)
                        }
                        this@BrowserActivity.productData = productList[0]
                    }

                }
                override fun onError(error: String) {
                    Log.e(TAG, "Error adding document $error")
                    Toast.makeText(this@BrowserActivity, "Something went wrong to added product", Toast.LENGTH_SHORT).show()
                }
            }
        )

        createListBrowserAdapter = CreateListBrowserAdapter(
            filterList,
            object: CreateListBrowserAdapter.CreateListBrowserListener {
                override fun onItemClick(data: ListCollectionModel) {
                    isListItemSelected = true
                    selectedListId = data.listId
                    selectedListProducts.clear()
                    selectedListProducts.addAll(filterList[filterList.indexOf(data)].productsList)
                    Log.e(TAG, "onItemClick: list name :- $selectedListId and selectedListProduct $selectedListProducts")
                }
            }
        )


        getList(rvList!!, tvNoListAvailable!!)

        rvList?.layoutManager =
            LinearLayoutManager(this@BrowserActivity, LinearLayoutManager.HORIZONTAL, false)
        rvList?.adapter = createListBrowserAdapter

        /*if(productData?.isFavorite!!){
            ivFavoriteEditBottomSheet?.setImageDrawable(ContextCompat.getDrawable(this@BrowserActivity, R.drawable.ic_favorite_fill))
        }else{
            ivFavoriteEditBottomSheet?.setImageDrawable(ContextCompat.getDrawable(this@BrowserActivity, R.drawable.ic_not_favorite))
        }*/



        ivFavorite.setOnClickListener {

            // TODO :- This model is used for if using the allproduct collection then the uncomment code and comment product collection
            /*val hashProductGetIndex = productList[0].product_urls!!.indexOf(hashUrl)

            val requireModel = FavoritePurchasedCustomListModel(
                id_product = productData?.id_product,
                url = productData?.seller_list?.get(hashProductGetIndex)?.direct_link,
                url_hash = productData?.product_urls?.get(0)
            )*/

            // TODO :- This model is used for if using the old product collection then the uncomment code and comment all product collection
            val requireModel = FavoritePurchasedCustomListModel(
                id_product = productData?.id_product,
                url_hash = productData?.product_urls?.get(0)
            )
            if(productData?.isFavorite!!){
                removeFavoriteProduct(requireModel, ivFavorite)
            }else{
                addFavoriteProduct(requireModel)
            }
        }

        ll25Per?.setOnClickListener {
            iv25Per?.visibility = VISIBLE
            iv50Per?.visibility = GONE
            notificationDiscount = 25
        }

        ll50Per?.setOnClickListener {
            iv25Per?.visibility = GONE
            iv50Per?.visibility = VISIBLE
            notificationDiscount = 50
        }

        cvCreateList?.setOnClickListener {
            openCreateNewProductListBottomSheet(rvList, tvNoListAvailable)
        }


        ivMarkFullFilled?.setOnClickListener{
            if (!isPurchased){
                ivMarkFullFilled.setImageDrawable(ContextCompat.getDrawable(this@BrowserActivity, R.drawable.ic_success_feedback))
                isPurchased = true
            }else{
                ivMarkFullFilled.setImageDrawable(ContextCompat.getDrawable(this@BrowserActivity, R.drawable.ic_right_rounded_cut))
                isPurchased = false
            }
        }

        llUpdate?.setOnClickListener {
            Log.e(TAG, "editSaveItemBottomSheet: purchased $isPurchased and isListItemSelected $isListItemSelected" )

            val hashUrl = ConvertUrlToSHA().getConvertedSHA(webView?.url!!)
            val hashProductGetIndex = productList[0].product_urls!!.indexOf(hashUrl)

            if(isListItemSelected){
                // TODO :- If addPurchased product in add using product collection unComment and comment allproduct collection
                addPurchasedProduct(
                    isPurchased,
                    editItemSaveBottomSheet,
                    FavoritePurchasedCustomListModel(
                        id_product = productData?.id_product,
                        url_hash = productData?.product_urls?.get(0),
                        mute_notification = switchNotification?.isChecked,
                        notification_min_discount =  notificationDiscount
                    )
                )

                // TODO :- If addPurchased product in add using allproduct collection unComment and comment product collection
                /*addPurchasedProduct(
                    isPurchased,
                    editItemSaveBottomSheet,
                    FavoritePurchasedCustomListModel(
                        id_product = productData?.id_product,
                        url = productData?.seller_list?.get(hashProductGetIndex)?.direct_link,
                        url_hash = productData?.product_urls?.get(0),
                        mute_notification = switchNotification?.isChecked,
                        notification_min_discount =  notificationDiscount
                    )
                )*/
            }else{
                Toast.makeText(editItemSaveBottomSheet.context, "Please select anyone list..", Toast.LENGTH_SHORT).show()
            }
        }

        ivClose?.setOnClickListener {
            editItemSaveBottomSheet.dismiss()
        }

        editItemSaveBottomSheet.show()
    }

    private fun addPurchasedProduct(isPurchased: Boolean, bottomSheet: BottomSheetDialog, productModel: FavoritePurchasedCustomListModel) {

        if(isPurchased) {
            FireStoreHelper().addPurchasedProduct(
                auth.currentUser!!.uid,
                productModel,
                object : FireStoreAddPurchasedProductInterface {
                    override fun onSuccess(msg: String) {
                        bottomSheet.dismiss()
                        Toast.makeText(
                            this@BrowserActivity,
                            "Added successfully..",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    override fun onError(error: String) {
                        Log.e(TAG, "Error adding document $error")
                        Toast.makeText(
                            this@BrowserActivity,
                            "Something went wrong to added product",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            )
        }

        /*var isProductAdding = true
        selectedListProducts.forEachIndexed{ index, model ->
            if(model.id_product == productModel.id_product){
                if(model != productModel){
                    selectedListProducts[index] = productModel
                }
                isProductAdding = false
                return@forEachIndexed
            }
        }

        if(isProductAdding){
            selectedListProducts.add(productModel)
        }

        if(!selectedListId.isNullOrBlank()) {
            val requireModel = ListCollectionModel(
                productsList = selectedListProducts,
                listId = selectedListId!!,
            )

            val addListInProduct: MutableMap<String, Any> = mutableMapOf()

            addListInProduct[FireStorageKeys.KEYUT_PRODUCT_LIST] = selectedListProducts

            FireStoreHelper().addListProduct(
                auth.currentUser!!.uid,
                requireModel,
                addListInProduct,
                object : FireStoreAddListProductInterface {
                    override fun onSuccess(msg: String) {
                        if(bottomSheet.isShowing){
                            bottomSheet.dismiss()
                            Toast.makeText(
                                this@BrowserActivity,
                                "product added successfully into list",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                    override fun onError(error: String) {
                        Toast.makeText(this@BrowserActivity, "Something went wrong", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            )
        }*/
    }
    

    private fun getList(recyclerView: RecyclerView, noDataFound: TextView) {

        FireStoreHelper().getListProductFilter(
            auth.currentUser!!.uid,
            object: FireStoreGetFilterListListener {
                override fun onSuccess(productFilterList: QuerySnapshot) {
                    filterList.clear()
                    for (i in productFilterList) {
                        val jsonData = Utils.convertToJson(i.data)
                        val gsn = Gson()
                        val requireModel: ListCollectionModel = gsn.fromJson(
                            jsonData.toString(),
                            ListCollectionModel::class.java
                        )
                        filterList.add(requireModel)
                    }
                    if(filterList.isNotEmpty()){
                        recyclerView.visibility = VISIBLE
                        noDataFound.visibility = GONE
                    }else{
                        recyclerView.visibility = GONE
                        noDataFound.visibility = VISIBLE
                    }
                    createListBrowserAdapter.updateList(filterList)
                }

                override fun onError(error: String) {
                    
                    Toast.makeText(this@BrowserActivity, "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    private fun addFavoriteProduct(productModel: FavoritePurchasedCustomListModel, favoriteImageView: ImageView? = null) {

        FireStoreHelper().addFavoriteProduct(
            auth.currentUser!!.uid,
            productModel,
            object : FireStoreAddFavoriteProductInterface {
                override fun onSuccess(msg: String) {
                    productData?.isFavorite = true

                    if(::ivFavorite.isInitialized){
                        ivFavorite.setImageDrawable(ContextCompat.getDrawable(this@BrowserActivity, R.drawable.ic_favorite_fill))
                    }
                    favoriteImageView?.let {
                        favoriteImageView.setImageDrawable(ContextCompat.getDrawable(this@BrowserActivity, R.drawable.ic_not_favorite))
                    }
                }
                override fun onError(error: String) {
                    Log.e(TAG, "Error adding document $error")
                    productData?.isFavorite = false

                    if(::ivFavorite.isInitialized){
                        ivFavorite.setImageDrawable(ContextCompat.getDrawable(this@BrowserActivity, R.drawable.ic_not_favorite))
                    }
                }
            }
        )
    }

    private fun removeFavoriteProduct(productModel: FavoritePurchasedCustomListModel, favoriteImageView: ImageView? = null) {
        FireStoreHelper().removeFavoriteProduct(
            auth.currentUser!!.uid,
            productModel,
            object : FireStoreRemoveFavoriteProductInterface {
                override fun onSuccess(msg: String) {
                    productData?.isFavorite = false

                    favoriteImageView?.let {
                        favoriteImageView.setImageDrawable(ContextCompat.getDrawable(this@BrowserActivity, R.drawable.ic_not_favorite))
                    }
                }
                override fun onError(error: String) {
                    Log.e(TAG, "Error unFavorite Product $error")
                }
            }
        )
    }
    


    private fun openCreateNewProductListBottomSheet(rvList: RecyclerView, tvNoListAvailable: TextView) {
        createNewProductListBottomSheet = BottomSheetDialog(this@BrowserActivity, R.style.AppBottomSheetDialogTheme)
        createNewProductListBottomSheet.setContentView(R.layout.create_product_list_bottomsheet_layout)
        val etListName = createNewProductListBottomSheet.findViewById<EditText>(R.id.etListName)
        val etListDis = createNewProductListBottomSheet.findViewById<EditText>(R.id.etListDis)
        val llUpdate = createNewProductListBottomSheet.findViewById<LinearLayout>(R.id.llUpdate)
        val ivClose = createNewProductListBottomSheet.findViewById<ImageView>(R.id.ivClose)
        createNewProductListBottomSheet.behavior.state = BottomSheetBehavior.STATE_EXPANDED

        llUpdate?.setOnClickListener {
            if(etListName!!.text.isNotBlank()){
                val requireModel = ListCollectionModel(
                    listName = etListName.text.toString(),
                    description = etListDis?.text.toString()
                )
                addListProduct(rvList, tvNoListAvailable, requireModel)
            }else{
                Toast.makeText(createNewProductListBottomSheet.context, "Please Enter Create List Name..", Toast.LENGTH_SHORT).show()
            }
        }

        ivClose?.setOnClickListener {
            createNewProductListBottomSheet.dismiss()
        }

        createNewProductListBottomSheet.show()
    }

    
    private fun addListProduct(rvList: RecyclerView, tvNoListAvailable: TextView, requireModel: ListCollectionModel) {
        FireStoreHelper().addListCollection(
            auth.currentUser!!.uid,
            requireModel,
            object : FireStoreAddListInterface {
                override fun onSuccess(msg: String) {
                    Toast.makeText(this@BrowserActivity, msg, Toast.LENGTH_SHORT).show()
                    createNewProductListBottomSheet.dismiss()
                    filterList.add(requireModel)
                    if(filterList.isNotEmpty()){
                        rvList.visibility = VISIBLE
                        tvNoListAvailable.visibility = GONE
                    }else{
                        rvList.visibility = GONE
                        tvNoListAvailable.visibility = VISIBLE
                    }
                    createListBrowserAdapter.updateList(filterList)
                }
                override fun onError(error: String) {
                    Toast.makeText(this@BrowserActivity, "Something went wrong", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }



    private fun openStoreBottomSheet() {
        storeProductsBottomSheetDialog = BottomSheetDialog(this, R.style.AppBottomSheetDialogTheme)
        storeProductsBottomSheetDialog.setContentView(R.layout.store_bottomsheet_layout)

        //New added
        rvFavoriteStores = storeProductsBottomSheetDialog.findViewById<RecyclerView>(R.id.rvFavoriteStores)
        val rvHorizontalDetails = storeProductsBottomSheetDialog.findViewById<RecyclerView>(R.id.rvHorizontalDetails)
        val cvCouponsNotFound = storeProductsBottomSheetDialog.findViewById<CardView>(R.id.cvNoCouponsRoot)
        val imgProfile = storeProductsBottomSheetDialog.findViewById<ImageView>(R.id.ivProfile)
        val tvTitle = storeProductsBottomSheetDialog.findViewById<TextView>(R.id.tvTitle)



        val rvStoreProducts = storeProductsBottomSheetDialog.findViewById<RecyclerView>(R.id.rvStoreProducts)
        rvAllStores = storeProductsBottomSheetDialog.findViewById<FastScrollRecyclerView>(R.id.rvAllStores)

        val llFavStores = storeProductsBottomSheetDialog.findViewById<LinearLayout>(R.id.llFavStores)
        val llAllStores = storeProductsBottomSheetDialog.findViewById<LinearLayout>(R.id.llAllStores)!!
        val etSearchStores = storeProductsBottomSheetDialog.findViewById<EditText>(R.id.etSearch)!!
        val ivClose: ImageView = storeProductsBottomSheetDialog.findViewById<ImageView>(R.id.ivClose)!!
        val imgAdd: ImageView = storeProductsBottomSheetDialog.findViewById<ImageView>(R.id.imgAdd)!!


        val cstFavStores: ConstraintLayout = storeProductsBottomSheetDialog.findViewById<ConstraintLayout>(R.id.cstFavStores)!!
        val cstAllStores: ConstraintLayout = storeProductsBottomSheetDialog.findViewById<ConstraintLayout>(R.id.cstAllStores)!!

        val btnSaveStore: AppCompatButton = storeProductsBottomSheetDialog.findViewById<AppCompatButton>(R.id.btnSaveStore)!!



        val llEmpty = storeProductsBottomSheetDialog.findViewById<LinearLayout>(R.id.llEmpty)!!
        val tvCountCoupon: TextView = storeProductsBottomSheetDialog.findViewById<TextView>(R.id.tvCountCoupon)!!
        val tvNoSimilarProduct: TextView = storeProductsBottomSheetDialog.findViewById<TextView>(R.id.tvNoSimilarProduct)!!
        val tvCountCouponSim: TextView = storeProductsBottomSheetDialog.findViewById<TextView>(R.id.tvCountCouponSim)!!
        val imgSimilarProduct: ImageView = storeProductsBottomSheetDialog.findViewById<ImageView>(R.id.imgSimilarProduct)!!


        llFavStores!!.backgroundTintList = ContextCompat.getColorStateList(this, android.R.color.white)
        llAllStores.backgroundTintList = ContextCompat.getColorStateList(this, android.R.color.transparent)

        //recyclerview scroll inside nested scrollview
        rvAllStores!!.setOnTouchListener(OnTouchListener { v, event ->
            val action = event.action
            when (action) {
                MotionEvent.ACTION_DOWN -> {
                    // Disallow NestedScrollView to intercept touch events.
                    Log.d("MotionEvent","ACTION_DOWN")
                    v.parent.requestDisallowInterceptTouchEvent(true)
                }
                MotionEvent.ACTION_UP -> {
                    // Allow NestedScrollView to intercept touch events.
                    Log.d("MotionEvent","ACTION_UP")
                    v.parent.requestDisallowInterceptTouchEvent(false)
                }

            }
            // Handle RecyclerView touch
            v.onTouchEvent(event)
            true
        })


        ivClose.setOnClickListener {
            storeProductsBottomSheetDialog.dismiss()
        }

        //Search filter edittext
        etSearchStores.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                filter(s.toString());
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        btnSaveStore.setOnClickListener{

            if(!allStores.isNullOrEmpty()){
                val selectedStores: ArrayList<AllStoreModel> = ArrayList()

                for (i in 0 until allStores.size) {
                    if(allStores.get(i).checked == true) {
                        selectedStores.add(allStores.get(i))
                    }
                }

                if (!selectedStores.isNullOrEmpty()){
                    Log.d("SEL_STORE", GsonBuilder().setPrettyPrinting().create().toJson(selectedStores))

                    addToFavoriteFirebase(selectedStores)


                }else{
                    Toast.makeText(this,"Please select at least one store",Toast.LENGTH_SHORT).show()
                }

            }

        }








        //Bottom Categories adapter
        val adapter = StoreProductAdapter(this)
        rvStoreProducts!!.adapter = adapter
//        rvStoreProducts.layoutManager = GridLayoutManager(this, 2)
        rvStoreProducts.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        //Horizontal Recyclerview Detail adapter
        horizontalProductDetailAdapter = HorizontalProductDetailAdapter(
            this,
            selectedStorefinalList,
            selectedStoresCouponsAllModel?.logoUrl,
            object: HorizontalProductDetailAdapter.SelectedStoreCouponsClickListener{
                override fun couponRootClick(
                    data: RoModel.CouponModel,
                    position: Int,
                    logoUrl: String
                ) {
                    Log.e(TAG, "couponRootClick: $data")
                    couponBottomSheet(data, position, logoUrl)
                }

                override fun onCouponClick(view: View, position: Int) {
                    val codeCopy = selectedStorefinalList[position].code
                    val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
                    val clip = ClipData.newPlainText("Coupon", codeCopy)
                    clipboard.setPrimaryClip(clip)

//                    view.mySnack("Coupon Copied"){}


                    val snackBar = showSnackBar(
                        this@BrowserActivity,
                        cvParentLayout!!,
                        Snackbar.LENGTH_SHORT,
                        "Coupon Copied",
                        selectedStorefinalList[position].offerUrl ?: "url not available",
                        couponsAllModel?.logoUrl!!
                    )
                    snackBar.show()

                    offersDBHelper.insertOffer(
                        CopiedCouponsModel(
                            selectedStorefinalList[position].idOffer.toString(),
                            selectedStorefinalList[position].code.toString(),
                            selectedStorefinalList[position].offerUrl ?: "",
                            System.currentTimeMillis().toString()
                        )
                    )
                }

            }
        )
        rvHorizontalDetails!!.adapter = horizontalProductDetailAdapter
        rvHorizontalDetails.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        //FAVORITE STORES Recyclerview adapter
        favoriteStoresAdapter = FavoriteStoresAdapter(
            this,
            favoriteStores,
            // for only favorites store selection purpose click to show it company coupons
            object: FavoriteStoresAdapter.StoresCouponsListener {
                override fun favoriteStoreClick(data: FavoriteStore) {
                    FireStoreHelper().getDataListTemp(data.storeName!!, object : FireStoreListener {
                        override fun onComplete(response: String) {
                            selectedStoresCouponsAllModel = Gson().fromJson(
                                response,
                                CouponsAllModel::class.java
                            )
                            if (selectedStoresCouponsAllModel != null) {
                                showCouponsRecycleView(rvHorizontalDetails, cvCouponsNotFound!!, imgProfile!!, tvTitle!!, true, data.logoUrl!!, data.storeName!!)

                                countryList.clear()
                                for (i in 0 until selectedStoresCouponsAllModel?.countries?.size!!) {

                                    val countryName = (selectedStoresCouponsAllModel!!.countries?.get(i).toString())
                                    if (i == 0) {
                                        countryList.add(
                                            CountryCouponsModel(
                                                countryName, selectedStoresCouponsAllModel!!.countries?.get(i).toString(), true
                                            )
                                        )
                                        selectedCountry = selectedStoresCouponsAllModel!!.countries?.get(i).toString()
                                        val couponsListDD = selectedStoresCouponsAllModel?.coupons?.get(selectedCountry)
                                        addSelectedStoreRoModel(couponsListDD, isSimilarCoupons, countryName)
                                        setSelectedStoreCoponsAdapter(
                                            selectedRoModel?.couponList!!,
                                        )
                                    } else {
                                        countryList.add(
                                            CountryCouponsModel(
                                                countryName, couponsAllModel!!.countries?.get(i).toString(), false
                                            )
                                        )
                                    }
                                }
                            } else {
                                showCouponsRecycleView(rvHorizontalDetails, cvCouponsNotFound!!, imgProfile!!, tvTitle!!, false, data.logoUrl!!, data.storeName!!)
                            }
                        }

                        override fun onError(error: String) {
                            FireStoreServiceCall.responseError.postValue(error)
                            showCouponsRecycleView(rvHorizontalDetails, cvCouponsNotFound!!, imgProfile!!, tvTitle!!, false, data.logoUrl!!, data.storeName!!)
                        }
                    }
                    )
                }

            }
        )


        rvFavoriteStores!!.adapter = favoriteStoresAdapter
        rvFavoriteStores!!.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        Log.d("allStores",""+ GsonBuilder().setPrettyPrinting().create().toJson(allStores))





        llFavStores.setOnClickListener {
            getFavoriteStores()

            cstFavStores.visibility = View.VISIBLE
            cstAllStores.visibility = View.GONE
            btnSaveStore.visibility = View.GONE

            llFavStores.backgroundTintList = ContextCompat.getColorStateList(this, android.R.color.white)
            llAllStores.backgroundTintList = ContextCompat.getColorStateList(this, android.R.color.transparent)

        }

        llAllStores.setOnClickListener {
            getAllStores()

            cstFavStores.visibility = View.GONE
            cstAllStores.visibility = View.VISIBLE
            btnSaveStore.visibility = View.VISIBLE

            llFavStores.backgroundTintList = ContextCompat.getColorStateList(this, android.R.color.transparent)
            llAllStores.backgroundTintList = ContextCompat.getColorStateList(this, android.R.color.white)
        }

        imgAdd.setOnClickListener{
            llAllStores.performClick()
        }















        /*BOTTOMSHEET*/
        storeProductsBottomSheetDialog.show()
        rvStoreProducts!!.setOnTouchListener(OnTouchListener { v, event ->
            val action = event.action
            when (action) {
                MotionEvent.ACTION_DOWN ->                         // Disallow NestedScrollView to intercept touch events.
                    v.parent.requestDisallowInterceptTouchEvent(true)
                MotionEvent.ACTION_UP ->                         // Allow NestedScrollView to intercept touch events.
                    v.parent.requestDisallowInterceptTouchEvent(false)
            }

            // Handle RecyclerView touch events.
            v.onTouchEvent(event)
            true

        })

        val bottom_dialog = storeProductsBottomSheetDialog as BottomSheetDialog
        val bottomSheet = (bottom_dialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?)!!
        val displayMetrics: DisplayMetrics = resources.displayMetrics
        val height = displayMetrics.heightPixels
        val maxHeight = (height * 0.80).toInt()
        BottomSheetBehavior.from(bottomSheet).peekHeight = maxHeight
        BottomSheetBehavior.from(bottomSheet).maxHeight = maxHeight
        storeProductsBottomSheetDialog.show()

        storeProductsBottomSheetDialog.behavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    (storeProductsBottomSheetDialog.behavior).isDraggable = true
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                if (slideOffset < -0.05) {
                    (storeProductsBottomSheetDialog.behavior).state =
                        BottomSheetBehavior.STATE_HIDDEN
                    (storeProductsBottomSheetDialog.behavior).isDraggable = false
                }
            }
        })

        /*BOTTOMSHEET*/


    }

    // Coupons Bottomsheet
    private fun couponBottomSheet(data: RoModel.CouponModel, position: Int, logoUrl: String) {
        couponBottomSheet = BottomSheetDialog(this@BrowserActivity, R.style.AppBottomSheetDialogTheme)
        couponBottomSheet.setContentView(R.layout.bottomsheet_coupon)
        val ivClose = couponBottomSheet.findViewById<ImageView>(R.id.ivClose)
        val ivProfile = couponBottomSheet.findViewById<ImageView>(R.id.ivProfile)
        val tvTitle = couponBottomSheet.findViewById<TextView>(R.id.tvTitle)
        val tvDescription = couponBottomSheet.findViewById<TextView>(R.id.tvDescription)
        val btnCopyCoupon = couponBottomSheet.findViewById<AppCompatButton>(R.id.btnCopyCoupon)

        ivProfile?.let {
            Glide.with(couponBottomSheet.context).load(logoUrl!!).into(ivProfile)
        }

        tvTitle?.text = data.offerUrl
        tvDescription?.text = data.offerName

        ivClose?.setOnClickListener {
            couponBottomSheet.dismiss()
        }

        btnCopyCoupon?.setOnClickListener {
            val codeCopy = selectedStorefinalList[position].code
            val clipboard = getSystemService(CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("Coupon", codeCopy)
            clipboard.setPrimaryClip(clip)

//                    view.mySnack("Coupon Copied"){}


            val snackBar = showSnackBar(
                this@BrowserActivity,
                cvParentLayout!!,
                Snackbar.LENGTH_SHORT,
                "Coupon Copied",
                selectedStorefinalList[position].offerUrl ?: "url not available",
                couponsAllModel?.logoUrl!!
            )
            snackBar.show()

            offersDBHelper.insertOffer(
                CopiedCouponsModel(
                    selectedStorefinalList[position].idOffer.toString(),
                    selectedStorefinalList[position].code.toString(),
                    selectedStorefinalList[position].offerUrl ?: "",
                    System.currentTimeMillis().toString()
                )
            )
        }

        couponBottomSheet.show()
    }

    fun showCouponsRecycleView(rvHorizontalDetails: RecyclerView, cvCouponsNotFound: CardView, ivProfile: ImageView, titleTextView: TextView, isShowCouponsList: Boolean, logoUrl: String, title: String){
        if(isShowCouponsList){
            rvHorizontalDetails.visibility = VISIBLE
            cvCouponsNotFound.visibility = GONE
        }else{
            cvCouponsNotFound.visibility = VISIBLE
            rvHorizontalDetails.visibility = GONE
            Glide.with(this).load(logoUrl).into(ivProfile)
            titleTextView.text = title
        }

    }

    // for only favorites store selection purpose
    private fun addSelectedStoreRoModel(couponsListDD: Any?, isSimilarCoupan: Boolean, countryName: String, isSelectedStore: Boolean = false) {

        selectedRoModel = if (couponsListDD != null) {
            val gson = Gson()
            val toJson = gson.toJson(couponsListDD)
            gson.fromJson(toJson, RoModel::class.java)

        } else {
            null
        }

        if (selectedRoModel != null) {
            val roModelBackupModel = RoModelBackupModel()
            roModelBackupModel.data = roModel
            roModelBackupModel.countryName = countryName
            roModelBackupModel.isSimilarCoupon = isSimilarCoupan
            selectedStoreRoBackUpObjectsList.add(roModelBackupModel)
        }

    }

    // for only favorites store selection purpose
    fun setSelectedStoreCoponsAdapter(
        couponList: ArrayList<RoModel.CouponModel?>,
    ){
        @Suppress("UNCHECKED_CAST") val list = couponList as ArrayList<RoModel.CouponModel>
        selectedStorefinalList.clear()
        selectedStorefinalList.addAll(list)
        Log.e(TAG, "setCouponRecycler selectedStorefinalList: $selectedStorefinalList", )
        horizontalProductDetailAdapter.setNewListAndRefresh(selectedStorefinalList, selectedStoresCouponsAllModel?.logoUrl!!)
    }

    private fun filter(text: String) {
        val filteredArray: ArrayList<AllStoreModel> = ArrayList()
        for (i in 0 until allStores.size) {
            if(allStores.get(i).storeName.toString().toLowerCase(Locale.getDefault())
                    .contains(text.lowercase(Locale.getDefault()))){
                filteredArray.add(allStores.get(i))
            }
        }

        //calling a method of the adapter class and passing the filtered list
        allStoresAdapter.filterList(filteredArray)
    }

    fun addToFavoriteFirebase(selectedStores: ArrayList<AllStoreModel>) {

        Log.d("C_ID",""+auth.currentUser!!.uid)
        val addUserToArrayMap: MutableMap<String, Any> = HashMap()

        // That used to update (remove) not working for the favorite list
        /*for(i in 0 until selectedStores.size){
            addUserToArrayMap[FireStorageKeys.KEYUT_FAVORITE_STORES] = FieldValue.arrayUnion(
                selectedStores.get(i)
            )

            FireStoreHelper.db?.collection(FireStorageKeys.KEY_USER_TABLE)!!.document(auth.currentUser!!.uid)
                .update(addUserToArrayMap)

        }*/

        // These changes used the add a new store to the favorite list and also remove the favorite stores.
        addUserToArrayMap[FireStorageKeys.KEYUT_FAVORITE_STORES] = selectedStores
        FireStoreHelper.db?.collection(FireStorageKeys.KEY_USER_TABLE)!!
            .document(auth.currentUser!!.uid)
            .update(addUserToArrayMap).addOnSuccessListener {
                Toast.makeText(context,"Added to Favorite",Toast.LENGTH_SHORT).show()
                getFavoriteStores()
            }

//        Toast.makeText(context,"Added to Favorite",Toast.LENGTH_SHORT).show()

    }

    fun getAllStores(){
        //GET DATA FROM FIREBASE
        allStores.clear()
        FireStoreHelper.db?.collection(FireStoreHelper.PARENT_STORE_KEY)
            ?.get()
            ?.addOnSuccessListener { firstResult ->
                try {

                    for (document in firstResult) {
                        Log.d("ALL_ST",""+document.id)

                        val allStoreModel = AllStoreModel(
                            document.id,
                            document.get("cashback").toString(),
                            false,
                            document.get("logo_url").toString()
                        )



                        allStores.add(allStoreModel); // All stores names eg. admin.ro, airbnb.com etc...


                        // filter list of remove already favorite list from all store listing.
                        filterAllStores.clear()
                        for (i in allStores) {
                            for (j in favoriteStores) {
                                if (i.storeName == j.storeName) {
                                    filterAllStores.add(i)
                                }
                            }
                        }

                        // remove from all store in already to added
                        /*for(i in filterAllStores){
                            allStores.remove(i)
                        }*/

                        // Checked the already favorite store in all stores.
                        for(i in filterAllStores){
                            if(i in allStores){
                                allStores[allStores.indexOf(i)].checked = true
                            }
                        }


                        //ALL STORES adapter
                        allStoresAdapter = AllStoreAdapter(this,allStores,this)
                        rvAllStores!!.adapter = allStoresAdapter
                        rvAllStores!!.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)


                    }
                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }?.addOnFailureListener {
                Log.d("onFailed",""+it.message.toString())
            }
    }

    fun getFavoriteStores(){
        //GET DATA FROM FIREBASE


        FireStoreHelper.db?.collection(FireStorageKeys.KEY_USER_TABLE)!!.document(auth.currentUser!!.uid)
            ?.get()
            ?.addOnSuccessListener { firstResult ->
                try {

                    val jsonData = convertToJson(firstResult.data)
                    val gsn = Gson()
                    val userModel: UserModel = gsn.fromJson<UserModel>(
                        jsonData.toString(),
                        UserModel::class.java
                    )

                    if(!favoriteStores.isNullOrEmpty()){
                        favoriteStores.clear()
                        favoriteStoresAdapter.notifyDataSetChanged()
                    }



                    favoriteStores.addAll(userModel.favorite_stores!!)
                    favoriteStoresAdapter.notifyDataSetChanged()

                    Log.d("FAV_STORES",""+ GsonBuilder().setPrettyPrinting().create().toJson(userModel.favorite_stores))




                }catch (ex:Exception){
                    ex.printStackTrace()
                }
            }?.addOnFailureListener {
                Log.d("onFailed",""+it.message.toString())
            }
    }

    private fun convertToJson(mapData: Map<String, Any>?): String? {
        val gsn = Gson()
        return gsn.toJson(mapData)
    }

    override fun onCheckBoxClicked() {
        Log.d("CHECK_BOX",""+ GsonBuilder().setPrettyPrinting().create().toJson(allStores))
    }






}
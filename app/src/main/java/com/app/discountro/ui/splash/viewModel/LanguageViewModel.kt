package com.app.discountro.ui.splash.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.app.discountro.base.BaseViewModel
import com.app.discountro.ui.splash.model.LanguageModel
import com.app.discountro.ui.splash.model.SplashModel
import com.app.discountro.utils.KeysUtils
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.ktx.Firebase


class LanguageViewModel : BaseViewModel() {

    val languageModel: MutableLiveData<LanguageModel> = MutableLiveData()
    val splashModel: MutableLiveData<SplashModel> = MutableLiveData()
    private val db = Firebase.firestore

    fun getFireLanguage() {
        db.collection("language").get().addOnSuccessListener { result ->
            for (document in result) {
                languageModel.value = document.toObject()
                Log.e("MyDataValue", "getFireLanguage: ${languageModel.value}")
            }
        }.addOnFailureListener { exception ->
            Log.e("errGetFireLanguage", "Error getting documents.", exception)
        }
    }

    fun getFireSplashData(langISO: String) {
        db.collection(KeysUtils.KEYFIRELANGUAGE_Collection).document(langISO)
            .collection(KeysUtils.KEYFIRESPLASH_Collection).get().addOnSuccessListener {
                for (doc in it) {
                    splashModel.value = doc.toObject()
                    Log.e("MySplashDataValue", ":: ${doc.id} :: ${splashModel.value?.title}")
                }
            }.addOnFailureListener {
                Log.e("errGetFireSplashData", "getFireSplashData: ", it)
            }
    }

}
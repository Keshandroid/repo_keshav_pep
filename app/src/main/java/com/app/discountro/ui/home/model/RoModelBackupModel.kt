package com.app.discountro.ui.home.model

import com.app.discountro.fireStore.models.RoModel

class RoModelBackupModel
{
    var data : RoModel ?= null
    var countryName = ""
    var isSimilarCoupon = false
}
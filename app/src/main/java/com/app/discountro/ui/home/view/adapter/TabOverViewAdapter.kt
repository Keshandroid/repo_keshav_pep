package com.app.discountro.ui.home.view.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.recyclerview.widget.RecyclerView
import com.app.discountro.R
import com.app.discountro.databinding.AlbumBinding
import com.app.discountro.ui.home.model.TabsModel
import com.app.discountro.ui.home.view.BrowserActivity
import com.app.discountro.utils.browser_utils.my_object.BrowserContainer

class TabOverViewAdapter(val mContext: Context, var tabsDetailsArr: MutableList<TabsModel>?) :
    RecyclerView.Adapter<TabOverViewAdapter.ViewHolder>() {
    val TAG = javaClass.simpleName
//    private var viewItemInterface: RecyclerViewItemInterface? = null

    class ViewHolder(val mBinding: AlbumBinding) : RecyclerView.ViewHolder(mBinding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(AlbumBinding.inflate(LayoutInflater.from(mContext), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.mBinding.albumTitle.text = tabsDetailsArr?.get(position)?.albumTitle
        holder.mBinding.albumCover.setImageBitmap(tabsDetailsArr?.get(position)?.albumImg)
        val siteIcon = (tabsDetailsArr?.get(position)?.cAlbumCon as WebView).favicon
        if (siteIcon == null)
            holder.mBinding.ivAlbumSiteIcon.setImageDrawable(mContext.getDrawable(R.drawable.icon_earth_white))
        else
            holder.mBinding.ivAlbumSiteIcon.setImageBitmap(siteIcon)

        Log.e(TAG, "1onBindViewHolder: tabsDetailsArr.size:" + tabsDetailsArr!!.size)
        Log.e(TAG, "1onBindViewHolder: BrowserContainer.size:" + BrowserContainer.size())

//        if (position < BrowserContainer.size()) {
        holder.itemView.setOnClickListener {
            (mContext as BrowserActivity).showAlbum(tabsDetailsArr!![position].cAlbumCon, false)
            mContext.hideOverview()
        }

        holder.itemView.setOnLongClickListener {
            (mContext as BrowserActivity).removeAlbum(tabsDetailsArr!![position].cAlbumCon)
            true
        }

        holder.mBinding.albumClose.setOnClickListener { v: View? ->
            (mContext as BrowserActivity).removeAlbum(tabsDetailsArr!![position].cAlbumCon)
        }
//        }

        /*holder.itemView.setOnClickListener {
            if (viewItemInterface != null) {
                viewItemInterface!!.onItemClick(holder.adapterPosition, note)
            }
        }*/
    }

    /*fun setItemClickListener(viewItemInterface: RecyclerViewItemInterface?) {
        this.viewItemInterface = viewItemInterface
    }*/

    override fun getItemCount(): Int {
        return tabsDetailsArr?.size ?: 0
    }

    fun updateTabData(tabsDetailsArr: MutableList<TabsModel>) {
        this.tabsDetailsArr = tabsDetailsArr
    }

}
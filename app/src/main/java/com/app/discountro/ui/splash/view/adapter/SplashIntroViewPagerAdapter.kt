package com.app.discountro.ui.splash.view.adapter

import android.content.Context
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.app.discountro.R
import com.app.discountro.ui.splash.model.SplashIntroItemModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener


class SplashIntroViewPagerAdapter(
    var mContext: Context,
    var splashIntroArrayList: ArrayList<SplashIntroItemModel>
) : PagerAdapter() {

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val inflater = mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val layoutScreen: View = inflater.inflate(R.layout.splash_intro_items, null)

        val imgSlide: ImageView = layoutScreen.findViewById(R.id.ivImageUrl)
        val title = layoutScreen.findViewById<TextView>(R.id.tvTitle)
        val description = layoutScreen.findViewById<TextView>(R.id.tvDescription)
        val progress = layoutScreen.findViewById<LinearLayout>(R.id.progressBar)

        if (splashIntroArrayList[position].image_url.isEmpty()) {
            Glide.with(mContext)
                .load(R.drawable.intro_images)
                .fitCenter()
                .into(imgSlide)
        } else {
            progress.visibility = View.VISIBLE
            Glide.with(mContext)
                .load(splashIntroArrayList[position].image_url)
                .listener(object : RequestListener<Drawable?> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable?>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progress.visibility = View.GONE
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable?>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        progress.visibility = View.GONE
                        return false
                    }
                })
                .fitCenter()
                .error(R.drawable.ic_brocken_image)
                .fallback(R.drawable.ic_brocken_image)
                .into(imgSlide)

        }

        title.text = splashIntroArrayList[position].title
        description.text = splashIntroArrayList[position].description

        container.addView(layoutScreen)
        return layoutScreen
    }

    override fun getCount(): Int {
        return splashIntroArrayList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }
}
package com.app.discountro.fireStore.models

data class UserDataModel (
    var active: String,
    var date_registered: String,
    var birth_date: String,
    var email: String,
    var id_user: String,
    var password: String,
    var social_type: String,
    var user_name: String,
    var user_public_token: String,
    var gender: String,
    var phone_no: String,
    var country_code: String,
    var country_ph_code: String,
)
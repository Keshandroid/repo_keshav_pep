package com.app.discountro.fireStore.models
import com.google.gson.annotations.SerializedName
data class CouponsAllModel(
    @SerializedName("offers")
    val offers: HashMap<String,Any>? = null,
    @SerializedName("logo_url")
    val logoUrl: String? = "",
    @SerializedName("similar_coupons")
    var similarCoupons: HashMap<String,Any>? = null,
    @SerializedName("coupon_count")
    val couponCount: Int? = 0,
    @SerializedName("homepage_url")
    val homepageUrl: String? = "",
    @SerializedName("similar_coupons_count")
    val similarCouponsCount: Int? = 0,
    @SerializedName("free_shipping_above")
    val freeShippingAbove: Int? = 0,
    @SerializedName("countries")
    val countries: List<String?>? = listOf(),
    @SerializedName("id_store")
    val idStore: String? = "",
    @SerializedName("similar_offers")
    val similarOffers: HashMap<String,Any>? = null,
    @SerializedName("total_offers")
    val totalOffers: Int? = 0,
    @SerializedName("public_token")
    val publicToken: String? = "",
    @SerializedName("coupons")
    var coupons: HashMap<String,Any>? = null,
    @SerializedName("similar_offers_count")
    val similarOffersCount: Int? = 0,
    @SerializedName("domain")
    val domain: String? = "",
    @SerializedName("offers_count")
    val offersCount: Int? = 0,
    @SerializedName("sku_info")
    var sku_info: HashMap<String,Any>? = null
)

class SkuObject{
    var jquery_path = ""
    var remove_strings = ArrayList<String>()
}

data class RoModel(
    @SerializedName("country_code")
    val countryCode: String? = "",
    @SerializedName("country_name")
    val countryName: String? = "",
    @SerializedName("coupon_list")
    var couponList: ArrayList<CouponModel?>? = ArrayList()
) {
    data class CouponModel(
        @SerializedName("offer_url")
        var offerUrl: String? = "",
        @SerializedName("offer_text")
        var offerText: String? = "",
        @SerializedName("code")
        var code: String? = "",
        @SerializedName("id_offer")
        var idOffer: String? = "",
        @SerializedName("affiliate_url")
        var affiliateUrl: String? = "",
        @SerializedName("offer_details")
        var offerDetails: String? = "",
        @SerializedName("offer_name")
        var offerName: String? = "",
        @SerializedName("isCopied")
        var isCopied: Boolean? = false,
        @SerializedName("offer_type")
        var offerType: String? = "",
        @SerializedName("logo_url")
        var logo_url: String? = ""
    )
}
package com.app.discountro.fireStore.models

data class OverlayTextsDialogModel(
    val almost_done: String,
    val i_understand: String,
    val steps: String
)
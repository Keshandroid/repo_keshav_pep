package com.app.discountro.fireStore.models;

public class SellerListModelNew {
    private String condition;
    private String hours_updated_ago;
    private String price;
    private String store_logo;
    private String comment;
    private String currency;
    private String background_link;
    private String direct_link;
    private String status;

    public SellerListModelNew(){}

    public SellerListModelNew(String condition, String hours_updated_ago, String price, String store_logo, String comment, String currency, String background_link, String direct_link, String status) {
        this.condition = condition;
        this.hours_updated_ago = hours_updated_ago;
        this.price = price;
        this.store_logo = store_logo;
        this.comment = comment;
        this.currency = currency;
        this.background_link = background_link;
        this.direct_link = direct_link;
        this.status = status;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getHours_updated_ago() {
        return hours_updated_ago;
    }

    public void setHours_updated_ago(String hours_updated_ago) {
        this.hours_updated_ago = hours_updated_ago;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStore_logo() {
        return store_logo;
    }

    public void setStore_logo(String store_logo) {
        this.store_logo = store_logo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getBackground_link() {
        return background_link;
    }

    public void setBackground_link(String background_link) {
        this.background_link = background_link;
    }

    public String getDirect_link() {
        return direct_link;
    }

    public void setDirect_link(String direct_link) {
        this.direct_link = direct_link;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

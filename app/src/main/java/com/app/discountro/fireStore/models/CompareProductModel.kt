package com.app.discountro.fireStore.models
data class CompareProductModel(
    var listOfSeller: ArrayList<SellerModel>,
    var max_price: String,
    var id_product: String,
    var min_price: String,
    var similar_count: Int,
    var listOfSimilarProducts: ArrayList<SimilarProductModel>,
    var currency: String,
    var seller_count: Int,
    var skuList: ArrayList<String>,
    var product_urls: ArrayList<String>,
    var product_name: String,
    var sku_source: ArrayList<String>
)
data class SellerModel(
    val condition: String,
    val hours_updated_ago: String,
    val price: String,
    val store_logo: String,
    val comment: String,
    val currency: String,
    val background_link: Boolean,
    val direct_link: String,
    val status: String,
    val timestamp_added: String,
    val timestamp_updated: String

)

data class SimilarProductModel(
    val background_link: Boolean,
    val brand: String,
    val comment: String,
    val condition: String,
    val currency: String,
    val direct_link: String,
    val price: String,
    val product_logo: String,
    val status: Int,
    val title: String
)

//data class CompareProductModel(
//    val currency: String,
//    val max_price: String,
//    val min_price: String,
//    val product_name: String,
//    val product_urls: List<String>,
//    val seller_count: Int,
//    val seller_list: HashMap<String,SellerModel>,
//    val similar_count: Int,
//    val similar_products: HashMap<String,SimilarProductModel>,
//    val sku: List<String>
//)
//
//data class SellerModel(
//    val background_link: Boolean,
//    val comment: String,
//    val condition: String,
//    val currency: String,
//    val direct_link: String,
//    val hours_updated_ago: Int,
//    val price: String,
//    val status: Int,
//    val store_logo: String
//)
//
//data class SimilarProductModel(
//    val background_link: Boolean,
//    val brand: String,
//    val comment: String,
//    val condition: String,
//    val currency: String,
//    val direct_link: String,
//    val price: String,
//    val product_logo: String,
//    val status: Int,
//    val title: String
//)

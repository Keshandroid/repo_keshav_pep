package com.app.discountro.fireStore.models

data class SellerListModel(
    private var condition: String?,
    private var hours_updated_ago: String?,
    private var price: String?,
    private var store_logo: String?,
    private var comment: String?,
    private var currency: String?,
    private var background_link: String?,
    private var direct_link: String?,
    private var status: String?
) {


    fun getCondition(): String? {
        return condition
    }

    fun setCondition(condition: String?) {
        this.condition = condition
    }

    fun getHours_updated_ago(): String? {
        return hours_updated_ago
    }

    fun setHours_updated_ago(hours_updated_ago: String?) {
        this.hours_updated_ago = hours_updated_ago
    }

    fun getPrice(): String? {
        return price
    }

    fun setPrice(price: String?) {
        this.price = price
    }

    fun getStore_logo(): String? {
        return store_logo
    }

    fun setStore_logo(store_logo: String?) {
        this.store_logo = store_logo
    }

    fun getComment(): String? {
        return comment
    }

    fun setComment(comment: String?) {
        this.comment = comment
    }

    fun getCurrency(): String? {
        return currency
    }

    fun setCurrency(currency: String?) {
        this.currency = currency
    }

    fun getBackground_link(): String? {
        return background_link
    }

    fun setBackground_link(background_link: String?) {
        this.background_link = background_link
    }

    fun getDirect_link(): String? {
        return direct_link
    }

    fun setDirect_link(direct_link: String?) {
        this.direct_link = direct_link
    }

    fun getStatus(): String? {
        return status
    }

    fun setStatus(status: String?) {
        this.status = status
    }
}
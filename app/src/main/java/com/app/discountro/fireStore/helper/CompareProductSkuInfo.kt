package com.app.discountro.fireStore.helper

import android.util.Log
import com.app.discountro.fireStore.models.CouponsAllModel
import com.app.discountro.fireStore.models.SkuObject
import com.google.gson.Gson
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONObject
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import java.util.*

class CompareProductSkuInfo {
    companion object {
        const val TAG = "CompareProductSkuInfo"
    }

    fun getProductIdSku(
        couponsAllModel: CouponsAllModel,
        urlMain: String,
        compareProductInfoInt: CompareProductInfoInt
    ) {
        val skuObject = Gson().fromJson(
            (couponsAllModel.sku_info as Map<*, *>?)?.let { it1 -> JSONObject(it1).toString() },
            SkuObject::class.java
        )

        CompareProductSkuInfo().getProductId(
            urlMain,
            skuObject.jquery_path,
            skuObject.remove_strings,
            compareProductInfoInt
        )
    }


    private fun getProductId(
        url: String,
        className: String,
        removeStrings: ArrayList<String>,
        compareProductInfoInt: CompareProductInfoInt
    ) {
        try {
            try {
                CoroutineScope(Dispatchers.Default).launch {
                    val document: Document = Jsoup.connect(url).get()

                    var hit = document.select(className).text()
                    if (!hit.isNullOrEmpty()) {
                        for (item in removeStrings) {
                            hit = hit.replace(item, "")
                        }
                        hit = hit.trim().lowercase(Locale.getDefault())
                            .replace(("[^A-Za-z0-9]").toRegex(), "")
                        Log.e(TAG, "getProductId: $hit")

                    }
                    compareProductInfoInt.onComplete(hit)
                }
            } catch (e: Exception) {
                compareProductInfoInt.onFailed(e.message!!)
            }

        } catch (e: Exception) {
            compareProductInfoInt.onFailed(e.message!!)
        }
    }

}

interface CompareProductInfoInt {
    fun onComplete(productId: String)
    fun onFailed(string: String)
}
package com.app.discountro.fireStore.interfaces

import androidx.lifecycle.MutableLiveData

interface FireStoreListener {
    companion object{
        val response = MutableLiveData<FireStoreListener>()
    }
    fun onComplete(response: String)
    fun onError(error: String)
}

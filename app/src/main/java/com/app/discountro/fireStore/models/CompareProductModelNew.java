package com.app.discountro.fireStore.models;

import java.util.List;

public class CompareProductModelNew {
    private List<SellerListModel> listOfSeller;
    private String max_price;
    private String id_product;
    private String min_price;
    private int similar_count;
    private List<SimilarProductModelNew> listOfSimilarProducts;
    private String currency;
    private String seller_count;
    private List<String> skuList;
    private List<String> product_urls;
    private String product_name;
    private String sku_source;

    public CompareProductModelNew(){}

    public CompareProductModelNew(List<SellerListModel> listOfSeller, String max_price, String id_product, String min_price, int similar_count, List<SimilarProductModelNew> listOfSimilarProducts, String currency, String seller_count, List<String> skuList, List<String> product_urls, String product_name, String sku_source) {
        this.listOfSeller = listOfSeller;
        this.max_price = max_price;
        this.id_product = id_product;
        this.min_price = min_price;
        this.similar_count = similar_count;
        this.listOfSimilarProducts = listOfSimilarProducts;
        this.currency = currency;
        this.seller_count = seller_count;
        this.skuList = skuList;
        this.product_urls = product_urls;
        this.product_name = product_name;
        this.sku_source = sku_source;
    }

    public List<SellerListModel> getListOfSeller() {
        return listOfSeller;
    }

    public void setListOfSeller(List<SellerListModel> listOfSeller) {
        this.listOfSeller = listOfSeller;
    }

    public String getMax_price() {
        return max_price;
    }

    public void setMax_price(String max_price) {
        this.max_price = max_price;
    }

    public String getId_product() {
        return id_product;
    }

    public void setId_product(String id_product) {
        this.id_product = id_product;
    }

    public String getMin_price() {
        return min_price;
    }

    public void setMin_price(String min_price) {
        this.min_price = min_price;
    }

    public int getSimilar_count() {
        return similar_count;
    }

    public void setSimilar_count(int similar_count) {
        this.similar_count = similar_count;
    }

    public List<SimilarProductModelNew> getListOfSimilarProducts() {
        return listOfSimilarProducts;
    }

    public void setListOfSimilarProducts(List<SimilarProductModelNew> listOfSimilarProducts) {
        this.listOfSimilarProducts = listOfSimilarProducts;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSeller_count() {
        return seller_count;
    }

    public void setSeller_count(String seller_count) {
        this.seller_count = seller_count;
    }

    public List<String> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<String> skuList) {
        this.skuList = skuList;
    }

    public List<String> getProduct_urls() {
        return product_urls;
    }

    public void setProduct_urls(List<String> product_urls) {
        this.product_urls = product_urls;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getSku_source() {
        return sku_source;
    }

    public void setSku_source(String sku_source) {
        this.sku_source = sku_source;
    }
}

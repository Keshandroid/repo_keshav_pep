package com.app.discountro.fireStore.services

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.app.discountro.common.sqlite.offerCoupons.CouponCodesSearchedModel
import com.app.discountro.common.sqlite.offerCoupons.DBHelperOffer
import com.app.discountro.fireStore.helper.FireStoreHelper
import com.app.discountro.fireStore.interfaces.FireStoreListener
import com.app.discountro.fireStore.models.CouponsAllModel
import com.app.discountro.fireStore.models.OverlayTextsDialogModel
import com.google.gson.Gson

class FireStoreServiceCall {
    companion object {
        val responseData = MutableLiveData<CouponsAllModel>()
        val responseError = MutableLiveData<String>()

        var status = true
        fun getAllCoupons(queryOfWebsite: String, offersDBHelper: DBHelperOffer) {
            FireStoreHelper().getDataListTemp(queryOfWebsite, object : FireStoreListener {
                override fun onComplete(response: String) {
                    offersDBHelper.insertSearchedOffer(
                        CouponCodesSearchedModel(
                            System.currentTimeMillis().toString(),
                            response,
                            queryOfWebsite
                        )
                    )
                    val result = getAsSpecificModel(response, CouponsAllModel::class.java)
                    responseData.postValue(result)
                }

                override fun onError(error: String) {
                    responseError.postValue(error)
                }
            })
        }

        fun getTextAppearOnTop(selectedLang: String , overlayTextCallback: OverlayTextCallback) {
            FireStoreHelper().getPermissionOverlayTexts(selectedLang, object : FireStoreListener {
                override fun onComplete(response: String) {
                    val result = getAsSpecificModelOverlay(response, OverlayTextsDialogModel::class.java)
                    overlayTextCallback.onSuccessOverlay(result!!)
                }

                override fun onError(error: String) {
                    overlayTextCallback.onErrorOverlay(error)
                }
            })
        }

        private fun getAsSpecificModel(
            response: String,
            className: Class<CouponsAllModel>
        ): CouponsAllModel {
            val gsn = Gson()
            return gsn.fromJson(response, className)
        }

        private fun getAsSpecificModelOverlay(
            response: String,
            className: Class<OverlayTextsDialogModel>
        ): OverlayTextsDialogModel? {
            val gsn = Gson()
            return gsn.fromJson(response, className)
        }
    }
}


interface OverlayTextCallback{
    fun onSuccessOverlay(response: OverlayTextsDialogModel)
    fun onErrorOverlay(error : String)
}
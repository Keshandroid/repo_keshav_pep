package com.app.discountro.fireStore.models;

public class SimilarProductModelNew {
    private boolean background_link;
    private String brand;
    private String comment;
    private String condition;
    private String currency;
    private String direct_link;
    private String price;
    private String product_logo;
    private int status;
    private String title;
    public SimilarProductModelNew(){}


    public SimilarProductModelNew(boolean background_link, String brand, String comment, String condition, String currency, String direct_link, String price, String product_logo, int status, String title) {
        this.background_link = background_link;
        this.brand = brand;
        this.comment = comment;
        this.condition = condition;
        this.currency = currency;
        this.direct_link = direct_link;
        this.price = price;
        this.product_logo = product_logo;
        this.status = status;
        this.title = title;
    }

    public boolean isBackground_link() {
        return background_link;
    }

    public void setBackground_link(boolean background_link) {
        this.background_link = background_link;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDirect_link() {
        return direct_link;
    }

    public void setDirect_link(String direct_link) {
        this.direct_link = direct_link;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProduct_logo() {
        return product_logo;
    }

    public void setProduct_logo(String product_logo) {
        this.product_logo = product_logo;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}

package com.app.discountro.fireStore.helper
import android.annotation.SuppressLint
import android.util.Log
import com.app.discountro.Interface.*
import com.app.discountro.fireStore.interfaces.FireStoreListener
import com.app.discountro.fireStore.models.*
import com.app.discountro.ui.product.model.FavoritePurchasedCustomListModel
import com.app.discountro.ui.product.model.ListCollectionModel
import com.app.discountro.utils.FireStorageKeys
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import org.json.JSONObject

class FireStoreHelper {
    companion object {
        var db: FirebaseFirestore? = null
        const val TAG = "FireStoreHelper"
        const val PARENT_STORE_KEY = "store"
        const val PARENT_STORE_KEY_LANGUAGE = "language"
        const val PARENT_PRODUCT_STORE_KEY = "products"
        const val PARENT_ALL_PRODUCT_STORE_KEY = "allProducts"
        const val PARENT_PRODUCT_CODE_KEY = "products code"
    }
    init {
        db = Firebase.firestore
    }

    fun getPermissionOverlayTexts(selectedLanguage: String, fireStoreListener: FireStoreListener) {
        if (selectedLanguage.isNotEmpty()){
            db?.collection(PARENT_STORE_KEY_LANGUAGE)?.document(selectedLanguage)
                ?.collection("overlay_permission")
                ?.document("overlay_permission")
                ?.get()
                ?.addOnCompleteListener { firstResult ->
                    try {
                        val jsonData = convertToJson(firstResult.result.data)
                        fireStoreListener.onComplete(jsonData.toString())
                    }catch (ex:Exception){
                        fireStoreListener.onError(ex.message.toString())
                    }
                }?.addOnFailureListener {
                    fireStoreListener.onError(it.message.toString())
                }
        }
    }


    fun getDataListTemp(nodeName: String, fireStoreListener: FireStoreListener) {
        db?.collection(PARENT_STORE_KEY)
//            ?.whereGreaterThanOrEqualTo("domain",nodeName)
//            ?.whereLessThanOrEqualTo("domain", "$nodeName\uF7FF")
            ?.whereEqualTo("domain",nodeName)
            ?.get()
            ?.addOnCompleteListener { firstResult ->
                try {
                    val jsonData = convertToJson(firstResult.result.documents[0].data)
                    fireStoreListener.onComplete(jsonData.toString())
                }catch (ex:Exception){
                    fireStoreListener.onError(ex.message.toString())
                }
            }?.addOnFailureListener {
                fireStoreListener.onError(it.message.toString())
            }
    }


    fun getAllStoreCoupon(fireStoreListener: FireStoreListener) {
        db?.collection(PARENT_STORE_KEY)
            ?.get()
            ?.addOnCompleteListener { firstResult ->
                try {
                    val jsonData = convertToJson(firstResult.result.documents[0].data)
                    fireStoreListener.onComplete(jsonData.toString())
                }catch (ex:Exception){
                    fireStoreListener.onError(ex.message.toString())
                }
            }?.addOnFailureListener {
                fireStoreListener.onError(it.message.toString())
            }
    }


//    fun getProductsForMatching(nodeName: String,productId: String, fireStoreListener: FireStoreListener) {
//        Log.e(TAG, "ConvertMyUrl: getProductsForMatching -> $nodeName" )
//        Log.e(TAG, "ConvertMyUrl: getProductsForMatching -> $productId" )
//        db?.collection(PARENT_PRODUCT_STORE_KEY)
////            ?.whereGreaterThanOrEqualTo("domain",nodeName)
////            ?.whereLessThanOrEqualTo("domain", "$nodeName\uF7FF")
//            ?.whereArrayContains("product_urls",nodeName)
//            ?.get()
//            ?.addOnCompleteListener { firstResult ->
//                try {
//                    if(firstResult.result.documents.size>0){
//                        val jsonData = convertToJson(firstResult.result.documents[0].data)
//                        //Log.e(TAG, "JSON_RESPONSE: 1 : "+jsonData.toString() )
//                        Log.e(TAG, "JSON_RESPONSE: 1 : " )
//                        fireStoreListener.onComplete(jsonData.toString())
//                        //val model = parseResponseForCompareProduct(firstResult.result.documents[0].data.toString())
//
//                        fireStoreListener.onComplete(firstResult.result.documents[0].data.toString())
//                    }else{
//
//                        Log.e(TAG, "JSON_RESPONSE: 2 : SIZE 0 ")
//                        if(productId != ""){
//                            getProductsProductCode(productId,fireStoreListener)
//                        }else{
//                            fireStoreListener.onComplete("")
//                        }
//
//                    }
//                }catch (ex:Exception){
//                    Log.e(TAG, "JSON_RESPONSE: 3 : ERROR: "+ex.message)
//                    fireStoreListener.onError(ex.message.toString())
//                }
//            }?.addOnFailureListener {
//                Log.e(TAG, "JSON_RESPONSE: 4 : ERROR: "+it.message.toString())
//                fireStoreListener.onError(it.message.toString())
//            }
//    }

    /**
     *  New Implementation of Product Comparison:
     */

    fun getProductsToCompare(nodeName: String,productId: String, compareProductListener : FirestoreProductMatchingInterface) {
        Log.e(TAG, "ConvertMyUrl: getProductsForMatching -> $nodeName" )
        Log.e(TAG, "ConvertMyUrl: getProductsForMatching -> $productId" )
        db?.collection(PARENT_PRODUCT_STORE_KEY)
//            ?.whereGreaterThanOrEqualTo("domain",nodeName)
//            ?.whereLessThanOrEqualTo("domain", "$nodeName\uF7FF")
            ?.whereArrayContains("product_urls",nodeName)
            ?.get()
            ?.addOnCompleteListener { firstResult ->
                try {
                    if(firstResult.result.documents.size>0){
                        //val jsonData = convertToJson(firstResult.result.documents[0].data)
                        //Log.e(TAG, "JSON_RESPONSE: 1 : "+jsonData.toString() )
                        Log.e(TAG, "JSON_RESPONSE: 1 : " )

                        //fireStoreListener.onComplete(jsonData.toString())
                        val model = parseResponseForCompareProduct(JSONObject(Gson().toJson(firstResult.result.documents[0].data).toString()))
                        compareProductListener.onComplete(model);
                        //fireStoreListener.onComplete(firstResult.result.documents[0].data.toString())
                    } else {
                        compareProductListener.onError("SIZE(0) : Not Found!")
                    }
                }catch (ex:Exception){
                    Log.e(TAG, "JSON_RESPONSE: 3 : ERROR: "+ex.message)
                        //fireStoreListener.onError(ex.message.toString())
                    compareProductListener.onError("ERROR (3): "+ex.message)
                }
            }?.addOnFailureListener {
                Log.e(TAG, "JSON_RESPONSE: 4 : ERROR: "+it.message.toString())
                compareProductListener.onError("ERROR (4): "+it.message.toString())
                //fireStoreListener.onError(it.message.toString())
            }
    }

    private fun parseResponseForCompareProduct(rootObject: JSONObject) : CompareProductModel {

        var sellerModel: SellerModel? = null;
        val listOfSeller  = ArrayList<SellerModel>();

        //val rootObject = JSONObject(response.toString());
        if (rootObject.has("seller_list")) { // Checking seller list is available or not in DB
            val sellerListArray = rootObject.getJSONArray("seller_list");
            Log.d(TAG, "parseResponseForCompareProduct: "+sellerListArray.length())
            if (sellerListArray.length() > 0) { // checking seller list size
                for (index in 0 until sellerListArray.length()) {
                    var condition = ""
                    var hours_updated_ago = ""
                    var price = "";
                    var store_logo = "";
                    var comment = "";
                    var currency = "";
                    var background_link = false;
                    var direct_link = "";
                    var status = "";

                    //NEW ADDED AD
                    var timestamp_added = "";
                    var timestamp_updated = "";

                    val singleSellerFromSellerList = sellerListArray.getJSONObject(index)
                    if (singleSellerFromSellerList.has("condition")) {
                        condition = singleSellerFromSellerList.get("condition") as String
                    }
                    if (singleSellerFromSellerList.has("hours_updated_ago")) {
                        hours_updated_ago = singleSellerFromSellerList.get("hours_updated_ago") as String
                    }
                    if (singleSellerFromSellerList.has("price")) {
                        price = singleSellerFromSellerList.get("price") as String
                    }

                    if (singleSellerFromSellerList.has("store_logo")) {
                        store_logo = singleSellerFromSellerList.get("store_logo") as String
                    }
                    if (singleSellerFromSellerList.has("comment")) {
                        comment = singleSellerFromSellerList.get("comment") as String
                    }
                    if (singleSellerFromSellerList.has("currency")) {
                        currency = singleSellerFromSellerList.get("currency") as String
                    }

                    if (singleSellerFromSellerList.has("background_link")) {
                        background_link = singleSellerFromSellerList.get("background_link") as Boolean
                    }
                    if (singleSellerFromSellerList.has("direct_link")) {
                        direct_link = singleSellerFromSellerList.get("direct_link") as String
                    }

                    if (singleSellerFromSellerList.has("status")) {
                        status = singleSellerFromSellerList.get("status") as String
                    }

                    //NEW ADDED AD
                    if (singleSellerFromSellerList.has("timestamp_added")) {
                        timestamp_added = singleSellerFromSellerList.get("timestamp_added") as String
                    }
                    if (singleSellerFromSellerList.has("timestamp_updated")) {
                        timestamp_updated = singleSellerFromSellerList.get("timestamp_updated") as String
                    }




                    sellerModel = SellerModel(condition, hours_updated_ago, price, store_logo, comment, currency, background_link, direct_link, status,timestamp_added,timestamp_updated);
                    listOfSeller.add(sellerModel)
                }
            } else { // seller list is empty

            }
        }
        var max_price = "";
        var id_product = "";
        var min_price = "";
        var similar_count = 0;
        if (rootObject.has("max_price'")) {
            max_price = rootObject.get("max_price") as String
        }
        if (rootObject.has("id_product'")) {
            id_product = rootObject.get("id_product") as String
        }
        if (rootObject.has("min_price'")) {
            min_price = rootObject.get("min_price") as String
        }

        Log.e(TAG, "Product ID: " + rootObject.get("id_product"))
        Log.e(TAG, "Result: " + rootObject.get("max_price"))
        Log.e(TAG, "Result: " + max_price)

        if (rootObject.has("similar_count'")) {
            similar_count = rootObject.get("similar_count") as Int
        }

        val listOfSimilarProduct =  ArrayList<SimilarProductModel>();
        if (rootObject.has("similar_products'")) { // Checking Similar Product in DB
            val similarProductListArray = rootObject.getJSONArray("similar_products");
            if (similarProductListArray.length() > 0) { // means it has the similar product
//                for (index in 0 until similarProductListArray.length()) {
//                    val singleObject = similarProductListArray.getJSONObject(index);
//                    if (singleObject.has())
//                }
            } else {
                // No similar product found
            }
        }

        var currency = "";
        var seller_count = 0;
        if (rootObject.has("currency")) {
            currency = rootObject.get("currency") as String;
        }
        if (rootObject.has("seller_count")) {
            seller_count = rootObject.get("seller_count") as Int;
        }
        if (rootObject.has("seller_count")) {
            seller_count = rootObject.get("seller_count") as Int;
        }

        val skuList = ArrayList<String>();
        if (rootObject.has("sku")) {
            val skuArray = rootObject.getJSONArray("sku");
            if (skuArray.length() > 0) {
                for (index in 0 until skuArray.length()) {
                    val sku = skuArray.get(index) as String
                    skuList?.add(sku);
                }

            } else {

            }
        }

        val productUrlList = ArrayList<String>();
        if (rootObject.has("product_urls")) {
            val urlsArray = rootObject.getJSONArray("product_urls");
            if (urlsArray.length() > 0) {
                for (index in 0 until urlsArray.length()) {
                    val urls = urlsArray.get(index) as String
                    productUrlList?.add(urls);
                }

            }
        }

        var product_name = ""
        if (rootObject.has("product_name")) {
            product_name = rootObject.getString("product_name");
        }

        val skuSourceList = ArrayList<String>();
        if (rootObject.has("sku_source")) {
            val skuSourceArray = rootObject.getJSONArray("sku_source");
            if (skuSourceArray.length() > 0) {
                for (index in 0 until skuSourceArray.length()) {
                    val skuSource = skuSourceArray.get(index) as String
                    skuSourceList?.add(skuSource);
                }

            }
        }

        val compareProductModel = CompareProductModel(
            listOfSeller,
            ""+rootObject.get("max_price"),
            ""+rootObject.get("id_product"),
            ""+rootObject.get("min_price"),
            rootObject.getInt("similar_count"),
            listOfSimilarProduct,
            ""+rootObject.get("currency"),
        rootObject.getInt("seller_count"),
            skuList,
            productUrlList,
            ""+rootObject.get("product_name"),
            skuSourceList
        )
        return compareProductModel;
    }

//    fun getProductSkuInfo(nodeName: String, fireStoreListener: FireStoreListener) {
//        db?.collection(PARENT_STORE_KEY)
////            ?.whereGreaterThanOrEqualTo("domain",nodeName)
//            ?.whereEqualTo("store",nodeName)
//            ?.get()
//            ?.addOnCompleteListener { firstResult ->
//                try {
//                    if(firstResult.result.documents.size>0){
//                        val jsonData = convertToJson(firstResult.result.documents[0].data)
//                        fireStoreListener.onComplete(jsonData.toString())
//                    }else{
//                        fireStoreListener.onComplete("")
//                    }
//                }catch (ex:Exception){
//                    fireStoreListener.onError(ex.message.toString())
//                }
//            }?.addOnFailureListener {
//                fireStoreListener.onError(it.message.toString())
//            }
//    }

    private fun convertToJson(mapData: Map<String, Any>?): String? {
        val gsn = Gson()
        return gsn.toJson(mapData)
    }

    private fun getProductsProductCode(productId: String, fireStoreListener: FireStoreListener) {
        db?.collection(PARENT_PRODUCT_STORE_KEY)
//            ?.whereGreaterThanOrEqualTo("domain",nodeName)
//            ?.whereLessThanOrEqualTo("domain", "$nodeName\uF7FF")
            ?.whereArrayContains("sku",productId)
            ?.get()
            ?.addOnCompleteListener { firstResult ->
                try {
                    if(firstResult.result.documents.size>0){
                        val jsonData = convertToJson(firstResult.result.documents[0].data)
                        fireStoreListener.onComplete(jsonData.toString())
                    }else{
                        fireStoreListener.onComplete("")
                    }
                }catch (ex:Exception){
                    fireStoreListener.onError(ex.message.toString())
                }
            }?.addOnFailureListener {
                fireStoreListener.onError(it.message.toString())
            }
    }

    fun getUserData(userId : String, fireStoreUserdata : FireStoreUserDataInterface){
        db?.collection(FireStorageKeys.KEY_USER_TABLE)?.document(userId)
            ?.get()
            ?.addOnSuccessListener { result ->
                try {
                    val jsonData = convertToJson(result.data)
                    val gsn = Gson()
                    val userModel: UserDataModel = gsn.fromJson(
                        jsonData.toString(),
                        UserDataModel::class.java
                    )
                    Log.e("FireStore", "onComplete userData: $userModel" )
                    fireStoreUserdata.onComplete(userModel)
                }catch (e: Exception){
                    Log.e("FireStore", "Error getting documents.1 ${e.message}")
                    e.printStackTrace()
                    fireStoreUserdata.onError("Error1 : ${e.message}")
                }
            }
            ?.addOnFailureListener {
                Log.e("FireStore", "Error getting documents.2", it)
                fireStoreUserdata.onError("Error2 : ${it.message}")
            }
    }
    
    fun updateUserData(userId : String, userData: UserDataModel, fireStoreUpdateUserDataInterface: FireStoreUpdateUserDataInterface){
        db?.collection(FireStorageKeys.KEY_USER_TABLE)?.document(userId)
            ?.set(userData, SetOptions.merge())
            ?.addOnSuccessListener {
                Log.d("firebaseData", ": $it")
                fireStoreUpdateUserDataInterface.onSuccess("Update data successfully")
            }
            ?.addOnFailureListener { e ->
                Log.d("firebaseData", "Error adding document ${e.message}")
                fireStoreUpdateUserDataInterface.onError(e.message.toString())
            }
    }

    // Add list collection and lists
    @SuppressLint("SuspiciousIndentation")
    fun addListCollection(userId: String, data: ListCollectionModel, fireStoreAddListInterface: FireStoreAddListInterface) {
        val listCollectionRef = db?.collection(FireStorageKeys.KEY_USER_TABLE)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_PRODUCTS)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_LIST)
            ?.document()

        data.listId = listCollectionRef?.id

            listCollectionRef
            ?.set(data)
            ?.addOnSuccessListener { result ->
                try {
                    Log.e(TAG, "List insert successfully")
                    fireStoreAddListInterface.onSuccess("List insert successfully..")

                } catch (e: Exception) {
                    Log.e("FireStore", "Error getting List ${e.message}")
                    fireStoreAddListInterface.onError(e.message.toString())
                }
            }
            ?.addOnFailureListener {e ->
                Log.e("FireStore", "Error getting List", e)
                fireStoreAddListInterface.onError(e.message.toString())
            }
    }

    // Add particular list collection in product
    fun addListProduct(userId: String, data: ListCollectionModel, addListInProduct: MutableMap<String, Any>, listener: FireStoreAddListProductInterface) {

        db?.collection(FireStorageKeys.KEY_USER_TABLE)!!
            .document(userId)
            .collection(FireStorageKeys.KEYUT_PRODUCTS)
            .document(userId)
            .collection(FireStorageKeys.KEYUT_LIST)
            .document(data.listId!!)
            .update(addListInProduct)
            .addOnSuccessListener {
                try {
                    Log.e(TAG, "List insert or update successfully")
                    listener.onSuccess("List insert or update successfully")

                } catch (e: Exception) {
                    Log.e("FireStore", "Error getting List ${e.message}")
                    listener.onError(e.message.toString())
                }
            }
            .addOnFailureListener {e ->
                Log.e("FireStore", "Error getting List", e)
                listener.onError(e.message.toString())
            }

        /*db?.collection(FireStorageKeys.KEY_USER_TABLE)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_PRODUCTS)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_LIST)
            ?.document(data.listName!!)
            ?.set(data)
            ?.addOnSuccessListener { result ->
                try {
                    Log.e(TAG, "List insert or update successfully")
                    fireStoreAddListInterface.onSuccess("List insert or update successfully")

                } catch (e: Exception) {
                    Log.e("FireStore", "Error getting List ${e.message}")
                    fireStoreAddListInterface.onError(e.message.toString())
                }
            }
            ?.addOnFailureListener {e ->
                Log.e("FireStore", "Error getting List", e)
                fireStoreAddListInterface.onError(e.message.toString())
            }*/
    }

    // Removing user list item
    fun removeList(userId: String, listModel: ListCollectionModel, fireStoreRemoveListInterface: FireStoreRemoveListInterface){
        db?.collection(FireStorageKeys.KEY_USER_TABLE)!!.document(userId)
            ?.collection(FireStorageKeys.KEYUT_PRODUCTS)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_LIST)
            ?.document(listModel.listId ?: "")
            ?.delete()
            ?.addOnSuccessListener {

                try {
                    Log.e(TAG, "List removed successfully")
                    fireStoreRemoveListInterface.onSuccess("List removed successfully")
                } catch (e: Exception) {
                    Log.e("FireStore", "Error remove list ${e.message}")
                    fireStoreRemoveListInterface.onError(e.message.toString())
                }
            }
            ?.addOnFailureListener {e ->
                Log.e("FireStore", "Error remove list1", e)
                fireStoreRemoveListInterface.onError(e.message.toString())
            }
    }

    // Edit and update List data
    fun editListData(userId: String, data: ListCollectionModel, fireStoreEditListInterface: FireStoreEditUpdateListInterface){
        val listCollectionRef = db?.collection(FireStorageKeys.KEY_USER_TABLE)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_PRODUCTS)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_LIST)
            ?.document(data.listId!!)

        listCollectionRef
            ?.set(data)
            ?.addOnSuccessListener { result ->
                try {
                    Log.e(TAG, "List insert or update successfully")
                    fireStoreEditListInterface.onSuccess("List update successfully")

                } catch (e: Exception) {
                    Log.e("FireStore", "Error getting List ${e.message}")
                    fireStoreEditListInterface.onError(e.message.toString())
                }
            }
            ?.addOnFailureListener {e ->
                Log.e("FireStore", "Error getting List", e)
                fireStoreEditListInterface.onError(e.message.toString())
            }
    }

    fun getListProductFilter(userId: String, getFilterListListener: FireStoreGetFilterListListener) {

        db?.collection(FireStorageKeys.KEY_USER_TABLE)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_PRODUCTS)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_LIST)
            ?.get()
            ?.addOnSuccessListener { result ->
                try {
                    Log.e("FireStore", "onComplete filter list product: ${result.size()}")
                    getFilterListListener.onSuccess(result)
                } catch (e: Exception) {
                    Log.e("FireStore", "Error getting documents.1 ${e.message}")
                    getFilterListListener.onError(e.message!!)
                }
            }
            ?.addOnFailureListener {e ->
                Log.e("FireStore", "Error getting FilterList.2", e)
                getFilterListListener.onError("Error2 : ${e.message}")
            }

    }

    fun getListByName(userId: String, listId: String, getFilterProductListListener: FireStoreGetFilterProductsListListener){

        db?.collection(FireStorageKeys.KEY_USER_TABLE)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_PRODUCTS)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_LIST)
            ?.document(listId)
            ?.get()
            ?.addOnSuccessListener { result ->
                try {
                    Log.e("FireStore", "onComplete filter list product: ${result.data}")
                    getFilterProductListListener.onSuccess(result)
                } catch (e: Exception) {
                    Log.e("FireStore", "Error getting documents.1 ${e.message}")
                    getFilterProductListListener.onError(e.message!!)
                }
            }
            ?.addOnFailureListener {e ->
                Log.e("FireStore", "Error getting productFilterList.1", e)
                getFilterProductListListener.onError("Error2 : ${e.message}")
            }
    }

    fun addFavoriteProduct(userId:String, productModel: FavoritePurchasedCustomListModel, fireStoreAddFavoriteProductInterface: FireStoreAddFavoriteProductInterface) {

        db?.collection(FireStorageKeys.KEY_USER_TABLE)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_PRODUCTS)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_FAVORITE_PRODUCT)
            ?.document(productModel.id_product!!)
            ?.set(productModel)
            ?.addOnSuccessListener { result ->
                try {
                    Log.e(TAG, "Favorite Product insert or update")
                    fireStoreAddFavoriteProductInterface.onSuccess("Favorite Product insert or update")
                } catch (e: Exception) {
                    Log.e("FireStore", "Error getting documents ${e.message}")
                    fireStoreAddFavoriteProductInterface.onError(e.message.toString())
                }
            }
            ?.addOnFailureListener {e ->
                Log.e("FireStore", "Error getting documents", e)
                fireStoreAddFavoriteProductInterface.onError(e.message.toString())
            }
    }

    fun removeFavoriteProduct(userId: String, productModel: FavoritePurchasedCustomListModel, fireStoreRemoveFavoriteProductInterface: FireStoreRemoveFavoriteProductInterface){
        db?.collection(FireStorageKeys.KEY_USER_TABLE)!!.document(userId)
            ?.collection(FireStorageKeys.KEYUT_PRODUCTS)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_FAVORITE_PRODUCT)
            ?.document(productModel.id_product!!)
            ?.delete()
            ?.addOnSuccessListener {

                try {
                    Log.e(TAG, "favorite Product removed successfully")
                    fireStoreRemoveFavoriteProductInterface.onSuccess("Favorite Product insert or update")
                } catch (e: Exception) {
                    Log.e("FireStore", "Error getting documents ${e.message}")
                    fireStoreRemoveFavoriteProductInterface.onError(e.message.toString())
                }
            }
            ?.addOnFailureListener {e ->
                Log.e("FireStore", "Error getting documents", e)
                fireStoreRemoveFavoriteProductInterface.onError(e.message.toString())
            }
    }

    fun addPurchasedProduct(userId:String, productModel: FavoritePurchasedCustomListModel, fireStoreAddPurchasedProductInterface: FireStoreAddPurchasedProductInterface) {

        db?.collection(FireStorageKeys.KEY_USER_TABLE)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_PRODUCTS)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_PURCHASED_PRODUCT)
            ?.document(productModel.id_product!!)
            ?.set(productModel)
            ?.addOnSuccessListener { result ->
                try {
                    Log.e(TAG, "Purchased Product insert or update")
                    fireStoreAddPurchasedProductInterface.onSuccess("Purchased Product insert or update")
                } catch (e: Exception) {
                    Log.e("FireStore", "Error getting documents ${e.message}")
                    fireStoreAddPurchasedProductInterface.onError(e.message.toString())
                }
            }
            ?.addOnFailureListener {e ->
                Log.e("FireStore", "Error getting documents", e)
                fireStoreAddPurchasedProductInterface.onError(e.message.toString())
            }
    }

    fun getPurchasedProduct(userId: String, getPurchasedProductInterface: FireStoreGetPurchasedProductInterface){
        db?.collection(FireStorageKeys.KEY_USER_TABLE)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_PRODUCTS)
            ?.document(userId)
            ?.collection(FireStorageKeys.KEYUT_PURCHASED_PRODUCT)
            ?.get()
            ?.addOnSuccessListener { result ->
                try {
                    Log.e("FireStore", "onComplete get purchasedProducts list : ${result.size()}")
                    getPurchasedProductInterface.onSuccess(result)
                } catch (e: Exception) {
                    Log.e("FireStore", "Error getting purchasedProduct documents.1 ${e.message}")
                    e.printStackTrace()
                    getPurchasedProductInterface.onError(e.message!!)
                }
            }
            ?.addOnFailureListener {e ->
                Log.e("FireStore", "Error getting purchasedProductList.2", e)
                getPurchasedProductInterface.onError("Error2 : ${e.message}")
            }
    }

    fun getProductFromHashUrl(hashProductUrl: String, listener: FireStoreGetProductFromHashUrl) {
        db?.collection(PARENT_PRODUCT_STORE_KEY)
            ?.whereArrayContains("product_urls",hashProductUrl)
            ?.get()
            ?.addOnSuccessListener { result ->
                try {
                    Log.e("FireStore", "onComplete get getHashProducts list : ${result.size()}")
                    listener.onSuccess(result)
                } catch (e: Exception) {
                    Log.e("FireStore", "Error getting getHashProducts documents.1 ${e.message}")
                    e.printStackTrace()
                    listener.onError(e.message!!)
                }
            }
            ?.addOnFailureListener {e ->
                Log.e("FireStore", "Error getting getHashProducts.2", e)
                listener.onError("Error2 : ${e.message}")
            }
    }


}
package com.app.discountro.utils.browser_utils.my_object

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.view.View

object ViewUnit {
    fun bound(context: Context, view: View) {
        val windowWidth = getWindowWidth(context)
        val windowHeight = getWindowHeight(context)
        val widthSpec = View.MeasureSpec.makeMeasureSpec(windowWidth, View.MeasureSpec.EXACTLY)
        val heightSpec = View.MeasureSpec.makeMeasureSpec(windowHeight, View.MeasureSpec.EXACTLY)
        view.measure(widthSpec, heightSpec)
        view.layout(0, 0, view.measuredWidth, view.measuredHeight)
    }

    fun createImage(width: Int, height: Int, color: Int): Bitmap {
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        val paint = Paint()
        paint.color = color
        paint.alpha = 50
        canvas.drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)
        return bitmap
    }

    @JvmStatic
    fun capture(view: View, width: Float, height: Float, config: Bitmap.Config?): Bitmap {
        view.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        val bitmap = Bitmap.createBitmap(width.toInt(), height.toInt(), config!!)
        bitmap.eraseColor(Color.WHITE)
        val canvas = Canvas(bitmap)
        val left = view.left
        val top = view.top
        val status = canvas.save()
        canvas.translate(-left.toFloat(), -top.toFloat())
        val scale = width / view.width
        canvas.scale(scale, scale, left.toFloat(), top.toFloat())
        view.draw(canvas)
        canvas.restoreToCount(status)
        val alphaPaint = Paint()
        alphaPaint.color = Color.TRANSPARENT
        canvas.drawRect(0f, 0f, 1f, height, alphaPaint)
        canvas.drawRect(width - 1f, 0f, width, height, alphaPaint)
        canvas.drawRect(0f, 0f, width, 1f, alphaPaint)
        canvas.drawRect(0f, height - 1f, width, height, alphaPaint)
        canvas.setBitmap(null)
        return bitmap
    }

    fun getDensity(context: Context): Float {
        return context.resources.displayMetrics.density
    }

     fun getDrawable(context: Context, id: Int): Drawable {
        return context.resources.getDrawable(id, null)
    }

    private fun getWindowHeight(context: Context): Int {
        return context.resources.displayMetrics.heightPixels
    }

    fun getWindowWidth(context: Context): Int {
        return context.resources.displayMetrics.widthPixels
    }
}
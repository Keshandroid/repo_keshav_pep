package com.app.discountro.utils.browser_utils
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.webkit.*
import com.app.discountro.ui.home.viewmodel.BrowserWebView
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.SharedPrefsUtils
import com.app.discountro.utils.Utils
import com.app.discountro.utils.browser_utils.my_object.BrowserUnit
import com.app.discountro.utils.browser_utils.my_object.HelperUnit
import com.app.discountro.utils.browser_utils.my_object.IntentUnit.context
class BrowserWebChromeClient(private val browserWebView: BrowserWebView) : WebChromeClient() {
    private lateinit var webViewParent: ViewGroup
    val TAG = javaClass.simpleName
    var oldUrl = ""


    override fun onProgressChanged(view: WebView, progress: Int) {
        super.onProgressChanged(view, progress)
//        browserWebView.update(progress, view)
        if(view.url!!.length > 80 && oldUrl.contains(view.url!!.substring(0,70)) && view.url!!.contains("https://www.google.com/search") && (progress == 10 || progress ==100)){
            // ignore progress
            browserWebView.getBrowserController()!!.hideProgressBar(0)
            Log.e( "onProgressChanged: ","Progress Ignored" )
        }else{
            browserWebView.getBrowserController()!!.updateProgressBar(progress)
        }

        oldUrl = view.url!!
    }
    override fun onCreateWindow(
        webView: WebView,
        dialog: Boolean,
        userGesture: Boolean,
        resultMsg: Message,
    ): Boolean {
        val newWebView = WebView(webView.context).apply { initWebView(this) }
        newWebView.webViewClient = object : WebViewClient() {
            private var isUrlProcessed = false

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?,
            ): Boolean {
                val urlString = request?.url?.toString() ?: return false

                return if (Utils.isFacebookLoginUrl(urlString)) {
                    val fbDesktopUrl = Utils.changeFbMobToDesktopUrl(urlString)
                    Log.e(TAG, "shouldOverrideUrlLoading:1 newDeskTop url:$fbDesktopUrl")
                    Log.e(TAG, "shouldOverrideUrlLoading:webView.url: " + webView.url)

                    view!!.loadUrl(webView.url!!)
                    view.settings.userAgentString = KeysUtils.DESKTOP_USERAGENT
                    view.loadUrl(fbDesktopUrl)
                    isUrlProcessed = true
                    false

                } else if (Utils.isFbLoginCompleteUrl(urlString)) {
                    Log.e(TAG, "shouldOverrideUrlLoading: here in fb-login complete")
                    view?.loadUrl(urlString)
                    view?.settings?.userAgentString =
                        SharedPrefsUtils.getStringPreference(KeysUtils.keyDefaultMobUserAgent)
                    true
                } else if (isGoogleLoginUrl(urlString)) {
                    Log.e(TAG, "shouldOverrideUrlLoading: isGoogleLoginUrl")
                    view?.loadUrl(urlString)
                    true
                } else if (urlString.contains("mailto:")) {
                    Log.e(TAG, "webView:- webView.url!!: " + webView.url!!)
                    view!!.loadUrl(webView.url!!)
                    Handler(Looper.getMainLooper()).postDelayed({
                        context?.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(urlString)))
                    }, 0)
                    isUrlProcessed = true
                    false
                } else if (!isUrlProcessed) {
                    request.url?.let {
                        Log.e("KNVPN", "shouldOverrideUrlLoading:- => " + urlString)
                        handleWebViewLinks(urlString)
                        isUrlProcessed = true
                    } // you can get your target url here
                    false
                } else false
            }
        }

        newWebView.webChromeClient = object : WebChromeClient() {
            override fun onCloseWindow(window: WebView?) {
                webViewParent.removeView(window)
            }
        }
        webViewParent = browserWebView.parent as ViewGroup
        webViewParent.addView(newWebView)

        val transport = resultMsg.obj as WebView.WebViewTransport
        transport.webView = newWebView
        resultMsg.sendToTarget()
        return true
    }

    override fun onJsAlert(view: WebView, url: String, message: String, result: JsResult): Boolean {
        //Required functionality here
        return super.onJsAlert(view, url, message, result)
    }

    private fun initWebView(webView: WebView) {
        val webSettings = webView.settings
//        val defaultUserAgent = webSettings.userAgentString
        val defaultUserAgentString = WebSettings.getDefaultUserAgent(context).replace("wv", "")
        val prefix: String =
            defaultUserAgentString.substring(0, defaultUserAgentString.indexOf(")") + 1)
        val myUserAgentValue = defaultUserAgentString.replace(prefix, BrowserUnit.UA_MOBILE_PREFIX)
//        webSettings.userAgentString = defaultUserAgent.replace("wv", "")
        webSettings.userAgentString = myUserAgentValue
//        webSettings.userAgentString = KeysUtils.DESKTOP_USERAGENT
        webSettings.setAppCacheEnabled(true)
        webSettings.cacheMode = WebSettings.LOAD_DEFAULT
        webSettings.allowFileAccessFromFileURLs = true
        webSettings.allowUniversalAccessFromFileURLs = true
        webSettings.domStorageEnabled = true
        webSettings.databaseEnabled = true
        webSettings.javaScriptEnabled = true
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
        val manager = CookieManager.getInstance()
        manager.setAcceptThirdPartyCookies(webView, true)

        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH)
    }

    private fun isGoogleLoginUrl(url: String): Boolean {
        Log.e(TAG, "isGoogleLoginUrl: $url")
        return url.contains("accounts.google.com")
    }

    override fun onReceivedTitle(view: WebView, title: String) {
        super.onReceivedTitle(view, title)
        browserWebView.update(title, view.url)
    }

    override fun onCloseWindow(window: WebView?) {
        (browserWebView.parent as ViewGroup).removeView(window)
    }

    private fun handleWebViewLinks(url: String) =
        browserWebView.getBrowserController()?.addNewTab(url)

    override fun onShowCustomView(view: View, callback: CustomViewCallback) {
        browserWebView.getBrowserController()!!.onShowCustomView(view, callback)
        super.onShowCustomView(view, callback)
    }

    override fun onHideCustomView() {
        Log.e(TAG, "onHideCustomView: onnn???-")
        browserWebView.getBrowserController()!!.onHideCustomView()
        super.onHideCustomView()
    }

    override fun onShowFileChooser(
        webView: WebView,
        filePathCallback: ValueCallback<Array<Uri>>,
        fileChooserParams: FileChooserParams,
    ): Boolean {
        browserWebView.getBrowserController()!!.showFileChooser(filePathCallback)
        return true
    }

    override fun onGeolocationPermissionsShowPrompt(
        origin: String,
        callback: GeolocationPermissions.Callback,
    ) {
        val activity = browserWebView.context as Activity
        HelperUnit.grantPermissionsLoc(activity)
        callback.invoke(origin, true, false)
        super.onGeolocationPermissionsShowPrompt(origin, callback)
    }
}




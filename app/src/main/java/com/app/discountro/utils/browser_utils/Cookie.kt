package com.app.discountro.utils.browser_utils

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.app.discountro.common.sqlite.browser_db.RecordAction
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.*

class Cookie(private val context: Context?) {
    fun isWhite(url: String?): Boolean {
        for (domain: String? in whitelistCookie) {
            if (url != null && url.contains((domain)!!)) {
                return true
            }
        }
        return false
    }

    @Synchronized
    fun addDomain(domain: String) {
        val action = RecordAction(context)
        action.open(true)
        action.addDomainCookie(domain)
        action.close()
        whitelistCookie.add(domain)
    }

    @Synchronized
    fun removeDomain(domain: String) {
        val action = RecordAction(context)
        action.open(true)
        action.deleteDomainCookie(domain)
        action.close()
        whitelistCookie.remove(domain)
    }

    @Synchronized
    fun clearDomains() {
        val action = RecordAction(context)
        action.open(true)
        action.clearDomainsCookie()
        action.close()
        whitelistCookie.clear()
    }

    companion object {
        private val FILE = "cookieHosts.txt"
        private val hostsCookie: MutableSet<String> = HashSet()
        private val whitelistCookie: MutableList<String> = ArrayList()

        @SuppressLint("ConstantLocale")
        private val locale = Locale.getDefault()
        private fun loadHosts(context: Context?) {
            val thread = Thread{
                val manager = context?.assets
                try {
                    val reader = BufferedReader(InputStreamReader(manager?.open(FILE)))
                    var line: String
                    while ((reader.readLine().also { line = it }) != null) {
                        hostsCookie.add(line.lowercase(locale))
                    }
                } catch (i: IOException) {
                    Log.e("Browser", "Error loading hosts")
                }
            }
            thread.start()
        }

        @Synchronized
        private fun loadDomains(context: Context?) {
            val action = RecordAction(context)
            action.open(false)
            whitelistCookie.clear()
            whitelistCookie.addAll(action.listDomainsCookie())
            action.close()
        }
    }

    init {
        if (hostsCookie.isEmpty()) {
            loadHosts(context)
        }
        loadDomains(context)
    }
}
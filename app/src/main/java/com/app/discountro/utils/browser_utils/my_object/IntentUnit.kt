package com.app.discountro.utils.browser_utils.my_object

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.MailTo
import com.app.discountro.R

object IntentUnit {

    private const val INTENT_TYPE_MESSAGE_RFC822 = "message/rfc822"
    fun getEmailIntent(mailTo: MailTo): Intent {
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(mailTo.to))
        intent.putExtra(Intent.EXTRA_TEXT, mailTo.body)
        intent.putExtra(Intent.EXTRA_SUBJECT, mailTo.subject)
        intent.putExtra(Intent.EXTRA_CC, mailTo.cc)
        intent.type = INTENT_TYPE_MESSAGE_RFC822
        return intent
    }

    fun share(context: Context, title: String?, url: String?) {
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, title)
        sharingIntent.putExtra(Intent.EXTRA_TEXT, url)
        context.startActivity(
            Intent.createChooser(
                sharingIntent,
                context.getString(R.string.menu_share_link)
            )
        )
    }

    // Activity holder
    @SuppressLint("StaticFieldLeak")
    var context: Context? = null

    @set:Synchronized
    var isClear = false
}
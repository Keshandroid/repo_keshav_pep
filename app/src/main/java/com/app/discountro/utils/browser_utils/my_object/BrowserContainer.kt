package com.app.discountro.utils.browser_utils.my_object

import com.app.discountro.common.album_service.AlbumController
import com.app.discountro.ui.home.viewmodel.BrowserWebView
import java.util.*
object BrowserContainer {
    private val list: MutableList<AlbumController> = LinkedList()
    operator fun get(index: Int): AlbumController {
        return list[index]
    }
    @Synchronized
    fun add(controller: AlbumController) {
        list.add(controller)
    }
    @Synchronized
    fun add(controller: AlbumController, index: Int) {
        list.add(index, controller)
    }
    @Synchronized
    fun remove(controller: AlbumController) {
        if (controller is BrowserWebView) {
            controller.destroy()
        }
        list.remove(controller)
    }
    fun indexOf(controller: AlbumController): Int {
        return list.indexOf(controller)
    }
    fun list(): List<AlbumController> {
        return list
    }

    fun size(): Int {
        return list.size
    }
    @JvmStatic
    @Synchronized
    fun clear() {
        for (albumController in list) {
            if (albumController is BrowserWebView) {
                albumController.destroy()
            }
        }
        list.clear()
    }
}
package com.app.discountro.utils.browser_utils

import android.os.Handler
import android.os.Looper
import android.os.Message
import com.app.discountro.ui.home.viewmodel.BrowserWebView

class BrowserClickHandler(private val webView: BrowserWebView) : Handler(Looper.getMainLooper()) {
    override fun handleMessage(message: Message) {
        super.handleMessage(message)
        webView.getBrowserController()!!.onLongPress(message.data.getString("url"))
    }
}
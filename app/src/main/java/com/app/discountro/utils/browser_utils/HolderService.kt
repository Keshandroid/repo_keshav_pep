package com.app.discountro.utils.browser_utils

import android.app.Service
import android.content.Intent
import android.net.Uri
import android.os.IBinder
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.webkit.ValueCallback
import android.webkit.WebChromeClient.CustomViewCallback
import android.webkit.WebView
import android.widget.Toast
import com.app.discountro.R
import com.app.discountro.common.album_service.AlbumController
import com.app.discountro.common.my_interface.BrowserController
import com.app.discountro.common.sqlite.record.RecordUnit
import com.app.discountro.ui.home.view.BrowserActivity
import com.app.discountro.ui.home.view.BrowserContextWrapper
import com.app.discountro.ui.home.viewmodel.BrowserWebView
import com.app.discountro.utils.browser_utils.my_object.BrowserContainer.add
import com.app.discountro.utils.browser_utils.my_object.BrowserContainer.clear
import com.app.discountro.utils.browser_utils.my_object.IntentUnit
import com.app.discountro.utils.browser_utils.my_object.NotificationUnit
import com.app.discountro.utils.browser_utils.my_object.ViewUnit


class HolderService : Service(), BrowserController {
    override fun updateAutoComplete() {}
    override fun hideOverview() {}
    override fun updateTabsAdapterData(progress: Int, webView: WebView) {}
    override fun addNewTab(url: String) {}
    override fun updateProgressBar(progress: Int) {
    }

    override fun hideProgressBar(progress: Int) {

    }

    override fun updateBookmarks() {}
    override fun updateProgress(progress: Int) {}
    override fun showAlbum(albumController: AlbumController?, isIncoTab: Boolean) {}
    override fun removeAlbum(albumController: AlbumController?, isIncoTab: Boolean) {}
    override fun showFileChooser(filePathCallback: ValueCallback<Array<Uri>>) {}
    override fun onShowCustomView(view: View?, callback: CustomViewCallback?) {}
    override fun onHideCustomView(): Boolean {
        return true
    }

    override fun onLongPress(url: String?) {}
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val sp = PreferenceManager.getDefaultSharedPreferences(this)
        if (sp.getBoolean("sp_background", true)) {
            WebView.enableSlowWholeDocumentDraw()
            val webView = BrowserWebView(null,BrowserContextWrapper(this))
            webView.setBrowserController(this@HolderService)
            webView.setAlbumCover(null)
            webView.albumTitle = getString(R.string.app_name)
            ViewUnit.bound(this, webView)
            webView.loadUrl(RecordUnit.holder!!.uRL!!)
            webView.deactivate()
            add(webView)
            updateNotification()
        } else {
            Log.e("LoginActivity", "BrowserActivity::class.java 5")

            val toActivity = Intent(this@HolderService, BrowserActivity::class.java)
            toActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            toActivity.action = Intent.ACTION_SEND
            toActivity.putExtra(Intent.EXTRA_TEXT, RecordUnit.holder!!.uRL!!)
            startActivity(toActivity)
        }
        return START_STICKY
    }

    override fun onDestroy() {
        if (IntentUnit.isClear) {
            clear()
        }
        stopForeground(true)
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun updateNotification() {
        val notification = NotificationUnit.getHBuilder(this).build()
        startForeground(NotificationUnit.HOLDER_ID, notification)
        Toast.makeText(this, R.string.please_wait, Toast.LENGTH_LONG).show()
    }
}
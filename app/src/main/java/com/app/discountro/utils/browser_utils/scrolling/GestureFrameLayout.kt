/*
package com.app.discountro.utils.browser_utils.scrolling

import android.content.Context
import android.gesture.GestureOverlayView
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import com.app.discountro.R
import com.google.android.material.appbar.AppBarLayout


class GestureFrameLayout(context: Context, attrs: AttributeSet?) : FrameLayout(context, attrs) {
    private var viewOffset = 0
    private val gestureOverlay: CustomGestureOverlayView by bind(R.id.webGestureOverlayViewInner)

    fun setWebFrame(appBarLayout: AppBarLayout) {
        appBarLayout.addOnOffsetChangedListener{ _, verticalOffset ->
            viewOffset = verticalOffset + appBarLayout.totalScrollRange
            gestureOverlay.translationY = -viewOffset.toFloat()
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        val event = MotionEvent.obtain(ev)
        val offsetX = scrollX - gestureOverlay.left
        val offsetY = scrollY - gestureOverlay.top + viewOffset
        event.offsetLocation(offsetX.toFloat(), offsetY.toFloat())
        Log.e("dispatchTouchEvent", "offsetY:$offsetY" )
        return gestureOverlay.preDispatchTouchEvent(event) or super.dispatchTouchEvent(ev)
    }

    fun setGestureVisible(visible: Boolean) {
        gestureOverlay.isGestureVisible = visible
    }

    fun removeAllOnGestureListeners() {
        gestureOverlay.removeAllOnGestureListeners()
    }

    fun removeAllOnGesturePerformedListeners() {
        gestureOverlay.removeAllOnGesturePerformedListeners()
    }

    fun addOnGestureListener(listener: GestureOverlayView.OnGestureListener?) {
        gestureOverlay.addOnGestureListener(listener)
    }

    fun addOnGesturePerformedListener(listener: GestureOverlayView.OnGesturePerformedListener?) {
        gestureOverlay.addOnGesturePerformedListener(listener)
    }

    override fun setOnTouchListener(l: OnTouchListener?) {
        gestureOverlay.setOnTouchListener(l)
    }

    override fun isEnabled(): Boolean {
        return gestureOverlay.isEnabled
    }

    override fun setEnabled(enabled: Boolean) {
        gestureOverlay.isEnabled = enabled
        super.setEnabled(enabled)
    }

    private fun <T : View> bind(idRes: Int): Lazy<T> {
        @Suppress("UNCHECKED_CAST")
        return lazy(LazyThreadSafetyMode.NONE) { findViewById(idRes) }
    }
}
*/

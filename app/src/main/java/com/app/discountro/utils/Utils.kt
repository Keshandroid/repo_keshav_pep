package com.app.discountro.utils

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.database.Cursor
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.net.Uri
import android.provider.MediaStore
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.WebView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.app.discountro.R
import com.app.discountro.base.extentions.convertDate
import com.google.gson.Gson
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*


class Utils {

    companion object {
        fun Context.toast(msg: String) {
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
        }

        fun changeFbMobToDesktopUrl(urlString: String): String {
            return urlString.replace("m.facebook.com", "www.facebook.com")
                .replace("display=touch", "display=popup")
        }

        fun isFacebookLoginUrl(url: String): Boolean {
            Log.e("isFacebookLoginUrl", ": $url")
            return url.contains("m.facebook.com/login.php") ||
//                url.contains("m.facebook.com") ||
                    url.contains("dialog/oauth?") &&
                    url.contains("joey") &&
                    !url.contains("www.facebook.com") &&
                    !url.contains("hash") &&
                    !url.contains("story") &&
                    !url.contains("l.php")

        }

        fun isFbWebLoginUrl(url: String): Boolean {
            Log.e("isFbWebLoginUrl", ": $url")
            return url.contains("www.facebook.com/login") &&
//                url.contains("m.facebook.com") ||
//                    url.contains("dialog/oauth?") &&
//                    url.contains("joey") &&
                    !url.contains("m.facebook.com") &&
                    !url.contains("hash") &&
                    !url.contains("story") &&
                    !url.contains("l.php")
        }

        fun isDesktopSite(webView: WebView): Boolean {
            Log.e("isDesktopSite", " url:" + webView.url)
            return webView.settings.userAgentString == KeysUtils.DESKTOP_USERAGENT
        }

        fun isFbUrl(url: String): Boolean {
            Log.e("isFbUrl", ": $url")
            return url.contains("www.facebook.com") ||
                    url.contains("m.facebook.com")
        }

        fun isFbLoginCompleteUrl(url: String): Boolean {

            Log.e("isFbLoginCompleteUrl", ": $url")
            return url.contains("https://www.facebook.com/dialog/oauth?") &&
                    url.contains("client_id") &&
                    url.contains("redirect_uri") &&
                    url.contains("hash") &&
//                || url.contains("m.facebook.com") ||
//                url.contains("www.facebook.com") &&
                    !url.contains("story") &&
                    !url.contains("l.php")
        }

        //        private var googleApiClient: GoogleApiClient? = null
        val inputFormatExpiryDate = "yyyy-MM-dd"
        val outPutFormatExpiryDate = "MMMM d,yyyy"
        val inputFormatOrder = "yyyy-MM-dd HH:mm:ss"
        val outputFormatOrder = "E dd MMM yyyy,hh:mm a"

        fun getTimeZone(): String {
            val calendar = Calendar.getInstance(TimeZone.getDefault())
            val zone = calendar.timeZone
            return zone.id
        }

        fun getRealPathFromURI(context: Context, contentUri: Uri?): String? {
            var path: String? = null
            val proj = arrayOf(MediaStore.MediaColumns.DATA)
            val cursor: Cursor =
                contentUri?.let { context.contentResolver.query(it, proj, null, null, null) }!!
            if (cursor.moveToFirst()) {
                val column_index: Int = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA)
                path = cursor.getString(column_index)
            }
            cursor.close()
            return path
        }

        fun utcToLocal(dateStr: String, inputFormat: String): String {
            val df = SimpleDateFormat(inputFormat, Locale.ENGLISH)
            df.timeZone = TimeZone.getTimeZone("UTC")
            val date = df.parse(dateStr)
            df.timeZone = TimeZone.getDefault()
            val formattedDate = df.format(date)

            return formattedDate
        }

        fun shareReferralCode(context: Context, referralCode: String) {
            val appName: String = context.getString(com.app.discountro.R.string.app_name)
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.type = "text/plain"
            val shareBodyText = referralCode
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, referralCode)
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareBodyText)
            context.startActivity(
                Intent.createChooser(
                    shareIntent,
                    context.getString(R.string.share_with)
                )
            )
        }

        fun getVersionCode(context: Context): String {
            var version = "1.0.0"
            try {
                val pInfo: PackageInfo =
                    context.packageManager.getPackageInfo(context.packageName, 0)
                version = pInfo.versionName
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
            return version
        }


        private fun hasGPSDevice(context: Activity): Boolean {
            val mgr = context
                .getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val providers = mgr.allProviders
            return providers.contains(LocationManager.GPS_PROVIDER)
        }

        fun getScreenWidth(activity: Activity): Int {
            val metrics: DisplayMetrics = activity.resources.displayMetrics
            return metrics.widthPixels

        }

        fun getScreenHeight(activity: Activity): Int {
            val metrics: DisplayMetrics = activity.resources.displayMetrics
            return metrics.heightPixels
        }

        fun getCompleteAddressString(
            context: Activity,
            LATITUDE: Double,
            LONGITUDE: Double,
        ): String {
            var strAdd = ""
            val geocoder = Geocoder(context, Locale.getDefault())
            try {
                val addresses: List<Address> =
                    geocoder.getFromLocation(LATITUDE, LONGITUDE, 1)
                if (addresses != null) {
                    val returnedAddress: Address = addresses[0]
                    val strReturnedAddress = StringBuilder("")
                    for (i in 0..returnedAddress.maxAddressLineIndex) {
                        strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n")
                    }
                    strAdd = strReturnedAddress.toString()
                    Log.e("MyCurrentloctionaddress", strReturnedAddress.toString())
                } else {
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return strAdd
        }

        fun getUniqueStringEveryTime(): String? {
            val dNow = Date()
            val ft = SimpleDateFormat("yyyyMMddHHmmssSS")
            val datetime: String = ft.format(dNow)
            try {
                Thread.sleep(1)
            } catch (e: java.lang.Exception) {
            }
            return datetime
        }


        fun getDayNumberSuffix(date1: String): String {

            val inputDateFormat = "yyyy-MM-dd HH:mm:ss"
            var format: String? = null
            if (date1.endsWith("1") && !date1.endsWith("11"))
                format =
                    "d'st' MMM yyyy hh:mm a"
            else if (date1.endsWith("2") && !date1.endsWith(
                    "12"
                )
            )
                format =
                    "d'nd' MMM yyyy hh:mm a"
            else if (date1.endsWith("3") && !date1.endsWith(
                    "13"
                )
            )
                format = "d'rd'MMM yyyy hh:mm a"
            else
                format =
                    "d'th' MMM yyyy hh:mm a"

            Log.e("date", "--" + date1)
            val yourDate: String = date1.convertDate(inputDateFormat, format)
            return yourDate
        }

        /**
         * To show Toast message in app
         */
        fun showToast(
            activity: Activity,
            msg: String?,
            onPositiveButtonClickListener: DialogInterface.OnClickListener?,
        ) {
            showAlert(
                activity = activity,
                message = msg,
                onPositiveButtonClickListener = onPositiveButtonClickListener
            )
        }

        /**
         * This method is used to show default alert for app by passing message.
         */
        fun showAlert(
            activity: Activity,
            title: String? = null,
            message: String?,
            buttonText: String? = activity.getString(R.string.ok),
            onPositiveButtonClickListener: DialogInterface.OnClickListener?,
        ) {
            activity.runOnUiThread {
                activity?.let { context ->
                    if (title == null) {
                        val alertDialogTemp = AlertDialog.Builder(context)
                        alertDialogTemp.setMessage(message)
                        alertDialogTemp.setPositiveButton(buttonText, onPositiveButtonClickListener)
                        alertDialogTemp.create().show()
                    } else {
                        val alertDialogTemp = AlertDialog.Builder(context)
                        alertDialogTemp.setTitle(title)
                        alertDialogTemp.setMessage(message)
                        alertDialogTemp.setPositiveButton(buttonText, onPositiveButtonClickListener)
                        alertDialogTemp.create().show()
                    }
                }
            }
        }

        fun hideKeyboardFrom(context: Context, view: View) {
            val imm: InputMethodManager =
                context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        /**
         * This method is used to show default alert for app by passing message.
         */
        fun showAlert(
            activity: Activity,
            title: String? = null,
            message: String?,
            buttonText: String? = activity.getString(R.string.yes),
            onPositiveButtonClickListener: DialogInterface.OnClickListener?,
            buttonNoText: String? = activity.getString(R.string.no),
            onNegativeButtonClickListener: DialogInterface.OnClickListener?,
        ) {
            activity.runOnUiThread {
                activity?.let { context ->
                    if (title == null) {
                        val alertDialogTemp = AlertDialog.Builder(context)
                        alertDialogTemp.setMessage(message)
                        alertDialogTemp.setPositiveButton(buttonText, onPositiveButtonClickListener)
                        alertDialogTemp.setNegativeButton(
                            buttonNoText,
                            onNegativeButtonClickListener
                        )
                        alertDialogTemp.create().show()
                    } else {
                        val alertDialogTemp = AlertDialog.Builder(context)
                        alertDialogTemp.setTitle(title)
                        alertDialogTemp.setMessage(message)
                        alertDialogTemp.setPositiveButton(buttonText, onPositiveButtonClickListener)
                        alertDialogTemp.setNegativeButton(
                            buttonNoText,
                            onNegativeButtonClickListener
                        )
                        alertDialogTemp.create().show()
                    }
                }
            }
        }

//        @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
//        fun getDeviceCountryCode(context: Context): String {
//            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                Calendar.getInstance().timeZone.toZoneId().toString()
//            } else {
//                val locale = Locale.forLanguageTag(Locale.getDefault().language);
//                val cal = Calendar.getInstance(locale);
//                val timeZone = cal.timeZone;
//                return timeZone.id;
//            }
//        }

        fun getDeviceCountryCode(context: Context): String {
            return context.resources.configuration.locale.country
        }

        fun convertToJson(mapData: Map<String, Any>?): String? {
            val gsn = Gson()
            return gsn.toJson(mapData)
        }

        fun <T : Serializable> getSerializable(activity: Activity, name: String, model: Class<T>): T
        {
            // TODO When up grade the android 33 (Tiramisu) then uncomment this code and comment below code
            /*return if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU)
                activity.intent.getSerializableExtra(name, model)!!
            else
                activity.intent.getSerializableExtra(name) as T*/

            // TODO comment this when update to 33 version of app.
            return activity.intent.getSerializableExtra(name) as T
        }

        fun convertUtcToLocalTime(date: Date): String {
            val simpleDateFormat = SimpleDateFormat("yyyy-MMM-dd", Locale.getDefault())
            return simpleDateFormat.format(date)
        }
    }
}
package com.app.discountro.utils.browser_utils

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.app.discountro.common.sqlite.browser_db.RecordAction
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.util.*

class Javascript(private val context: Context?) {
    fun isWhite(url: String?): Boolean {
        for (domain: String? in whitelistJS) {
            if (url != null && url.contains((domain)!!)) {
                return true
            }
        }
        return false
    }

    @Synchronized
    fun addDomain(domain: String) {
        val action = RecordAction(context)
        action.open(true)
        action.addDomainJS(domain)
        action.close()
        whitelistJS.add(domain)
    }

    @Synchronized
    fun removeDomain(domain: String) {
        val action = RecordAction(context)
        action.open(true)
        action.deleteDomainJS(domain)
        action.close()
        whitelistJS.remove(domain)
    }

    @Synchronized
    fun clearDomains() {
        val action = RecordAction(context)
        action.open(true)
        action.clearDomainsJS()
        action.close()
        whitelistJS.clear()
    }

    companion object {
        private val FILE = "javaHosts.txt"
        private val hostsJS: MutableSet<String> = HashSet()
        private val whitelistJS: MutableList<String> = ArrayList()

        @SuppressLint("ConstantLocale")
        private val locale = Locale.getDefault()
        private fun loadHosts(context: Context?) {
            val thread = Thread {
                val manager = context?.assets
                try {
                    val reader = BufferedReader(InputStreamReader(manager?.open(FILE)))
                    var line: String
                    while ((reader.readLine().also { line = it }) != null) {
                        hostsJS.add(line.lowercase(locale))
                    }
                } catch (i: IOException) {
                    Log.w("Browser", "Error loading hosts")
                }
            }
            thread.start()
        }

        @Synchronized
        private fun loadDomains(context: Context?) {
            val action = RecordAction(context)
            action.open(false)
            whitelistJS.clear()
            whitelistJS.addAll(action.listDomainsJS())
            action.close()
        }
    }

    init {
        if (hostsJS.isEmpty()) {
            loadHosts(context)
        }
        loadDomains(context)
    }
}
package com.app.discountro.utils.browser_utils

import android.content.Context
import android.graphics.Bitmap
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.app.discountro.R
import com.app.discountro.common.album_service.AlbumController
import com.app.discountro.common.my_interface.BrowserController


internal class Album(
    private val context: Context,
    private val albumController: AlbumController?,
    private var browserController: BrowserController?
) {

    var albumView: View? = null
        private set
    private var cardView: CardView? = null
    private var albumCover: ImageView? = null
    private var albumTitle: TextView? = null

    fun setAlbumCover(bitmap: Bitmap?) {
        albumCover!!.setImageBitmap(bitmap)
    }

    fun getAlbumTitle(): String {
        return albumTitle!!.text.toString()
    }

    fun setAlbumTitle(title: String?) {
        albumTitle!!.text = title
    }

    fun setBrowserController(browserController: BrowserController?) {
        this.browserController = browserController
    }

  /*  @SuppressLint("InflateParams")
    private fun initUI() {
        albumView = LayoutInflater.from(context).inflate(R.layout.album, null, false)
        albumView?.setOnClickListener {
            browserController?.showAlbum(albumController)
            browserController?.hideOverview()
        }
        albumView?.setOnLongClickListener {
            browserController?.removeAlbum(albumController)
            true
        }
        val albumClose = albumView?.findViewById<ImageView>(R.id.album_close)
        cardView = albumView?.findViewById(R.id.cardView)
        albumCover = albumView?.findViewById(R.id.album_cover)
        albumTitle = albumView?.findViewById(R.id.album_title)
        albumClose?.setOnClickListener { v: View? ->
            browserController?.removeAlbum(
                albumController
            )
        }
    }*/

    fun activate() {
  //      cardView!!.setBackgroundResource(R.drawable.album_shape_accent)
        albumTitle!!.text = context.getString(R.string.app_name)
     //   albumTitle!!.setTextColor(context.resources.getColor(R.color.white_color))
      //  val sp = PreferenceManager.getDefaultSharedPreferences(context)
    //    val color = sp.getInt("vibrantColor", 0)
   //     val shape = context.getDrawable(R.drawable.album_shape_accent)
    //    shape?.setTint(color)
      //  shape!!.setTintMode(PorterDuff.Mode.SRC_OVER)
    }

    fun deactivate() {
      //  cardView!!.setBackgroundResource(R.drawable.album_shape_gray)
      //  albumTitle!!.setTextColor(context.resources.getColor(R.color.color_black))
    }

  /*  init {
        initUI()
    }*/
}
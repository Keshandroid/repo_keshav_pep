package com.app.discountro.utils

class FireStorageKeys {
    companion object {
        /**
         *USER TABLE
         */
        const val KEY_USER_TABLE = "user"
        const val KEYUT_ID_USER = "id_user"
        const val KEYUT_ID_NAME = "user_name"
        const val KEYUT_SOCIAL_TYPE = "social_type"
        const val KEYUT_USER_PUBLIC_TOKEN = "user_public_token"
        const val KEYUT_P_TOKEN = "user_public_token"
        const val KEYUT_EMAIL = "email"
        const val KEYUT_PWD = "password"
        const val KEYUT_DB_REG = "date_registered"
        const val KEYUT_ACTIVE = "active"
        const val KEYUT_FAVORITE_STORES = "favorite_stores"
        const val KEYUT_NOTIFICATION = "notification"
        const val KEYUT_FEATURED = "featured"
        const val KEYUT_PRODUCTS = "products"
        const val KEYUT_FAVORITE_PRODUCT = "favorite_products"
        const val KEYUT_LIST = "list"
        const val KEYUT_PURCHASED_PRODUCT = "purchased_product"
        const val KEYUT_PRODUCT_LIST = "productsList"


        /**
         *LANGUAGE TABLE
         */

    }
}
package com.app.discountro.utils.browser_utils

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import com.app.discountro.common.sqlite.browser_db.RecordAction
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.URI
import java.net.URISyntaxException
import java.util.*

class AdBlock(private val context: Context) {
    fun isWhite(url: String?): Boolean {
        for (domain: String? in whitelist) {
            if (url != null && url.contains((domain)!!)) {
                return true
            }
        }
        return false
    }

    fun isAd(url: String): Boolean {
        val domain: String
        try {
            domain = getDomain(url)
        } catch (u: URISyntaxException) {
            return false
        }
        return hosts.contains(domain.lowercase(locale))
    }

    @Synchronized
    fun addDomain(domain: String) {
        val action = RecordAction(context)
        action.open(true)
        action.addDomain(domain)
        action.close()
        whitelist.add(domain)
    }

    @Synchronized
    fun removeDomain(domain: String) {
        val action = RecordAction(context)
        action.open(true)
        action.deleteDomain(domain)
        action.close()
        whitelist.remove(domain)
    }

    @Synchronized
    fun clearDomains() {
        val action = RecordAction(context)
        action.open(true)
        action.clearDomains()
        action.close()
        whitelist.clear()
    }

    companion object {
        private val FILE = "hosts.txt"
        private val hosts: MutableSet<String> = HashSet()
        private val whitelist: MutableList<String> = ArrayList()

        @SuppressLint("ConstantLocale")
        private val locale = Locale.getDefault()
        private fun loadHosts(context: Context) {
            val thread = Thread(Runnable {
                val manager = context.assets
                try {
                    val reader = BufferedReader(InputStreamReader(manager.open(FILE)))
                    var line: String?
                    while ((reader.readLine().also {
                            line = it
                    }) != null) {
                        hosts.add(line!!.lowercase(locale))
                    }
                } catch (i: IOException) {
                    Log.w("Browser", "Error loading hosts", i)
                }
            })
            thread.start()
        }

        @Synchronized
        private fun loadDomains(context: Context) {
            val action = RecordAction(context)
            action.open(false)
            whitelist.clear()
            whitelist.addAll(action.listDomains())
            action.close()
        }

        @Throws(URISyntaxException::class)
        private fun getDomain(url: String): String {
            var url = url
            url = url.lowercase(locale)
            val index = url.indexOf('/', 8) // -> http://(7) and https://(8)
            if (index != -1) {
                url = url.substring(0, index)
            }
            val uri = URI(url)
            val domain = uri.host ?: return url
            return if (domain.startsWith("www.")) domain.substring(4) else domain
        }
    }

    init {
        if (hosts.isEmpty()) {
            loadHosts(context)
        }
        loadDomains(context)
    }
}
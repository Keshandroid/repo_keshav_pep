package com.app.discountro.utils.browser_utils.my_object

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.app.discountro.R
import com.app.discountro.ui.home.view.BrowserActivity
 import com.app.discountro.utils.browser_utils.HolderService
import com.app.discountro.utils.browser_utils.my_object.BrowserContainer.clear
import com.app.discountro.utils.browser_utils.my_object.IntentUnit.isClear


object NotificationUnit {
    const val HOLDER_ID = 0x65536
    fun getHBuilder(context: Context): NotificationCompat.Builder {
        val builder: NotificationCompat.Builder
        val stopNotificationReceiver: BroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                val toHolderService = Intent(context, HolderService::class.java)
                toHolderService.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                isClear = false
                context.stopService(toHolderService)
                clear()
            }
        }
        val intentFilter = IntentFilter("stopNotification")
        context.registerReceiver(stopNotificationReceiver, intentFilter)
        val stopNotification = Intent("stopNotification")
        val stopNotificationPI = PendingIntent.getBroadcast(
            context,
            0,
            stopNotification,
            PendingIntent.FLAG_CANCEL_CURRENT or PendingIntent.FLAG_IMMUTABLE
        )
        val action_UN = NotificationCompat.Action.Builder(
            R.drawable.icon_earth,
            context.getString(R.string.toast_closeNotification),
            stopNotificationPI
        ).build()
        val mNotificationManager =
            context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val CHANNEL_ID = "browser_not" // The id of the channel.
            val name: CharSequence =
                context.getString(R.string.app_name) // The user-visible name of the channel.
            val mChannel =
                NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_HIGH)
            assert(mNotificationManager != null)
            mNotificationManager.createNotificationChannel(mChannel)
            NotificationCompat.Builder(context, CHANNEL_ID)
        } else {
            NotificationCompat.Builder(context)
        }
        builder.setCategory(Notification.CATEGORY_MESSAGE)
        builder.setSmallIcon(R.drawable.ic_notification)
        builder.setContentTitle(context.getString(R.string.notification_content_holderTitle))
        builder.setContentText(context.getString(R.string.notification_content_holder))
        builder.color = ContextCompat.getColor(context, R.color.red_primary)
        builder.setAutoCancel(true)
        builder.priority = Notification.PRIORITY_HIGH
        builder.setVibrate(LongArray(0))
        builder.addAction(action_UN)
        val toActivity = Intent(context, BrowserActivity::class.java)
        toActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        var pin: PendingIntent? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            pin = PendingIntent.getActivity(context, 0, toActivity, PendingIntent.FLAG_IMMUTABLE)
        }
        builder.setContentIntent(pin)
        return builder
    }
}
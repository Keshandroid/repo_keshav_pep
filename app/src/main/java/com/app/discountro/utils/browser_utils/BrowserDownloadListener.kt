package com.app.discountro.utils.browser_utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.webkit.DownloadListener
import android.webkit.URLUtil
import android.widget.Button
import android.widget.TextView
import com.app.discountro.R
import com.app.discountro.utils.browser_utils.my_object.BrowserUnit
import com.app.discountro.utils.browser_utils.my_object.HelperUnit
import com.app.discountro.utils.browser_utils.my_object.IntentUnit
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog

class BrowserDownloadListener(private val context: Context) : DownloadListener {
    override fun onDownloadStart(
        url: String,
        userAgent: String,
        contentDisposition: String,
        mimeType: String,
        contentLength: Long
    ) {
        val holder = IntentUnit.context
        if (holder !is Activity) {
            BrowserUnit.download(context, url, contentDisposition, mimeType)
            return
        }
        val text = holder.getString(R.string.dialog_title_download) + " - " + URLUtil.guessFileName(
            url,
            contentDisposition,
            mimeType
        )
        val dialog = BottomSheetDialog(holder)
        val dialogView = View.inflate(holder, R.layout.dialog_action, null)
        val textView = dialogView.findViewById<TextView>(R.id.dialog_text)
        textView.text = text
        val action_ok = dialogView.findViewById<Button>(R.id.action_ok)
        action_ok.setOnClickListener {
            BrowserUnit.download(holder, url, contentDisposition, mimeType)
            dialog.cancel()
        }
        val action_cancel = dialogView.findViewById<Button>(R.id.action_cancel)
        action_cancel.setOnClickListener { dialog.cancel() }
        dialog.setContentView(dialogView)
        dialog.show()
        HelperUnit.setBottomSheetBehavior(dialog, dialogView, BottomSheetBehavior.STATE_EXPANDED)
    }
}
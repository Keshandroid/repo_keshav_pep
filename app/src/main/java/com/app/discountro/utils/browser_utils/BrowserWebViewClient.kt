package com.app.discountro.utils.browser_utils

import android.annotation.TargetApi
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.Bitmap
import android.graphics.drawable.Icon
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.preference.PreferenceManager
import android.util.Base64
import android.util.Log
import android.view.View
import android.webkit.*
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.app.discountro.R
import com.app.discountro.common.sqlite.browser_db.RecordAction
import com.app.discountro.common.sqlite.record.Record
import com.app.discountro.ui.home.view.BrowserActivity
import com.app.discountro.ui.home.viewmodel.BrowserWebView
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.SharedPrefsUtils
import com.app.discountro.utils.Utils
import com.app.discountro.utils.browser_utils.my_object.BrowserToast
import com.app.discountro.utils.browser_utils.my_object.BrowserUnit
import com.app.discountro.utils.browser_utils.my_object.HelperUnit
import com.app.discountro.utils.browser_utils.my_object.IntentUnit
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputLayout
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.net.MalformedURLException
import java.net.URISyntaxException
import java.net.URL

// add swipe refresh object in primary constructor
class BrowserWebViewClient(private val swipeView : SwipeRefreshLayout?=null , private val browserWebView: BrowserWebView, val isIncoTab: Boolean) :
    WebViewClient() {
    private val context: Context
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor
    private val sp: SharedPreferences
    private val adBlock: AdBlock
    private val cookie: Cookie
    private var white: Boolean
    private var test: String = "nothing"
    private var isIncoWebView = false
    val TAG = javaClass.simpleName

    fun updateWhite(white: Boolean) {
        this.white = white
    }

    private var enable: Boolean
    fun enableAdBlock(enable: Boolean) {
        this.enable = enable
    }

    override fun onPageFinished(view: WebView, url: String) {
        Log.e(TAG, "onPageFinished:userAgentString: " + view.settings.userAgentString + "url:$url")
        Log.e(TAG, "onPageFinished:isDesktopSite: " + Utils.isDesktopSite(webView = view))
        Log.e(TAG, "onPageFinished:title: " + view.title)
      // stop swipe animation when page load finish
        swipeView?.isRefreshing = false
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        editor = sharedPreferences.edit()
//        editor.remove("EditFavicon")
        editor.putString("EditTitle", view.title)
        editor.putString("EditUrl", url)
        editor.putString("EditFavicon", view.favicon?.let { encodeTobase64(it) })
        editor.apply()


        val currentIcon = (context as BrowserActivity).findViewById<ImageView>(R.id.currentIcon) as ImageView
        val ClMainTb = (context as BrowserActivity).findViewById<ConstraintLayout>(R.id.ClMainTb) as ConstraintLayout
        val etFullName = (context as BrowserActivity).findViewById<LinearLayout>(R.id.etFullName) as LinearLayout

        if (url.startsWith("https://www.google.com")) {
//            val r = Runnable {
                currentIcon.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_web_icon));
//            }
//            Handler(Looper.getMainLooper()).postDelayed(r, 1000)
        }else{
            val r1 = Runnable {
                currentIcon.setImageBitmap(view.favicon)
            }
            Handler(Looper.getMainLooper()).postDelayed(r1, 1000)
        }



        Log.e("Narola--", "onPageFinished: kn-url => " + url)
        Log.e("Kirtan--", "onPageFinished: kn-favicon => " + view.favicon)

        if (Utils.isFbWebLoginUrl(url)) {
//            view.evaluateJavascript("javascript:document.getElementById('email').focus();", null)
            view.evaluateJavascript("javascript:document.body.style.zoom = \"220%\";", null)
        } else if (Utils.isDesktopSite(webView = view) && !Utils.isFbUrl(url) /* && !Utils.isFbWebLoginUrl(url) && !Utils.isFacebookLoginUrl(url)*/) {
            // TODO: It's Desktop Agent So Refresh Page For Mobile View
            view.loadUrl(url)
//            view.settings.userAgentString =
//                SharedPrefsUtils.getStringPreference(KeysUtils.keyDefaultMobUserAgent)
        }

        super.onPageFinished(view, url)
        val cookieManager = CookieManager.getInstance()

        Log.e(TAG, "onPageFinished: isIncoTab:$isIncoTab")
        cookieManager.setAcceptCookie(!isIncoTab)

        /*  if (isIncoTab) {
              cookieManager.setAcceptCookie(false)
          } else {
  //            cookieManager.flush()
  //            cookieManager.removeAllCookies { }
              cookieManager.setAcceptCookie(true)
          }*/

//        val cookieManager = CookieManager.getInstance()
        val myCookies = cookieManager.getCookie(url)
        Log.e(TAG, "onPageFinished:: url:: $url # & # myCookie:: $myCookies")

        /*  CookieManager.getInstance().getCookie(url)
              ?.let { sIt ->
                  val uri = URI(url)
                  HttpCookie.parse(sIt).forEach {
                      java.net.CookieManager().cookieStore.add(uri, it)
                  }
                  test = cookieManager.getCookie(url)
              }
          Log.e("kjbgjdsbfg", "onPageFinished: $test")*/

//        cookieManager.removeSessionCookies()

        /* val webVu = view
         webVu.clearCache(true)
         webVu.clearFormData()
         webVu.clearHistory()
         webVu.clearSslPreferences()

         val cookieManager = CookieManager.getInstance()
         cookieManager.flush()
         cookieManager.removeSessionCookies {  }*/
//        cookieManager.removeAllCookies { }
//
//        BrowserUnit.clearCookie()

//        WebStorage.getInstance().deleteAllData()

        var shortcutManager: ShortcutManager? = null
        val title = browserWebView.title
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            shortcutManager = context.getSystemService(ShortcutManager::class.java)
        }
        if (sp.getBoolean("saveHistory", true) && !isIncoTab) {

            val action = RecordAction(context)
            action.open(true)
            if (action.checkHistory(url)) {
                action.deleteHistoryOld(url)
                action.addHistory(Record(browserWebView.title, url, System.currentTimeMillis()))
            } else {
                action.addHistory(Record(browserWebView.title, url, System.currentTimeMillis()))
            }
            action.close()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1 && !title!!.isEmpty() && !url.isEmpty()) {
                try {
                    if (sp.getInt("shortcut_number", 0) == 0) {
                        val shortcut = ShortcutInfo.Builder(context, "0")
                            .setShortLabel(title)
                            .setLongLabel(title)
                            .setIcon(Icon.createWithResource(context, R.drawable.qc_history))
                            .setIntent(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                            .build()
                        shortcutManager!!.addDynamicShortcuts(listOf(shortcut))
                        sp.edit().putInt("shortcut_number", 1).apply()
                    } else {
                        val shortcut = ShortcutInfo.Builder(context, "1")
                            .setShortLabel(title)
                            .setLongLabel(title)
                            .setIcon(Icon.createWithResource(context, R.drawable.qc_history))
                            .setIntent(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                            .build()
                        shortcutManager!!.addDynamicShortcuts(listOf(shortcut))
                        sp.edit().putInt("shortcut_number", 0).apply()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                shortcutManager!!.removeAllDynamicShortcuts()
            }
        }
        if (browserWebView.isForeground) {
            browserWebView.invalidate()
        } else {
            browserWebView.postInvalidate()
        }
    }

    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
        val uri = Uri.parse(url)
        return handleUri(view, uri)
    }

    @TargetApi(Build.VERSION_CODES.N)
    override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
        val uri = request.url
        return handleUri(view, uri)
    }

    // method for bitmap to base64
    fun encodeTobase64(image: Bitmap): String? {
        val baos = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()
        val imageEncoded = Base64.encodeToString(b, Base64.DEFAULT)
//        Log.d("Image Log:", imageEncoded)
        return imageEncoded
    }

    private fun handleUri(webView: WebView, uri: Uri): Boolean {
        val url = uri.toString()
        Log.e(TAG, "handleUri:url: $url")
        // Based on some condition you need to determine if you are going to load the url
        // in your web view itself or in a browser.
        // You can use `host` or `scheme` or any part of the `uri` to decide.
        // open web links as usual
        if (url.startsWith("http")) {

            var hostName: String? = url
            if (!url.equals("")) {
                if (url.startsWith("http") || url.startsWith("https")) {
                    try {
                        val netUrl = URL(url)
                        val host: String = netUrl.getHost()
                        hostName = if (host.startsWith("www")) {
                            host.substring("www".length + 1)
                        } else {
                            host
                        }
                    } catch (e: MalformedURLException) {
                        hostName = url
                    }
                } else if (url.startsWith("www")) {
                    hostName = url.substring("www".length + 1)
                }
            } else {
                ""
            }

            webView.loadUrl(url, browserWebView.requestHeaders)
            (context as BrowserActivity).setSearchBarData(webView)
            if (Utils.isFacebookLoginUrl(url)) {
                Log.e(TAG, "handleUri: isFbLoginUrl")
                val fbDesktopUrl = Utils.changeFbMobToDesktopUrl(url)
//                webView.settings.userAgentString = KeysUtils.DESKTOP_USERAGENT
                webView.loadUrl(fbDesktopUrl)

            } else if (Utils.isFbLoginCompleteUrl(url)) {
                Log.e(TAG, "handleUri: fbLogin Complete.")
//                webView.settings.userAgentString =
//                    SharedPrefsUtils.getStringPreference(KeysUtils.keyDefaultMobUserAgent)
            }
            return true
        }

        //try to find browse activity to handle uri
        val parsedUri = Uri.parse(url)
        val packageManager = context.packageManager
        val browseIntent = Intent(Intent.ACTION_VIEW).setData(parsedUri)
        if (browseIntent.resolveActivity(packageManager) != null) {
            context.startActivity(browseIntent)
            return true
        }
        //if not activity found, try to parse intent://
        if (url.startsWith("intent:")) {
            try {
                val intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME)
                if (intent.resolveActivity(context.packageManager) != null) {
                    try {
                        context.startActivity(intent)
                    } catch (e: Exception) {
                        BrowserToast.show(context, R.string.toast_load_error)
                    }
                    return true
                }
                //try to find fallback url
                val fallbackUrl = intent.getStringExtra("browser_fallback_url")
                if (fallbackUrl != null) {
                    webView.loadUrl(fallbackUrl)
                    return true
                }
                //invite to install
                val marketIntent =
                    Intent(Intent.ACTION_VIEW).setData(Uri.parse("market://details?id=" + intent.getPackage()))
                if (marketIntent.resolveActivity(packageManager) != null) {
                    context.startActivity(marketIntent)
                    return true
                }
            } catch (e: URISyntaxException) {
                //not an intent uri
                return false
            }
        }
        white = adBlock.isWhite(url)
        return true //do nothing in other cases
    }

    override fun shouldInterceptRequest(view: WebView, url: String): WebResourceResponse? {
        if (enable && !white && adBlock.isAd(url)) {
            return WebResourceResponse(
                BrowserUnit.MIME_TYPE_TEXT_PLAIN,
                BrowserUnit.URL_ENCODING,
                ByteArrayInputStream("".toByteArray())
            )
        }
        if (!sp.getBoolean(context.getString(R.string.sp_cookies), true)) {
            if (cookie.isWhite(url)) {
                val manager = CookieManager.getInstance()
                manager.getCookie(url)
//                manager.setAcceptCookie(true)
            } else {
                val manager = CookieManager.getInstance()
                manager.setAcceptCookie(false)
            }
        }
        return super.shouldInterceptRequest(view, url)
    }

    override fun shouldInterceptRequest(
        view: WebView,
        request: WebResourceRequest,
    ): WebResourceResponse? {
        if (enable && !white && adBlock.isAd(request.url.toString())) {
            return WebResourceResponse(
                BrowserUnit.MIME_TYPE_TEXT_PLAIN,
                BrowserUnit.URL_ENCODING,
                ByteArrayInputStream("".toByteArray())
            )
        }
        if (!sp.getBoolean(context.getString(R.string.sp_cookies), true)) {
            if (cookie.isWhite(request.url.toString())) {
                val manager = CookieManager.getInstance()
                manager.getCookie(request.url.toString())
//                manager.setAcceptCookie(true)
            } else {
                val manager = CookieManager.getInstance()
                manager.setAcceptCookie(false)
            }
        }
        return super.shouldInterceptRequest(view, request)
    }

    override fun onFormResubmission(view: WebView, doNotResend: Message, resend: Message) {
        val holder = IntentUnit.context as? Activity ?: return
        val dialog = BottomSheetDialog(holder)
        val dialogView = View.inflate(holder, R.layout.dialog_action, null)
        val textView = dialogView.findViewById<TextView>(R.id.dialog_text)
        textView.setText(R.string.dialog_content_resubmission)
        val action_ok = dialogView.findViewById<Button>(R.id.action_ok)
        action_ok.setOnClickListener {
            resend.sendToTarget()
            dialog.cancel()
        }
        val action_cancel = dialogView.findViewById<Button>(R.id.action_cancel)
        action_cancel.setOnClickListener {
            doNotResend.sendToTarget()
            dialog.cancel()
        }
        dialog.setContentView(dialogView)
        dialog.show()
    }

    override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
        /*val holder = IntentUnit.context as? Activity ?: return
        val dialog = BottomSheetDialog(holder)
        val dialogView = View.inflate(holder, R.layout.dialog_action, null)
        val textView = dialogView.findViewById<TextView>(R.id.dialog_text)
        textView.setText(R.string.dialog_content_ssl_error)
        val action_ok = dialogView.findViewById<Button>(R.id.action_ok)
        action_ok.setOnClickListener {
            handler.proceed()
            dialog.cancel()
        }
        val action_cancel = dialogView.findViewById<Button>(R.id.action_cancel)
        action_cancel.setOnClickListener {
            handler.cancel()
            dialog.cancel()
        }
        dialog.setContentView(dialogView)
        dialog.show()
        HelperUnit.setBottomSheetBehavior(dialog, dialogView, BottomSheetBehavior.STATE_EXPANDED)*/
    }

    override fun onReceivedHttpAuthRequest(
        view: WebView,
        handler: HttpAuthHandler,
        host: String,
        realm: String,
    ) {
        val holder = IntentUnit.context as? Activity ?: return
        val dialog = BottomSheetDialog(holder)
        val dialogView = View.inflate(holder, R.layout.dialog_edit_bookmark, null)
        val pass_userNameET = dialogView.findViewById<EditText>(R.id.pass_userName)
        val pass_userPWET = dialogView.findViewById<EditText>(R.id.pass_userPW)
        val login_title = dialogView.findViewById<TextInputLayout>(R.id.login_title)
        login_title.visibility = View.GONE
        val action_ok = dialogView.findViewById<Button>(R.id.action_ok)
        action_ok.setOnClickListener {
            val user = pass_userNameET.text.toString().trim { it <= ' ' }
            val pass = pass_userPWET.text.toString().trim { it <= ' ' }
            handler.proceed(user, pass)
            dialog.cancel()
        }
        val action_cancel = dialogView.findViewById<Button>(R.id.action_cancel)
        action_cancel.setOnClickListener {
            handler.cancel()
            dialog.cancel()
        }
        dialog.setContentView(dialogView)
        dialog.show()
        HelperUnit.setBottomSheetBehavior(dialog, dialogView, BottomSheetBehavior.STATE_EXPANDED)
    }

    init {
        context = browserWebView.context
        sp = PreferenceManager.getDefaultSharedPreferences(context)
        adBlock = browserWebView.adBlock!!
        cookie = browserWebView.cookieHosts!!
        white = false
        enable = true
    }
}
package com.app.discountro.utils

enum class TabEnums(val staringVal: String, val posVal: Int) {
    ME("GetAllUser", 0),
    MAN_DO_BK("ManDoBkShop", 1),
    NEAR_BY("NearByShop", 2),
    MY_ORDER("MyOrder", 3),
}

enum class NotificationTypesEnums(val stringVal: String, val posVal: Int) {
    NOTIFICATION_REMINDER("purchase_reminder", 0),
    NOTIFICATION_REVIEW("purchase_review", 1),
    NOTIFICATION_GENERAL("general", 2),
    NOTIFICATION_PROMOTION("promotion", 3),
}
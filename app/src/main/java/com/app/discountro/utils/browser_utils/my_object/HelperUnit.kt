package com.app.discountro.utils.browser_utils.my_object

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.drawable.Icon
import android.net.Uri
import android.os.Build
import android.preference.PreferenceManager
import android.provider.Settings
import android.text.Html
import android.text.SpannableString
import android.text.TextUtils
import android.text.util.Linkify
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.app.discountro.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.android.material.bottomsheet.BottomSheetDialog
import java.text.SimpleDateFormat
import java.util.*

object HelperUnit {
    private const val REQUEST_CODE_ASK_PERMISSIONS = 123
    private const val REQUEST_CODE_ASK_PERMISSIONS_1 = 1234
    lateinit var sp: SharedPreferences

    fun isOnFistCol(index: Int): Boolean {
//        return index % 2 == 0
        return true
    }

    fun View?.removeSelf() {
        this ?: return
        val parentView = parent as? ViewGroup ?: return
        parentView.removeView(this)
    }

    fun grantPermissionsStorage(activity: Activity?) {
        if (Build.VERSION.SDK_INT >= 23) {
            val hasWRITE_EXTERNAL_STORAGE =
                activity!!.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            if (hasWRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
                if (!activity.shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    val bottomSheetDialog = BottomSheetDialog(activity)
                    val dialogView = View.inflate(activity, R.layout.dialog_action, null)
                    val textView = dialogView.findViewById<TextView>(R.id.dialog_text)
                    textView.setText(R.string.toast_permission_sdCard)
                    val action_ok = dialogView.findViewById<Button>(R.id.action_ok)
                    action_ok.setOnClickListener {
                        activity.requestPermissions(
                            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                            REQUEST_CODE_ASK_PERMISSIONS
                        )
                        bottomSheetDialog.cancel()
                    }
                    val action_cancel = dialogView.findViewById<Button>(R.id.action_cancel)
                    action_cancel.setText(R.string.setting_label)
                    action_cancel.setOnClickListener {
                        val intent = Intent()
                        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                        val uri = Uri.fromParts("package", activity.packageName, null)
                        intent.data = uri
                        activity.startActivity(intent)
                        bottomSheetDialog.cancel()
                    }
                    bottomSheetDialog.setContentView(dialogView)
                    bottomSheetDialog.show()
                    setBottomSheetBehavior(
                        bottomSheetDialog,
                        dialogView,
                        BottomSheetBehavior.STATE_EXPANDED
                    )
                }
            }
        }
    }

    fun grantPermissionsLoc(activity: Activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            val hasACCESS_FINE_LOCATION =
                activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)
            if (hasACCESS_FINE_LOCATION != PackageManager.PERMISSION_GRANTED) {
                val bottomSheetDialog = BottomSheetDialog(activity)
                val dialogView = View.inflate(activity, R.layout.dialog_action, null)
                val textView = dialogView.findViewById<TextView>(R.id.dialog_text)
                textView.setText(R.string.toast_permission_loc)
                val action_ok = dialogView.findViewById<Button>(R.id.action_ok)
                action_ok.setOnClickListener {
                    activity.requestPermissions(
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        REQUEST_CODE_ASK_PERMISSIONS_1
                    )
                    bottomSheetDialog.cancel()
                }
                val action_cancel = dialogView.findViewById<Button>(R.id.action_cancel)
                action_cancel.setText(R.string.setting_label)
                action_cancel.setOnClickListener {
                    val intent = Intent()
                    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                    val uri = Uri.fromParts("package", activity.packageName, null)
                    intent.data = uri
                    activity.startActivity(intent)
                    bottomSheetDialog.cancel()
                }
                bottomSheetDialog.setContentView(dialogView)
                bottomSheetDialog.show()
                setBottomSheetBehavior(
                    bottomSheetDialog,
                    dialogView,
                    BottomSheetBehavior.STATE_EXPANDED
                )
            }
        }
    }

    @JvmStatic
    fun applyTheme(context: Context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context)
        if (sp.getBoolean("sp_lightUI", false)) {
            context.setTheme(R.style.AppTheme)
        } else {
            context.setTheme(R.style.AppTheme_dark)
        }
    }

    fun setFavorite(context: Context?, url: String?) {
        sp = PreferenceManager.getDefaultSharedPreferences(context)
        sp.edit().putString("favoriteURL", url).apply()
        BrowserToast.show(context!!, R.string.toast_fav)
    }

    fun setBottomSheetBehavior(dialog: BottomSheetDialog?, view: View, beh: Int) {
        val mBehavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(view.parent as View)
        mBehavior.state = beh
        mBehavior.addBottomSheetCallback(object : BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                /* try {
                     Handler().postDelayed({ dialog!!.cancel() }, 250)
                 } catch (e: Exception) {
                     Log.w("Browser", "Error cancel dialog")
                 }*/

//                if (newState === BottomSheetBehavior.STATE_DRAGGING) {
//                    mBehavior.state =
//                        BottomSheetBehavior.STATE_EXPANDED
//                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                Log.d("onSlide: ",slideOffset.toString())
            }
        })
    }

    fun createShortcut(context: Context, title: String?, url: String?) {
        val i = Intent()
        i.action = Intent.ACTION_VIEW
        i.data = Uri.parse(url)
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) { // code for adding shortcut on pre oreo device
            val installer = Intent()
            installer.putExtra("android.intent.extra.shortcut.INTENT", i)
            installer.putExtra("android.intent.extra.shortcut.NAME", title)
            installer.putExtra(
                Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(
                    context.applicationContext,
                    R.drawable.qc_bookmarks
                )
            )
            installer.action = "com.android.launcher.action.INSTALL_SHORTCUT"
            context.sendBroadcast(installer)
        } else {
            val shortcutManager = context.getSystemService(
                ShortcutManager::class.java
            )!!
            if (shortcutManager.isRequestPinShortcutSupported) {
                val pinShortcutInfo = ShortcutInfo.Builder(context, url)
                    .setShortLabel(title!!)
                    .setLongLabel(title)
                    .setIcon(Icon.createWithResource(context, R.drawable.qc_bookmarks))
                    .setIntent(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                    .build()
                shortcutManager.requestPinShortcut(pinShortcutInfo, null)
            } else {
                println("failed_to_add")
            }
        }
    }

    /*fun show_dialogHelp(context: Context) {
        val bottomSheetDialog = BottomSheetDialog(context)
        val dialogView = View.inflate(context, R.layout.dialog_help, null)
        val dialogHelp_tip = dialogView.findViewById<ImageButton>(R.id.dialogHelp_tip)
        val dialogHelp_overview = dialogView.findViewById<ImageButton>(R.id.dialogHelp_overview)
        val dialogHelp_gestures = dialogView.findViewById<ImageButton>(R.id.dialogHelp_gestures)
        val dialogHelp_filter = dialogView.findViewById<ImageButton>(R.id.dialogHelp_filter)
        val dialogHelp_iv = dialogView.findViewById<ImageView>(R.id.dialogHelp_iv)
        val dialogHelp_tv_title = dialogView.findViewById<TextView>(R.id.dialogHelp_title)
        val dialogHelp_tv_text = dialogView.findViewById<TextView>(R.id.dialogHelp_tv)
        dialogHelp_tv_title.text =
            textSpannable(context.resources.getString(R.string.dialogHelp_tipTitle))
        dialogHelp_tv_text.text =
            textSpannable(context.resources.getString(R.string.dialogHelp_tipText))
        dialogHelp_iv.visibility = View.GONE
        dialogHelp_tip.setOnClickListener {
            dialogHelp_iv.visibility = View.GONE
            dialogHelp_tv_title.text =
                textSpannable(context.resources.getString(R.string.dialogHelp_tipTitle))
            dialogHelp_tv_text.text =
                textSpannable(context.resources.getString(R.string.dialogHelp_tipText))
        }
        dialogHelp_overview.setOnClickListener {
            dialogHelp_iv.visibility = View.VISIBLE
            dialogHelp_iv.setImageDrawable(context.resources.getDrawable(R.drawable.help_start))
            dialogHelp_tv_title.text =
                textSpannable(context.resources.getString(R.string.dialogHelp_overviewTitle))
            dialogHelp_tv_text.text =
                textSpannable(context.resources.getString(R.string.dialogHelp_overviewText))
        }
        dialogHelp_gestures.setOnClickListener {
            dialogHelp_iv.visibility = View.GONE
            dialogHelp_tv_title.text =
                textSpannable(context.resources.getString(R.string.dialogHelp_gesturesTitle))
            dialogHelp_tv_text.text =
                textSpannable(context.resources.getString(R.string.dialogHelp_gesturesText))
        }
        dialogHelp_filter.setOnClickListener {
            dialogHelp_iv.visibility = View.VISIBLE
            dialogHelp_iv.setImageDrawable(context.resources.getDrawable(R.drawable.help_filter))
            dialogHelp_tv_title.text =
                textSpannable(context.resources.getString(R.string.dialogHelp_filterTitle))
            dialogHelp_tv_text.text =
                textSpannable(context.resources.getString(R.string.dialogHelp_filterText))
        }
        bottomSheetDialog.setContentView(dialogView)
        bottomSheetDialog.show()
        setBottomSheetBehavior(bottomSheetDialog, dialogView, BottomSheetBehavior.STATE_EXPANDED)
    }*/

    /*fun show_dialogChangelog(context: Context) {
        sp = PreferenceManager.getDefaultSharedPreferences(context)
        val dialog = BottomSheetDialog(context)
        val dialogView = View.inflate(context, R.layout.dialog_text, null)
        val dialog_title = dialogView.findViewById<TextView>(R.id.dialog_title)
        dialog_title.setTextColor(context.resources.getColor(R.color.white_color))
        dialog_title.setText(R.string.changelog_title)
        val dialog_text = dialogView.findViewById<TextView>(R.id.dialog_text)
        dialog_text.text = textSpannable(context.getString(R.string.changelog_dialog))
        dialog_text.movementMethod = LinkMovementMethod.getInstance()
        val fab = dialogView.findViewById<ImageButton>(R.id.floatButton_ok)
        fab.setOnClickListener {
            dialog.cancel()
            try {
                val pInfo = context.packageManager.getPackageInfo(context.packageName, 0)
                val versionName = pInfo.versionName
                val oldVersionName = sp.getString("oldVersionName", "0.0")
                if (Objects.requireNonNull(oldVersionName) != versionName) {
                    sp.edit().putString("oldVersionName", versionName).apply()
                }
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
        }
        val fab_help = dialogView.findViewById<ImageButton>(R.id.floatButton_help)
        fab_help.setOnClickListener {
            dialog.cancel()
//            show_dialogHelp(context)
        }
        val fab_settings = dialogView.findViewById<ImageButton>(R.id.floatButton_settings)
        fab_settings.visibility = View.GONE
        dialog.setContentView(dialogView)
        dialog.show()
        setBottomSheetBehavior(dialog, dialogView, BottomSheetBehavior.STATE_EXPANDED)
    }*/

    /*fun switchIcon(activity: Activity?, string: String?, fieldDB: String?, be: ImageView?) {
        sp = PreferenceManager.getDefaultSharedPreferences(activity)
        assert(be != null)
        when (string) {
            "01" -> {
                be!!.setImageResource(R.drawable.circle_red)
                sp.edit().putString(fieldDB, "01").apply()
            }
            "02" -> {
                be!!.setImageResource(R.drawable.circle_pink)
                sp.edit().putString(fieldDB, "02").apply()
            }
            "03" -> {
                be!!.setImageResource(R.drawable.circle_purple)
                sp.edit().putString(fieldDB, "03").apply()
            }
            "04" -> {
                be!!.setImageResource(R.drawable.circle_blue)
                sp.edit().putString(fieldDB, "04").apply()
            }
            "05" -> {
                be!!.setImageResource(R.drawable.circle_teal)
                sp.edit().putString(fieldDB, "05").apply()
            }
            "06" -> {
                be!!.setImageResource(R.drawable.circle_green)
                sp.edit().putString(fieldDB, "06").apply()
            }
            "07" -> {
                be!!.setImageResource(R.drawable.circle_lime)
                sp.edit().putString(fieldDB, "07").apply()
            }
            "08" -> {
                be!!.setImageResource(R.drawable.circle_yellow)
                sp.edit().putString(fieldDB, "08").apply()
            }
            "09" -> {
                be!!.setImageResource(R.drawable.circle_orange)
                sp.edit().putString(fieldDB, "09").apply()
            }
            "10" -> {
                be!!.setImageResource(R.drawable.circle_brown)
                sp.edit().putString(fieldDB, "10").apply()
            }
            "11" -> {
                be!!.setImageResource(R.drawable.circle_grey)
                sp.edit().putString(fieldDB, "11").apply()
            }
            else -> {
                be!!.setImageResource(R.drawable.circle_red)
                sp.edit().putString(fieldDB, "01").apply()
            }
        }
    }*/


    fun fileName(url: String?): String? {
        return try {
            val sdf = SimpleDateFormat("yyyy-MM-dd_HH-mm-ss", Locale.getDefault())
            val currentTime = sdf.format(Date())
            val domain =
                Uri.parse(url).host!!.replace("www.", "").trim { it <= ' ' }
            domain.replace(".", "_").trim { it <= ' ' } + "_" + currentTime.trim { it <= ' ' }
        } catch (e: Exception) {
            Log.e("Error : ", e.message!!)
            null
        }
    }

    fun secString(string: String?): String {
        return if (TextUtils.isEmpty(string!!)) {
            ""
        } else {
            string.replace("'".toRegex(), "\'\'")
        }
    }

    fun textSpannable(text: String?): SpannableString {
        val s: SpannableString
        s = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            SpannableString(Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY))
        } else {
            SpannableString(Html.fromHtml(text))
        }
        Linkify.addLinks(s, Linkify.WEB_URLS)
        return s
    }
}
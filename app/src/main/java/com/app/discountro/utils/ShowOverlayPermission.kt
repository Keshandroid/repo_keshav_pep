package com.app.discountro.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.CheckBox
import android.widget.TextView
import com.app.discountro.R
import com.app.discountro.utils.DialogMyInterface.Companion.ACTION_CANCEL
import com.app.discountro.utils.DialogMyInterface.Companion.ACTION_OK

fun Activity.showPermissionOverlay(dialogMyInterface: DialogMyInterface): Dialog {
    val dialog = Dialog(this)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.setCancelable(false)
    // set custom layout
    dialog.setContentView(R.layout.dialog_permission_appear_top)
    // set height and width
    val width = WindowManager.LayoutParams.MATCH_PARENT
    val height = WindowManager.LayoutParams.MATCH_PARENT
    // set to custom layout
    dialog.window?.setLayout(width, height)
    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    val params: WindowManager.LayoutParams = dialog.window!!.attributes
    params.gravity = Gravity.CENTER

    // find view
    val checkShowAgain = dialog.findViewById<CheckBox>(R.id.checkShowAgain)
    val actionCancel = dialog.findViewById<TextView>(R.id.action_cancel)
    val actionOK = dialog.findViewById<TextView>(R.id.action_ok)

    actionOK?.setOnClickListener {
        dialogMyInterface.callBack(it, ACTION_OK, checkShowAgain.isChecked)
        dialog.dismiss()
    }

    actionCancel?.setOnClickListener {
        dialogMyInterface.callBack(it, ACTION_CANCEL, checkShowAgain.isChecked)
        dialog.dismiss()
    }
    return dialog
}

fun Context.showPermissionOverlay(dialogMyInterface: DialogMyInterface): Dialog {
    val dialog = Dialog(this)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.setCancelable(false)
    // set custom layout
    dialog.setContentView(R.layout.dialog_permission_appear_top)
    // set height and width
    val width = WindowManager.LayoutParams.MATCH_PARENT
    val height = WindowManager.LayoutParams.MATCH_PARENT
    // set to custom layout
    dialog.window?.setLayout(width, height)
    dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    val params: WindowManager.LayoutParams = dialog.window!!.attributes
    params.gravity = Gravity.CENTER
    // find view
    val checkShowAgain = dialog.findViewById<CheckBox>(R.id.checkShowAgain)
    val actionCancel = dialog.findViewById<TextView>(R.id.action_cancel)
    val actionOK = dialog.findViewById<TextView>(R.id.action_ok)
    actionOK?.setOnClickListener {
        dialogMyInterface.callBack(it, ACTION_OK, checkShowAgain.isChecked)
        dialog.dismiss()
    }

    actionCancel?.setOnClickListener {
        dialogMyInterface.callBack(it, ACTION_CANCEL, checkShowAgain.isChecked)
        dialog.dismiss()
    }
    return dialog
}

interface DialogMyInterface {
    companion object {
        const val ACTION_OK = 1
        const val ACTION_CANCEL = 2
    }

    fun callBack(view: View, buttonAction: Int, neverShowAgain: Boolean)
}
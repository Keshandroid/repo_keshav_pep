package com.app.discountro.utils.browser_utils.my_object
import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.app.discountro.R
object BrowserToast {
    fun show(context: Context, stringResId: Int) {
        show(context, context.getString(stringResId))
    }

    fun show(context: Context, text: String) {
        val activity = context as Activity
        val inflater = activity.layoutInflater
        val layout = inflater.inflate(
            R.layout.dialog_bottom,
            activity.findViewById<View>(R.id.dialog_toast) as? ViewGroup
        )
        val dialogText = layout.findViewById<TextView>(R.id.dialog_text)
        dialogText.setTextColor(Color.WHITE)
        dialogText.text = text
        val toast = Toast(activity.applicationContext)
        toast.setGravity(Gravity.BOTTOM or Gravity.FILL_HORIZONTAL, 0, 0)
        toast.duration = Toast.LENGTH_SHORT
        toast.view = layout
        toast.show()
        //Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
}
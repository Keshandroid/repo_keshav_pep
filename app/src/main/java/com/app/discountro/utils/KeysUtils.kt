package com.app.discountro.utils

class KeysUtils {

    companion object {

        val VIBRANTCOLOR = "vibrantcolor"
        val SAVED_KEY = "saved_key"
        val SAVED_KEY_OK = "saved_key_ok"
        val KEY_ON_OFF_SWIPE_MENU = "swipe_refresh"
        val SETTING_GESTURE_TB_UP = "setting_gesture_tb_up"
        val SETTING_GESTURE_TB_DOWN = "setting_gesture_tb_down"
        val SETTING_GESTURE_TB_LEFT = "setting_gesture_tb_left"
        val SETTING_GESTURE_TB_RIGHT = "setting_gesture_tb_right"
        val SETTING_GESTURE_NAV_UP = "setting_gesture_nav_up"
        val SETTING_GESTURE_NAV_DOWN = "setting_gesture_nav_down"
        val SETTING_GESTURE_NAV_LEFT = "setting_gesture_nav_left"
        val SETTING_GESTURE_NAV_RIGHT = "setting_gesture_nav_right"
        val SP_LOCATION_9 = "SP_LOCATION_9"
        val RESTART_CHANGED = "restart_changed"
        val KEY_START_TAB= "start_tab"
        val KEY_OVERVIEW_HIDE= "overView_hide"
        val KEY_LOCATION = "location"
        val KEY_IMAGE = "Image"
        val KEY_REMOTE =  "remote"
        val KEY_FONT_SIZE = "font_size"
        val KEYS_INVERT = "invert"
        val KEY_HIDENAV = "hideNav"
        val Key_HIDE_TOOLBAR = "hideToolbar"
        val KEY_FAVOURITE_URL=  "favoriteURL"
        val KEY_LIGHT_UI=  "lightUI"
        val KEY_SHORTCUT_NO = "shortcut_number"
        val KEY_SAVE_HISTORY = "saveHistory"
        val KEY_COOKIE = "cookies"
        val KEY_JAVA_SCRIPT =    "javascript"
        val KEY_START_SITE_DEFAULT = "ordinal"
        val KEY_START_SITE = "sort_startSite"
        val KEY_SEARCH_ENGINE_DEFAULT = "Key_Search_Engine_Default"
        val KEY_SEARCH_ENGINE = "key_Search_Engine"
        val KeyPleaseWait = "please_wait"
        val KEYFIRELANGUAGE_Collection = "language"
        val KEYFIRESPLASH_Collection = "splash_screens"
        val KEYFIRETRANSLATION_Collection = "translation"
        val keyLangISO = "lang_iso"
        val KeyModelTranslation = "myTranslationVarModel"

        //Sign-Up

        var keyReferralCode = "KEY_REFERRAL_CODE"
        var keyFirebaseToken = "KEY_FIREBASE_TOKEN"

        var keyCurrentLatitude = "KEY_CURRENT_LATITUDE"
        var keyCurrentLongitude = "KEY_CURRENT_LONGITUDE"
        var KeyUUID = "KEY_USER_ID"

        var KeyFireGoogle = "Google"
        var KeyFirefacebook = "facebook"
        var KeyFireEmail = "fire email"

        //Profile Data
        var keyProfileData = "KEY_PROFILE_DATA"
        var keyAboutUs = "KEY_ABOUT_US"
        var keyPrivacyPolicy = "KEY_PRIVACY_POLICY"
        var keyTermService = "KEY_TERM_SERVICE"
        var keyGenderModel = "KEY_GENDER_MODEL"
        var keyOccupationModel = "KEY_OCCUPATION_MODEL"

        var keyFocusedWebView = "KEY_FOCUSED_WEBVIEW"
        var keyIsIncoModeOn = "KEY_IS_INCO_MODE_ON"

        var keyDefaultMobUserAgent="KEY_DEFAULT_MOB_USERAGENT"

        val DESKTOP_USERAGENT =
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.134 Safari/537.36"

        const val KEY_STORE_MODEL = "store_model"

        // ProductModel
        const val KEY_PRODUCT_MODEL = "product_model"

        val KEY_MODE_TYPE = "MODE_TYPE"

    }
}

package com.app.discountro.utils.browser_utils.my_object

import android.Manifest
import android.app.Activity
import android.app.DownloadManager
import android.content.Context
import android.content.pm.PackageManager
import android.content.pm.ShortcutManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.Environment.DIRECTORY_DOWNLOADS
import android.preference.PreferenceManager
import android.util.Log
import android.webkit.CookieManager
import android.webkit.URLUtil
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.app.discountro.R
import com.app.discountro.common.sqlite.browser_db.RecordAction
import com.app.discountro.ui.download.view.DownloadsListActivity.Companion.DOWNLOAD_FOLDER_NAME
import com.app.discountro.utils.browser_utils.AdBlock
import com.app.discountro.utils.browser_utils.Cookie
import com.app.discountro.utils.browser_utils.Javascript
import java.io.*
import java.net.URLEncoder
import java.util.*
import java.util.regex.Pattern

object BrowserUnit {
    val UA_DESKTOP_PREFIX = "Mozilla/5.0 (X11; Linux " + System.getProperty("os.arch") + ")"
    val UA_MOBILE_PREFIX = "Mozilla/5.0 (Linux; Android " + Build.VERSION.RELEASE + ")"
    const val PROGRESS_MAX = 100
    const val SUFFIX_PNG = ".png"
    private const val SUFFIX_TXT = ".txt"
    const val MIME_TYPE_TEXT_PLAIN = "text/plain"
    private const val SEARCH_ENGINE_GOOGLE = "https://www.google.com/search?q="
    private const val SEARCH_ENGINE_DUCKDUCKGO = "https://duckduckgo.com/?q="
    private const val SEARCH_ENGINE_STARTPAGE = "https://startpage.com/do/search?query="
    private const val SEARCH_ENGINE_BING = "http://www.bing.com/search?q="
    private const val SEARCH_ENGINE_BAIDU = "https://www.baidu.com/s?wd="
    private const val SEARCH_ENGINE_QWANT = "https://www.qwant.com/?q="
    private const val SEARCH_ENGINE_ECOSIA = "https://www.ecosia.org/search?q="
    private const val SEARCH_ENGINE_STARTPAGE_DE =
        "https://startpage.com/do/search?lui=deu&language=deutsch&query="
    private const val SEARCH_ENGINE_SEARX = "https://searx.me/?q="
    const val URL_ENCODING = "UTF-8"
    private const val URL_ABOUT_BLANK = "about:blank"
    const val URL_SCHEME_ABOUT = "about:"
    const val URL_SCHEME_MAIL_TO = "mailto:"
    private const val URL_SCHEME_FILE = "file://"
    private const val URL_SCHEME_HTTP = "https://"
    const val URL_SCHEME_INTENT = "intent://"
    private const val URL_PREFIX_GOOGLE_PLAY = "www.google.com/url?q="
    private const val URL_SUFFIX_GOOGLE_PLAY = "&sa"
    private const val URL_PREFIX_GOOGLE_PLUS = "plus.url.google.com/url?q="
    private const val URL_SUFFIX_GOOGLE_PLUS = "&rct"

    val File.size get() = if (!exists()) 0.0 else length().toDouble()
    val File.sizeInKb get() = size / 1024
    val File.sizeInMb get() = sizeInKb / 1024
    val File.sizeInGb get() = sizeInMb / 1024
    val File.sizeInTb get() = sizeInGb / 1024
    @JvmStatic
    fun isURL(url: String?): Boolean {
        var url = url ?: return false
        url = url.lowercase(Locale.getDefault())
        if (url.startsWith(URL_ABOUT_BLANK)
            || url.startsWith(URL_SCHEME_MAIL_TO)
            || url.startsWith(URL_SCHEME_FILE)
        ) {
            return true
        }
        val regex = ("^((ftp|http|https|intent)?://)" // support scheme
                + "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" // ftp的user@
                + "(([0-9]{1,3}\\.){3}[0-9]{1,3}" // IP形式的URL -> 199.194.52.184
                + "|" // 允许IP和DOMAIN（域名）
                + "(.)*" // 域名 -> www.
                // + "([0-9a-z_!~*'()-]+\\.)*"                               // 域名 -> www.
                + "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\\." // 二级域名
                + "[a-z]{2,6})" // first level domain -> .com or .museum
                + "(:[0-9]{1,4})?" // 端口 -> :80
                + "((/?)|" // a slash isn't required if there is no file name
                + "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$")
        val pattern = Pattern.compile(regex)
        return pattern.matcher(url).matches()
    }

    fun queryWrapper(context: Context, query: String): String {
        // Use prefix and suffix to process some special links
        var query1 = query
        val temp = query1.lowercase(Locale.getDefault())
        if (temp.contains(URL_PREFIX_GOOGLE_PLAY) && temp.contains(URL_SUFFIX_GOOGLE_PLAY)) {
            val start = temp.indexOf(URL_PREFIX_GOOGLE_PLAY) + URL_PREFIX_GOOGLE_PLAY.length
            val end = temp.indexOf(URL_SUFFIX_GOOGLE_PLAY)
            query1 = query1.substring(start, end)
        } else if (temp.contains(URL_PREFIX_GOOGLE_PLUS) && temp.contains(URL_SUFFIX_GOOGLE_PLUS)) {
            val start = temp.indexOf(URL_PREFIX_GOOGLE_PLUS) + URL_PREFIX_GOOGLE_PLUS.length
            val end = temp.indexOf(URL_SUFFIX_GOOGLE_PLUS)
            query1 = query1.substring(start, end)
        }
        if (isURL(query1)) {
            if (query1.startsWith(URL_SCHEME_ABOUT) || query1.startsWith(URL_SCHEME_MAIL_TO)) {
                return query1
            }
            query1 = query1.replace("www.","")
            query1 = query1.replace("http://","")
            query1 = query1.replace("https://","")

            query1 = "https://www.$query1"

            return query1
        }
        try {
            // STEP 2
            query1 = URLEncoder.encode(query1, URL_ENCODING)
        } catch (u: UnsupportedEncodingException) {
            Log.w("Browser", "Unsupported Encoding Exception")
        }

       val sp = PreferenceManager.getDefaultSharedPreferences(context)
        val custom = sp.getString(
            context.getString(R.string.sp_search_engine_custom),
            SEARCH_ENGINE_STARTPAGE
        )
        val i = Integer.valueOf(
//            SharedPrefsUtils.getIntegerPreference(KeysUtils.KEY_SEARCH_ENGINE,5)
            sp.getString(
                context.getString(R.string.sp_search_engine),
                "5"
            )!!
        )


        // STEP 3
        return when (i) {
            0 -> SEARCH_ENGINE_STARTPAGE + query1
            1 -> SEARCH_ENGINE_STARTPAGE_DE + query1
            2 -> SEARCH_ENGINE_BAIDU + query1
            3 -> SEARCH_ENGINE_BING + query1
            4 -> SEARCH_ENGINE_DUCKDUCKGO + query1
            5 -> SEARCH_ENGINE_GOOGLE + query1
            6 -> SEARCH_ENGINE_SEARX + query1
            7 -> SEARCH_ENGINE_QWANT + query1
            9 -> SEARCH_ENGINE_ECOSIA + query1
            8 -> custom + query1
            else -> SEARCH_ENGINE_ECOSIA + query1
        }
    }

    fun bitmap2File(context: Context, bitmap: Bitmap, filename: String?): Boolean {
        try {
            val fileOutputStream = context.openFileOutput(filename, Context.MODE_PRIVATE)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream)
            fileOutputStream.flush()
            fileOutputStream.close()
        } catch (e: Exception) {
            return false
        }
        return true
    }

    @JvmStatic
    fun file2Bitmap(context: Context, filename: String?): Bitmap? {
        return try {
            val fileInputStream = context.openFileInput(filename)
            BitmapFactory.decodeStream(fileInputStream)
        } catch (e: Exception) {
            null
        }
    }

    fun download(context: Context, url: String?, contentDisposition: String?, mimeType: String?) {
        val discountRoDir = File(Environment.getExternalStorageDirectory().toString()
                + "/$DOWNLOAD_FOLDER_NAME")

        val request = DownloadManager.Request(Uri.parse(url))
        val filename =
            URLUtil.guessFileName(url, contentDisposition, mimeType) // Maybe unexpected filename.
        val cookieManager = CookieManager.getInstance()
        val cookie = cookieManager.getCookie(url)
        request.addRequestHeader("Cookie", cookie)
        request.allowScanningByMediaScanner()
        request.setVisibleInDownloadsUi(true)
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        request.setTitle(filename)
        request.setMimeType(mimeType)

        // TODO: set download destination-
        request.setDestinationInExternalPublicDir(DIRECTORY_DOWNLOADS, File.separator + DOWNLOAD_FOLDER_NAME + File.separator + filename)

        val manager = (context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager)
        if (Build.VERSION.SDK_INT >= 23) {
            val hasWRITE_EXTERNAL_STORAGE =
                context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            if (hasWRITE_EXTERNAL_STORAGE != PackageManager.PERMISSION_GRANTED) {
                val activity = context as Activity
                HelperUnit.grantPermissionsStorage(activity)
            } else {

                // TODO: create DiscountRo Folder in downloads:
                if (!discountRoDir.exists()) {
                    discountRoDir.mkdirs()
                }

                manager.enqueue(request)
                try {
                    BrowserToast.show(context, R.string.toast_start_download)
                } catch (e: Exception) {
                    Toast.makeText(context, R.string.toast_start_download, Toast.LENGTH_SHORT)
                        .show()
                }
            }
        } else {
            manager.enqueue(request)
            try {
                BrowserToast.show(context, R.string.toast_start_download)
            } catch (e: Exception) {
                Toast.makeText(context, R.string.toast_start_download, Toast.LENGTH_SHORT).show()
            }
        }
    }

    /*fun screenshot(context: Context, bitmap: Bitmap?, name: String?): String? {
        var name = name
        if (bitmap == null) {
            return null
        }
        val dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
        if (name == null || name.trim { it <= ' ' }.isEmpty()) {
            name = System.currentTimeMillis().toString()
        }
        name = name.trim { it <= ' ' }
        var count = 0
        var file = File(dir, name + SUFFIX_PNG)
        while (file.exists()) {
            count++
            file = File(dir, name + "_" + count + SUFFIX_PNG)
        }
        return try {
            val stream = FileOutputStream(file)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
            stream.flush()
            stream.close()
            context.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file)))
            val sp = PreferenceManager.getDefaultSharedPreferences(context)
            sp.edit().putString("screenshot_path", file.path).apply()
            file.absolutePath
        } catch (e: Exception) {
            null
        }
    }*/



    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun exportWhitelist(context: Context, i: Int): String? {
        val action = RecordAction(context)
        val list: List<String>
        val filename: String
        action.open(false)
        when (i) {
            0 -> {
                list = action.listDomains()
                filename = context.getString(R.string.export_whitelistAdBlock)
            }
            1 -> {
                list = action.listDomainsJS()
                filename = context.getString(R.string.export_whitelistJS)
            }
            else -> {
                list = action.listDomainsCookie()
                filename = context.getString(R.string.export_whitelistCookie)
            }
        }
        action.close()
        val file = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
            "browser_backup//" + filename + SUFFIX_TXT
        )
        return try {
            val writer = BufferedWriter(FileWriter(file, false))
            for (domain in list) {
                writer.write(domain)
                writer.newLine()
            }
            writer.close()
            file.absolutePath
        } catch (e: Exception) {
            null
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun importWhitelist(context: Context): Int {
        val filename = context.getString(R.string.export_whitelistAdBlock)
        val file = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
            "browser_backup//" + filename + SUFFIX_TXT
        )
        val adBlock = AdBlock(context)
        var count = 0
        try {
            val action = RecordAction(context)
            action.open(true)
            val reader = BufferedReader(FileReader(file))
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                if (!action.checkDomain(line)) {
                    adBlock.addDomain(line!!)
                    count++
                }
            }
            reader.close()
            action.close()
        } catch (e: Exception) {
            Log.w("Browser", "Error reading file", e)
        }
        return count
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun importWhitelistJS(context: Context): Int {
        val filename = context.getString(R.string.export_whitelistJS)
        val file = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
            "browser_backup//" + filename + SUFFIX_TXT
        )
        val js = Javascript(context)
        var count = 0
        try {
            val action = RecordAction(context)
            action.open(true)
            val reader = BufferedReader(FileReader(file))
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                if (!action.checkDomainJS(line)) {
                    js.addDomain(line!!)
                    count++
                }
            }
            reader.close()
            action.close()
        } catch (e: Exception) {
            Log.w("Browser", "Error reading file", e)
        }
        return count
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun importWhitelistCookie(context: Context): Int {
        val filename = context.getString(R.string.export_whitelistCookie)
        val file = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS),
            "browser_backup//" + filename + SUFFIX_TXT
        )
        val cookie = Cookie(context)
        var count = 0
        try {
            val action = RecordAction(context)
            action.open(true)
            val reader = BufferedReader(FileReader(file))
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                if (!action.checkDomainCookie(line)) {
                    cookie.addDomain(line!!)
                    count++
                }
            }
            reader.close()
            action.close()
        } catch (e: Exception) {
            Log.w("Browser", "Error reading file", e)
        }
        return count
    }

    fun clearHome(context: Context?) {
        val action = RecordAction(context)
        action.open(true)
        action.clearHome()
        action.close()
    }

    fun clearCache(context: Context) {
        try {
            val dir = context.cacheDir
            if (dir != null && dir.isDirectory) {
                deleteDir(dir)
            }
        } catch (exception: Exception) {
            Log.w("Browser", "Error clearing cache")
        }
    }

    // CookieManager.removeAllCookies() must be called on a thread with a running Looper.
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    fun clearCookie() {
        val cookieManager = CookieManager.getInstance()
        cookieManager.flush()
        cookieManager.removeAllCookies { }
    }

    fun clearHistory(context: Context) {
        val action = RecordAction(context)
        action.open(true)
        action.clearHistory()
        action.close()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            val shortcutManager = context.getSystemService(
                ShortcutManager::class.java
            )
            shortcutManager.removeAllDynamicShortcuts()
        }
    }

    fun clearIndexedDB(context: Context) {
       /* val data = Environment.getDataDirectory()
        val indexedDB = "//data//" + context.packageName + "//app_webview//" + "//IndexedDB"
        val localStorage = "//data//" + context.packageName + "//app_webview//" + "//Local Storage"
        val indexedDB_dir = File(data, indexedDB)
        val localStorage_dir = File(data, localStorage)
        deleteDir(indexedDB_dir)
        deleteDir(localStorage_dir)*/
    }

    fun deleteDir(dir: File?): Boolean {
        if (dir != null && dir.isDirectory) {
            val children = dir.list()
            for (aChildren in children) {
                val success = deleteDir(File(dir, aChildren))
                if (!success) {
                    return false
                }
            }
        }
        return dir != null && dir.delete()
    }
}
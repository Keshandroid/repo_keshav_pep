package com.app.discountro.Firebase;

import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.discountro.Interface.ComparableProductInterface;
import com.app.discountro.Interface.CouponListInterface;
import com.app.discountro.Interface.FirestoreProductMatchingInterface;
import com.app.discountro.Interface.SemiComparableProductInterface;
import com.app.discountro.R;
import com.app.discountro.common.service.CoupanPopService;
import com.app.discountro.common.sqlite.offerCoupons.CopiedCouponsModel;
import com.app.discountro.common.sqlite.offerCoupons.DBHelperOffer;
import com.app.discountro.common.urlConverter.UrlToShaConverter;
import com.app.discountro.fireStore.helper.CompareProductInfoInt;
import com.app.discountro.fireStore.helper.CompareProductSkuInfo;
import com.app.discountro.fireStore.helper.FireStoreHelper;
import com.app.discountro.fireStore.interfaces.FireStoreListener;
import com.app.discountro.fireStore.models.CompareProductModel;
import com.app.discountro.fireStore.models.CouponsAllModel;
import com.app.discountro.fireStore.models.RoModel;
import com.app.discountro.ui.home.view.adapter.CallBackRecInt;
import com.app.discountro.ui.home.view.adapter.CompareProductAdapter;
import com.app.discountro.ui.home.view.adapter.CountriesCouponAdapter;
import com.app.discountro.ui.home.view.adapter.CouponsListAdapter;
import com.app.discountro.ui.home.view.models.CountryCouponsModel;
import com.app.discountro.ui.home.view.models.CountryNameAndDataModel;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class RetrieveDataFromFirebase {
    private static final String TAG = "RetrieveFromFirebase";

    // Instance Variable:
    private DBHelperOffer offersDBHelper;

    private static CouponsAllModel couponsAllModel;
    private static CompareProductModel compareProductModel;

    //Public Constructor to initiate data
    public RetrieveDataFromFirebase(DBHelperOffer offersDBHelper) {
        this.offersDBHelper = offersDBHelper;
    }

    /**
     * COUPON PART
     */
    // check the coupon from URL:
    public void getCouponCode(String websiteName, CouponListInterface couponListInterface) {
        Log.d("RetrieveDataFirebase", "getCouponCode: " + websiteName);
        ArrayList<CopiedCouponsModel> offersList = offersDBHelper.readAllOffers();
        /*Remove Values from local data if website not matched with previous visited website*/
        if (!offersList.isEmpty() && offersList.size() > 0) {
            for (int i = 0; i < offersList.size(); i++) {
                if (!offersList.get(i).getWebsiteName().equals(websiteName)) {
                    offersDBHelper.deleteOfferByWebsite(offersList.get(i).getWebsiteName());
                    offersList.remove(offersList.get(i));
                }
            }
        }

        // TEST: anvelope-autobon.ro
        URL urlMain = null;
        try {
            urlMain = new URL(websiteName); // https://www.amazon.com
        } catch (MalformedURLException e) {
            //Log.d("WEBSITE_NAME", "MalformedURLException : " + e.getMessage());
            e.printStackTrace();
        }
        String[] str = urlMain.getHost().split("\\."); // www amazon com
        //String webSiteName = str[1] + "." + str[2]; // amazon.com
        String webSiteName = "";

        int i = str.length - 1;
        int j = i - 1;

        webSiteName = str[j] + "." + str[i]; // amazon.com
        Log.e(TAG,"===str===" +str[j]);
        Log.e(TAG,"===str===" +str[i]);
       /* if(str.length == 2)
        {
            webSiteName = str[0] + "." + str[1]; // amazon.com
            Log.e(TAG,"===str===" +str[0]);
            Log.e(TAG,"===str===" +str[1]);
        }
        else{
            webSiteName = str[1] + "." + str[2]; // amazon.com
            Log.e(TAG,"===str===" +str[1]);
            Log.e(TAG,"===str===" +str[2]);
        }*/


//        if (websiteNameOf.isEmpty())
//            websiteNameOf = webSiteName;
        Log.d("WEBSITE_NAME", " : " + webSiteName);

        new FireStoreHelper().getDataListTemp(webSiteName, new FireStoreListener() {

            @Override
            public void onError(@NonNull String error) {
                Log.d("COUPON_RESPONSE", "Error: " + error);
                couponListInterface.errorOnCouponList("ERROR : " + error);
            }

            @Override
            public void onComplete(@NonNull String response) {
                Log.d("COUPON_RESPONSE", "onComplete: " + response.toString());
                couponsAllModel = new Gson().fromJson(response, CouponsAllModel.class);
                if (couponsAllModel != null) {
                    couponListInterface.getCouponList(couponsAllModel);
                } else {
                    couponListInterface.errorOnCouponList("NULL");
                }

                //parseNShowCoupon(couponsAllModel);

            }

        });
    }

    /**
     * COMPARE PRODUCT
     */
    public void findComparableProduct(String url, SemiComparableProductInterface semiComparableProductInterface) {
        try {
            URL urlMain = new URL(url);// https://www.amazon.com
            Log.e(TAG,"===urlMain===" + url);
            String[] str = urlMain.getHost().split("\\."); // www amazon com

            Log.e(TAG,"===str===" + str);
            Log.e(TAG,"===str size===" + str.length);

            String webSiteName = "";
            int i = str.length - 1;
            int j = i - 1;

            webSiteName = str[j] + "." + str[i]; // amazon.com
            Log.e(TAG,"===str===" +str[j]);
            Log.e(TAG,"===str===" +str[i]);


          /*  if(str.length == 2)
            {
                webSiteName = str[0] + "." + str[1]; // amazon.com
                Log.e(TAG,"===str===" +str[0]);
                Log.e(TAG,"===str===" +str[1]);
            }
            else{
                webSiteName = str[1] + "." + str[2]; // amazon.com
                Log.e(TAG,"===str===" +str[1]);
                Log.e(TAG,"===str===" +str[2]);
            }*/

            // val webSiteName = str[1] // amazon
            String finalWebSiteName = webSiteName;
            new FireStoreHelper().getDataListTemp(webSiteName, new FireStoreListener() {
                @Override
                public void onComplete(@NonNull String response) {
                    Gson gsn = new Gson();
                    CouponsAllModel couponsAllModel = gsn.fromJson(response, CouponsAllModel.class);

                    HashMap<String, Object> coupons = couponsAllModel.getCoupons();
                    for (Map.Entry<String, Object> map : coupons.entrySet()) {
                        Log.d("SHA_CHECK", "Coupons: " + map.getValue());
                    }

                    if (couponsAllModel.getSku_info() != null) {
                        new CompareProductSkuInfo().getProductIdSku(couponsAllModel, urlMain.toString(), new CompareProductInfoInt() {
                            @Override
                            public void onFailed(@NonNull String string) {
                                Log.e("ConvertMyUrl", "onStartCommand: if " + urlMain);
                                //String convertUrl = ConvertUrlToSHA.Companion.convertStringToSha(urlMain.toString());
                                String convertedSHA = UrlToShaConverter.getConvertedSHA(urlMain.toString());
                                Log.e("ConvertMyUrl", "onStartCommand: " + convertedSHA);
                                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                                    //comparableProducts(convertedSHA, "", comparableProductInterface);
                                    semiComparableProductInterface.SHA_INFO(convertedSHA, "");
                                }, 100);
                                //getProductsForMatching(convertUrl, "");

                                Log.d("SHA_CHECK", "onFailed: ");
                            }

                            @Override
                            public void onComplete(@NonNull String productId) {
                                Log.e("ConvertMyUrl", "onStartCommand: if " + urlMain);
                                //String convertUrl = ConvertUrlToSHA.Companion.convertStringToSha(urlMain.toString());
                                String convertedSHA = UrlToShaConverter.getConvertedSHA(urlMain.toString());
                                Log.e("ConvertMyUrl", "onStartCommand: " + convertedSHA);
                                new Handler(Looper.getMainLooper()).postDelayed(() -> {
                                    //comparableProducts(convertedSHA, productId, comparableProductInterface);
                                    semiComparableProductInterface.SHA_INFO(convertedSHA, productId);
                                }, 100);
                                Log.d("SHA_CHECK", "onComplete: ");
                            }
                        });
                    } else {
                        Log.e("ConvertMyUrl", "onStartCommand: if " + urlMain);
                        //String convertUrl = ConvertUrlToSHA.Companion.convertStringToSha(urlMain.toString());
                        String convertedSHA = UrlToShaConverter.getConvertedSHA(urlMain.toString());
                        Log.e("ConvertMyUrl", "onStartCommand: " + convertedSHA);
                        //comparableProducts(convertedSHA, "", comparableProductInterface);
                        semiComparableProductInterface.SHA_INFO(convertedSHA, "");
                        Log.d("SHA_CHECK", "SKU INFO IS NULL : " + finalWebSiteName + "\n HASH: " + convertedSHA);
                    }
                }

                @Override
                public void onError(@NonNull String error) {
                    Log.e("ConvertMyUrl", "onStartCommand: if " + urlMain);
                    //String convertUrl = ConvertUrlToSHA.Companion.convertStringToSha(urlMain.toString());
                    String convertedSHA = UrlToShaConverter.getConvertedSHA(urlMain.toString());
                    Log.e("ConvertMyUrl", "onStartCommand: " + convertedSHA);
                    semiComparableProductInterface.SHA_INFO(convertedSHA, "");
                    //comparableProducts(convertedSHA, "", comparableProductInterface);
                }
            });
        } catch (MalformedURLException e) {
            Log.e("ConvertMyUrl", "onStartCommand: if " + url);
            //String convertUrl = ConvertUrlToSHA.Companion.convertStringToSha(url);
            String convertedSHA = UrlToShaConverter.getConvertedSHA(url);
            Log.e("ConvertMyUrl", "onStartCommand: " + convertedSHA);
            semiComparableProductInterface.SHA_INFO(convertedSHA, "");
            //comparableProducts(convertedSHA, "", comparableProductInterface);
            e.printStackTrace();
        }
    }

    /**
     * New Implementation of compare product:
     */

    public void comparableProducts(String sha, String productId, ComparableProductInterface comparableProductInterface) {
        //progressBar.setVisibility(View.VISIBLE);
        new FireStoreHelper().getProductsToCompare(sha, productId, new FirestoreProductMatchingInterface() {
            @Override
            public void onComplete(@NonNull CompareProductModel model) {
                compareProductModel = model;
                if (compareProductModel != null) {
                    Log.d(TAG, "onComplete: 10: Found data");
                    comparableProductInterface.getComparableProduct(compareProductModel);
                } else {
                    Log.d(TAG, "onComplete: 1 : Null");
                    comparableProductInterface.errorOnComparableProduct("NULL");
                }
            }

            @Override
            public void onError(@NonNull String error) {
                compareProductModel = null;
                Log.d(TAG, "onError: 2: " + error);
                comparableProductInterface.errorOnComparableProduct("ERROR: " + error);
            }
        });


    }


//    public void comparableProducts(String sha, String productId, ComparableProductInterface comparableProductInterface) {
//        //progressBar.setVisibility(View.VISIBLE);
//        new FireStoreHelper().getProductsForMatching(sha, productId, new FireStoreListener() {
//            @Override
//            public void onComplete(@NonNull String response) {
//                //progressBar.setVisibility(View.GONE);
//                Log.d("response", "RESPONSE----->" + response);
//                //compareProductModel = new Gson().fromJson(response, CompareProductModel.class);
//                compareProductModel = new Gson().fromJson(response, CompareProductModel.class);
//                if (compareProductModel != null) {
//                    Log.d(TAG, "onComplete: 10: Found data");
//                    comparableProductInterface.getComparableProduct(compareProductModel);
//                } else {
//                    Log.d(TAG, "onComplete: 1 : Null");
//                    comparableProductInterface.errorOnComparableProduct("NULL");
//                }
//            }
//
//            @Override
//            public void onError(@NonNull String error) {
//                //progressBar.setVisibility(View.GONE);
//                Log.e("TAG", "response------->$error");
//                compareProductModel = null;
//                Log.d(TAG, "onError: 2: "+error);
//                comparableProductInterface.errorOnComparableProduct("ERROR: "+error);
//                //couponsRecyclerView.setAdapter(null);
//            }
//        });
//
//
//    }
}

package com.app.discountro.Interface

interface FireStoreAddFavoriteProductInterface {
    fun onSuccess(msg: String)
    fun onError(error: String)
}
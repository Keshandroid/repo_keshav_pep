package com.app.discountro.Interface

import com.app.discountro.fireStore.models.UserDataModel

interface FireStoreUpdateUserDataInterface {
    fun onSuccess(msg: String)
    fun onError(error: String)
}
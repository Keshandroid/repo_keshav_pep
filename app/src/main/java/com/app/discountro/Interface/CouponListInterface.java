package com.app.discountro.Interface;

import com.app.discountro.fireStore.models.CouponsAllModel;

public interface CouponListInterface {

    void getCouponList(CouponsAllModel couponsAllModel);
    void errorOnCouponList(String error);
}

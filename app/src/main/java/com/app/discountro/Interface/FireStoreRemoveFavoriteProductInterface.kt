package com.app.discountro.Interface

interface FireStoreRemoveFavoriteProductInterface {
    fun onSuccess(msg: String)
    fun onError(error: String)
}
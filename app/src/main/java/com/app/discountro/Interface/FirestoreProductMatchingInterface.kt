package com.app.discountro.Interface

import com.app.discountro.fireStore.models.CompareProductModel

interface FirestoreProductMatchingInterface {
    fun onComplete(model: CompareProductModel)
    fun onError(error: String)
}
package com.app.discountro.Interface

interface FireStoreAddListInterface {
    fun onSuccess(msg: String)
    fun onError(error: String)
}
package com.app.discountro.Interface

import com.google.firebase.firestore.QuerySnapshot

interface FireStoreGetPurchasedProductInterface {
    fun onSuccess(purchasedProductListData: QuerySnapshot)
    fun onError(error: String)

}
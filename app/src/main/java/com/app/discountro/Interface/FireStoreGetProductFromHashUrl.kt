package com.app.discountro.Interface

import com.google.firebase.firestore.QuerySnapshot

interface FireStoreGetProductFromHashUrl {

    fun onSuccess(productData: QuerySnapshot)
    fun onError(error: String)
}
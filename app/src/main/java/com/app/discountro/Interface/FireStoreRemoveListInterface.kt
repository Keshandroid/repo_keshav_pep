package com.app.discountro.Interface

interface FireStoreRemoveListInterface {
    fun onSuccess(msg: String)
    fun onError(error: String)
}
package com.app.discountro.Interface

interface FireStoreAddPurchasedProductInterface {
    fun onSuccess(msg: String)
    fun onError(error: String)
}
package com.app.discountro.Interface

interface FireStoreEditUpdateListInterface {
    fun onSuccess(msg: String)
    fun onError(error: String)
}
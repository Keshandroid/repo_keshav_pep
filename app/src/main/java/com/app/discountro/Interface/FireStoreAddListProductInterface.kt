package com.app.discountro.Interface

interface FireStoreAddListProductInterface {
    fun onSuccess(msg: String)
    fun onError(error: String)
}
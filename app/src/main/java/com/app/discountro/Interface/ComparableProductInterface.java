package com.app.discountro.Interface;

import com.app.discountro.fireStore.models.CompareProductModel;
import com.app.discountro.fireStore.models.CouponsAllModel;

public interface ComparableProductInterface {
    void getComparableProduct(CompareProductModel compareProductModel);
    void errorOnComparableProduct(String error);
}

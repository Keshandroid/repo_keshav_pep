package com.app.discountro.Interface

import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.QuerySnapshot

interface FireStoreGetFilterProductsListListener {
    fun onSuccess(productFilterList: DocumentSnapshot)
    fun onError(error: String)
}
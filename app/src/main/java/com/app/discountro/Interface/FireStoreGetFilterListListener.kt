package com.app.discountro.Interface

import com.google.firebase.firestore.QuerySnapshot

interface FireStoreGetFilterListListener {
    fun onSuccess(productFilterList: QuerySnapshot)
    fun onError(error: String)
}
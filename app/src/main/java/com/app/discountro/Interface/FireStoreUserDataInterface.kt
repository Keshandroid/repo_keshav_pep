package com.app.discountro.Interface

import com.app.discountro.fireStore.models.UserDataModel

interface FireStoreUserDataInterface {
    fun onComplete(model: UserDataModel)
    fun onError(error: String)
}
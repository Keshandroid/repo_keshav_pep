package com.app.discountro

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.app.discountro.common.room.AppRoomDatabase
import com.google.firebase.messaging.FirebaseMessaging

class MyApplication : MultiDexApplication() {

    companion object {
        lateinit var context: Context
        private lateinit var myInstance: MyApplication
        fun getInstance(): MyApplication = myInstance

        private lateinit var appRoomDB: AppRoomDatabase
        fun getAppRoomDB(): AppRoomDatabase = appRoomDB
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        myInstance = this
        MultiDex.install(this)
        appRoomDB = AppRoomDatabase.getInstance(this)
        FirebaseMessaging.getInstance().isAutoInitEnabled = true
    }
}
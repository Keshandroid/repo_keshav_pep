package com.app.discountro.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
 import io.reactivex.disposables.CompositeDisposable

open class BaseViewModel : ViewModel() {

    protected val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val isLoading: MutableLiveData<Boolean> = MutableLiveData()
    val isError: MutableLiveData<Throwable> = MutableLiveData()
    val errorMessage: MutableLiveData<String> = MutableLiveData()
    val message: MutableLiveData<String> = MutableLiveData()

    val userDao = com.app.discountro.MyApplication.getAppRoomDB().userDao()
    val chatResponseDao = com.app.discountro.MyApplication.getAppRoomDB().chatResponseDao()


    var filterUpdated: MutableLiveData<Boolean> = MutableLiveData()

}
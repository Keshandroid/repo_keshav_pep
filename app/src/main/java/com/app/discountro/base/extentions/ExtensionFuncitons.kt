package com.app.discountro.base.extentions

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.text.TextUtils
import android.view.View
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.app.discountro.R
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.SnackbarLayout
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


fun Calendar.getDateInFormatOf(format: String): String {
    return SimpleDateFormat(format, Locale.getDefault()).format(this.time)
}

fun String.getCalenderDate(inputFormat: String): Calendar {
    val date = SimpleDateFormat(inputFormat, Locale.getDefault()).parse(this)
    val calendar = Calendar.getInstance()
    date?.let { calendar.time = date }
    return calendar
}

fun String.convertDate(inputFormat: String, outputFormat: String): String {
    var dateString = ""

        val date = SimpleDateFormat(inputFormat, Locale.getDefault()).parse(this)
        date?.let { dateString = SimpleDateFormat(outputFormat, Locale.getDefault()).format(date) }
        return dateString

    return dateString
}

fun uTCToLocal(
    dateFormatInPut: String?,
    dateFomratOutPut: String?,
    datesToConvert: String?
): String? {
    var dateToReturn = datesToConvert
    val sdf = SimpleDateFormat(dateFormatInPut)
    sdf.timeZone = TimeZone.getTimeZone("UTC")
    var gmt: Date? = null
    val sdfOutPutToSend = SimpleDateFormat(dateFomratOutPut)
    sdfOutPutToSend.timeZone = TimeZone.getDefault()
    try {
        gmt = sdf.parse(datesToConvert)
        dateToReturn = sdfOutPutToSend.format(gmt)
    } catch (e: ParseException) {
        e.printStackTrace()
    }
    return dateToReturn
}

fun String.validateAsEmail(): Boolean {
    if (TextUtils.isEmpty(this)) return false
    return android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun Context.pickColor(resourceId: Int): Int {
    return ContextCompat.getColor(this, resourceId)
}

fun Context.pickColorString(resourceId: Int): String {
    val colorInt = ContextCompat.getColor(this, resourceId)
    return java.lang.String.format("#%06X", 0xFFFFFF and colorInt)

}



fun Context.showToast(message: String = "") {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.showToastLong(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

val Int.dp: Int
    get() = (this * Resources.getSystem().displayMetrics.density + 0.5f).toInt()

fun Context.getDrawableResource(resourceId: Int): Drawable? =
    ContextCompat.getDrawable(this, resourceId)

fun View.mySnack(messageRes: String, length: Int = Snackbar.LENGTH_SHORT, f: Snackbar.() -> Unit) {
    val snackBar = Snackbar.make(this, messageRes, length)
    snackBar.f()
    snackBar.show()
}


fun showSnackBar(
    activity: Activity,
    coordinatorLayout: ConstraintLayout,
    duration: Int,
    description: String,
    title: String,
    image: String
): Snackbar { // Create the Snack bar

    val snackVar = Snackbar.make(coordinatorLayout, "", duration)
    // 15 is margin from all the sides for snack bar
    val marginFromSides = 20
    val height = 100f

    //inflate view
    val snackView: View = activity.layoutInflater.inflate(R.layout.snack_bar_view_show, null)

    val ivLogo = snackView.findViewById<ImageView>(R.id.ivLogo)
    val tvTitle = snackView.findViewById<TextView>(R.id.tvTitle)
    val tvDescription = snackView.findViewById<TextView>(R.id.tvDescription)

    Glide.with(activity).load(image).into(ivLogo)
    tvDescription.text = description
    tvTitle.text = title
    // White background
    snackVar.view.setBackgroundColor(Color.TRANSPARENT)
    // for rounded edges
    val snackBarView = snackVar.view as SnackbarLayout
    val parentParams = snackBarView.layoutParams as FrameLayout.LayoutParams
    parentParams.setMargins(marginFromSides, 0, marginFromSides, 100)
    parentParams.height = FrameLayout.LayoutParams.WRAP_CONTENT
    parentParams.width = FrameLayout.LayoutParams.MATCH_PARENT
    snackBarView.layoutParams = parentParams
    snackBarView.addView(snackView, 0)
    return snackVar
}
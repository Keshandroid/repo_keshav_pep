package com.app.discountro.common.room.model

import androidx.annotation.NonNull
import androidx.room.*

/**
 * This class contains declaration of all the table in detail which are used in the Local database
 */


@Entity(tableName = "user")
data class User(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id") var id: Int? = 0,
    @ColumnInfo(name = "name") var name: String? = "",
    @ColumnInfo(name = "email") var email: String? = "",
    @ColumnInfo(name = "password") var password: String? = "",
    @ColumnInfo(name = "place") var place: String? = ""
       /* if Any Sub Class there then use Type Converter in Room Db*/
    //@TypeConverters(DTCDriveIdentityDetail::class) @ColumnInfo(name = "driver_identity_detail") var driver_identity_detail: DriverIdentityDetail? = null,

)

@Entity(tableName = "coupons")
data class CouponSavedModel(
    @ColumnInfo(name = "offer_url") var offer_url : String?=null,
    @ColumnInfo(name = "offer_text")  var offerText: String? = "",
    @ColumnInfo(name = "code") var code: String? = "",
    @PrimaryKey @NonNull var idOffer: Int = 0,
    @ColumnInfo(name = "affiliate_url") var affiliateUrl: String? = "",
    @ColumnInfo(name = "offer_details") var offerDetails: String? = "",
    @ColumnInfo(name = "offer_name") var offerName: String? = "",
    @ColumnInfo(name = "isCopied") var isCopied: Boolean? = false,
    @ColumnInfo(name = "offer_type") var offerType: String? = ""
)

@Entity(tableName = "chat_response")
data class ChatResponse(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id") var id: Int?=0,
        @ColumnInfo (name = "chatId")var chatId: String? ="",
        @ColumnInfo (name = "sender_id")var sender_id: Int?= 0,
        @ColumnInfo (name = "receiver_id")var receiver_id: Int?= 0,
        @ColumnInfo (name = "message")var message: String?= null
)



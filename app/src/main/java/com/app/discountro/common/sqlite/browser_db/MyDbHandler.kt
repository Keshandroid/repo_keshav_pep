package com.app.discountro.common.sqlite.browser_db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteDatabase.CursorFactory
import android.database.sqlite.SQLiteOpenHelper
import com.app.discountro.ui.home.model.Websites

class MyDbHandler(context: Context?, name: String?, factory: CursorFactory?, version: Int) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        val query = "CREATE TABLE " + TABLE_SITES + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
                COLUMN_NAME + " TEXT " +
                ")"
        db.execSQL(query)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SITES)
        onCreate(db)
    }

    //ADD NEW ROW TO DATABASE
    fun addUrl(website: Websites) {
        val values = ContentValues()
        values.put(COLUMN_NAME, website._url)
        val db = writableDatabase
        db.insert(TABLE_SITES, null, values)
        db.close()
    }

    //DELETE FROM DATABASE
    fun deleteUrl(urlName: String) {
        val db = writableDatabase
        db.execSQL("DELETE FROM $TABLE_SITES WHERE $COLUMN_NAME=\"$urlName\";")
    }

    //PRINT OUT THEHISTORY AS STRING
    fun databaseToString(): List<String> {
        val db = writableDatabase
        val query = "SELECT * FROM $TABLE_SITES"
        val dbstring: MutableList<String> = ArrayList()
        val c = db.rawQuery(query, null)
        c.moveToFirst()
        val i = 0
        if (c.moveToNext()) {
            do {
                if (c.getString(c.getColumnIndexOrThrow(COLUMN_NAME)) != null) {
                    var bstring = ""
                    bstring += c.getString(c.getColumnIndexOrThrow("url"))
                    dbstring.add(bstring)
                }
            } while (c.moveToNext())
        }
        return dbstring
    }

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "sites.db" //name of file
        const val TABLE_SITES = "sites" //name of table
        const val COLUMN_ID = "_id"
        const val COLUMN_NAME = "url"
    }
}
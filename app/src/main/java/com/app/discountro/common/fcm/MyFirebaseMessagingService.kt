package com.app.discountro.common.fcm

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.app.discountro.MyApplication
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.SharedPrefsUtils
import com.app.discountro.common.fcm.NotificationUtils

class MyFirebaseMessagingService : FirebaseMessagingService() {



    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
//    override fun onMessageReceived(remoteMessage: RemoteMessage) {

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Log.e("Device token", "2==>$token")
        getSharedPreferences(SharedPrefsUtils.sharedprefernce_key, SharedPrefsUtils.mode).edit().putString(KeysUtils.keyFirebaseToken, token).commit()
//        MyApplication.context.setString(DEVICE_TOKEN, token)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.e("NOTI_LOGS", "onMessageServiceX==>${remoteMessage.data}")
        val notificationUtils = NotificationUtils(MyApplication.context)
        notificationUtils.startNotification(remoteMessage)
    }



}


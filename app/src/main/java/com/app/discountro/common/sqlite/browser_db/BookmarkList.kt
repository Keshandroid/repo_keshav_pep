package com.app.discountro.common.sqlite.browser_db

import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.os.Build
import android.preference.PreferenceManager
import androidx.annotation.RequiresApi
import java.util.*

class BookmarkList(
    private val c: Context
) {
    private class DatabaseHelper internal constructor(context: Context?) :
        SQLiteOpenHelper(context, dbName, null, dbVersion) {
        override fun onCreate(db: SQLiteDatabase) {
            db.execSQL("CREATE TABLE IF NOT EXISTS $dbTable (_id INTEGER PRIMARY KEY autoincrement, pass_title, pass_content, pass_icon, pass_attachment, pass_creation, UNIQUE(pass_content))")
        }

        override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
            db.execSQL("DROP TABLE IF EXISTS $dbTable")
            onCreate(db)
        }
    }

    private var sqlDb: SQLiteDatabase? = null
    @Throws(SQLException::class)
    fun open() {
        val dbHelper = DatabaseHelper(c)
        sqlDb = dbHelper.writableDatabase
    }

    //insert data
    fun insert(
        pass_title: String,
        pass_content: String,
        pass_icon: String,
        pass_attachment: String,
        pass_creation: String
    ) {
        if (!isExist(pass_content)) {
            sqlDb!!.execSQL("INSERT INTO pass (pass_title, pass_content, pass_icon, pass_attachment, pass_creation) VALUES('$pass_title','$pass_content','$pass_icon','$pass_attachment','$pass_creation')")
        }
    }

    //check entry already in database or not
    fun isExist(pass_content: String): Boolean {
        val query = "SELECT pass_title FROM pass WHERE pass_content='$pass_content' LIMIT 1"
        @SuppressLint("Recycle") val row = sqlDb!!.rawQuery(query, null)
        return row.moveToFirst()
    }

    //edit data
    fun update(
        id: Int,
        pass_title: String,
        pass_content: String,
        pass_icon: String,
        pass_attachment: String,
        pass_creation: String?
    ) {
        sqlDb!!.execSQL("UPDATE $dbTable SET pass_title='$pass_title', pass_content='$pass_content', pass_icon='$pass_icon', pass_attachment='$pass_attachment', pass_creation='$pass_creation'   WHERE _id=$id")
    }

    //delete data
    fun delete(id: Int) {
        sqlDb!!.execSQL("DELETE FROM $dbTable WHERE _id=$id")
    }

    //fetch data
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun fetchAllData(activity: Context?): Cursor? {
        val sp = PreferenceManager.getDefaultSharedPreferences(activity)
        val columns = arrayOf(
            "_id",
            "pass_title",
            "pass_content",
            "pass_icon",
            "pass_attachment",
            "pass_creation"
        )
        when (Objects.requireNonNull(sp.getString("sortDBB", "title"))) {
            "title" -> return sqlDb!!.query(
                dbTable, columns, null, null, null, null, "pass_title" + " COLLATE NOCASE DESC;"
            )
            "icon" -> {
                val orderBy =
                    "pass_creation" + " COLLATE NOCASE DESC;" + "," + "pass_title" + " COLLATE NOCASE ASC;"
                return sqlDb!!.query(dbTable, columns, null, null, null, null, orderBy)
            }
        }
        return null
    }

    //fetch data by filter
    @Throws(SQLException::class)
    fun fetchDataByFilter(inputText: String?, filterColumn: String): Cursor? {
        val row: Cursor?
        var query = "SELECT * FROM $dbTable"
        if (inputText == null || inputText.isEmpty()) {
            row = sqlDb!!.rawQuery(query, null)
        } else {
            query =
                "SELECT * FROM $dbTable WHERE $filterColumn like '%$inputText%'"
            row = sqlDb!!.rawQuery(query, null)
        }
        row?.moveToFirst()
        return row
    }

    companion object {
        //define static variable
        private const val dbVersion = 7
        private const val dbName = "pass_DB_v01.db"
        private const val dbTable = "pass"
    }
}
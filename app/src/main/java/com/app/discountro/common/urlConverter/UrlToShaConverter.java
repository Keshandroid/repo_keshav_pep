package com.app.discountro.common.urlConverter;

import android.util.Log;

import java.math.BigInteger;
import java.security.MessageDigest;

public class UrlToShaConverter {

    public static String getConvertedSHA(String URL) {
        String SHA = "";

        String[] threshold = {"https://", "http://","www."};
        for (String index : threshold) {
            if (URL.contains(index)) {
                URL = URL.replace(index,"").trim();
            }
        }

        if (URL.contains("?")) {
            int indexOf = URL.indexOf("?");
            URL = URL.substring(0, indexOf);
        }

        //System.out.println(value);

        if (URL.endsWith("/")) {
            URL = URL.substring(0,URL.length() - 1);
        }

        System.out.println(URL);

        // With the java libraries
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.reset();
            digest.update(URL.getBytes("utf8"));
            SHA = String.format("%040x", new BigInteger(1, digest.digest()));
        } catch (Exception e){
            e.printStackTrace();
        }
        if (SHA.equals("1e4bf2251e6f346502e41cfa9eeebeb523b14e57")) {
            Log.d("SHA_GENERATED", "Matched");
        } else {
            Log.d("SHA_GENERATED", "Not Matched");
        }

        return SHA;
    }

}

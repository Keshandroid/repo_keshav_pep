
package com.app.discountro.common.my_interface

interface OnScrollableChangeListener {
    fun onScrollableChanged(scrollable: Boolean)
}
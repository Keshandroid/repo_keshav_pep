package com.app.discountro.common.sqlite.record

class Record {
    var title: String?
    var uRL: String?
    var time: Long

    constructor() {
        title = null
        uRL = null
        time = 0L
    }

    constructor(title: String?, url: String?, time: Long) {
        this.title = title
        uRL = url
        this.time = time
    }
}
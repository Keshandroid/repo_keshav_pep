package com.app.discountro.common.sqlite.offerCoupons

class CouponCodesSearchedModel(
    val searchTime: String,
    val couponData: String,
    val websiteName: String)

class CopiedCouponsModel(
    val offerID: String,
    val couponCode: String,
    val websiteName: String,
    val searchTime: String,
)
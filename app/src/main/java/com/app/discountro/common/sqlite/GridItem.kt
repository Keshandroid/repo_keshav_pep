package com.app.discountro.common.sqlite

class GridItem {
    var title: String?
    var uRL: String?
    var filename: String?
    var ordinal: Int

    constructor() {
        title = null
        uRL = null
        filename = null
        ordinal = -1
    }

    constructor(title: String?, url: String?, filename: String?, ordinal: Int) {
        this.title = title
        uRL = url
        this.filename = filename
        this.ordinal = ordinal
    }
}
package com.app.discountro.common.room.dao

import androidx.room.*
import com.app.discountro.common.room.model.CouponSavedModel
import io.reactivex.Completable
import io.reactivex.Maybe

@Dao
interface CouponCodeSaved {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun add(coupon: CouponSavedModel): Completable

/*    @Query("SELECT * FROM coupons WHERE idOffer IN (:idOffer)")
    fun loadAllByOfferIds(idOffer: String): List<CouponSavedModel>*/

    @Delete
    fun remove(coupon: CouponSavedModel): Completable

    @Query("SELECT * FROM coupons")
    fun getAll(): Maybe<List<CouponSavedModel>>

    @Query("SELECT * FROM coupons WHERE code like :code AND idOffer like :idOffer")
    fun getCopiedData(code: String, idOffer: String): Maybe<CouponSavedModel>

    @Insert
    fun insertAll(coupon: CouponSavedModel)

    @Delete
    fun delete(coupon: CouponSavedModel)
}
package com.app.discountro.common.urlConverter

import java.io.UnsupportedEncodingException
import java.math.BigInteger
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

class ConvertUrlToSHA {
    companion object{
        fun convertStringToSha(input: String): String? {
            var sha1: String? = ""
            try {
                val crypt = MessageDigest.getInstance("SHA-1")
                crypt.reset()
                crypt.update(input.toByteArray(charset("UTF-8")))
                sha1 = byteToHex(crypt.digest())
            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }
            return sha1
        }

        private fun byteToHex(hash: ByteArray): String? {
            val formatter = Formatter()
            for (b in hash) {
                formatter.format("%02x", b)
            }
            val result = formatter.toString()
            formatter.close()
            return result
        }

    }

    fun getConvertedSHA(URL: String): String {
        var URL = URL
        var SHA = ""
        val threshold = arrayOf("https://", "http://", "www.")
        for (index in threshold) {
            if (URL.contains(index)) {
                URL = URL.replace(index, "").trim { it <= ' ' }
            }
        }
        if (URL.contains("?")) {
            val indexOf = URL.indexOf("?")
            URL = URL.substring(0, indexOf)
        }

        //System.out.println(value);
        if (URL.endsWith("/")) {
            URL = URL.substring(0, URL.length - 1)
        }
        println(URL)

        // With the java libraries
        try {
            val digest = MessageDigest.getInstance("SHA-1")
            digest.reset()
            digest.update(URL.toByteArray(charset("utf8")))
            SHA = String.format("%040x", BigInteger(1, digest.digest()))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return SHA
    }

}


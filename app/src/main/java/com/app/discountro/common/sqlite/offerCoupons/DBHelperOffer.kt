package com.app.discountro.common.sqlite.offerCoupons

import android.annotation.SuppressLint
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteConstraintException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import android.database.sqlite.SQLiteOpenHelper

class DBHelperOffer(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_ENTRIES_SEARCHED)
        db.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES_SEARCHED)
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    @Throws(SQLiteConstraintException::class)
    fun insertSearchedOffer(couponCodesSearchedModel: CouponCodesSearchedModel): Boolean {
        // Gets the data repository in write mode
        val db = writableDatabase

        // Create a new map of values, where column names are the keys
        val values = ContentValues()
        values.put(DBOffer.SearchedCouponsEntry.TIME_OF_SEARCH, couponCodesSearchedModel.searchTime)
        values.put(DBOffer.SearchedCouponsEntry.COUPON_DATA, couponCodesSearchedModel.couponData)
        values.put(DBOffer.SearchedCouponsEntry.WEBSITE_NAME, couponCodesSearchedModel.websiteName)

        // Insert the new row, returning the primary key value of the new row
        val newRowId = db.insert(DBOffer.SearchedCouponsEntry.TABLE_NAME, null, values)

        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun insertOffer(couponCodesSearchedModel: CopiedCouponsModel): Boolean {
        // Gets the data repository in write mode
        val db = writableDatabase

        // Create a new map of values, where column names are the keys
        val values = ContentValues()
        values.put(DBOffer.OffersEntry.OFFER_ID, couponCodesSearchedModel.offerID)
        values.put(DBOffer.OffersEntry.COUPON_CODE, couponCodesSearchedModel.couponCode)
        values.put(DBOffer.OffersEntry.WEBSITE_NAME, couponCodesSearchedModel.websiteName)
        values.put(DBOffer.OffersEntry.TIME_OF_SEARCH, couponCodesSearchedModel.searchTime)

        // Insert the new row, returning the primary key value of the new row
        val newRowId = db.insert(DBOffer.OffersEntry.TABLE_NAME, null, values)

        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun deleteSearchedOffer(websiteName: String): Boolean {
        // Gets the data repository in write mode
        val db = writableDatabase
        // Define 'where' part of query.
        val selection = DBOffer.SearchedCouponsEntry.WEBSITE_NAME + " LIKE ?"
        // Specify arguments in placeholder order.
        val selectionArgs = arrayOf(websiteName)
        // Issue SQL statement.
        db.delete(DBOffer.SearchedCouponsEntry.TABLE_NAME, selection, selectionArgs)

        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun deleteOffer(offerId: String): Boolean {
        // Gets the data repository in write mode
        val db = writableDatabase
        // Define 'where' part of query.
        val selection = DBOffer.OffersEntry.OFFER_ID + " LIKE ?"
        // Specify arguments in placeholder order.
        val selectionArgs = arrayOf(offerId)
        // Issue SQL statement.
        db.delete(DBOffer.OffersEntry.TABLE_NAME, selection, selectionArgs)
        return true
    }

    @Throws(SQLiteConstraintException::class)
    fun deleteOfferByWebsite(websiteName: String): Boolean {
        // Gets the data repository in write mode
        val db = writableDatabase
        // Define 'where' part of query.
        val selection = DBOffer.OffersEntry.WEBSITE_NAME + " LIKE ?"
        // Specify arguments in placeholder order.
        val selectionArgs = arrayOf(websiteName)
        // Issue SQL statement.
        db.delete(DBOffer.OffersEntry.TABLE_NAME, selection, selectionArgs)
        return true
    }


    @SuppressLint("Range")
    fun readSearchedOffers(websiteName: String): ArrayList<CouponCodesSearchedModel> {
        val offers = ArrayList<CouponCodesSearchedModel>()
        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery(
                "select * from " + DBOffer.SearchedCouponsEntry.TABLE_NAME + " WHERE " + DBOffer.SearchedCouponsEntry.WEBSITE_NAME + "='" + websiteName + "'",
                null
            )
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            db.execSQL(SQL_CREATE_ENTRIES_SEARCHED)
            return ArrayList()
        }

        var searchTime: String

        var couponData: String
        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                searchTime =
                    cursor.getString(cursor.getColumnIndex(DBOffer.SearchedCouponsEntry.TIME_OF_SEARCH))
                couponData =
                    cursor.getString(cursor.getColumnIndex(DBOffer.SearchedCouponsEntry.COUPON_DATA))

                offers.add(CouponCodesSearchedModel(searchTime, couponData, websiteName))
                cursor.moveToNext()
            }
        }
        return offers
    }

    @SuppressLint("Range")
    fun readOffers(offerId: String): ArrayList<CopiedCouponsModel> {
        val offers = ArrayList<CopiedCouponsModel>()
        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery(
                "select * from " + DBOffer.OffersEntry.TABLE_NAME + " WHERE " + DBOffer.OffersEntry.WEBSITE_NAME + "='" + offerId + "'",
                null
            )
        } catch (e: SQLiteException) {
            // if table not yet present, create it
            db.execSQL(SQL_CREATE_ENTRIES)
            return ArrayList()
        }

        var websiteName: String
        var couponData: String
        var searchTime: String
        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                websiteName =
                    cursor.getString(cursor.getColumnIndex(DBOffer.OffersEntry.WEBSITE_NAME))
                couponData =
                    cursor.getString(cursor.getColumnIndex(DBOffer.OffersEntry.COUPON_CODE))
                searchTime =
                    cursor.getString(cursor.getColumnIndex(DBOffer.OffersEntry.TIME_OF_SEARCH))

                offers.add(CopiedCouponsModel(offerId, couponData, websiteName, searchTime))
                cursor.moveToNext()
            }
        }
        return offers
    }

    @SuppressLint("Range")
    fun readAllSearchedOffers(): ArrayList<CouponCodesSearchedModel> {
        val offers = ArrayList<CouponCodesSearchedModel>()
        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery("select * from " + DBOffer.SearchedCouponsEntry.TABLE_NAME, null)
        } catch (e: SQLiteException) {
            db.execSQL(SQL_CREATE_ENTRIES_SEARCHED)
            return ArrayList()
        }

        var timeOfSearch: String
        var couponsData: String
        var websiteName: String
        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                timeOfSearch =
                    cursor.getString(cursor.getColumnIndex(DBOffer.SearchedCouponsEntry.TIME_OF_SEARCH))
                couponsData =
                    cursor.getString(cursor.getColumnIndex(DBOffer.SearchedCouponsEntry.COUPON_DATA))
                websiteName =
                    cursor.getString(cursor.getColumnIndex(DBOffer.SearchedCouponsEntry.WEBSITE_NAME))

                offers.add(CouponCodesSearchedModel(timeOfSearch, couponsData, websiteName))
                cursor.moveToNext()
            }
        }
        return offers
    }


    @SuppressLint("Range")
    fun readAllOffers(): ArrayList<CopiedCouponsModel> {
        val offers = ArrayList<CopiedCouponsModel>()
        val db = writableDatabase
        var cursor: Cursor? = null
        try {
            cursor = db.rawQuery("select * from " + DBOffer.OffersEntry.TABLE_NAME, null)
        } catch (e: SQLiteException) {
            db.execSQL(SQL_CREATE_ENTRIES)
            return ArrayList()
        }

        var offerId: String
        var couponCode: String
        var websiteName: String
        var searchTime: String
        if (cursor!!.moveToFirst()) {
            while (!cursor.isAfterLast) {
                offerId =
                    cursor.getString(cursor.getColumnIndex(DBOffer.OffersEntry.OFFER_ID))
                couponCode =
                    cursor.getString(cursor.getColumnIndex(DBOffer.OffersEntry.COUPON_CODE))
                websiteName =
                    cursor.getString(cursor.getColumnIndex(DBOffer.OffersEntry.WEBSITE_NAME))
                searchTime =
                    cursor.getString(cursor.getColumnIndex(DBOffer.OffersEntry.TIME_OF_SEARCH))

                offers.add(CopiedCouponsModel(offerId, couponCode, websiteName, searchTime))
                cursor.moveToNext()
            }
        }
        return offers
    }

    companion object {
        // If you change the database schema, you must increment the database version.
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "CouponsCode.db"

        private const val SQL_CREATE_ENTRIES_SEARCHED =
            "CREATE TABLE " + DBOffer.SearchedCouponsEntry.TABLE_NAME + " (" +
                    DBOffer.SearchedCouponsEntry.WEBSITE_NAME + " TEXT PRIMARY KEY," +
                    DBOffer.SearchedCouponsEntry.COUPON_DATA + " TEXT," +
                    DBOffer.SearchedCouponsEntry.TIME_OF_SEARCH + " TEXT)"

        private const val SQL_CREATE_ENTRIES =
            "CREATE TABLE " + DBOffer.OffersEntry.TABLE_NAME + " (" +
                    DBOffer.OffersEntry.OFFER_ID + " TEXT PRIMARY KEY," +
                    DBOffer.OffersEntry.COUPON_CODE + " TEXT," +
                    DBOffer.OffersEntry.WEBSITE_NAME + " TEXT," +
                    DBOffer.SearchedCouponsEntry.TIME_OF_SEARCH + " TEXT)"


        private const val SQL_DELETE_ENTRIES_SEARCHED =
            "DROP TABLE IF EXISTS " + DBOffer.SearchedCouponsEntry.TABLE_NAME
        private const val SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + DBOffer.OffersEntry.TABLE_NAME
    }

}
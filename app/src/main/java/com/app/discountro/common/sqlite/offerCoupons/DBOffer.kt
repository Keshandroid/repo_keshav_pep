package com.app.discountro.common.sqlite.offerCoupons

import android.provider.BaseColumns

class DBOffer {

    class OffersEntry : BaseColumns {
        companion object {
            const val TABLE_NAME = "couponCode"
            const val OFFER_ID = "offerId"
            const val COUPON_CODE = "coupon_code"
            const val WEBSITE_NAME = "website_name"
            const val TIME_OF_SEARCH = "time_of_search"
        }
    }

    class SearchedCouponsEntry : BaseColumns {
        companion object {
            const val TABLE_NAME = "searched_coupons"
            const val TIME_OF_SEARCH = "time_of_search"
            const val COUPON_DATA = "coupon_data"
            const val WEBSITE_NAME = "website_name"
        }
    }

}
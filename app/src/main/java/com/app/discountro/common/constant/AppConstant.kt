package com.app.discountro.common.constant

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.provider.Settings
import android.util.Base64
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.app.discountro.R
import com.app.discountro.ui.splash.model.MyTranslationVarModel
import com.app.discountro.utils.KeysUtils
import com.app.discountro.utils.SharedPrefsUtils
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

/**
 * This class is used for declaring constant values which ar used in app
 */

class AppConstant {

    companion object {
        var progressDiscRoDialog: Dialog? = null
        const val API_AUTH_USERNAME = "50096754-1122-44a0-aaa6-4a1e04b9a062"
        const val API_AUTH_SECREAT = ""
    }

    fun showDiscoRoProgress(context: Context) {
        /*  val myTransDataWelcomeActivity: MyTranslationVarModel?
          val myTranslationArrayData: ArrayList<MyTranslationVarModel> = arrayListOf()
          if (SharedPrefsUtils.getModelPreferences(KeysUtils.KeyModelTranslation, MyTranslationVarModel::class.java) != ""
          ) {
              val myTranslationVarModel: MyTranslationVarModel? = SharedPrefsUtils.getModelPreferences(
                  KeysUtils.KeyModelTranslation,
                  MyTranslationVarModel::class.java
              ) as MyTranslationVarModel
              if (myTranslationVarModel != null) {
                  myTranslationArrayData.add(myTranslationVarModel)
              }
              myTranslationArrayData[0]
          }*/
        try {
            //  myTransDataWelcomeActivity = myTranslationArrayData[0]

            progressDiscRoDialog = null
            progressDiscRoDialog = Dialog(context)
            if (progressDiscRoDialog != null) {
                if (progressDiscRoDialog?.isShowing == true) {
                    progressDiscRoDialog?.dismiss()
                }
            }
            progressDiscRoDialog?.setCancelable(false)
            progressDiscRoDialog?.setContentView(R.layout.disc_ro_progress)

            if (SharedPrefsUtils.getModelPreferences(
                    KeysUtils.KeyModelTranslation, MyTranslationVarModel
                    ::class.java
                ) != null
            ) {
                val tvProgressTxt =
                    progressDiscRoDialog?.findViewById(R.id.tvProgressTxt) as TextView
                tvProgressTxt.text = SharedPrefsUtils.getStringPreference(KeysUtils.KeyPleaseWait)
                    .ifEmpty { context.resources.getString(R.string.please_wait) }

            }

            progressDiscRoDialog?.window!!.setBackgroundDrawableResource(R.color.transparent_Progress_100)
            progressDiscRoDialog?.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun dismissDiscRoProgress() {
        if (progressDiscRoDialog?.isShowing == true || progressDiscRoDialog != null) {
            progressDiscRoDialog?.dismiss()
        }
    }

    fun getHashKey(pContext: Context) {
        try {
            val info: PackageInfo = pContext.packageManager
                .getPackageInfo(pContext.packageName, PackageManager.GET_SIGNATURES)
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                val hashKey = String(Base64.encode(md.digest(), 0))
                Log.e("KeyStone", "printHashKey() Hash Key: $hashKey")
            }
        } catch (e: NoSuchAlgorithmException) {
            Log.e("KeyStoneError", "printHashKey()", e)
        } catch (e: Exception) {
            Log.e("KeyStoneError", "printHashKey()", e)
        }
    }

    /**
     * This annotation is used for some default values which will be used in app
     */
    annotation class DefaultValues {
        companion object {
            const val minAge: Int = -21
            const val SPLASH_DISPLAY_LENGTH: Long = 1500
            const val DATABASE_NAME: String = "DiscountRo"
            const val remoteMessage = "remote_message"
            const val dateFormat = "yyyy-MM-dd HH:mm:ss"

        }
    }

    fun showAlertMessage(
        mContext: Context,
        title: String,
        message: String,
        btnPositive: String,
        btnNegative: String
    ) {
        val alertDialog: AlertDialog.Builder = AlertDialog.Builder(mContext)

        // Setting Dialog Title
        alertDialog.setTitle(title)

        // Setting Dialog Message
        alertDialog.setMessage(message)

        // On pressing Settings button
        alertDialog.setPositiveButton(btnPositive)
        { _, _ ->
            val intent = Intent(Settings.ACTION_SETTINGS)
            mContext.startActivity(intent)
        }
        alertDialog.setNegativeButton(btnNegative) { dialog, _ ->
            dialog.dismiss()
        }

        val alert: AlertDialog = alertDialog.show()
        alert.getButton(AlertDialog.BUTTON_POSITIVE)
            .setTextColor(mContext.resources.getColor(R.color.black))
        alert.getButton(AlertDialog.BUTTON_NEGATIVE)
            .setTextColor(mContext.resources.getColor(R.color.black))
    }


    fun setLocal(Lang: String, baseContext: Context) {

        val locale = Locale(Lang)

        Locale.setDefault(locale)

        val config = Configuration()

        config.locale = locale
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
    }

    /**
     * This annotation is used for opening screen after app launch
     */
    annotation class OpenScreenType {
        companion object {
            const val OpenScreenValue = "OpenScreenValue"
            const val LoginScreen: Int = 1
            const val MainHomeScreen: Int = 2
        }
    }

    /**
     * This annotation is used for opening Order Details Screen Type
     */
    annotation class CheckClientToken {
        companion object {
            const val tokenNotAvailable: Int = 0
            const val tokenAvailable: Int = 1

        }
    }

    /**
     * This annotation is used for opening Order Details Screen
     */
    annotation class OpenDetailsScreenType {
        companion object {
            const val OpenScreenValue = "OpenScreenValue"
            const val RequestDetailsScreen: Int = 1
            const val CurrentOrderDetailsScreen: Int = 2
            const val PastOrderDetailsScreen: Int = 3
            const val ChatScreen: Int = 4
        }
    }

    /**
     * Used for Earning type
     */
    annotation class Earning {
        companion object {
            const val Daily = 0
            const val Weekly = 1
            const val Monthly = 2

        }
    }

    /**
     * This annotation is used for Notification Type on Notification Screen
     */


    /**
     * Used for Url type
     */
    annotation class UrlType {
        companion object {
            val ShopLogo = "Shop"
            val ShopGallery = "ShopGallery"
            val ProductLogo = "Product"
            val ProductGallery = "ProductGallery"
            val Users = "Users"
            val Driver = "Driver"
            val Orders = "Orders"
        }
    }

    /**
     * Used for Url type
     */
    annotation class UrlReplaceValue {
        companion object {
            val ImageName = "{ImageName}"
            val Id = "{Id}"
        }
    }


    /**
     * This annotation class is used for view type in adapter class.
     */
    annotation class ViewType {
        companion object {
            const val Title: Int = 0
            const val Data: Int = 1
            const val NoData: Int = 2
            const val NoSearchData: Int = 3
        }
    }

    /**
     * This annotation class is used for view type in adapter class.
     */
    annotation class ChatViewType {
        companion object {
            const val SenderData: Int = 1
            const val ReceiverData: Int = 2
            const val NoData: Int = 3
        }
    }

    /**
     * This annotation class is used for Login type in adapter class.
     */
    annotation class LoginType {
        companion object {
            const val Normal = "1"
            const val FaceBook = "2"
            const val Google = "3"
            const val Apple = "4"
        }
    }

    /**
     * This annotation class is used for request code type in app for activity is result
     */
    annotation class RequestCodeType {
        companion object {
            const val Google = 1001
            const val Facebook = 1002
        }
    }

    /**
     * Date filter type
     */
    annotation class UserAvailableCheck {
        companion object {
            const val Is_User_Available_Register = 0
            const val Is_User_Available_Login = 1
        }
    }

    /**
     * This annotation class is used for social media user exist check : is_for_validation
     */
    annotation class SocialMediaUserCheck {
        companion object {
            const val LoginUser = "1"
            const val RegisterUser = "0"
        }
    }

    /**
     * Date filter type
     */
    annotation class DateFilterType {
        companion object {
            const val Default = "Default"
            const val Day = "Day"
            const val Week = "Week"
            const val Month = "Month"
            const val Custom = "Custom"
        }
    }

    /**
     * Used Order Status
     */
    annotation class OrderStatus {
        companion object {
            const val Pending = "Pending"
            val AcceptedByShop = "AcceptedByShop"
            val AcceptedByDriver = "AcceptedByDriver"
            val Confirmed = "Confirmed"
            val OrderLeft = "OrderLeft"
            val Delivered = "Delivered"
            val RejectedByShop = "RejectedByShop"
            val Rejected = "Rejected"
            val Cancelled = "Cancelled"
        }
    }


}
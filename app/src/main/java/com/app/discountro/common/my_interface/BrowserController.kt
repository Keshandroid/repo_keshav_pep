package com.app.discountro.common.my_interface

import android.net.Uri
import android.view.View
import android.webkit.ValueCallback
import android.webkit.WebChromeClient.CustomViewCallback
import android.webkit.WebView
import com.app.discountro.common.album_service.AlbumController

interface BrowserController {
    fun updateAutoComplete()
    fun updateBookmarks()
    fun updateProgress(progress: Int)
    fun showAlbum(albumController: AlbumController?, isIncoTab: Boolean = false)
    fun removeAlbum(albumController: AlbumController?,isIncoTab: Boolean=false)
    fun showFileChooser(filePathCallback: ValueCallback<Array<Uri>>)
    fun onShowCustomView(view: View?, callback: CustomViewCallback?)
    fun onHideCustomView(): Boolean
    fun onLongPress(url: String?)
    fun hideOverview()
    fun updateTabsAdapterData(progress: Int, webView: WebView)
    fun addNewTab(url: String)
    fun updateProgressBar(progress: Int)
    fun hideProgressBar(progress: Int)
}
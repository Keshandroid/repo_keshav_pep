package com.app.discountro.common.sqlite.record

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.preference.PreferenceManager
import com.app.discountro.common.sqlite.GridItem
import java.util.*

class RecordAction(context: Context?) {
    private var database: SQLiteDatabase? = null
    private val helper: RecordHelper
    fun open(rw: Boolean) {
        database = if (rw) helper.writableDatabase else helper.readableDatabase
    }

    fun close() {
        helper.close()
    }

    fun addHistory(record: Record?) {
        if (record?.title == null || record.title!!.trim { it <= ' ' }.isEmpty()
            || record.uRL == null || record.uRL!!.trim { it <= ' ' }.isEmpty()
            || record.time < 0L
        ) {
            return
        }
        val values = ContentValues()
        values.put(RecordUnit.COLUMN_TITLE, record.title!!.trim { it <= ' ' })
        values.put(RecordUnit.COLUMN_URL, record.uRL!!.trim { it <= ' ' })
        values.put(RecordUnit.COLUMN_TIME, record.time)
        database!!.insert(RecordUnit.TABLE_HISTORY, null, values)
    }

    fun addDomain(domain: String?) {
        if (domain == null || domain.trim { it <= ' ' }.isEmpty()) {
            return
        }
        val values = ContentValues()
        values.put(RecordUnit.COLUMN_DOMAIN, domain.trim { it <= ' ' })
        database!!.insert(RecordUnit.TABLE_WHITELIST, null, values)
    }

    fun addDomainJS(domain: String?) {
        if (domain == null || domain.trim { it <= ' ' }.isEmpty()) {
            return
        }
        val values = ContentValues()
        values.put(RecordUnit.COLUMN_DOMAIN, domain.trim { it <= ' ' })
        database!!.insert(RecordUnit.TABLE_JAVASCRIPT, null, values)
    }

    fun addDomainCookie(domain: String?) {
        if (domain == null || domain.trim { it <= ' ' }.isEmpty()) {
            return
        }
        val values = ContentValues()
        values.put(RecordUnit.COLUMN_DOMAIN, domain.trim { it <= ' ' })
        database!!.insert(RecordUnit.TABLE_COOKIE, null, values)
    }

    fun addGridItem(item: GridItem?): Boolean {
        if (item?.title == null || item.title!!.trim { it <= ' ' }.isEmpty()
            || item.uRL == null || item.uRL!!.trim { it <= ' ' }.isEmpty()
            || item.filename == null || item.filename!!.trim { it <= ' ' }.isEmpty()
            || item.ordinal < 0
        ) {
            return false
        }
        val values = ContentValues()
        values.put(RecordUnit.COLUMN_TITLE, item.title!!.trim { it <= ' ' })
        values.put(RecordUnit.COLUMN_URL, item.uRL!!.trim { it <= ' ' })
        values.put(RecordUnit.COLUMN_FILENAME, item.filename!!.trim { it <= ' ' })
        values.put(RecordUnit.COLUMN_ORDINAL, item.ordinal)
        database!!.insert(RecordUnit.TABLE_GRID, null, values)
        return true
    }

    fun updateGridItem(item: GridItem?) {
        if (item == null || item.title == null || item.title!!.trim { it <= ' ' }.isEmpty()
            || item.uRL == null || item.uRL!!.trim { it <= ' ' }.isEmpty()
            || item.filename == null || item.filename!!.trim { it <= ' ' }.isEmpty()
            || item.ordinal < 0
        ) {
            return
        }
        val values = ContentValues()
        values.put(RecordUnit.COLUMN_TITLE, item.title!!.trim { it <= ' ' })
        values.put(RecordUnit.COLUMN_URL, item.uRL!!.trim { it <= ' ' })
        values.put(RecordUnit.COLUMN_FILENAME, item.filename!!.trim { it <= ' ' })
        values.put(RecordUnit.COLUMN_ORDINAL, item.ordinal)
        database!!.update(
            RecordUnit.TABLE_GRID,
            values,
            RecordUnit.COLUMN_URL + "=?",
            arrayOf(item.uRL)
        )
    }

    fun deleteHistoryOld(domain: String?) {
        if (domain == null || domain.trim { it <= ' ' }.isEmpty()) {
            return
        }
        database!!.execSQL("DELETE FROM " + RecordUnit.TABLE_HISTORY + " WHERE " + RecordUnit.COLUMN_URL + " = " + "\"" + domain.trim { it <= ' ' } + "\"")
    }

    fun checkHistory(url: String?): Boolean {
        if (url == null || url.trim { it <= ' ' }.isEmpty()) {
            return false
        }
        val cursor = database!!.query(
            RecordUnit.TABLE_HISTORY, arrayOf(RecordUnit.COLUMN_URL),
            RecordUnit.COLUMN_URL + "=?", arrayOf(url.trim { it <= ' ' }),
            null,
            null,
            null
        )
        if (cursor != null) {
            val result = cursor.moveToFirst()
            cursor.close()
            return result
        }
        return false
    }

    fun checkDomain(domain: String?): Boolean {
        if (domain == null || domain.trim { it <= ' ' }.isEmpty()) {
            return false
        }
        val cursor = database!!.query(
            RecordUnit.TABLE_WHITELIST, arrayOf(RecordUnit.COLUMN_DOMAIN),
            RecordUnit.COLUMN_DOMAIN + "=?", arrayOf(domain.trim { it <= ' ' }),
            null,
            null,
            null
        )
        if (cursor != null) {
            val result = cursor.moveToFirst()
            cursor.close()
            return result
        }
        return false
    }

    fun checkDomainJS(domain: String?): Boolean {
        if (domain == null || domain.trim { it <= ' ' }.isEmpty()) {
            return false
        }
        val cursor = database!!.query(
            RecordUnit.TABLE_JAVASCRIPT, arrayOf(RecordUnit.COLUMN_DOMAIN),
            RecordUnit.COLUMN_DOMAIN + "=?", arrayOf(domain.trim { it <= ' ' }),
            null,
            null,
            null
        )
        if (cursor != null) {
            val result = cursor.moveToFirst()
            cursor.close()
            return result
        }
        return false
    }

    fun checkDomainCookie(domain: String?): Boolean {
        if (domain == null || domain.trim { it <= ' ' }.isEmpty()) {
            return false
        }
        val cursor = database!!.query(
            RecordUnit.TABLE_COOKIE, arrayOf(RecordUnit.COLUMN_DOMAIN),
            RecordUnit.COLUMN_DOMAIN + "=?", arrayOf(domain.trim { it <= ' ' }),
            null,
            null,
            null
        )
        if (cursor != null) {
            val result = cursor.moveToFirst()
            cursor.close()
            return result
        }
        return false
    }

    fun checkGridItem(url: String?): Boolean {
        if (url == null || url.trim { it <= ' ' }.isEmpty()) {
            return false
        }
        val cursor = database!!.query(
            RecordUnit.TABLE_GRID, arrayOf(RecordUnit.COLUMN_URL),
            RecordUnit.COLUMN_URL + "=?", arrayOf(url.trim { it <= ' ' }),
            null,
            null,
            null
        )
        if (cursor != null) {
            val result = cursor.moveToFirst()
            cursor.close()
            return result
        }
        return false
    }

    fun deleteHistory(record: Record?) {
        if (record == null || record.time <= 0) {
            return
        }
        database!!.execSQL("DELETE FROM " + RecordUnit.TABLE_HISTORY + " WHERE " + RecordUnit.COLUMN_TIME + " = " + record.time)
    }

    fun deleteDomain(domain: String?) {
        if (domain == null || domain.trim { it <= ' ' }.isEmpty()) {
            return
        }
        database!!.execSQL("DELETE FROM " + RecordUnit.TABLE_WHITELIST + " WHERE " + RecordUnit.COLUMN_DOMAIN + " = " + "\"" + domain.trim { it <= ' ' } + "\"")
    }

    fun deleteDomainJS(domain: String?) {
        if (domain == null || domain.trim { it <= ' ' }.isEmpty()) {
            return
        }
        database!!.execSQL("DELETE FROM " + RecordUnit.TABLE_JAVASCRIPT + " WHERE " + RecordUnit.COLUMN_DOMAIN + " = " + "\"" + domain.trim { it <= ' ' } + "\"")
    }

    fun deleteDomainCookie(domain: String?) {
        if (domain == null || domain.trim { it <= ' ' }.isEmpty()) {
            return
        }
        database!!.execSQL("DELETE FROM " + RecordUnit.TABLE_COOKIE + " WHERE " + RecordUnit.COLUMN_DOMAIN + " = " + "\"" + domain.trim { it <= ' ' } + "\"")
    }

    fun deleteGridItem(item: GridItem?) {
        if (item == null || item.uRL == null || item.uRL!!.trim { it <= ' ' }.isEmpty()) {
            return
        }
        database!!.execSQL("DELETE FROM " + RecordUnit.TABLE_GRID + " WHERE " + RecordUnit.COLUMN_URL + " = " + "\"" + item.uRL!!.trim { it <= ' ' } + "\"")
    }

    fun clearHome() {
        database!!.execSQL("DELETE FROM " + RecordUnit.TABLE_GRID)
    }

    fun clearHistory() {
        database!!.execSQL("DELETE FROM " + RecordUnit.TABLE_HISTORY)
    }

    fun clearDomains() {
        database!!.execSQL("DELETE FROM " + RecordUnit.TABLE_WHITELIST)
    }

    fun clearDomainsJS() {
        database!!.execSQL("DELETE FROM " + RecordUnit.TABLE_JAVASCRIPT)
    }

    fun clearDomainsCookie() {
        database!!.execSQL("DELETE FROM " + RecordUnit.TABLE_COOKIE)
    }

    private fun getRecord(cursor: Cursor): Record {
        val record = Record()
        record.title = cursor.getString(0)
        record.uRL = cursor.getString(1)
        record.time = cursor.getLong(2)
        return record
    }

    private fun getGridItem(cursor: Cursor): GridItem {
        val item = GridItem()
        item.title = cursor.getString(0)
        item.uRL = cursor.getString(1)
        item.filename = cursor.getString(2)
        item.ordinal = cursor.getInt(3)
        return item
    }

    fun listHistory(): MutableList<Record> {
        val list: MutableList<Record> = ArrayList()
        val cursor = database!!.query(
            RecordUnit.TABLE_HISTORY, arrayOf(
                RecordUnit.COLUMN_TITLE,
                RecordUnit.COLUMN_URL,
                RecordUnit.COLUMN_TIME
            ),
            null,
            null,
            null,
            null,
            RecordUnit.COLUMN_TIME + " asc"
        ) ?: return list
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            list.add(getRecord(cursor))
            cursor.moveToNext()
        }
        cursor.close()
        return list
    }

    fun listDomains(): MutableList<String> {
        val list: MutableList<String> = ArrayList()
        val cursor = database!!.query(
            RecordUnit.TABLE_WHITELIST, arrayOf(RecordUnit.COLUMN_DOMAIN),
            null,
            null,
            null,
            null,
            RecordUnit.COLUMN_DOMAIN
        ) ?: return list
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            list.add(cursor.getString(0))
            cursor.moveToNext()
        }
        cursor.close()
        return list
    }

    fun listDomainsJS(): MutableList<String> {
        val list: MutableList<String> = ArrayList()
        val cursor = database!!.query(
            RecordUnit.TABLE_JAVASCRIPT, arrayOf(RecordUnit.COLUMN_DOMAIN),
            null,
            null,
            null,
            null,
            RecordUnit.COLUMN_DOMAIN
        ) ?: return list
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            list.add(cursor.getString(0))
            cursor.moveToNext()
        }
        cursor.close()
        return list
    }

    fun listDomainsCookie(): MutableList<String> {
        val list: MutableList<String> = ArrayList()
        val cursor = database!!.query(
            RecordUnit.TABLE_COOKIE, arrayOf(RecordUnit.COLUMN_DOMAIN),
            null,
            null,
            null,
            null,
            RecordUnit.COLUMN_DOMAIN
        ) ?: return list
        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            list.add(cursor.getString(0))
            cursor.moveToNext()
        }
        cursor.close()
        return list
    }


    fun listGrid(context: Context?): List<GridItem>? {
        val list: MutableList<GridItem> = LinkedList()
        val list2: MutableList<GridItem> = LinkedList()
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        when (Objects.requireNonNull(sp.getString("sort_startSite", "ordinal"))) {
            "ordinal" -> {
                val cursor = database!!.query(
                    RecordUnit.TABLE_GRID, arrayOf(
                        RecordUnit.COLUMN_TITLE,
                        RecordUnit.COLUMN_URL,
                        RecordUnit.COLUMN_FILENAME,
                        RecordUnit.COLUMN_ORDINAL
                    ),
                    null,
                    null,
                    null,
                    null,
                    RecordUnit.COLUMN_ORDINAL
                ) ?: return list
                cursor.moveToFirst()
                while (!cursor.isAfterLast) {
                    list.add(getGridItem(cursor))
                    cursor.moveToNext()
                }
                cursor.close()
                return list
            }
            "title" -> {
                val cursor2 = database!!.query(
                    RecordUnit.TABLE_GRID, arrayOf(
                        RecordUnit.COLUMN_TITLE,
                        RecordUnit.COLUMN_URL,
                        RecordUnit.COLUMN_FILENAME,
                        RecordUnit.COLUMN_ORDINAL
                    ),
                    null,
                    null,
                    null,
                    null,
                    RecordUnit.COLUMN_TITLE
                ) ?: return list2
                cursor2.moveToFirst()
                while (!cursor2.isAfterLast) {
                    list2.add(getGridItem(cursor2))
                    cursor2.moveToNext()
                }
                cursor2.close()
                return list2
            }
        }
        return null
    }

    init {
        helper = RecordHelper(context)
    }
}
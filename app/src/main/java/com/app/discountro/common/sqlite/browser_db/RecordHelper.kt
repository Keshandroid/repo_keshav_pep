package com.app.discountro.common.sqlite.browser_db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class RecordHelper(context: Context?) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
    override fun onCreate(database: SQLiteDatabase) {
        database.execSQL(BrowserDbUnit.CREATE_HISTORY)
        database.execSQL(BrowserDbUnit.CREATE_WHITELIST)
        database.execSQL(BrowserDbUnit.CREATE_JAVASCRIPT)
        database.execSQL(BrowserDbUnit.CREATE_COOKIE)
        database.execSQL(BrowserDbUnit.CREATE_GRID)
        database.execSQL(BrowserDbUnit.CREATE_COUPON_CODE)
    }

    // UPGRADE ATTENTION!!!
    override fun onUpgrade(database: SQLiteDatabase, oldVersion: Int, newVersion: Int) {}

    companion object {
        private const val DATABASE_NAME = "DiscountRO.db"
        private const val DATABASE_VERSION = 1
    }
}
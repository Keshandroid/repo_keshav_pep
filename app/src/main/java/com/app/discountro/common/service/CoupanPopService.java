package com.app.discountro.common.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.discountro.Firebase.RetrieveDataFromFirebase;
import com.app.discountro.Interface.ComparableProductInterface;
import com.app.discountro.Interface.CouponListInterface;
import com.app.discountro.Interface.SemiComparableProductInterface;
import com.app.discountro.R;
import com.app.discountro.common.sqlite.offerCoupons.CopiedCouponsModel;
import com.app.discountro.common.sqlite.offerCoupons.DBHelperOffer;
import com.app.discountro.fireStore.models.CompareProductModel;
import com.app.discountro.fireStore.models.CouponsAllModel;
import com.app.discountro.fireStore.models.RoModel;
import com.app.discountro.ui.home.view.adapter.CallBackRecInt;
import com.app.discountro.ui.home.view.adapter.CompareProductAdapter;
import com.app.discountro.ui.home.view.adapter.CountriesCouponAdapter;
import com.app.discountro.ui.home.view.adapter.CouponsListAdapter;
import com.app.discountro.ui.home.view.models.CountryCouponsModel;
import com.app.discountro.ui.home.view.models.CountryNameAndDataModel;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

public class CoupanPopService extends Service {
    private BottomSheetBehavior sheetBehavior;
    private FrameLayout frameLayout;

    private WindowManager windowManager;
    private View chatHead;
    private View removeView;
    private View closeView;
    private ImageView imgDelete;
    private View bottomSheetView;
    private DBHelperOffer offersDBHelper;
    private CardView appLogoCard, contentLay;
    private Boolean isRemoveCardVisible = false;
    int actionBarHeight = 0;

    private CoordinatorLayout rootLay;
    private String websiteNameOf = "";

    private static final int MAX_CLICK_DURATION = 200;
    private long startClickTime;
    private boolean isVibrated = false;


    private int xLastPosition = -1;
    private int yLastPosition = -1;

    private String tabOpened = "first";



    private boolean isMoved = false;

    private static String URL_FOR_COMPARE_PRODUCTS = "", DOMAIN_FOR_COUPONS = "";

    private static Map<String, View> listOfUiElements = new HashMap<>();

    // UI elements:
    private TextView couponsTextView, compareProductTextView, couponCounter, popCounter, compareProductCounter;
    private TextView comparableProductName, comparableProductPriceRange;
    private TextView noCouponTextView;
    private ImageView closeBottomSheet, couponTabImage, compareProductTabImage, noCouponFoundImageIcon;
    private LinearLayout couponsTab, compareProductTab, nothingFoundLinearLayout;
    private RecyclerView countryRecyclerView, couponsRecyclerView;
    private ProgressBar progressBar;

    private CouponsAllModel couponsAllModel;
    private CompareProductModel compareProductModel;

    private int totalCouponFound = 0, totalCompareProductFound = 0;

    @Override
    public IBinder onBind(Intent intent) {
        // Not used
        Log.e("onBind: ", "service binded");
        return null;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onCreate() {
        super.onCreate();
        offersDBHelper = new DBHelperOffer(this);
        setUp();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String url = intent.getStringExtra("domain");
        if (!Objects.equals(url, "") && ((url.charAt(url.length() - 1)) + "").equals("/")) {
            url = url.substring(0, url.length() - 1);
            URL_FOR_COMPARE_PRODUCTS = url;
        } else if (!url.equals("") && (!url.endsWith("/"))) {
            URL_FOR_COMPARE_PRODUCTS = url;
        }

        Log.e("Domain", url);

        if (intent.hasExtra("domain")) {
            DOMAIN_FOR_COUPONS = intent.getStringExtra("domain");
            if (chatHead == null) {
                Log.d("VAL_CHECKING", "onStartCommand: -1");
                setUp();
            }
            couponsAllModel = null;
            compareProductModel = null;
            compareProduct(0);
            couponOfProduct();
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    compareProduct();
//                }
//            }, 1000);
            appLogoCard.performClick();
        } else {
            Log.d("VAL_CHECKING", "onStartCommand: 2");
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void tabImageUpdate(ImageView firstTabImg, ImageView secondTabImg) {
        firstTabImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_coupon));
        secondTabImg.setImageDrawable(getResources().getDrawable(R.drawable.ic_compare));
    }

    private void couponOfProduct() {
        couponsTextView.setText(String.valueOf("Coupons"));
        compareProductTextView.setText(getString(R.string.compareProducts));
        countryRecyclerView.setVisibility(View.GONE);
        tabImageUpdate(couponTabImage, compareProductTabImage);
        progressBar.setVisibility(View.VISIBLE);

        new RetrieveDataFromFirebase(offersDBHelper).getCouponCode(DOMAIN_FOR_COUPONS, new CouponListInterface() {
            @Override
            public void getCouponList(CouponsAllModel model) {
                couponsAllModel = model;
                //send coupan data
                parseNShowCoupon(couponsAllModel);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void errorOnCouponList(String error) {
                noCouponTextView.setText("There are no coupon available!");
                Log.d("CHECKING_ALLPRO", "errorOnCouponList: " + error);
                progressBar.setVisibility(View.GONE);
                //coupan count debug
                couponCounter.setText("0");
                compareProductCounter.setText("0");
                //popCounter.setText("0");
                totalCouponFound = 0;
                nothingFoundLinearLayout.setVisibility(View.VISIBLE);

                couponsRecyclerView.setAdapter(null);
                countryRecyclerView.setAdapter(null);
            }
        });
    }

    public void parseNShowCoupon(CouponsAllModel couponsAllModel) {
        progressBar.setVisibility(View.GONE);
        if (couponsAllModel.getCoupons() != null && couponsAllModel.getCoupons().size() > 0) {
            ArrayList<CountryNameAndDataModel> countryNameAndDataModelList = new ArrayList<>();
            ArrayList<CountryCouponsModel> countryData = new ArrayList<>();

            for (String key : Objects.requireNonNull(couponsAllModel.getCoupons()).keySet()) {

                CountryNameAndDataModel model = new CountryNameAndDataModel();
                model.setCountryName(key);

                RoModel roModel = new Gson().fromJson(Objects.requireNonNull(new Gson().toJson(couponsAllModel.getCoupons().get(key))), RoModel.class);
                model.setCountryData(roModel.getCouponList());

                CountryCouponsModel country = new Gson().fromJson(Objects.requireNonNull(new Gson().toJson(couponsAllModel.getCoupons().get(key))), CountryCouponsModel.class);
                countryData.add(country);

                CountryCouponsModel countryCouponsModel = new Gson().fromJson((new Gson().toJson(couponsAllModel.getCoupons().get(key))), CountryCouponsModel.class);
                model.setCountryListData(countryCouponsModel);

                countryNameAndDataModelList.add(model);

            }

            if (countryNameAndDataModelList.size() > 0) {
                nothingFoundLinearLayout.setVisibility(View.GONE);
                countryData.get(0).setSelectedCountry(true);
                CountriesCouponAdapter countriesCouponAdapter = new CountriesCouponAdapter(this, countryData, new CallBackRecInt() {
                    @Override
                    public void onItemSelected(@NonNull View view, int position) {
                        Log.d("ADAPTER_TESTING", "onItemSelected: 5");
                        setUpAdapter(countryNameAndDataModelList.get(position).getCountryData(), couponsRecyclerView);
                    }
                });
                countryRecyclerView.setAdapter(countriesCouponAdapter);
                countryRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                setUpAdapter(countryNameAndDataModelList.get(0).getCountryData(), couponsRecyclerView);
                Log.d("ADAPTER_TESTING", "onItemSelected: 6");
            } else {
                nothingFoundLinearLayout.setVisibility(View.VISIBLE);
            }
        }
        if (couponsAllModel.getCouponCount() != null) {
            totalCouponFound = couponsAllModel.getCouponCount();
        } else {
            totalCouponFound = 0;
        }

        Log.d("popCounter"," Coupon : " + totalCouponFound + "  Compare : " + totalCompareProductFound);

        popCounter.setText(String.valueOf(totalCouponFound + totalCompareProductFound));
        //coupan counter
        couponCounter.setText(couponsAllModel.getCouponCount().toString());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (chatHead != null) {
            windowManager.removeView(chatHead);
            chatHead = null;
        }
    }

    private void setUpAdapter(ArrayList<RoModel.CouponModel> countryList, RecyclerView rvCoupons) {

        ArrayList<CopiedCouponsModel> offersList = offersDBHelper.readAllOffers();

        /*Remove values from local data after 30 minutes of all websites*/
        for (int i = 0; i < offersList.size(); i++) {
            long resultTime = System.currentTimeMillis() - Long.parseLong(offersList.get(i).getSearchTime());
            int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(resultTime);
            Log.e("LEFT_TIME_BE", "setUpAdapter: " + minutes);
            if (minutes >= 30) {
                offersDBHelper.deleteOffer(offersList.get(i).getOfferID());
                offersList.remove(offersList.get(i));
            }
        }

        ArrayList<RoModel.CouponModel> countryListNew = checkCopiedValues(countryList, offersList);


        CouponsListAdapter couponsListAdapter = new CouponsListAdapter(this, countryListNew, (view, position) -> {
            String codeCopy = countryListNew.get(position).getCode();
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Coupon", codeCopy);
            clipboard.setPrimaryClip(clip);

            offersDBHelper.insertOffer(new
                            CopiedCouponsModel(
                            Objects.requireNonNull(countryListNew.get(position).getIdOffer()),
                            Objects.requireNonNull(countryListNew.get(position).getCode()),
                            websiteNameOf,
                            String.valueOf(System.currentTimeMillis())));

            //TOAST AD
            new Handler(Looper.getMainLooper()).post(() -> Toast.makeText(this, "Coupon Copied!!", Toast.LENGTH_SHORT).show());

            closeBottomSheet.performClick();
        }, false, "");

        couponsRecyclerView.setAdapter(couponsListAdapter);
        couponsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }


    private void parseNShowComparableProduct(CompareProductModel compareProductModel) {
        progressBar.setVisibility(View.GONE);
        if (compareProductModel != null) {
            nothingFoundLinearLayout.setVisibility(View.GONE);
            comparableProductName.setVisibility(View.VISIBLE);
            comparableProductPriceRange.setVisibility(View.VISIBLE);

            comparableProductName.setText(String.valueOf(compareProductModel.getProduct_name()));
            comparableProductPriceRange.setText(String.valueOf("Lei " + compareProductModel.getMin_price() + " - " + compareProductModel.getMax_price()));

            CompareProductAdapter adapter = new CompareProductAdapter(
                    this,
                    (view, directLink) -> {
                        openNewTabForCompareService(directLink);
                    }
                    , compareProductModel.getListOfSeller());
            couponsRecyclerView.setAdapter(adapter);
            compareProductCounter.setText(String.valueOf(compareProductModel.getListOfSeller().size()));
            couponsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            popCounter.setText(String.valueOf(compareProductModel.getSeller_count() + totalCouponFound));
            Log.d("COMPARE_PRODUCTS", "onComplete: 2");
        } else {
            comparableProductName.setVisibility(View.GONE);
            comparableProductPriceRange.setVisibility(View.GONE);
            //tvCountCoupon.setText("0");
            compareProductCounter.setText("0");
            couponsRecyclerView.setAdapter(null);
            nothingFoundLinearLayout.setVisibility(View.VISIBLE);
            noCouponFoundImageIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_compare));
            noCouponTextView.setText("There are no product found to compare!");
            popCounter.setText("0");
            Log.d("COMPARE_PRODUCTS", "onComplete: 3");
        }
    }

    private ArrayList<RoModel.CouponModel> checkCopiedValues(
            ArrayList<RoModel.CouponModel> countryList,
            ArrayList<CopiedCouponsModel> offersList) {
        for (int i = 0; i < offersList.size(); i++) {
            CopiedCouponsModel copied = offersList.get(i);
            for (int j = 0; j < countryList.size(); j++) {
                if (copied.getOfferID().equals(countryList.get(j).getIdOffer())) {
                    //offersDBHelper.deleteOffer(Objects.requireNonNull(countryList.get(j).getIdOffer()));
                    countryList.get(j).setCopied(true);
                }
            }
        }
        return countryList;
    }

    void vibrateAnimateDevice(){
        try {
            //Animate the view
            final Animation animShake = AnimationUtils.loadAnimation(this, R.anim.shake);
            imgDelete.startAnimation(animShake);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    void makeWindowVisible() {
        if (isRemoveCardVisible) {
            return;
        }

        isRemoveCardVisible = true;
        int LAYOUT_FLAG;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                PixelFormat.TRANSPARENT);


        removeView = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.item_pop_bottom, null, false);
        closeView = removeView.findViewById(R.id.closeView);
        imgDelete = removeView.findViewById(R.id.imgDelete);


        windowManager.addView(removeView, params);


    }


    void makeWindowInvisible() {
        if (isRemoveCardVisible) {
            windowManager.removeView(removeView);
        }
        isRemoveCardVisible = false;
    }

    @SuppressLint("ClickableViewAccessibility")
    void setUp() {

        // Calculate ActionBar height
        TypedValue tv = new TypedValue();

        if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
            Log.e("setUp: ", actionBarHeight + "");
        }

        windowManager = (WindowManager) getSystemService(WINDOW_SERVICE);


        chatHead = ((LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.popup_window, frameLayout);

        initChatHeadUI();

        contentLay = chatHead.findViewById(R.id.contentLay);
        closeBottomSheet = chatHead.findViewById(R.id.ivClose);
        appLogoCard = chatHead.findViewById(R.id.appLogoCard);
        rootLay = chatHead.findViewById(R.id.rootLay);
       /* rootLay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivClose.performClick();
            }
        });*/

        sheetBehavior = BottomSheetBehavior.from(contentLay);
        sheetBehavior.setHideable(true);
        sheetBehavior.setDraggable(true);


        chatHead.setFocusable(true);
        /*chatHead.setOnKeyListener((view, i, keyEvent) -> {
            Toast.makeText(getApplicationContext(), "Pressed", Toast.LENGTH_SHORT).show();
            return true;
        });*/
        sheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.d("BottomSheetBe","111");
                        closeBottomSheet.performClick();
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.d("BottomSheetBe","222");
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.d("BottomSheetBe","333");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.d("BottomSheetBe","444");
                        break;
                    case BottomSheetBehavior.STATE_HALF_EXPANDED:
                        Log.d("BottomSheetBe","555");
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.d("BottomSheetBe","666");
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        //sheetBehavior.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);


        bottomSheetView = chatHead.findViewById(R.id.bottomSheetView);
        bottomSheetView.setOnClickListener(view -> closeBottomSheet.performClick());

        int LAYOUT_FLAG;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE;
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);

        sheetBehavior.setMaxHeight(displayMetrics.heightPixels-200);//new added


        //old
        /*WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                PixelFormat.TRANSLUCENT);*/

        //New
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT,
                LAYOUT_FLAG,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                PixelFormat.TRANSLUCENT);

        params.gravity = Gravity.TOP | Gravity.LEFT;





        params.x = 0;
        params.y = (displayMetrics.heightPixels - contentLay.getHeight()) + actionBarHeight;



        disableWindowAnimations(params);

        frameLayout = new FrameLayout(this) {
            @Override
            public boolean dispatchKeyEvent(KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                    Log.e("COUPON_SERVICE", "Back Pressed...");

                    closeBottomSheet.performClick();
                    return true;
                }
                return super.dispatchKeyEvent(event);
            }
        };


        closeBottomSheet.setOnClickListener(view -> {
            Log.e("COUPON_SERVICE", "setUp: CLOSED BUTTON CLICK");
            appLogoCard.setOnTouchListener(new View.OnTouchListener() {


                private int initialX;
                private int initialY;
                private float initialTouchX;
                private float initialTouchY;
                long pressStart = 0;
                long pressEnd = 0;

                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    switch (event.getAction()) {

                        case MotionEvent.ACTION_DOWN:
                            pressStart = System.currentTimeMillis();
                            initialX = params.x;
                            initialY = params.y;
                            initialTouchX = event.getRawX();
                            initialTouchY = event.getRawY();
                            Log.e("MOVE", "onTouch: ACTION_DOWN " + params.x + "+  " + params.y);

                            isVibrated = false;
                            startClickTime = Calendar.getInstance().getTimeInMillis();


                            return true;

                        case MotionEvent.ACTION_UP:
                            pressEnd = System.currentTimeMillis();
                            makeWindowInvisible();

                            Log.d("onTouch: ", (initialTouchY - event.getRawY()) + "");
                            Log.d("onTouch: ", (event.getRawY() - initialTouchY) + "");



                            if (pressEnd - pressStart < 500) {
                                if (initialTouchY > event.getRawY() && initialTouchY - event.getRawY() < 10) {
                                    appLogoCard.performClick();
                                    return true;
                                }
                                if (initialTouchY <= event.getRawY() && event.getRawY() - initialTouchY < 10) {
                                    appLogoCard.performClick();
                                    return true;
                                }
                            }

                            if (event.getRawY() > closeView.getY()) {
                                /*new Handler().postDelayed(() -> {

                                }, 400);*/

                                Log.e("DELETE_CLICK", "true");

                                Log.e("TAG", "onTouch: close");
                                sheetBehavior.setHideable(true);
                                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                                sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                                windowManager.removeView(chatHead);
                                chatHead = null;


                            } else {
                                Log.e("DELETE_CLICK", "false");
                                WindowManager.LayoutParams param = params;
                                if (param.x + chatHead.getLayoutParams().width / 2 > displayMetrics.widthPixels / 2) {
                                    param.x = displayMetrics.widthPixels - chatHead.getLayoutParams().width;
                                } else {
                                    param.x = 0;
                                }

                                windowManager.updateViewLayout(chatHead, param);


                                //NEW AD
                                isMoved = true;
                                yLastPosition = params.y;
                                xLastPosition = params.x;
                            }

                            //OLD AD
//                            isMoved = true;
//                            yLastPosition = params.y;
//                            xLastPosition = params.x;

                            Log.e("MOVE", "onTouch: ACTION_UP " + params.x + "+  " + params.y);

                            return true;

                        case MotionEvent.ACTION_MOVE:

                            Log.e("MOVE", "onTouch: ACTION_MOVE ");

                            params.x = initialX + (int) (event.getRawX() - initialTouchX);
                            params.y = initialY + (int) (event.getRawY() - initialTouchY);

                            windowManager.updateViewLayout(chatHead, params);

                            long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                            if(clickDuration < MAX_CLICK_DURATION) {
                                Log.d("MOVE", "Clicked...");
                            }else {
                                Log.d("MOVE", "Dragging...");
                                if(!isVibrated){
                                    vibrateAnimateDevice();
                                    isVibrated = true;
                                }
                            }

                            makeWindowVisible();
                            return true;

                    }
                    return false;
                }
            });


            try {

                Log.d("POSITIONxy","XXX : "+yLastPosition + " YYY :" +xLastPosition);

                new Handler().postDelayed(() -> {
                    params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
                    if (params.x != 0) {
                        if (isMoved) {
                            params.x = xLastPosition;
                        } else {
                            params.x = displayMetrics.widthPixels;//OLD AD
                            //params.x = 0; //NEW AD
                        }
                    }

                    params.height = (int) Math.round(Double.parseDouble("" + (displayMetrics.widthPixels * 0.15)));
                    params.width = (int) Math.round(Double.parseDouble("" + (displayMetrics.widthPixels * 0.15)));

                    if (isMoved) {
                        params.y = yLastPosition;
                    } else {
                        params.y = displayMetrics.heightPixels / 2;

                    }

                    //isMoved = false;
                    windowManager.updateViewLayout(chatHead, params);
                    appLogoCard.setVisibility(View.VISIBLE);

                }, 300);
            }catch (Exception e){
                e.printStackTrace();
            }


            bottomSheetView.setVisibility(View.GONE);
            contentLay.setVisibility(View.GONE);
            slideDown(contentLay);
        });

        appLogoCard.setOnClickListener(view -> {
            appLogoCard.setVisibility(View.GONE);

            Log.d("ICONCLICK","Yes...");

            new Handler().postDelayed(() -> {
                params.flags = 0;

                params.height = WindowManager.LayoutParams.MATCH_PARENT;
                params.width = WindowManager.LayoutParams.MATCH_PARENT;

//                sheetBehavior.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);//OLD AD
                sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);//NEW AD

                params.y = (displayMetrics.heightPixels - contentLay.getHeight()) + actionBarHeight;
                windowManager.updateViewLayout(chatHead, params);

                if(tabOpened.equalsIgnoreCase("first")){
                    couponsTab.performClick();
                }else {
                    compareProductTab.performClick();
                }


                bottomSheetView.setVisibility(View.VISIBLE);
                contentLay.setVisibility(View.VISIBLE);
                slideUp(contentLay);

            }, 300);
        });

        windowManager.addView(chatHead, params);

    }

    private void initChatHeadUI() {
        couponsTextView = chatHead.findViewById(R.id.tvCoupons);
        compareProductTextView = chatHead.findViewById(R.id.tvSimilarCoupons);
        couponCounter = chatHead.findViewById(R.id.tvCountCoupon);
        popCounter = chatHead.findViewById(R.id.popCount);
        compareProductCounter = chatHead.findViewById(R.id.tvCountCouponSim);
        closeBottomSheet = chatHead.findViewById(R.id.ivClose);

        couponTabImage = chatHead.findViewById(R.id.firstTabImg);
        compareProductTabImage = chatHead.findViewById(R.id.secondTabImg);
        couponsTab = chatHead.findViewById(R.id.llCoupons);
        compareProductTab = chatHead.findViewById(R.id.llSimilarCoupons);
        countryRecyclerView = chatHead.findViewById(R.id.rvCountry);
        couponsRecyclerView = chatHead.findViewById(R.id.rvCoupons);
        noCouponFoundImageIcon = chatHead.findViewById(R.id.imgCoupon);
        noCouponTextView = chatHead.findViewById(R.id.tvNoCoupon);
        progressBar = chatHead.findViewById(R.id.progress);

        // No need for now:
//        CardView cvAmazonCoupon = chatHead.findViewById(R.id.cvAmazonCoupon);
//        CardView cvSimilarCoupon = chatHead.findViewById(R.id.cvSimilarCoupon);
        nothingFoundLinearLayout = chatHead.findViewById(R.id.llEmpty);

        comparableProductName = chatHead.findViewById(R.id.comparableProductName);
        comparableProductPriceRange = chatHead.findViewById(R.id.comparableProductPriceRange);

        comparableProductName.setVisibility(View.GONE);
        comparableProductPriceRange.setVisibility(View.GONE);


        //recyclerview scroll inside bottomsheet new added
        couponsRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        Log.d("MotionEvent","ACTION_DOWN");
                        view.getParent().requestDisallowInterceptTouchEvent(true);
                        return true;

                    case MotionEvent.ACTION_UP:
                        Log.d("MotionEvent","ACTION_UP");
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        return true;
                }

                view.onTouchEvent(motionEvent);
                return true;
            }
        });


        // On Click Listener:
        couponsTab.setOnClickListener(view -> {
            tabOpened = "first";
            comparableProductName.setVisibility(View.GONE);
            comparableProductPriceRange.setVisibility(View.GONE);

            couponsTab.setBackgroundTintList(ContextCompat.getColorStateList(this, android.R.color.white));
            compareProductTab.setBackgroundTintList(ContextCompat.getColorStateList(this, android.R.color.transparent));

            //getCouponCode(DOMAIN_FOR_COUPONS);
            if (couponsAllModel != null) {
                parseNShowCoupon(couponsAllModel);
            } else {
                couponOfProduct();
            }
        });

        compareProductTab.setOnClickListener(view -> {
            tabOpened = "second";
            compareProductTab.setBackgroundTintList(ContextCompat.getColorStateList(this, android.R.color.white));
            couponsTab.setBackgroundTintList(ContextCompat.getColorStateList(this, android.R.color.transparent));
            countryRecyclerView.setVisibility(View.GONE);
            if (compareProductModel != null) {
                parseNShowComparableProduct(compareProductModel);
            } else {
                compareProduct(1);
            }
        });
    }

    private void compareProduct(int flag) {
        progressBar.setVisibility(View.VISIBLE);
        RetrieveDataFromFirebase retrieveDataFromFirebase = new RetrieveDataFromFirebase(offersDBHelper);
        retrieveDataFromFirebase.findComparableProduct(URL_FOR_COMPARE_PRODUCTS, new SemiComparableProductInterface() {
            @Override
            public void SHA_INFO(String SHA, String productId) {
                Log.d("COMPARABLE_PRODUCT", "SHA_INFO: " + SHA + " : " + productId);
                retrieveDataFromFirebase.comparableProducts(SHA, productId, new ComparableProductInterface() {
                    @Override
                    public void getComparableProduct(CompareProductModel model) {
                        Log.d("COMPARABLE_PRODUCT", "getComparableProduct: ");
                        progressBar.setVisibility(View.GONE);
                        compareProductModel = model;
                        totalCompareProductFound = compareProductModel.getSeller_count();
                        if (flag == 0) {
                            compareProductCounter.setText(String.valueOf(compareProductModel.getListOfSeller().size()));
                        } else {
                            parseNShowComparableProduct(compareProductModel);
                        }

                        Log.d("popCounter","     NEW ========================   Coupon : " + totalCouponFound + "  Compare : " + totalCompareProductFound);

                        popCounter.setText(String.valueOf(totalCouponFound + totalCompareProductFound));

                    }

                    @Override
                    public void errorOnComparableProduct(String error) {
                        totalCompareProductFound = 0;
                        Log.d("COMPARABLE_PRODUCT", "errorOnComparableProduct: " + error);
                        progressBar.setVisibility(View.GONE);
                        if (flag != 0) {
                            nothingFoundLinearLayout.setVisibility(View.VISIBLE);
                            compareProductModel = null;
                            couponsRecyclerView.setAdapter(null);
                            noCouponTextView.setText("There are no product found to compare!");
                        }

                    }
                });
            }
        });
    }

    void disableWindowAnimations(WindowManager.LayoutParams wp) {
        String className = "android.view.WindowManager$LayoutParams";
        try {
            Class layoutParamsClass = Class.forName(className);

            Field privateFlags = layoutParamsClass.getField("privateFlags");
            Field noAnim = layoutParamsClass.getField("PRIVATE_FLAG_NO_MOVE_ANIMATION");

            int privateFlagsValue = privateFlags.getInt(wp);
            int noAnimFlag = noAnim.getInt(wp);
            privateFlagsValue |= noAnimFlag;

            privateFlags.setInt(wp, privateFlagsValue);

            // Dynamically do stuff with this class
            // List constructors, fields, methods, etc.

        } catch (ClassNotFoundException e) {
            // Class not found!
        } catch (Exception e) {
//            Log.e(e.toString());
            // Unknown exception
        }
    }

    // slide the view from below itself to the current position
    public void slideUp(View view) {

        Log.d("slideUPP","called...");

        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(200);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }

    // slide the view from its current position to below itself
    public void slideDown(View view) {

        Log.d("slideUPP","down called...");

        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(200);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }


    private void openNewTabForCompareService(String directLink) {
        closeBottomSheet.performClick();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(directLink));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setPackage("com.android.chrome");
        try {
            this.startActivity(intent);
        } catch (ActivityNotFoundException ex) {
            // Chrome browser presumably not installed so allow user to choose instead
            intent.setPackage(null);
            this.startActivity(intent);
        }
    }

}

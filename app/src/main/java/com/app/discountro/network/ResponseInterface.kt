package com.app.discountro.network

interface ResponseInterface
{
    fun onSuccess(tag : String, response : String)
    fun onError(tag : String, error : String)
    fun onFailure(tag : String,error : String)
    fun onNoConnection(tag : String,msg : String)
    fun onInvalidAuth(tag : String, response : String)
}
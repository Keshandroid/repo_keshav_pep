package com.app.discountro.network

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.app.discountro.R
import com.app.discountro.databinding.ListItemBinding

class StringRecord(private val _context: Context, list: MutableList<String>) :
    ArrayAdapter<String?>(
        _context, R.layout.list_item, list as List<String?>
    ) {
    private val list: MutableList<String>

    private class Holder {
        var title: TextView? = null
        var time: TextView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val view = ListItemBinding.inflate(LayoutInflater.from(_context), parent, false)
        val holder = Holder()
        holder.title =  view.recordItemTitle
        holder.time = view.recordItemTime
        holder.title!!.setTextColor(Color.BLACK)
        holder.time!!.setTextColor(Color.BLACK)

        val record = list[position]
//        val sdf = SimpleDateFormat("MMM dd", Locale.getDefault())
        holder.title!!.text = record
//        holder.time!!.text = sdf.format(record.time)
        return view.root
    }

    init {
        this.list = list
    }
}
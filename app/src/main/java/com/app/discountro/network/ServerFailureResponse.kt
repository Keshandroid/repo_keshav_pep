package com.app.discountro.network
data class ServerFailureResponse(
    val code: Int,
    val `data`: Data,
    val status: Boolean
)

data class Data(
    val error: String
)
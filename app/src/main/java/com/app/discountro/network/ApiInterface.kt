package com.app.discountro.network

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import io.reactivex.Observable
import retrofit2.http.*

interface ApiInterface
{
    @GET
    fun login(@Url url:String, @QueryMap map: MutableMap<String, String>) : Observable<JsonElement>

}
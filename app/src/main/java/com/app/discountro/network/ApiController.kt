package com.app.discountro.network

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.json.JSONException

class ApiController(var context : Context, var responseInterface : ResponseInterface)
{
    private val compositeDisposable = CompositeDisposable()

    fun getData(tag: String, map: MutableMap<String, String>)
    {

        ApiClient.getClient(context).login(Constants.BASEURL+tag,map)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .unsubscribeOn(Schedulers.io())
            .subscribe(object : Observer<JsonElement> {

                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                    //           compositeDisposable.add(d)
                }

                override fun onNext(response: JsonElement) {
//                    val baseApiResponse = Gson().fromJson<ServerResponseModel>(response, ServerResponseModel::class.java)
//                    if (baseApiResponse.status == true) {
//                        try {
                            responseInterface.onSuccess(tag, response.toString())
//                        } catch (e: JSONException) {
//                            Log.e("Exception",e.toString())
//                        }
//                    } else {
//                        val errorResponse =
//                            Gson().fromJson<ServerFailureResponse>(response, ServerFailureResponse::class.java)
//
//                        if(errorResponse.code == 401){
//                            responseInterface.onInvalidAuth(tag, errorResponse.data.error.toString())
//                        }else{
//                            responseInterface.onError(tag, errorResponse.data.error.toString())
//                        }
//                    }

                }

                override fun onError(e: Throwable) {
                    responseInterface.onFailure(tag, e.toString())
                }
            })
    }
}